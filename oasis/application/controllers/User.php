<?php
class User extends CI_Controller {
	public function __construct(){
        parent::__construct();
  		$this->load->helper('url');
  	 	$this->load->model('User_Model');
        $this->load->library('session');
	}
	public function create(){
		$now = new DateTime();
		$user=array(
			'id'=>md5($this->input->post('eml')),
			'fname'=>$this->input->post('fnm'),
			'lname'=>$this->input->post('lnm'),
			'dob'=>$this->input->post('dob'),
			'username'=>$this->input->post('unm'),
			'email'=>$this->input->post('eml'),
			'password'=>md5($this->input->post('upd')),
			'cdate'=>$now->format('Y-m-d H:i:s'),
			'mdate'=>NULL,
			'logindate'=>NULL
		);
		if($this->input->post('id') != null)
			$user['id'] = $this->input->post('id');
		if($this->input->post('role') == null)
			$user['role'] = "3";
        $email_check=$this->User_Model->email_check($user['email']);
		if($email_check){
		  $this->User_Model->register_user($user);
		  echo json_encode( array('success_msg' => 'Registration Successful!') );
		}
		else{
		  echo json_encode( array('error_msg'=>'Email already registered with us!'));
		}
	}
	function login(){
		$user_login=array(
			'username'=>$this->input->post('unm'),
			'password'=>md5($this->input->post('upd'))
		);
		$data=$this->User_Model->login_user($user_login);
		if($data){
			$this->session->set_userdata('id',$data['id']);
			$this->session->set_userdata('email',$data['email']);
			$this->session->set_userdata('username',$data['username']);
			$this->session->set_userdata('name', $data['fname'].' '.$data['lname']);
			$this->session->set_userdata('role', $data['role']);
			unset($data['logindate'], $data['cdate'], $data['mdate'], $data['password']);
			echo json_encode( $data );
		}
		else{
			echo json_encode( array('error_msg'=>'Invalid login details!'));
			http_response_code(401);
			//header('HTTP/1.0 401 Unauthorized');
		}	
	}
	public function logout(){
		$this->session->sess_destroy();
		echo json_encode( array('error_msg'=>'Logout successful'));
	}
	public function lout(){
		$this->session->sess_destroy();
		redirect(base_url("oasis/home"));
	}
	public function update(){
		$now = new DateTime();
		$user=array(
			'fname'=>$this->input->post('fname'),
			'lname'=>$this->input->post('lname'),
			'dob'=>$this->input->post('dob'),
			'email'=>$this->input->post('email'),
			'password'=>md5($this->input->post('password')),
			'cdate'=>$now->format('Y-m-d H:i:s'),
			'mdate'=>NULL,
			'logindate'=>NULL
		);
	}
	public function getdata(){
		//return $this->response(200, json_encode($this->User_Model->getUserById($id)));
		return $this->response(200, json_encode($this->User_Model->getUserData()));
	}
	private function response($status, $response){
		if($status){
			$this->output->set_header('HTTP/1.0 200 OK');
			$this->output->set_header('HTTP/1.1 200 OK');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
			$this->output->set_content_type('application/json', 'utf-8');
			$this->output->set_output($response);
		}
		else{
			$this->output->set_header('HTTP/1.0 401');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
			$this->output->set_content_type('application/json', 'utf-8');
			$this->output->set_output($response);
		}
	}
	public function edit(){
		$date = new DateTime();
		$now = $date->format('Y-m-d H:i:s');
		$id = $this->input->post('id');
		$user = array();
		if($this->input->post('fnm') != ""){
			$user['fname'] = $this->input->post('fname');
		}
		if($this->input->post('lnm') != ""){
			$user['lname'] = $this->input->post('lname');
		}
		if($this->input->post('unm') != ""){
			$user['dob'] = $this->input->post('eml');
		}
		if($this->input->post('eml') != ""){
			$user['username'] = $this->input->post('username');
		}
		if($this->input->post('upd') != ""){
			$user['role'] = $this->input->post('r');
		}
		if($this->input->post('dob') != ""){
			$user['password'] = $this->input->post('password');
		}
		if($this->input->post('email') != ""){
			$user['email'] = $this->input->post('email');
		}
		$user['mdate'] = $now;
		$this->User_Model->edit($user, $id);
		echo json_encode( array('success_msg' => 'Record Update was successful') );
	}
	public function delete(){
		$id = $this->input->post('id');
		$this->User_Model->delete($id);
		echo json_encode( array('success_msg' => 'Record Update was successful') );
	}
	public function uedit(){
		$date = new DateTime();
		$now = $date->format('Y-m-d H:i:s');
		$id = $this->input->post('id');
		$user = array();
		if($this->input->post('fnm') != ""){
			$user['fname'] = $this->input->post('fnm');
		}
		if($this->input->post('lnm') != ""){
			$user['lname'] = $this->input->post('lnm');
		}
		if($this->input->post('eml') != ""){
			$user['email'] = $this->input->post('eml');
		}
		if($this->input->post('unm') != ""){
			$user['username'] = $this->input->post('unm');
		}
		if($this->input->post('r') != ""){
			$user['role'] = $this->input->post('r');
		}
		$user['mdate'] = $now;
		$this->User_Model->uedit($user, $id);
		echo json_encode( array('success_msg' => 'User\'s details updated successfully') );
	}
	public function register(){
		$data = array();
		$data['lgnCls'] = "block";
		$data['pnlCls'] = "none";	
		$data['title'] = "OASIS Admin Console";
		$this->load->view('header', $data);
		$this->load->view('register', $data);
		$this->load->view('footer');
	}
	public function getEList(){
		return $this->response(200, json_encode($this->User_Model->getEList()));
	}
}
?>
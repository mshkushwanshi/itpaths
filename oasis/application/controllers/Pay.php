<?php
class Pay extends CI_Controller {
	public function __construct(){
        parent::__construct();
  		$this->load->helper('url');
  	 	$this->load->model('Pay_Model');
        $this->load->library('session');
	}
	public function payWallet(){
		$date = new DateTime();
		$now = $date->format('Y-m-d H:i:s');
		$pay=array(
			'id'=>NULL,
			'uid'=>$this->input->post('id'),
			'amount'=>$this->input->post('amount'),
			'paytime'=>$now
		);
        $this->Pay_Model->addWallet($pay);
		echo "Amount added in wallet successfully";
	}
	public function makePayment(){
		$date = new DateTime();
		$now = $date->format('Y-m-d H:i:s');
		$pay=array(
			'id'=>NULL,
			'uid'=>$this->input->post('id'),
			'payTime'=>$now,
			'amount'=>$this->input->post('amount'),
			'type'=>$this->input->post('type')
		);
		$this->Pay_Model->makePayment($pay);
		echo json_encode( array('msg' => 'Payment was successful') );
	}
	public function getPayments(){
		$id = $this->session->userdata('uid');
		return $this->response(200, json_encode($this->Pay_Model->getPayments($id)));
	}
	public function getWPay(){
		$id = $this->input->post('id');
		return $this->response(200, json_encode($this->Pay_Model->getWalletPayments($id)));
	}
	public function getPaytype(){
		$this->db->select('*');
		$this->db->from('wp3h_paymode');
		return $aid = $this->db->get()->row_array();
	}
	public function addPaytype(){
		$data = array('id'=>NULL, 'type' => $this->input->post('title'));
		$this->Pay_Model->addPaytype($data);
		return $this->response(200, json_encode('{"msg": "Payment type added"}'));
	}
	public function placeOrder(){
		$data = json_decode($this->input->post('data'));
		$oDat = array(
			  "oId" 	=> NULL,
			  "oCid" 	=> $data->mCustomersUid,
			  "oDate" 	=> $data->mDate,
			  "oFrom" 	=> $data->mFrom,
			  "oTo" 	=> $data->mTo,
			  "ofLat" 	=> $data->mFromPlaceLat,
			  "ofLng" 	=> $data->mFromPlaceLng,
			  "oExp" 	=> $data->mIsExpress,
			  "oCExp" 	=> $data->mIsCarExpress,
			  "oSExp" 	=> $data->mIsSuperExpress,
			  "oStatus" => $data->mType,
			  "oToken" 	=> $data->mUserToken,
			  "otLat" 	=> $data->mToPlaceLat,
			  "otLng" 	=> $data->mToPlaceLng,
			  "oDistance" => $data->mDistance
		);
		
		$date = new DateTime();
		$now = $date->format('Y-m-d H:i:s');
		$pDat=array(
			'id'=>NULL,
			'uid'=>$data->mCustomersUid,
			'payTime'=>$now,
			'amount'=>$data->pay,
			'type'=>$data->mPayType,
			'status' => $data->payStatus
		);
		print_r($pDat);
		print_r($oDat);
		$this->Pay_Model->addOrder($oDat, $pDat);
		
		return $this->response(200, "Order Placed Successfully");
	}
	public function getAllOrders(){
		return $this->response(200, json_encode($this->Pay_Model->getAllOrders()));
	}
	public function getOrderByUser(){
		$id = $this->input->post('uid');
		return $this->response(200, json_encode($this->Pay_Model->getAllOrdersByUser($id)));
	}
	public function updateOrder(){
		echo $token = base64_decode($this->input->get('oid'));
		echo $status = $this->input->get('stat');
		echo $eid = $this->input->get('eid');
		return $this->response(200, json_encode($this->Pay_Model->updateOrderStatus($token, $status, $eid)));
	}
	private function response($status, $response){
		if($status){
			$this->output->set_header('HTTP/1.0 200 OK');
			$this->output->set_header('HTTP/1.1 200 OK');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
			$this->output->set_content_type('application/json', 'utf-8');
			$this->output->set_output($response);
		}
		else{
			$this->output->set_header('HTTP/1.0 401');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
			$this->output->set_content_type('application/json', 'utf-8');
			$this->output->set_output($response);
		}
	}
	public function mobile(){
		$receive_momo_request = array(
			 'CustomerName' => 'Kelvin Olukoju',
			  'CustomerMsisdn'=> '575570023',
			  'CustomerEmail'=> 'nellyktechconsult@gmail.com',
			  'Channel'=> 'mtn-gh',
			  'Amount'=> 0.8,
			  'PrimaryCallbackUrl'=> 'http://requestb.in/1minotz1',
			  'SecondaryCallbackUrl'=> 'http://requestb.in/1minotz1',
			  'Description'=> 'T Shirt',
			  'ClientReference'=> '23213',
		);

		//API Keys

		$clientId = 'vdgtuaqs';
		$clientSecret = 'qxkiztna';
		$basic_auth_key =  'Basic ' . base64_encode($clientId . ':' . $clientSecret);
		$request_url = 'https://api.hubtel.com/v1/merchantaccount/merchants/HMXXXXXXX/receive/mobilemoney';
		$receive_momo_request = json_encode($receive_momo_request);

		$ch =  curl_init($request_url);  
				curl_setopt( $ch, CURLOPT_POST, true );  
				curl_setopt( $ch, CURLOPT_POSTFIELDS, $receive_momo_request);  
				curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );  
				curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
					'Authorization: '.$basic_auth_key,
					'Cache-Control: no-cache',
					'Content-Type: application/json',
				  ));

		$result = curl_exec($ch); 
		$err = curl_error($ch);
		curl_close($ch);

		if($err){
			echo $err;
		}else{
			echo $result;
		}
	}
	function success(){
		$this->load->view('oasis/pay_success');
	}
}
?>
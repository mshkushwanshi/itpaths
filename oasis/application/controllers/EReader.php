<?php
class EReader extends CI_Controller {
	public function __construct(){
        parent::__construct();
  		$this->load->helper('url');
  	 	$this->load->model('Exam_Model');
		$this->load->model('User_Model');
        $this->load->library('session');
		$this->load->library('excel');
		$this->load->library('session');
	}
	public function index(){}
	public function read(){
		$file = './files/test.xlsx';
 
		//load the excel library
		$this->load->library('excel');
		 
		//read file from path
		$objPHPExcel = PHPExcel_IOFactory::load($file);
		 
		//get only the Cell Collection
		$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
		 
		//extract to a PHP readable array format
		foreach ($cell_collection as $cell) {
			$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
			$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
			$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
		 
			//The header will/should be in row 1 only. of course, this can be modified to suit your need.
			if ($row == 1) {
				$header[$row][$column] = $data_value;
			} else {
				$arr_data[$row][$column] = $data_value;
			}
		}
		 
		//send the data in an array format
		$data['header'] = $header;
		$data['values'] = $arr_data;

	}
	public function create(){
		
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('test worksheet');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'This is just some text value');
		//change the font size
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
		//make the font become bold
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//merge cell A1 until D1
		$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		$filename='just_some_random_name.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');

	}
	public function upload(){
		$configVideo['upload_path'] = 'assets/gallery/images';
		//$configVideo['max_size'] = '102400';
		//$configVideo['allowed_types'] = 'mp4'; # add video extenstion on here
		$configVideo['overwrite'] = FALSE;
		$configVideo['remove_spaces'] = TRUE;
		$video_name = random_string('numeric', 5);
		$configVideo['file_name'] = $video_name;

		$this->load->library('upload', $configVideo);
		$this->upload->initialize($configVideo);

		if (!$this->upload->do_upload('uploadan')){
			# Upload Failed
			$this->session->set_flashdata('error', $this->upload->display_errors());
			redirect('controllerName/method');
		}
		else{
			# Upload Successfull
			$url = 'assets/gallery/images'.$video_name;
			$set1 =  $this->Model_name->uploadData($url);
			$this->session->set_flashdata('success', 'Video Has been Uploaded');
			redirect('controllerName/method');
		}
	}
	public function iupload(){
		$upload_path = './uploads';
        $uid=$this->session->userdata('id'); //creare seperate folder for each user 
        $upPath=$upload_path."/".$uid;
		if (!is_dir($upPath)) {
			mkdir($upPath, 0777, TRUE);
		}
		else{
			$files = glob($upPath.'/*'); // get all file names
			foreach($files as $file){ // iterate files
			  if(is_file($file))
				unlink($file); // delete file
			}
		}
		if($_SERVER['REQUEST_METHOD'] == 'POST') {
			$new_image_name = time()."_".str_replace(str_split(' ()\\/,:*?"<>|'), '', $_FILES['webcam']['name']);
			$config['upload_path'] = $upPath;
			$config['allowed_types'] = 'gif|jpg|png|bmp|jpeg';
			$config['file_name'] = $new_image_name;
			$config['overwrite'] = true;
			
			$this->load->library('upload', $config);
			$upload = $this->upload->do_upload('webcam');
			if ( ! $this->upload->do_upload('webcam')){
				//print_r($this->upload->display_errors());
				echo "error.png";
			}
			else{
				/*
				* update image id in database.
				*/
				$this->User_Model->updateImg($this->session->userdata('id'), $new_image_name);
				echo "<img src='".base_url($upPath."/".$new_image_name)."' class='captured'/>";
				//sprint_r($this->upload->data());
			}
			$title=$this->input->post('title');
			$value=array('title'=>$title,'image_name'=>$new_image_name,'crop_name'=>"test");
		}
	 }
	 public function updatefile(){
		 print_r($_POST);
		 $this->input->post('path');
	 }
	 public function record(){
		 $this->load->view('record/index');
	 }
}?>
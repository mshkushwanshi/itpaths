<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE');
header('Access-Control-Allow-Headers: Access-control-allow-headers, Authorization, Origin, Content-Type, X-Auth-Token');
header('Access-Control-AllowCredentials: true');

class Oasis extends CI_Controller {
	public function __construct(){
        parent::__construct();
  		$this->load->helper('url');
		$this->load->library('session');
	}
	public function index(){
	redirect("/oasis/home");
		$data = array();
		if($this->session->userdata('id') == null){
			$data['lgnCls'] = "block";
			$data['pnlCls'] = "none";
		}
		else{
			$data['lgnCls'] = "none";
			$data['pnlCls'] = "block";
		}
		$data['title'] = "OASIS Admin Console";
		$this->load->view('header', $data);
		$this->load->view('home', $data);
		$this->load->view('footer');
	}
	public function home(){
		$this->load->view('oasis/header');
		$this->load->view('oasis/index');
		$this->load->view('oasis/footer');
	}
	public function about(){
		$this->load->view('oasis/header');
		$this->load->view('oasis/about');
		$this->load->view('oasis/footer');
	}
	public function solution(){
		$this->load->view('oasis/header');
		$this->load->view('oasis/solution');
		$this->load->view('oasis/footer');
	}
	public function delivery(){
		$this->load->view('oasis/header');
		$this->load->view('oasis/delivery');
		$this->load->view('oasis/footer');
	}
	public function contact(){
		$this->load->view('oasis/header');
		$this->load->view('oasis/contact');
		$this->load->view('oasis/footer');
	}
	public function faq(){
		$this->load->view('oasis/header');
		$this->load->view('oasis/faq');
		$this->load->view('oasis/footer');
	}
	public function send(){
		//redirect('oasis/home');
		$config = Array(
		  'protocol' => 'smtp',
		  'smtp_host' => 'localhost',
		  'smtp_port' => 465,
		  'smtp_user' => 'info@oasisexpressgh.com',
		  'mailtype' => 'html',
		  'charset' => 'iso-8859-1',
		  'wordwrap' => TRUE
		);

		$message = '';
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from($this->input->post('email'));
		$this->email->to('info@oasisexpressgh.com');
		$this->email->subject($this->input->post('title'));
		$this->email->message($this->input->post('message'));
		
		if($this->email->send()){
			$config = Array(
			  'protocol' => 'smtp',
			  'smtp_host' => 'localhost',
			  'smtp_port' => 465,
			  'smtp_user' => 'info@oasisexpressgh.com',
			  'mailtype' => 'html',
			  'charset' => 'iso-8859-1',
			  'wordwrap' => TRUE
			);

			$message = '';
			$this->load->library('email', $config);
			$this->email->set_newline("\r\n");
			$this->email->from('info@oasisexpressgh.com');
			$this->email->to($this->input->post('email'));
			$this->email->subject("Welcome from OASIS");
			$message = "Thank you for writing us.\n\rYour enquiry on: ".$this->input->post('title')."\n\rWe'll reach you soon.";
			$this->email->message($message);
			
			echo 'Email sent.';
		}
		else{
			show_error($this->email->print_debugger());
		}
	}
	public function cpanel(){
	    $role = $this->session->userdata('role');
		if($role != 2){
			redirect(base_url("oasis/home"));
		}
		
		$data = array();
		if($this->session->userdata('id') == null){
			$data['lgnCls'] = "block";
			$data['pnlCls'] = "none";
		}
		else{
			$data['lgnCls'] = "none";
			$data['pnlCls'] = "block";
		}
		$this->load->view('header', $data);
		$this->load->view('home', $data);
		$this->load->view('footer');
	}
}
?>
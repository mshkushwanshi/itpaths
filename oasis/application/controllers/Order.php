<?php
class Order extends CI_Controller {
	public function __construct(){
        parent::__construct();
  		$this->load->helper('url');
  	 	$this->load->model('Order_Model');
        $this->load->library('session');
	}
	public function placeOrder(){
		$data = json_decode($this->input->post('data'));
		$oDat = array(
			  "oId" 	=> NULL,
			  "oCid" 	=> $data->mCustomersUid,
			  "oDate" 	=> $data->mDate,
			  "oFrom" 	=> $data->mFrom,
			  "oTo" 	=> $data->mTo,
			  "ofLat" 	=> $data->mFromPlaceLat,
			  "ofLng" 	=> $data->mFromPlaceLng,
			  "oExp" 	=> $data->mIsExpress,
			  "oCExp" 	=> $data->mIsCarExpress,
			  "oSExp" 	=> $data->mIsSuperExpress,
			  "oStatus" => $data->mType,
			  "oToken" 	=> $data->mUserToken,
			  "otLat" 	=> $data->mToPlaceLat,
			  "otLng" 	=> $data->mToPlaceLng,
			  "oDistance" => $data->mDistance
		);
		$pDat = array('id'=>NULL, 'uid' => $data->mCustomersUid, 'payTime' => $data->mDate, 'amount' => $data->mPay);
		$this->Pay_Model->addOrder($oDat, $pDat, $data->payType);
		return $this->response(200, "Order Placed Successfully");
	}
	public function getAllOrders(){
		return $this->response(200, json_encode($this->Pay_Model->getAllOrders()));
	}
	public function getOrderByUser(){
		$id = $this->input->post('uid');
		return $this->response(200, json_encode($this->Pay_Model->getAllOrdersByUser($id)));
	}
	public function updateOrder(){
		$token = $this->input->post('tkn');
		$status = $this->input->post('stat');
		return $this->response(200, json_encode($this->Pay_Model->updateOrderStatus($token, $status)));
	}
	private function response($status, $response){
		if($status){
			$this->output->set_header('HTTP/1.0 200 OK');
			$this->output->set_header('HTTP/1.1 200 OK');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
			$this->output->set_content_type('application/json', 'utf-8');
			$this->output->set_output($response);
		}
		else{
			$this->output->set_header('HTTP/1.0 401');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0');
			$this->output->set_header('Pragma: no-cache');
			$this->output->set_content_type('application/json', 'utf-8');
			$this->output->set_output($response);
		}
	}
	public function track(){
		$id = base64_decode($this->input->post('oid'));
		return $this->response(200, json_encode($this->Order_Model->getOrdersById($id)));
	}
}
?>
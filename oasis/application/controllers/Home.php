<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE');
header('Access-Control-Allow-Headers: Access-control-allow-headers, Authorization, Origin, Content-Type, X-Auth-Token');
header('Access-Control-AllowCredentials: true');

class Home extends CI_Controller {
	public function __construct(){
        parent::__construct();
  		$this->load->helper('url');
        $this->load->library('session');
	}
	public function index(){
		$this->load->view('header');
		$this->load->view('content');
		$this->load->view('footer');
	}
}
?>
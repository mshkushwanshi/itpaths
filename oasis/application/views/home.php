<script>
	var domain = "<?php echo base_url("");?>";
</script>
<style>
	#odGrid-bar img{
		cursor:pointer;
	}
	table.odrinfo td {
		border-bottom: 1px solid wheat;
		padding: 2px;
	}
	.hdr{font-weight: bold;}
	div#odGrid-bar{margin:5px;}
	div#uodr-dat select {
		margin-bottom: 10px;
		margin-top: 10px;
	}
	.waves-effect{cursor:pointer;}
	.container.data{margin-left: 5px; margin-bottom: 10px;}
	.container.data input[type="text"] {
		margin-left: 10px;
	}
	#astat{
		font-weight: bold;
		margin-bottom: 5px;
	}
	.prof{display:none;}
	.prof input[type="text"] {
		margin: 10px;
		font-weight: normal;disabled;
	}
	.prof input[type="file"] {
		margin: 10px;
	}
	.prof input[type="file"] {
		margin: 10px;
	}
	table#udGrid, table#odGrid {
		font-family: verdana;
		width: 100%;
		border: 1px solid #d0c8c8;
	}
	table#udGrid td, table#udGrid th, table#odGrid td, table#odGrid th {
		padding: 5px;
		width: 400px !important;
		display: table-cell;
		text-align: left;
		vertical-align: top;
	}
	table#udGrid th, table#odGrid th {
		font-weight: bold;
	}
</style>
<div style="display:<?php echo $lgnCls;?>" class="lgnPnl">
    <?php $this->load->view('login');?>
</div>
<script>
	$(document).ready(function(){
		$("#odGrid").hide();
		$(".container.data").hide();
	});
</script>
<div style="display:<?php echo $pnlCls;?>" class="admnPnl">
    <aside class="left-sidebar">
        <div class="scroll-sidebar">
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li>
                        <a class = "waves-effect" onClick="dashboard()"><i class="fa fa-clock-o m-r-10" aria-hidden="true"></i>Dashboard</a>
                    </li>
					<li>
                        <a class = "waves-effect" onClick="listOrderView()"><i class="fa fa-clock-o m-r-10" aria-hidden="true"></i>List All Orders</a>
                    </li>
                    <li>
                        <a class = "waves-effect"onclick="profile()"><i class="fa fa-user m-r-10" aria-hidden="true"></i>Profile</a>
                    </li>
                </ul>
            </nav>
        </div>
    </aside>
    <div class="page-wrapper" id="home">
        <div class="container-fluid">
            <div class="row page-titles">
                <div class="align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0" class="htitle">Dashboard</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home&nbsp;></a></li>
                        <li class="breadcrumb-item active" class="htitle">Dashboard</li>
                    </ol>
					<div align="center" id="astat"></div>
					<div id="udRole">Roles: No Access: 1, Admin: 2, Driver: 3, Default: 4, Enterprise: 5</div>
					<div id="udGrid"></div>
					<div id="uodr"><div id="uodr-dat"></div></div>
					<script>
						$( function() {
							$( "#uodr" ).dialog({
							  autoOpen: false,
							  title: "Update Order",
							  show: {
								effect: "blind",
								duration: 1000
							  },
							  hide: {
								effect: "explode",
								duration: 1000
							  }
							});
						});
						$( function() {
							$( "#odr-data" ).dialog({
							  autoOpen: false,
							  title: "Order Detail",
							  show: {
								effect: "blind",
								duration: 1000
							  },
							  hide: {
								effect: "explode",
								duration: 1000
							  }
							});
						});
				  </script>
					<script>
						$(document).ready(function(){
							$.ajax({url: "<?php echo base_url('/user/getdata');?>"})
							.done(function(res){
								var roles = [
									{ Name: "1", Id: "1" },
									{ Name: "2", Id: "2" },
									{ Name: "3", Id: "3" },
									{ Name: "4", Id: "4" },
									{ Name: "5", Id: "5" }
								];
								$("#udGrid").jsGrid({
									width: "100%",
									height: "400px",
									inserting: false,
									editing: true,
									sorting: true,
									paging: true,
							 
									data: res,
									controller: {
										updateItem: function(item) {
											var json = {'id': item.id, 'fnm': item.fname, 'lnm': item.lname, 'eml': item.email, 'unm': item.username, 'r': item.role}
											$.ajax({
												type: "POST",
												url: "<?Php echo base_url("/user/uedit/");?>",
												data: json
											}).done(function(res){astat(JSON.parse(res).success_msg);console.log(res);}).error(function(){astat("Error");console.log("Error");});
											return item;
										},
										deleteItem: function(item) {
											var uid = item.id;
											$.ajax({
												type: "POST",
												url: "<?Php echo base_url("/user/delete/");?>",
												data: {"id": uid}
											}).done(function(res){astat(JSON.parse(res).success_msg);}).error(function(){astat("Error");console.log("Error");});
											return true;
										}
									},
									fields: [
										{ name: "fname", type: "text", title: "First Name", width: 50 },
										{ name: "lname", type: "text", title: "Last Name", width: 50 },
										{ name: "email", type: "text", title: "Email"},
										{ name: "username", type: "text", title: "Username", sorting: true },
										{ name: "role", type: "select", items: roles, title: "Access Role", valueField: "Id", textField: "Name"},
										{ type: "control" }
									]
								});
							})
							.fail(function(res) {
								if(res.status == 401)
									$($('.help-block')[2]).html('Invalid login credentials');
								else
									$($('.help-block')[2]).html('Something went wrong');
							})
						});
					</script>
					<div id="odGrid-bar">
						<img src="<?php echo base_url("assets/images/reload.jpg");?>" width="25"/>
					</div>
					<div id="odr-data">
						<div id="odr-info"></div>
					</div>
					<div id="odGrid"></div>
					<style>
						.odate{display: none;}
						.amount{display: none;}
						.pstatus{display: none;}
						.pmode{display: none;}
						.distance{display: none;}
					</style>
					<div id="eodr"></div>
				</div>
            </div>
            <div>
                <?php $this->load->view('prof'); ?>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url('/js/admin.js');?>"></script>
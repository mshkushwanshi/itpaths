<div class="col-md-6 col-md-offset-3">
    <h2>Register</h2>
	<form>
		<div class="form-group">
			<input type="text" class="form-control" name="fnm" placeholder="Enter First Name"/>
			<div class="help-block"></div>
		</div>
		<div class="form-group">
			<input type="text" class="form-control" name="lnm" placeholder="Enter Last Name"/>
			<div class="help-block"></div>
		</div>
		<div class="form-group">
			<input type="text" class="form-control" name="unm" id="unm" placeholder="Enter Username"/>
			<div class="help-block"></div>
		</div>
		<div class="form-group">
			<input type="text" class="form-control" name="dob" placeholder="Date of Birth [YYYY-MM-DD]" />
			<div class="help-block"></div>
		</div>
		<div class="form-group">
			<input type="text" class="form-control" name="eml" placeholder="Enter Email"/>
			<div class="help-block"></div>
		</div>
		<div class="form-group">
			<input type="password" class="form-control" name="upd" id="upd" placeholder="Enter Password"/>
			<div class="help-block"></div>
		</div>
		<div class="form-group">
			<input type="password" class="form-control" name="cpd" id="cpd" placeholder="Enter Confirm Password"/>
			<div class="help-block"></div>
		</div>
		<div class="form-group" style="cursor:pointer;">
			<button class="btn btn-primary"	onClick="return false;">Submit</button>
			<input type="reset" value="Clear">
			&nbsp;Login?,&nbsp;<span class="back">Click Here!</span>
		</div>
		<div class="rstatus"></div>
	</form>
	<br><br><br>
</div>
<script>

$(document).ready(function(){
	if($(window).width() <= 750)
		$(".col-md-6.col-md-offset-3").css('padding-top', '100px');
		else
			$(".col-md-6.col-md-offset-3").css('padding-top', '0');
	$(window).resize(function(){
	if($(window).width() <= 750)
		$(".col-md-6.col-md-offset-3").css('padding-top', '100px');
		else
			$(".col-md-6.col-md-offset-3").css('padding-top', '0');
	});
});
var rgstr = function(){
		var temp = true;
		if($('input[name="fnm"]').val() == "" && $('input[name="fnm"]').val().length < 3 && !/^[a-zA-Z]/.test($('input[name="fnm"]').val())){
			$($('.help-block')[3]).html('Invalid first name');
			temp = false;
		}
		if($('input[name="lnm"]').val() == "" && $('input[name="lnm"]').val().length < 3 && !/^[a-zA-Z]/.test($('input[name="lnm"]').val())){
			$($('.help-block')[4]).html('Invalid last name');
			temp = false;
		}
		if($('#unm').val() == "" && $('#unm').val().length < 3 && !/^[a-zA-Z]/.test($('#unm').val())){
			$($('.help-block')[5]).html('Invalid username');
			temp = false;
		}
		if($('input[name="dob"]').val() == "" && $('input[name="dob"]').val().length < 3 && !/([0-9][1-2])\/([0-2][0-9]|[3][0-1])\/((19|20)[0-9]{2})/.test($('input[name="dob"]').val())){
			$($('.help-block')[6]).html('Invalid date of birth [MM/DD/YYYY]');
			temp = false;
		}
		if($('input[name="eml"]').val() == "" && $('input[name="eml"]').val().length < 3 && !/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test($('input[name="eml"]').val())){
			$($('.help-block')[7]).html('Invalid email');
			temp = false;
		}
		if($("#upd").val() == "" && $("#upd").val().length < 5){
			$($('.help-block')[8]).html('Invalid password');
			temp = false;
		}
		if($("#cpd").val() == "" && $("#cpd").val().length < 5){
			$($('.help-block')[9]).html('Invalid confirm password');
			temp = false;
		}
		if($("#upd").val() != $("#cpd").val()){
			$($('.help-block')[10]).html('Password doesn\'t match');
			temp = false;
		}
		if(temp){
			$($('.help-block')[0]).html('');
			$($('.help-block')[1]).html('');
			$($('.help-block')[2]).html('');
			$($('.help-block')[3]).html('');
			$($('.help-block')[4]).html('');
			$($('.help-block')[5]).html('');
			$($('.help-block')[6]).html('');
			$($('.help-block')[7]).html('');
			$($('.help-block')[8]).html('');
			$($('.help-block')[9]).html('');
			$($('.help-block')[10]).html('');
			$('.rstatus').attr("style", "color:blue;font-weight:bold;");
			$('.rstatus').html('Please wait..');
			$.ajax({url: "<?php echo base_url('/user/create');?>", type: 'post', data: {
				"unm": $("#unm").val(),
				"fnm": $("input[name='fnm']").val(),
				"lnm": $("input[name='lnm']").val(),
				"eml": $("input[name='eml']").val(),
				"dob": $("input[name='dob']").val(),
				"upd": $("#upd").val()
				}
			})
			.done(function(res){
				$("#unm").val("");
				$("input[name='fnm']").val("");
				$("input[name='lnm']").val("");
				$("input[name='eml']").val("");
				$("input[name='dob']").val("");
				$("input[name='upd']").val("");
				$('.rstatus').attr("style", "color:green;font-weight:bold;");
				$(".rstatus").html(JSON.parse(res).success_msg);
			})
			.fail(function(res) {
				$('.rstatus').attr("style", "color:red;font-weight:bold;");
				if(res.status == 401)
					$('.rstatus').html('Invalid login credentials');
				else
					$('.rstatus').html('Something went wrong');
			});
		}
	};
$(window).keypress(function(e){
	if(e.keyCode == 13){
		rgstr();
	}
});
$('.btn.btn-primary').click(function(){
	rgstr();
});
$(".back").click(function(){
	$("#rstr").dialog("close");
	$("#dialog").dialog("open");
});
</script>
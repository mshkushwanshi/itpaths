<div class="col-md-6 col-md-offset-3">
    <div id="lgnPnl">
		<h2>Login</h2>
		<div class="form-group">
			<label for="unm">Username</label>
			<input type="text" class="form-control" name="unm" placeholder="Enter Username" />
			<div class="help-block"></div>
		</div>
		<div class="form-group">
			<label for="upd">Password</label>
			<input type="password" class="form-control" name="upd" placeholder="Enter Password" />
			<div class="help-block"></div>
		</div>
		<div class="form-group">
			<button class="btn btn-primary">Login</button>
			<button class="btn btn-primary">Register</button>
			<!--<a class="btn btn-link">Register</a>-->
			<div class="help-block"></div>
		</div>
	</div>
</div>
<script>
var lgnSbmt = function(){
		if($('input[name="unm"').val() == "")
			$($('.help-block')[0]).html('Username is required');
		else if($('input[name="upd"').val() == ""){
			$($('.help-block')[0]).html('');
			$($('.help-block')[1]).html('Password is required');
		}
		else{
			$($('.help-block')[0]).html('');
			$($('.help-block')[1]).html('');
			$($('.help-block')[2]).html('Please wait..');
			$.ajax({url: "<?php echo base_url('/user/login');?>", method: 'post', data: {"unm": $("input[name='unm']").val(), "upd": $("input[name='upd']").val()}})
			.done(function(res){
				localStorage.setItem("prof", res);
				$($('.help-block')[2]).html('');
				$('div#usr-info').attr('style', 'display: block');
				$('div.lgnPnl').attr('style', 'display: none');
				$('div.admnPnl').attr('style', 'display: block');
				var logout = $('div#usr-info a');
				var resJson = JSON.parse(res);
				var name = "Hi&nbsp;" + resJson.fname + "&nbsp;" + resJson.lname + "&nbsp;&nbsp;&nbsp;";
				$($('div#usr-info div')[1]).html(name);
				$($('div#usr-info div')[1]).append(logout);
			})
			.fail(function(res) {
				if(res.status == 401)
					$($('.help-block')[2]).html('Invalid login credentials');
				else
					$($('.help-block')[2]).html('Something went wrong');
			});
		}
	};
$(document).ready(function(){
	if($(window).width() <= 767)
		$(".col-md-6.col-md-offset-3").css('padding-top', '100px');
	else
		$(".col-md-6.col-md-offset-3").css('padding-top', '0');
	$(window).resize(function(){
	if($(window).width() <= 767)
		$(".col-md-6.col-md-offset-3").css('padding-top', '100px');
		else
			$(".col-md-6.col-md-offset-3").css('padding-top', '0');
	});
});
$(window).keypress(function(e){
	if(e.keyCode == 13){
		lgnSbmt();
	}
});
$('.btn.btn-primary').click(function(){
	lgnSbmt();
});
$($('.lgnpnl button.btn.btn-primary')[1]).click(function(){
	window.open("<?php echo base_url('/user/register');?>", "_parent");
});
</script>
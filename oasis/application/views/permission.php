<div class="container" ng-controller="permCtrl" ng-init="permPnl=false;loadPnl=true;tknSts=false;sts=false;">
    <div ng-hide="permPnl" align="center" id="permPnl">
        <img src="<?php echo base_url('images/cam-permission.png');?>" width="100%" />
        <div align="right" class="btn-holder" style="margin:20px">
            <button class=" btn btn-info btn-lg nxt" data-toggle="modal" data-target="#icMdl" ng-click="permission()">Next</button>		
        </div>
    </div>
    <div ng-hide="loadPnl" align="center" class="aldr">
        <div class="row almsg">
            Your test is loading...
            <div id="progressbar"></div>
        </div>
        <div class="row almsg">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 almsgtxt" align="center">
            To ensure an uninterrupted test-taking experience,<br/> you may close all chat windows,<br/> screen-saver etc before starting the test.
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" align="center">
                <img src="<?php echo base_url('images/loadmsg.png');?>" class="alimg" />
            </div>
        </div>
    </div>
    <div ng-show="loggedin" ng-controller="usrCtrl">
        <input type="hidden" ng-model="tknSts" name="tknSts" value="<?php if(isset($tknSts['status']))echo $tknSts['status']; ?>"/>
        <div ng-show="tknSts" ng-bing="tknSts">
            <!--<?php $this->load->view('login.php');?>-->
            <?php $this->load->view('exam/sregister.php');?>
        </div>
        <div ng-controller="spnlCtrl">
            <?php $this->load->view('exam/ue.php');?>
        </div>
    </div>
</div>
<div class="modal fade" id="icMdl" role="dialog" data-show="false" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title">Internet Connection Issue</h2>
        </div>
        <div class="modal-body">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="min-height:200px;border-right: 1px solid;">
                - <b style="color:red;">Don't Worry.</b> timer has been paused,
                if you're in the middle of exam, and your responses have been saved (till we last synced with our servers).
                <br/>
                - Just sit back and relax for few minutes.
                <br/>
                <b style="color:red">Do not move away from this screen.</b><br/>
                - Your test will resume automatically from the same point once connection is restored.
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">Connecting to internet (attempt <span id="nconCtr"></span>) in <span id="itmr"></span></div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
</div>
<div class="modal fade" id="icrMdl" role="dialog" data-show="false" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h3 class="modal-title">Internet Connection Issue</h2>
        </div>
        <div class="modal-body">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="min-height:200px;border-right: 1px solid;">
                - <b style="color:red;">Don't Worry.</b> timer has been paused,
                if you're in the middle of exam, and your responses have been saved (till we last synced with our servers).
                <br/>
                - Just sit back and relax for few minutes.
                <br/>
                <b style="color:red">Do not move away from this screen.</b><br/>
                - Your test will resume automatically from the same point once connection is restored.
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">1. <b style="color:red">Connection to internet failed. </b> Please <b style="color:red">check your internet conection</b> and click <b style="color:red">Retry</b><br/><button class="btn-danger" id="icRty">Retry</button></div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
</div>
<div class="modal fade" id="efnsh" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><img src="<?php echo base_url('images/warn.png');?>" width="18px" style="margin-top:-5px;"/>&nbsp;Finish Test</h4>
            </div>
            <div class="modal-body">
                <div>Are you sure you want to finish this test?</div>
                <div>Summary of your attempts in this test is as shown below</div>
                <div class="progress" align="center"><div class="progress-bar" style="width:{{(sections.length*100)/2}}%"><span></span></div></div><br/>
                <table class="table" id="esumry">
                    <tr>
                        <th><b>Section Summary ({{sections.length}})</b></th>
                        <th>Select section names to go back to that section.</th>
                    </tr>
                    <tr ng-repeat="section in sections">
                        <td ng-click="loadSection([sections.length], [section], [$index]);" style="cursor:pointer;" data-dismiss="modal">{{section.name}}</td>
                        <td><div class="progress" align="center"><div class="progress-bar" style="width:{{(section.questions.length*100)/2}}%"><span></span></div></div></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer" align="center">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="modal" data-target="#efstp" data-backdrop="static" data-keyboard="false" onclick="loadfeedback();">Finish</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="efstp" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="left">
                <h4 class="modal-title"><img src="<?php echo base_url('images/stop.png');?>" width="35px" style="margin-top:-5px;"/>&nbsp;Finish Test</h4>
            </div>
            <div class="modal-body">
                <div align="center">Finishing your test here. Please do not close this window</div>
            </div>
            <div class="modal-footer" align="center"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="efdbk" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center"><h4 class="modal-title">Thank you for taking test<br/>
                <small>Please take a minute to provide us your feedback</small></h4></div>
            </div>
            <div class="modal-body">
                <table class="table fdtbl">
                    <tr>
                        <td width="5px">1.</td>
                        <td>The test instructions were</td>
                        <td align="right"><select class="form-control"><option>Largely clear</option></select></td>
                    </tr>
                    <tr>
                        <td width="5px">2</td>
                        <td>Overall test experience was</td>
                        <td align="right"><select class="form-control"><option>Good</option></select></td>
                    </tr>
                    <tr>
                        <td colspan="3">Any other feedback / suggestions?<br/><br/><textarea class="form-control"></textarea></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer" align="center">
                <button type="button" class="btn btn-default" data-dismiss="modal" data-toggle="modal" ng-click="efinish();">Submit</button>
            </div>
        </div>
    </div>
</div>
<script>
    function loadfeedback(){
        setInterval(function(){
            $("#efstp").modal('hide');
            $('.modal-backdrop').hide();
            $("#efdbk").modal('show');
        },3000);
    }
</script>
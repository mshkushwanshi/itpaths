<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $title;?></title>
  <script src="<?php echo site_url('js/angular.min.js');?>"></script>
  <link rel="stylesheet" href="<?php echo site_url('node_modules/font-awesome/css/font-awesome.min.css');?>"/>
  <link rel="stylesheet" href="<?php echo site_url('node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css');?>"/>
  <link rel="stylesheet" href="<?php echo site_url('css/style.css');?>"/>
  <link rel="shortcut icon" href="<?php echo site_url('images/favicon.png');?>"/>
<head>
<body>
  <div class=" container-scroller">
<html>
    <head>
        <title id="title"><?php if(isset($title)) echo $title; else echo "OASIS Admin Console";?></title>
		<link type="text/css" rel="stylesheet" href="<?php echo base_url("css/jsgrid.min.css");?>" />
		<link type="text/css" rel="stylesheet" href="<?php echo base_url("css/jsgrid-theme.min.css");?>" /> 
		<script type="text/javascript" src="<?php echo base_url("js/jsgrid.min.js");?>"></script>
        <style>
			#udRole{margin-left: 5px; margin-bottom: 10px;}
			img.dark-logo {
				height: 50px;
				margin-top: 10px;
			}
			.col-md-9.lgnDtl {
				margin-left: 35px;
				margin-top: 25px;
			}
			.col-md-3 {
				position: absolute;
			}
			div#usr-info {
				top: 0;
				left: 82%;
			}
			span.logo {
				font-size: 27px;
				position: absolute;
				font-weight: bold;
				color: #cc2525;
				font-variant-caps: petite-caps;
				margin-top: 13px;
			}
			ul#sidebarnav li {
				margin-bottom: 20px;
				display: inline;
				margin-top: 20px;
				margin-right: 25px;
			}
			ol.breadcrumb li {
				display: inline;
				font-variant-caps: all-petite-caps;
				font-variant: contextual;
				font-variant-east-asian: jis04;
			}
			.col-md-2 {
				margin-top: 10px;
			}
			.col-md-10 select {
				margin-left: 10px;
			}
			<?php if($lgnCls == "none"){?>
			#usr-info{
				display: block;
			}
			<?php }else{?>				
			div#usr-info{
				display:none;
			}
			<?php }?>
			.lgnPnl{
				min-height: 385px;
			}
			.topbar {
				height: 100px;
			}
			.lgnDtl{
				margin-top: 30px;
			 }
            
			.footer{margin-top: 10px;}
            table.table.fdtbl, table.table.fdtbl td{border:0;}
            table#captbl, table#captbl td {
                border: 0;
            }
            .col-sm-12.col-xs-12.almsgtxt{
                height: 100px;
            }
            .alimg{
                margin-top: -30px;
                margin-bottom: 10px;
                height: 150px;
            }
            .col-sm-12.col-xs-12.aldr{
                height: 330px;
            }
            .aldr{
                margin-top: 100px;
                border: 1px solid #cacacafa;
                border-radius: 4px;
            }
            .almsg{
                margin-top: 30px;
            }
            .ui-widget-header {
                font-weight: bold;
            }
            div#progressbar .col-sm-
            .ui-progressbar {
                height: 1em;
            }
            .col-md-12.col-lg-12.lprgbar{
                width: 65%;    
            }
            .col-sm-12.col-xs-12.lprgbar{
                width: 85%;
            }
            
            .ui-widget-content {
                border: 1px solid #dddddd;
            }
            .topbar .top-navbar .app-search .srh-btn{right: 30px;}
            form { margin-top: 15px; }
            .nxt{padding: 5px 20px 5px 20px;}
            .container {
                justify-content: space-between;
                flex-direction: column;
                height: auto;
                display: flex;
            }
            .container .btn-holder {
                justify-content: flex-end;
                display: flex;
            }
            .btn-group-lg>.btn, .btn-lg {
                padding: 5px 10px 5px 10px;
                font-size: 14px;
                line-height: 1.4;
                border-radius: 2px;
                position: absolute;
                vertical-align: bottom;
            }
            form > input { margin-right: 15px; }
            #results { float:right; padding: 6px 5px 0px 0px;}
            #results {margin-left: 10px; margin-top: 2px;}
            .profPicShow{display:inline;}
            .profPichide{display:none;}
            .qimg{
                max-width: 325px;
            }
            span.input-group-addon {
                cursor: pointer;
            }
            .topbar .navbar-header {
                background: #ffffff;
                box-shadow: 4px -4px 10px rgba(0, 0, 0, 0.05);
                height: auto;
                margin-top: 9px;
                border-radius: 4px;
            }   
            img.profile-pic.m-r-5 {
                height: 30px;
				margin-top: 25px;
            }
            @media  (min-width: 768px){
                .nxt{
                    margin-top: -60px !important;
                }
                .footer {
                    z-index: 1000;
                    left: 0;
                }
                .topbar .top-navbar .navbar-collapse {
                    width: 100%;
                    margin-top: -30px;
                }
                .topbar .top-navbar .app-search {
                    margin-top: 0px;
                }
            }
            
            @media only screen and (max-width: 500px) {
                div#permPnl {
                    margin-top: 100px;
                }
                #ewlc{
                    margin-top:100px;
                }
                div#my_camera video {
                    margin-top: 85px;
                }
                .nxt{
                    margin-top: 0;
                }
                div#usr-info {
                    margin-top: 25px;
                }
                .row.page-titles.col-md-12 {
                    margin-top: 100px;
                }
                .col-md-4 aside.left-sidebar{
                    width: 165px;
                }
                .row.page-titles {
                    margin-top: 90px;
                }
                .left-sidebar {
                    width: 170px;
                }
                .page-wrapper {
                    margin-left: 170px;
                }
                .footer {
                    left: 0px;
                    z-index: 1000;
                }
                .scroll-sidebar {
                    padding-bottom: 60px;
                    padding-top: 40px;
                }
                .page-titles .breadcrumb {
                    margin-left: -14px;
                    margin-top: 5px;
                }
                .col-sm-3.table-responsive.row{
                    margin-top: -25px;
                }
            }
            video#gum {
                height: 69px;
                margin-left: 50%;
                position: absolute;
            }
            ul.answer li{
                list-style-type: none;
            }
        </style>
        <script>
            window.api = "<?php echo base_url('');?>";
			$( "#eodr" ).dialog({
			  autoOpen: false,
			  show: {
				effect: "blind",
				duration: 1000
			  },
			  hide: {
				effect: "explode",
				duration: 1000
			  }
			});
			var dashboard = function(){
				$("#udGrid").show();
				$("#udRole").show();
				$("#odGrid").hide();
				$(".container.prof").hide();
				$("#odGrid-bar img").hide();
				$(".text-themecolor.m-b-0.m-t-0").html("Dashboard");
				$($(".breadcrumb-item")[1]).html("Dashboard");
				$("#grid-data").bootgrid({
					ajax: true,
					post: function (){
						return {
							id: "<?php echo $this->session->userdata('uid');?>"
						};
					},
					url: domain + "user/getdata",
					formatters: {
						"link": function(column, row)
						{
							return "<a href=\"#\">kl</a>";
						}
					}
				});
			}
		</script>
    </head>
    <body>
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-md navbar-light">
                <div class="col-xs-6 col-sm-6 col-md-9">
                    <span class="navbar-brand" href="<?php echo base_url('#');?>">
                        <span>
                            <img src="<?php echo base_url('assets/images/o-logo3x.png');?>" alt="homepage" class="dark-logo" height="65px"/>
							<span class="logo">Admin</span>
                        </span>
                    </span>
                </div>
				<div id="eodr" title="Edit Order"></div>
                <div id="usr-info" class="col-sm-6 col-xs-6 col-md-3"align="right">
                    <div class="col-md-3">
						<img src="<?php echo base_url('assets/images/users/person-default.png');?>" alt="fname" class="profile-pic m-r-5" />
					</div>
					<div class="col-md-9 lgnDtl" align="left">
                        <div>Hi&nbsp;<?php echo $this->session->userdata('name'); ?>&nbsp;</div><br>
                        <div><a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="<?php echo base_url("user/lout");?>">
						<u>Logout</u></a></div>
					</div>
                </div>
            </nav>
        </header>
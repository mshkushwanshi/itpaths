<div class="container data">
	<div class="row">
		<div class="col-md-1 col-1">
			
		</div>
		<div class="col-md-11 col-11">
			<div class="row">
				<div class="col-md-2">
				First Name:
				</div>
				<div class="col-md-10">
				<input disabled type="text" name="fname" />
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
				Last Name:
				</div>
				<div class="col-md-10">
				<input disabled type="text" name="lname" />
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
				Date of birth:
				</div>
				<div class="col-md-10">
				<input disabled type="text" name="dob" />
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
				Username:
				</div>
				<div class="col-md-10">
				<input disabled type="text" name="username" />
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
				email:
				</div>
				<div class="col-md-10">
				<input disabled type="text" name="email" />
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
				Role:
				</div>
				<div class="col-md-10">
				<select disabled>
					<option value="0">No Access</option>
					<option value="1">Admin</option>
					<option value="2">Driver</option>
					<option value="3">User</option>
					<option value="4">Enterprise User</option>
				</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
				<button class="dataBtn">Edit</button>
				</div>
			</div>
		</div>
	</div>
</div>
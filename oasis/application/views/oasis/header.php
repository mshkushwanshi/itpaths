<!DOCTYPE html>
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <style>
	img.b-logo {
		float: right;
		margin-top: 422px;
		position: absolute;
		margin-left: 885px;
		width: 53px;
		transform: rotate(270deg);
		-ms-transform: rotate(270deg);
		-moz-transform: rotate(270deg);
		-webkit-transform: rotate(363deg);
		-o-transform: rotate(270deg);
	}
	.hs-cta-wrapper p,
	.hs-cta-wrapper div {
		margin: 0;
		padding: 0;
	}
	a#cta_button_1818776_f814ad78-316a-4fe5-a84b-8be22084443c {
		-webkit-font-smoothing: antialiased;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-o-user-select: none;
		user-select: none;
		display: inline-block;
		font-weight: normal;
		text-align: center;
		text-decoration: none;
		font-family: sans-serif;
		background: rgb(0, 85, 165);
		color: rgb(255, 255, 255);
		border-radius: 6px;
		border-width: 0px;
		transition: all .4s ease;
		-moz-transition: all .4s ease;
		-webkit-transition: all .4s ease;
		-o-transition: all .4s ease;
		text-shadow: none;
		line-height: 1.5em;
		padding: 6px 18px;
	}
	a#cta_button_1818776_f814ad78-316a-4fe5-a84b-8be22084443c:hover {
		background: rgb(0, 93, 181);
		color: rgb(255, 255, 255);
	}
	a#cta_button_1818776_f814ad78-316a-4fe5-a84b-8be22084443c:active,
	#cta_button_1818776_f814ad78-316a-4fe5-a84b-8be22084443c:active:hover {
		background: rgb(0, 68, 132);
		color: rgb(244, 244, 244);
	}
	a#cta_button_1818776_f814ad78-316a-4fe5-a84b-8be22084443c {
		font-size: 14px;
		line-height: 21px;
		letter-spacing: 1.2px;
		text-decoration: none;
		color: #4a4a4a;
		font-weight: 600;
		max-width: 229px;
		display: inline-block;
		width: 100% !important;
		background-color: #fff !important;
		background-image: none;
		text-align: center;
		padding: 24px 0 28px;
		-webkit-appearance: none;
		font-family: 'open sans';
	}
	a#cta_button_1818776_f814ad78-316a-4fe5-a84b-8be22084443c img {
		margin: 0 7px -3px 0;
	}
	a#cta_button_1818776_f814ad78-316a-4fe5-a84b-8be22084443c:hover {
		color: #4a4a4a
	}
	.hs-cta-wrapper p,
	.hs-cta-wrapper div {
		margin: 0;
		padding: 0;
	}
	a#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23 {
		-webkit-font-smoothing: antialiased;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-o-user-select: none;
		user-select: none;
		display: inline-block;
		font-weight: normal;
		text-align: center;
		text-decoration: none;
		font-family: sans-serif;
		background: #e03e19;
		color: rgb(255, 255, 255);
		border-radius: 6px;
		border-width: 0px;
		transition: all .4s ease;
		-moz-transition: all .4s ease;
		-webkit-transition: all .4s ease;
		-o-transition: all .4s ease;
		text-shadow: none;
		line-height: 1.5em;
		padding: 17px 10px;
	}
	a#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23:hover {
		background: #b5472d;
		color: rgb(255, 255, 255);
	}
	a#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23:active,
	#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23:active:hover {
		background: rgb(88, 148, 30);
		color: rgb(244, 244, 244);
	}
	a#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23 {
		font-size: 14px;
		letter-spacing: 3px;
		font-weight: 700;
		border-radius: 50px;
		-webkit-appearance: none;
		width: calc(100% - 20px);
		max-width: 248px;
	}
	.hs-cta-wrapper p,
	.hs-cta-wrapper div {
		margin: 0;
		padding: 0;
	}
	a#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23 {
		-webkit-font-smoothing: antialiased;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-o-user-select: none;
		user-select: none;
		display: inline-block;
		font-weight: normal;
		text-align: center;
		text-decoration: none;
		font-family: sans-serif;
		background: #e03e19;
		color: rgb(255, 255, 255);
		border-radius: 6px;
		border-width: 0px;
		transition: all .4s ease;
		-moz-transition: all .4s ease;
		-webkit-transition: all .4s ease;
		-o-transition: all .4s ease;
		text-shadow: none;
		line-height: 1.5em;
		padding: 17px 10px;
	}
	a#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23:hover {
		background: #b5472d;
		color: rgb(255, 255, 255);
	}
	a#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23:active,
	#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23:active:hover {
		background: rgb(88, 148, 30);
		color: rgb(244, 244, 244);
	}
	a#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23 {
		font-size: 14px;
		letter-spacing: 3px;
		font-weight: 700;
		border-radius: 50px;
		-webkit-appearance: none;
		width: calc(100% - 20px);
		max-width: 248px;
	}
	.hs-cta-wrapper p,
	.hs-cta-wrapper div {
		margin: 0;
		padding: 0;
	}
	a#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23 {
		-webkit-font-smoothing: antialiased;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-o-user-select: none;
		user-select: none;
		display: inline-block;
		font-weight: normal;
		text-align: center;
		text-decoration: none;
		font-family: sans-serif;
		background: #e03e19;
		color: rgb(255, 255, 255);
		border-radius: 6px;
		border-width: 0px;
		transition: all .4s ease;
		-moz-transition: all .4s ease;
		-webkit-transition: all .4s ease;
		-o-transition: all .4s ease;
		text-shadow: none;
		line-height: 1.5em;
		padding: 17px 10px;
	}
	a#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23:hover {
		background: #b5472d;
		color: rgb(255, 255, 255);
	}
	a#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23:active,
	#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23:active:hover {
		background: rgb(88, 148, 30);
		color: rgb(244, 244, 244);
	}
	a#cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23 {
		font-size: 14px;
		letter-spacing: 3px;
		font-weight: 700;
		border-radius: 50px;
		-webkit-appearance: none;
		width: calc(100% - 20px);
		max-width: 248px;
	}
	.hs-cta-wrapper p,
	.hs-cta-wrapper div {
		margin: 0;
		padding: 0;
	}
	a#cta_button_1818776_049eb5a3-9d0f-439e-a3d1-7ba1f3ff1d02 {
		-webkit-font-smoothing: antialiased;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-o-user-select: none;
		user-select: none;
		display: inline-block;
		font-weight: normal;
		text-align: center;
		text-decoration: none;
		font-family: sans-serif;
		background: rgb(255, 255, 255);
		color: rgb(255, 255, 255);
		border-radius: 6px;
		border-width: 0px;
		transition: all .4s ease;
		-moz-transition: all .4s ease;
		-webkit-transition: all .4s ease;
		-o-transition: all .4s ease;
		text-shadow: none;
		line-height: 1.5em;
		padding: 17.5px 0px;
	}
	a#cta_button_1818776_049eb5a3-9d0f-439e-a3d1-7ba1f3ff1d02:hover {
		background: rgb(255, 255, 255);
		color: rgb(255, 255, 255);
	}
	a#cta_button_1818776_049eb5a3-9d0f-439e-a3d1-7ba1f3ff1d02:active,
	#cta_button_1818776_049eb5a3-9d0f-439e-a3d1-7ba1f3ff1d02:active:hover {
		background: rgb(204, 204, 204);
		color: rgb(244, 244, 244);
	}
	a#cta_button_1818776_049eb5a3-9d0f-439e-a3d1-7ba1f3ff1d02 {
		font-size: 14px;
		letter-spacing: 3px;
		font-weight: 700;
		border-radius: 50px;
		max-width: 320px;
		width: 100%;
		color: #4a4a4a;
		-webkit-appearance: none;
	}
	a#cta_button_1818776_049eb5a3-9d0f-439e-a3d1-7ba1f3ff1d02:hover {
		color: #4a4a4a;
	}
	.hs-cta-wrapper p,
	.hs-cta-wrapper div {
		margin: 0;
		padding: 0;
	}
	a#cta_button_1818776_0571f27a-af78-4320-9503-7a4f15d3adc3 {
		-webkit-font-smoothing: antialiased;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-o-user-select: none;
		user-select: none;
		display: inline-block;
		font-weight: normal;
		text-align: center;
		text-decoration: none;
		font-family: sans-serif;
		background: #e03e19;
		color: rgb(255, 255, 255);
		border-radius: 6px;
		border-width: 0px;
		transition: all .4s ease;
		-moz-transition: all .4s ease;
		-webkit-transition: all .4s ease;
		-o-transition: all .4s ease;
		text-shadow: none;
		line-height: 1.5em;
		padding: 17px 68px;
	}
	a#cta_button_1818776_0571f27a-af78-4320-9503-7a4f15d3adc3:hover {
		background: #b5472d;
		color: rgb(255, 255, 255);
	}
	a#cta_button_1818776_0571f27a-af78-4320-9503-7a4f15d3adc3:active,
	#cta_button_1818776_0571f27a-af78-4320-9503-7a4f15d3adc3:active:hover {
		background: rgb(88, 148, 30);
		color: rgb(244, 244, 244);
	}

	a#cta_button_1818776_0571f27a-af78-4320-9503-7a4f15d3adc3 {
		font-size: 14px;
		letter-spacing: 3px;
		font-weight: 700;
		border-radius: 50px;
		-webkit-appearance: none;
	}

	.hs-cta-wrapper p,
	.hs-cta-wrapper div {
		margin: 0;
		padding: 0;
	}

	a#cta_button_1818776_59339062-9d86-44d4-ad0e-dc8d38390e24 {
		-webkit-font-smoothing: antialiased;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-o-user-select: none;
		user-select: none;
		display: inline-block;
		font-weight: normal;
		text-align: center;
		text-decoration: none;
		font-family: sans-serif;
		background: #e03e19;
		color: rgb(255, 255, 255);
		border-radius: 6px;
		border-width: 0px;
		transition: all .4s ease;
		-moz-transition: all .4s ease;
		-webkit-transition: all .4s ease;
		-o-transition: all .4s ease;
		text-shadow: none;
		line-height: 1.5em;
		padding: 17.5px 0px;
	}

	a#cta_button_1818776_59339062-9d86-44d4-ad0e-dc8d38390e24:hover {
		background: #b5472d;
		color: rgb(255, 255, 255);
	}

	a#cta_button_1818776_59339062-9d86-44d4-ad0e-dc8d38390e24:active,
	#cta_button_1818776_59339062-9d86-44d4-ad0e-dc8d38390e24:active:hover {
		background: rgb(88, 148, 30);
		color: rgb(244, 244, 244);
	}

	a#cta_button_1818776_59339062-9d86-44d4-ad0e-dc8d38390e24 {
		font-size: 14px;
		letter-spacing: 3px;
		font-weight: 700;
		border-radius: 50px;
		max-width: 260px;
		width: 100%;
		-webkit-appearance: none;
	}

	.hs-cta-wrapper p,
	.hs-cta-wrapper div {
		margin: 0;
		padding: 0;
	}

	a#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05 {
		-webkit-font-smoothing: antialiased;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-o-user-select: none;
		user-select: none;
		display: inline-block;
		font-weight: normal;
		text-align: center;
		text-decoration: none;
		font-family: sans-serif;
		background: #e03e19;
		color: rgb(255, 255, 255);
		border-radius: 6px;
		border-width: 0px;
		transition: all .4s ease;
		-moz-transition: all .4s ease;
		-webkit-transition: all .4s ease;
		-o-transition: all .4s ease;
		text-shadow: none;
		line-height: 1.5em;
		padding: 17px 10px;
	}

	a#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05:hover {
		background: #b5472d;
		color: rgb(255, 255, 255);
	}

	a#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05:active,
	#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05:active:hover {
		background: rgb(88, 148, 30);
		color: rgb(244, 244, 244);
	}

	a#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05 {
		font-size: 14px;
		letter-spacing: 3px;
		font-weight: 700;
		border-radius: 50px;
		-webkit-appearance: none;
		width: calc(100% - 20px);
		max-width: 248px;
	}

	.hs-cta-wrapper p,
	.hs-cta-wrapper div {
		margin: 0;
		padding: 0;
	}

	a#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05 {
		-webkit-font-smoothing: antialiased;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-o-user-select: none;
		user-select: none;
		display: inline-block;
		font-weight: normal;
		text-align: center;
		text-decoration: none;
		font-family: sans-serif;
		background: #e03e19;
		color: rgb(255, 255, 255);
		border-radius: 6px;
		border-width: 0px;
		transition: all .4s ease;
		-moz-transition: all .4s ease;
		-webkit-transition: all .4s ease;
		-o-transition: all .4s ease;
		text-shadow: none;
		line-height: 1.5em;
		padding: 17px 10px;
	}

	a#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05:hover {
		background: #b5472d;
		color: rgb(255, 255, 255);
	}

	a#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05:active,
	#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05:active:hover {
		background: rgb(88, 148, 30);
		color: rgb(244, 244, 244);
	}

	a#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05 {
		font-size: 14px;
		letter-spacing: 3px;
		font-weight: 700;
		border-radius: 50px;
		-webkit-appearance: none;
		width: calc(100% - 20px);
		max-width: 248px;
	}

	.hs-cta-wrapper p,
	.hs-cta-wrapper div {
		margin: 0;
		padding: 0;
	}

	a#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05 {
		-webkit-font-smoothing: antialiased;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-o-user-select: none;
		user-select: none;
		display: inline-block;
		font-weight: normal;
		text-align: center;
		text-decoration: none;
		font-family: sans-serif;
		background: #e03e19;
		color: rgb(255, 255, 255);
		border-radius: 6px;
		border-width: 0px;
		transition: all .4s ease;
		-moz-transition: all .4s ease;
		-webkit-transition: all .4s ease;
		-o-transition: all .4s ease;
		text-shadow: none;
		line-height: 1.5em;
		padding: 17px 10px;
	}

	a#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05:hover {
		background: #b5472d;
		color: rgb(255, 255, 255);
	}

	a#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05:active,
	#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05:active:hover {
		background: rgb(88, 148, 30);
		color: rgb(244, 244, 244);
	}

	a#cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05 {
		font-size: 14px;
		letter-spacing: 3px;
		font-weight: 700;
		border-radius: 50px;
		-webkit-appearance: none;
		width: calc(100% - 20px);
		max-width: 248px;
	}

	.hs-cta-wrapper p,
	.hs-cta-wrapper div {
		margin: 0;
		padding: 0;
	}

	a#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35 {
		-webkit-font-smoothing: antialiased;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-o-user-select: none;
		user-select: none;
		display: inline-block;
		font-weight: normal;
		text-align: center;
		text-decoration: none;
		font-family: sans-serif;
		background: #e03e19;
		color: rgb(255, 255, 255);
		border-radius: 6px;
		border-width: 0px;
		transition: all .4s ease;
		-moz-transition: all .4s ease;
		-webkit-transition: all .4s ease;
		-o-transition: all .4s ease;
		text-shadow: none;
		line-height: 1.5em;
		padding: 17px 10px;
	}

	a#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35:hover {
		background: #b5472d;
		color: rgb(255, 255, 255);
	}

	a#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35:active,
	#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35:active:hover {
		background: rgb(88, 148, 30);
		color: rgb(244, 244, 244);
	}

	a#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35 {
		font-size: 14px;
		letter-spacing: 3px;
		font-weight: 700;
		border-radius: 50px;
		-webkit-appearance: none;
		width: calc(100% - 20px);
		max-width: 248px;
	}

	.hs-cta-wrapper p,
	.hs-cta-wrapper div {
		margin: 0;
		padding: 0;
	}

	a#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35 {
		-webkit-font-smoothing: antialiased;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-o-user-select: none;
		user-select: none;
		display: inline-block;
		font-weight: normal;
		text-align: center;
		text-decoration: none;
		font-family: sans-serif;
		background: #e03e19;
		color: rgb(255, 255, 255);
		border-radius: 6px;
		border-width: 0px;
		transition: all .4s ease;
		-moz-transition: all .4s ease;
		-webkit-transition: all .4s ease;
		-o-transition: all .4s ease;
		text-shadow: none;
		line-height: 1.5em;
		padding: 17px 10px;
	}

	a#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35:hover {
		background: #b5472d;
		color: rgb(255, 255, 255);
	}

	a#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35:active,
	#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35:active:hover {
		background: rgb(88, 148, 30);
		color: rgb(244, 244, 244);
	}

	a#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35 {
		font-size: 14px;
		letter-spacing: 3px;
		font-weight: 700;
		border-radius: 50px;
		-webkit-appearance: none;
		width: calc(100% - 20px);
		max-width: 248px;
	}

	.hs-cta-wrapper p,
	.hs-cta-wrapper div {
		margin: 0;
		padding: 0;
	}

	a#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35 {
		-webkit-font-smoothing: antialiased;
		cursor: pointer;
		-moz-user-select: none;
		-webkit-user-select: none;
		-o-user-select: none;
		user-select: none;
		display: inline-block;
		font-weight: normal;
		text-align: center;
		text-decoration: none;
		font-family: sans-serif;
		background: #e03e19;
		color: rgb(255, 255, 255);
		border-radius: 6px;
		border-width: 0px;
		transition: all .4s ease;
		-moz-transition: all .4s ease;
		-webkit-transition: all .4s ease;
		-o-transition: all .4s ease;
		text-shadow: none;
		line-height: 1.5em;
		padding: 17px 10px;
	}

	a#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35:hover {
		background: #b5472d;
		color: rgb(255, 255, 255);
	}

	a#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35:active,
	#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35:active:hover {
		background: rgb(88, 148, 30);
		color: rgb(244, 244, 244);
	}

	a#cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35 {
		font-size: 14px;
		letter-spacing: 3px;
		font-weight: 700;
		border-radius: 50px;
		-webkit-appearance: none;
		width: calc(100% - 20px);
		max-width: 248px;
	}
	#dialog, #track{
		display:none;
	}
	div[aria-labelledby='ui-id-2']{
		width: 450px !important;
	}
	.wordwrap { 
	   white-space: pre-wrap;      /* CSS3 */   
	   white-space: -moz-pre-wrap; /* Firefox */    
	   white-space: -pre-wrap;     /* Opera Older Version */   
	   white-space: -o-pre-wrap;   /* Opera 7 */    
	   word-wrap: break-word;      /* IE */
	}
  </style>
  <script src="<?php echo base_url("js/jquery-1.7.1.js");?>"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="<?php echo base_url("js/main.js");?>"></script>
  <div id="dialog" title="Login">
	<p>
		<?php include 'login.php'; ?>
	</p>
  </div>
  <div id="rstr">
	<div id="rstr-content"></div>
  </div>
  <div id="apnl" title="Track your order">
	<div id="apnl-dat"></div>
  </div>
  <div id="track" title="Track your order">
	<?php include 'track.php'; ?>
  </div>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="author" content="OASIS">
  <meta name="description" content="OASIS solves same-day delivery challenges so you can better serve your customers. We&#39;re not your typical courier. Flexible, professional delivery, real-time tracking + more.">
  <meta name="generator" content="HubSpot">
  <title>Fast, Prompt and Convenient | OASIS</title>
  <link rel="shortcut icon" href="<?php echo base_url("hubfs/favicon-196x196.png");?>">
  <script type="text/javascript">hsjQuery = window['jQuery'];</script>
  <link href="<?php echo base_url("css/public_common.css");?>" rel="stylesheet">
  <meta property="og:description" content="OASIS solves same-day delivery challenges so you can better serve your customers. We&#39;re not your typical courier. Flexible, professional delivery, real-time tracking + more.">
  <meta property="og:title" content="Courier and Same-Day Delivery | OASIS">
  <meta name="twitter:description" content="OASIS solves same-day delivery challenges so you can better serve your customers. We&#39;re not your typical courier. Flexible, professional delivery, real-time tracking + more.">
  <meta name="twitter:title" content="Courier and Same-Day Delivery | OASIS">
  <script type="text/javascript">
	 jQuery(document).ready( function(){
	  jQuery('.span3.widget-span.widget-type-menu.custom-menu-primary').fadeIn();
	 } );
  </script>
  <link rel="stylesheet" href="<?php echo base_url("css/font-awesome.min.css");?>">
  <script type="text/javascript">
	 $(window).load(function () {
	 $(".span12.widget-span.widget-type-raw_html.call-ad-cta").css("display", "block");
	   $(".span6.widget-span.widget-type-cell.contact-links").css("display", "block");
	 });
  </script>
  <style>
	 .span3.widget-span.widget-type-menu.custom-menu-primary{
	 display:none;}
	 .span12.widget-span.widget-type-raw_html.call-ad-cta{
	 display:none;}
	 .span6.widget-span.widget-type-cell.contact-links{
	 display:none;}
  </style>
  <meta name="msvalidate.01" content="F758E8333312913D652D378CE78FFFC0">
  <link href="<?php echo base_url("css/layout.min.css");?>" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url("css/Stylesheet.css");?>">
  <link rel="stylesheet" href="<?php echo base_url("css/Wireframe.min.css");?>">
  <script type="text/javascript" src="<?php echo base_url("js/testimonial-slider.min.js");?>"></script>
  <style>
	 .slider-nav a {
	 cursor: pointer;
	 }
	 #popup{
	 display: block;
	 position: relative !important;
	 top: auto !important;
	 }
  </style>
  <script type="text/javascript">
	 $(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	   var target = $(this.hash);
	   target = target.length ? target : $('[name=' + this.hash.substr(1) +']');
	   if (target.length) {
	 $('html,body').animate({
	   scrollTop: target.offset().top
	 }, 1000);
	 return false;
	   }
	  });
	 });
  </script>
  <script type="text/javascript" src="<?php echo base_url("js/jquerybxslider.min.js");?>"></script>
  <script type="text/javascript" language="javascript"> 
	 var sf14gv = 26813; 
	 (function() { 
	 var sf14g = document.createElement('script'); sf14g.type = 'text/javascript'; sf14g.async = true; 
	 sf14g.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 't.sf14g.com/sf14g.js'; 
	 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sf14g, s); 
	 })(); 
  </script>
  <!--<script async="" src="<?php echo base_url("js/modules-v9.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/loader-v2.js");?>"></script>--><script type="text/javascript" async="" src="<?php echo base_url("js/loader-v2_1.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/loader-v2_2.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/loader-v2_3.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/loader-v2_4.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/loader-v2_2.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/loader-v2_3.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/loader-v2_4.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/loader-v2_2.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/loader-v2_3.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/loader-v2_4.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/loader-v2_5.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/loader-v2_6.js");?>"></script><!--<script async="" src="<?php echo base_url("js/modules-v9.js");?>"></script>-->
  <link rel="shortcut icon" href="<?php echo base_url("css/style_1.css");?>">
  <script src="<?php echo base_url("js/f(1).js");?>"></script>
  <!--<script type="text/javascript" async="" src="<?php echo base_url("js/cta-loaded.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/cta-loaded_1.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/cta-loaded_2.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/cta-loaded_3.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/cta-loaded_4.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/cta-loaded_5.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/cta-loaded_6.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/cta-loaded_7.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/cta-loaded_8.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/cta-loaded_9.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/cta-loaded_10.js");?>"></script><script type="text/javascript" async="" src="<?php echo base_url("js/cta-loaded_11.js");?>"></script>--><script async="true" type="text/javascript" src="<?php echo base_url("js/roundtrip.js");?>"></script>
  <!--<script type="text/javascript" src="<?php echo base_url("js/WD5CSBXWGFFJFIYA3RNGZR");?>"></script>
  <script async="true" type="text/javascript" src="<?php echo base_url("js/YCAR6ISBHBHU7GXZOLMUXU");?>"></script>-->
  
</head>
<body class="home-up updtd-cties-2016 updt-sept-16hs-content-id-5371643716 hs-landing-page hs-page " >
  <div class="header-container-wrapper">
	 <div class="header-container container-fluid">
		<div class="row-fluid-wrapper row-depth-1 row-number-1 ">
		   <div class="row-fluid ">
			  <div class="span12 widget-span widget-type-global_group "  data-widget-type="global_group" data-x="0" data-w="12">
				 
				 <div class="" data-global-widget-path="generated_global_groups/5283694774.html">
					<div class="row-fluid-wrapper row-depth-1 row-number-1 ">
					   <div class="row-fluid ">
						  <div class="span12 widget-span widget-type-cell "  data-widget-type="cell" data-x="0" data-w="12">
							 <div class="row-fluid-wrapper row-depth-1 row-number-2 ">
								<div class="row-fluid ">
								   <div class="span12 widget-span widget-type-cell "  data-widget-type="cell" data-x="0" data-w="12">
									  <div class="row-fluid-wrapper row-depth-1 row-number-3 ">
										 <div class="row-fluid ">
											<div class="span12 widget-span widget-type-cell container hide-mobile"  data-widget-type="cell" data-x="0" data-w="12">
											   <div class="row-fluid-wrapper row-depth-1 row-number-4 ">
												  <div class="row-fluid ">
													 <div class="span5 widget-span widget-type-cell "  data-widget-type="cell" data-x="0" data-w="5">
														<div class="row-fluid-wrapper row-depth-2 row-number-1 ">
														   <div class="row-fluid ">
															  <div class="span12 widget-span widget-type-linked_image logo"  data-widget-type="linked_image" data-x="0" data-w="12">
																 <div class="cell-wrapper layout-widget-wrapper">
																	<span id="hs_cos_wrapper_module_14546601574353" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><a href="#" id="hs-link-module_14546601574353" style="border-width:0px;border:0px;"><img src="<?php echo base_url("assets/images/o-logo3x.png");?>"" class="hs-image-widget" width="435" alt="OASIS" title="OASIS" style="width: 80px !important;border-width: 0px;border: 0px;position: absolute !important;top: 10px;" sizes="(max-width: 435px) 100vw, 435px"></a></span>
																 </div>
																 
															  </div>
															  
														   </div>
														   <!--end row-->
														</div>
														
													 </div>
													 
													 <div class="span7 widget-span widget-type-cell "  data-widget-type="cell" data-x="5" data-w="7">
														<div class="row-fluid-wrapper row-depth-2 row-number-2 ">
														   <div class="row-fluid ">
															  <div class="span12 widget-span widget-type-raw_jinja cta-container"  data-widget-type="raw_jinja" data-x="0" data-w="12">
																 <div class="cta-item"><a href="#" class="phone">+233 (0) 302 547 848&nbsp;-&nbsp;OASIS</a></div>
																 <div class="cta-item"><a id="cta_button_1818776_2e59199b-abad-4408-899b-e5fc39971978" class="cta_button get-started" href="<?php echo base_url("oasis/faq");?>"title="Have doubts?">Have doubts?</a></div>
																 <div class="cta-item"><a id="cta_button_1818776_156e426f-5c99-4c33-a8f0-6438378633bf" class="cta_button login" href="#" data-toggle="modal" data-target="#login" title="LOGIN" data-toggle="modal" data-target="#login">LOGIN</a>
																 </div>
															  </div>
															  
														   </div>
														   <!--end row-->
														</div>
														
													 </div>
													 
												  </div>
												  <!--end row-->
											   </div>
											   
											</div>
											
										 </div>
										 <!--end row-->
									  </div>
									  
									  <div class="row-fluid-wrapper row-depth-1 row-number-1 ">
										 <div class="row-fluid ">
											<div class="span12 widget-span widget-type-raw_html " style="min-height:1px !important;" data-widget-type="raw_html" data-x="0" data-w="12">
											   <div class="cell-wrapper layout-widget-wrapper">
												  <span id="hs_cos_wrapper_module_1503947269431203" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_raw_html"  data-hs-cos-general-type="widget" data-hs-cos-type="raw_html">
													 <div class="hide-mobile header-divider-line"></div>
												  </span>
											   </div>
											   
											</div>
											
										 </div>
										 <!--end row-->
									  </div>
									  
									  <div class="row-fluid-wrapper row-depth-1 row-number-2 ">
										 <div class="row-fluid ">
											<div class="span12 widget-span widget-type-cell "  data-widget-type="cell" data-x="0" data-w="12">
											   <div class="row-fluid-wrapper row-depth-1 row-number-3 ">
												  <div class="row-fluid ">
													 <div class="span12 widget-span widget-type-cell container"  data-widget-type="cell" data-x="0" data-w="12">
														<div class="row-fluid-wrapper row-depth-2 row-number-1 ">
														   <div class="row-fluid ">
															  <div class="span8 widget-span widget-type-menu custom-menu-primary js-enabled"  data-widget-type="menu" data-x="0" data-w="8">
																 <div class="cell-wrapper layout-widget-wrapper">
																	<span id="hs_cos_wrapper_module_150224709488673" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_menu"  data-hs-cos-general-type="widget" data-hs-cos-type="menu">
																	   <div class="mobile-trigger"><i></i></div>
																	   <div id="hs_menu_wrapper_module_150224709488673" class="hs-menu-wrapper active-branch flyouts hs-menu-flow-horizontal" role="navigation" data-sitemap-name="OASIS Menu - Aug 2017">
																		  <ul>
																			 <li class="hs-menu-item hs-menu-depth-1 hs-item-has-children">
																				<a href="<?php echo base_url("");?>">Home</a> 
																				<div class="child-trigger"><i></i></div>
																					<ul class="hs-menu-children-wrapper">
																					   <li class="hs-menu-item hs-menu-depth-2"><a href="solution">Enterprise Solutions</a></li>
																					   <li class="hs-menu-item hs-menu-depth-2"><a href="delivery">Flexible Delivery</a></li>
																				</ul>
																			 </li>
																			 <li class="hs-menu-item hs-menu-depth-1 hs-item-has-children">
																				<a href="<?php echo base_url("oasis/about");?>">About</a> 
																				<div class="child-trigger"><i></i></div>
																				<ul class="hs-menu-children-wrapper">
																				   <li class="hs-menu-item hs-menu-depth-2"><a href="<?php echo base_url("oasis/about");?>">Company</a></li>
																				   <li class="hs-menu-item hs-menu-depth-2"><a href="<?php echo base_url("oasis/faq");?>">FAQ</a></li>
																				</ul>
																			 </li>
																			 <!--<li class="hs-menu-item hs-menu-depth-1 hs-item-has-children">
																				<a href="#">Cities</a> 
																				<div class="child-trigger"><i></i></div>
																				<ul class="hs-menu-children-wrapper">
																				   <li class="hs-menu-item hs-menu-depth-2"><a href="#">Abuja</a></li>
																				   <li class="hs-menu-item hs-menu-depth-2"><a href="#">Austin</a></li>
																				</ul>
																			 </li>-->
																			 <!--<li class="hs-menu-item hs-menu-depth-1 hs-item-has-children">
																				<a href="#">Clients</a> 
																				<div class="child-trigger"><i></i></div>
																				<ul class="hs-menu-children-wrapper">
																				   <li class="hs-menu-item hs-menu-depth-2"><a href="#">D'Cafe</a></li>
																				   <li class="hs-menu-item hs-menu-depth-2"><a href="#">Bread Boutique</a></li>
																				</ul>
																			 </li>-->
																			 <li class="hs-menu-item hs-menu-depth-1 track"><a href="#">Track your item(s)</a></li>
																			 <li class="hs-menu-item hs-menu-depth-1 hs-item-has-children"><a href="<?php echo base_url("oasis-express.apk");?>">Mobile App</a></li>
																			 <li class="hs-menu-item hs-menu-depth-1 hs-item-has-children"><a href="<?php echo base_url("oasis/contact");?>">Contact US</a></li>
																		  </ul>
																		  <script>$(".custom-menu-primary .hs-menu-wrapper ul li a").hover(function(){$(".custom-menu-primary .hs-menu-wrapper ul li a:hover").attr("style", "color:#ffffff;");$(".custom-menu-primary .hs-menu-wrapper ul li:hover").attr("style", "background: #0055a5;color:#ffffff;");});
$(".custom-menu-primary .hs-menu-wrapper ul li a").mouseout(function(){$(".custom-menu-primary .hs-menu-wrapper ul li a").attr("style", "color:");$(".custom-menu-primary .hs-menu-wrapper ul li").attr("style", "background:;border-top-left-radius:;border-top-right-radius:;color:");});

</script>
																		  <div class="mobile-login hide-desktop"><a href="#">Login</a></div>
																	   </div>
																	</span>
																 </div>
																 
															  </div>
															  
															  <div class="span4 widget-span widget-type-raw_jinja cta-container hide-desktop"  data-widget-type="raw_jinja" data-x="8" data-w="4">
																 <div class="fl">
																	<div class="logo"><a href="#"><img src="<?php echo base_url("assets/images/d-logo3x(1).png");?>""></a></div>
																 </div>
																 <div class="fr">
																	<div class="cta-item"><a href="#" class="phone"><img src="<?php echo base_url("assets/images/telephone-ico.svg");?>"></a></div>
																	<div class="cta-item"><a id="cta_button_1818776_2e59199b-abad-4408-899b-e5fc39971978" class="cta_button get-started" href="<?php echo base_url("oasis/about");?>"title="GET STARTED">GET STARTED</a></div>
																	<div class="cta-item"><a id="cta_button_1818776_156e426f-5c99-4c33-a8f0-6438378633bf" class="cta_button login" href="#"title="LOGIN">LOGIN</a></div>
																 </div>
															  </div>
															  
														   </div>
														   <!--end row-->
														</div>
														
														<div class="row-fluid-wrapper row-depth-2 row-number-2 ">
														   <div class="row-fluid ">
															  <div class="span12 widget-span widget-type-raw_html " style="min-height:0 !important;" data-widget-type="raw_html" data-x="0" data-w="12">
																 <div class="cell-wrapper layout-widget-wrapper">
																	<span id="hs_cos_wrapper_module_150343600724294" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_raw_html"  data-hs-cos-general-type="widget" data-hs-cos-type="raw_html">
																	   <style type="text/css");?>">
																		  .logo { 
																		  margin:13px; 
																		  margin-left:40px;
																		  opacity: 1;
																		  transition: opacity .25s ease-in-out;
																		  -moz-transition: opacity .25s ease-in-out;
																		  -webkit-transition: opacity .25s ease-in-out; }
																		  .header-divider-line {
																		  border-bottom: 1px solid #e1e1e1; width:100%; height:1px !important; }
																		  .header-divider-line .row-fluid .widget-type-raw_html {
																		  min-height:1px; }
																		  .fl { float:left; }
																		  .fr { float:right; }
																		  a#cta_button_1818776_2e59199b-abad-4408-899b-e5fc39971978 {
																		  font-size: 14px;
																		  line-height: 21px;
																		  letter-spacing: 1.2px;
																		  text-decoration: none;
																		  font-weight: 600;
																		  color: #fff;
																		  background-color: #b5472d;
																		  width:auto;
																		  display: inline-block;
																		  text-align: center;
																		  padding: 10px 20px;
																		  border-radius: 30px;
																		  -webkit-appearance: none;
																		  margin:10px auto;}
																		  a#cta_button_1818776_156e426f-5c99-4c33-a8f0-6438378633bf { 
																		  font-size: 14px;
																		  line-height: 21px;
																		  letter-spacing: 1.2px;
																		  text-decoration: none;
																		  font-weight: 600;
																		  color:#757575;
																		  padding: 10px 20px;
																		  border-radius: 30px; 
																		  margin:10px;
																		  border:solid 1px #ccc;
																		  }
																		  .cta-container { 
																		  text-align:right; }
																		  .cta-item {
																		  display:inline-block;  }
																		  .cta-item .phone {
																		  margin-right:10px; font-size: 14px; letter-spacing: 1.2px; vertical-align:middle; }
																		  .cta-item a.phone { font-weight:600; color:#757575; }
																		  li.column-clear { 
																		  display:table-cell; }
																		  .hs-menu-wrapper ul li { 
																		  text-align:left; }
																		  .custom-menu-primary ul li {
																		  margin-bottom: solid 5px #fff; }  
																		  .hs-menu-wrapper ul li a { 
																		  text-transform:uppercase; }
																		  .hs-menu-wrapper ul li.hs-menu-depth-2 { 
																		  text-transform:none; margin-bottom: 0;  }
																		  /* .hs-menu-wrapper ul li:nth-child(1) ul {
																		  left: -43px !important; }
																		  .hs-menu-wrapper ul li:nth-child(1) ul li {
																		  text-align:center; }
																		  .hs-menu-wrapper ul li:nth-child(2) ul {
																		  left: -55px !important; }
																		  .hs-menu-wrapper ul li:nth-child(2) ul li {
																		  text-align:center; }
																		  .hs-menu-wrapper ul li:nth-child(5) ul {
																		  left: -51px !important; }
																		  .hs-menu-wrapper ul li:nth-child(5) ul li {
																		  text-align:center; }  
																		  */ 
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(3) ul {
																		  columns: 3;
																		  -webkit-columns: 3;
																		  -moz-columns: 3;
																		  -webkit-column-gap: 30px; /* Chrome, Safari, Opera */
																		  -moz-column-gap: 30px; /* Firefox */
																		  column-gap: 0px;
																		  text-align:left; 
																		  }
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(3) ul li:last-child a {
																		  font-weight:bold;
																		  text-transform:uppercase; }
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(3) ul li:last-child a:after {
																		  content:" \f054";
																		  font-family:fontAwesome; }
																		  .custom-menu-primary .hs-menu-wrapper.hs-menu-flow-horizontal>ul li.hs-item-has-children ul.hs-menu-children-wrapper { 
																		  background-color:#eff1f0; }
																		  .custom-menu-primary .hs-menu-wrapper ul li a { 
																		  color:#757575;  }
																		  .custom-menu-primary {
																		  margin: 0 auto !important;
																		  text-align: center;
																		  width: 100% !important; }
																		  .custom-menu-primary ul {
																		  width: 100% !important;
																		  border-right: none;
																		  max-width: 800px;
																		  float: none; }
																		  .custom-menu-primary ul.hs-menu-children-wrapper {
																		  background: none;
																		  border-top: 5px solid #0055a5;
																		  box-shadow: none;
																		  float: none !important;
																		  padding: 20px 10px;
																		  text-align: left;
																		  top: 59px;
																		  transition: all 0.22s linear 0s;
																		  width: auto !important; }
																		  .custom-menu-primary ul li { 
																		  margin-right:0; padding:0 16px !important; }
																		  .custom-menu-primary ul li a {
																		  padding:20px 0 !important; } 
																		  .custom-menu-primary ul li  { 
																		  border-bottom: 5px solid #fff; }
																		  .custom-menu-primary ul li:hover  { 
																		  border-bottom: 5px solid #0055a5; }
																		  .custom-menu-primary ul li.hs-menu-depth-2,
																		  .custom-menu-primary ul li.hs-menu-depth-2:hover  { 
																		  border-bottom: 0; }
																		  .custom-menu-primary ul li ul li{ 
																		  margin-right:0; padding-right: 0; }
																		  .OASIS-menu-primary .hs-menu-wrapper > ul li a, 
																		  .custom-menu-primary .hs-menu-wrapper.hs-menu-flow-horizontal > ul li.hs-item-has-children ul.hs-menu-children-wrapper li a {
																		  }
																		  .custom-menu-primary ul.hs-menu-children-wrapper { 
																		  border-top: none; }
																		  .custom-menu-primary .hs-menu-wrapper.hs-menu-flow-horizontal > ul li.hs-menu-depth-1:last-child { display:inline-block; }
																		  @media (min-width: 768px) {
																		  .custom-menu-primary ul li.hs-item-has-children > .child-trigger > i:after {
																		  content: "";
																		  font: normal normal normal 14px/1 FontAwesome;
																		  position: absolute;
																		  font-size: 23px;
																		  height: 12px;
																		  line-height: 10px;
																		  top: 0px;
																		  bottom: 0;
																		  margin: auto;
																		  color: #757575;
																		  right: 0;
																		  transition: transform 0.3s ease 0s;
																		  -webkit-transform-style: preserve-3d;
																		  -moz-transform-style: preserve-3d;
																		  -o-transform-style: preserve-3d;
																		  transform-style: preserve-3d;
																		  }
																		  .hide-desktop { 
																		  display:none !important; }
																		  }
																		  @media (max-width: 767px) {
																		  /*** to be modified if the number of city submenu items changes */
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(3) ul li:nth-child(16),
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(3) ul li:nth-child(17),
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(3) ul li:nth-child(18),
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(3) ul li:nth-child(19) {
																		  display:none;
																		  }
																		  .logo { 
																		  margin:0px; margin-top:14px;  max-width:unset; min-width:unset; width:auto; float:left;  }
																		  .logo img {
																		  height:40px; }
																		  body.mobile-open .cta-container .fl { 
																		  display:none; }  
																		  body.mobile-open .logo { 
																		  opacity: 0;
																		  transition: opacity .1s ease-in-out;
																		  -moz-transition: opacity .1s ease-in-out;
																		  -webkit-transition: opacity .1s ease-in-out; }
																		  .custom-menu-primary .hs-menu-wrapper.hs-menu-flow-horizontal> ul > li.hs-menu-depth-1 {
																		  padding:0px !important; }
																		  .custom-menu-primary .hs-menu-wrapper.hs-menu-flow-horizontal> ul > li.hs-menu-depth-1 > a { 
																		  padding-left: 0px !important;  
																		  line-height: 6px !important; 
																		  font-weight:bold;}
																		  .custom-menu-primary .hs-menu-wrapper.hs-menu-flow-horizontal>ul li.hs-item-has-children ul.hs-menu-children-wrapper { 
																		  margin-top:0px !important; }
																		  .custom-menu-primary .hs-menu-wrapper.hs-menu-flow-horizontal > ul li:last-child a { 
																		  width:100% !important;  }
																		  .custom-menu-primary .hs-menu-wrapper.hs-menu-flow-horizontal > ul li:last-child a:active { 
																		  color: #0052ac; }
																		  .custom-menu-primary.js-enabled .hs-menu-wrapper, .custom-menu-primary.js-enabled .hs-menu-children-wrapper {
																		  display: none; }
																		  .custom-menu-primary, .custom-menu-primary .hs-menu-wrapper > ul, 
																		  .custom-menu-primary .hs-menu-wrapper > ul li, 
																		  .custom-menu-primary .hs-menu-wrapper > ul li a {
																		  display: block;
																		  float: none;
																		  position: static;
																		  top: auto;
																		  right: auto;
																		  left: auto;
																		  bottom: auto;
																		  padding: 0px;
																		  margin: 0px;
																		  background-image: none;
																		  background-color: transparent;
																		  border: 0px;
																		  -webkit-border-radius: 0px;
																		  -moz-border-radius: 0px;
																		  border-radius: 0px;
																		  -webkit-box-shadow: none;
																		  -moz-box-shadow: none;
																		  box-shadow: none;
																		  max-width: none;
																		  width: 92%;
																		  height: auto;
																		  line-height: 1;
																		  font-weight: normal;
																		  text-decoration: none;
																		  text-indent: 0px;
																		  text-align: left;
																		  color: #ffffff;
																		  }
																		  .custom-menu-primary .hs-menu-wrapper.hs-menu-flow-horizontal> ul > li.hs-menu-depth-1 {
																		  height: auto !important;
																		  }
																		  .mobile-login {
																		  text-align:center; }  
																		  .mobile-login a { 
																		  width: 90%;
																		  margin: 10px auto;
																		  font-size: 14px;
																		  line-height: 21px;
																		  letter-spacing: 1.2px;
																		  text-decoration: none;
																		  font-weight: 600;
																		  background-color: #fff;
																		  display: inline-block;
																		  text-align: center;
																		  padding: 14px 20px;
																		  border-radius: 30px;
																		  -webkit-appearance: none;
																		  border: solid 1px #d7d7d7;
																		  text-transform:uppercase; 
																		  color:#4d4d4d;
																		  }
																		  .custom-menu-primary .hs-menu-wrapper.hs-menu-flow-horizontal > ul li ul li:last-child a { 
																		  width:auto !important; background-color:transparent; font-weight:400; margin:0px; border:0; line-height:unset; }
																		  .row-fluid-wrapper:last-child .hs-menu-wrapper.hs-menu-flow-horizontal>ul { padding-bottom:30px; }
																		  .header-container-wrapper { 
																		  padding: 0; background:transparent; } 
																		  .header-container-wrapper .container { 
																		  padding:0 2%;  min-height:83px; background:#fff; }
																		  ul.hs-menu-children-wrapper { 
																		  padding: 0 !important; }
																		  ul.hs-menu-children-wrapper > li.hs-menu-depth-2 > a { 
																		  text-indent: 20px !important; }
																		  ul.hs-menu-children-wrapper > li.hs-menu-depth-2:last-child > a { 
																		  margin-bottom:0; }
																		  a#cta_button_1818776_2e59199b-abad-4408-899b-e5fc39971978 { padding: 12px 14px !important; margin:10px auto; }
																		  a#cta_button_1818776_156e426f-5c99-4c33-a8f0-6438378633bf { padding: 12px 14px !important; margin:10px auto;}
																		  .body-container-wrapper { 
																		  padding-top:83px; }
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(3) ul li:nth-child(6) {
																		  display:none; }
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(3) ul li:last-child {
																		  display:block; }
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(3) ul li:last-child a {
																		  font-weight:bold !important;
																		  text-transform:uppercase; }
																		  .custom-menu-primary .hs-menu-wrapper.hs-menu-flow-horizontal>ul li:nth-child(3) ul.hs-menu-children-wrapper {
																		  }
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(3) ul {
																		  columns: 1;
																		  -webkit-columns: 1;
																		  -moz-columns: 1;
																		  -webkit-column-gap: 20px;
																		  -moz-column-gap: 20px;
																		  column-gap: 20px;
																		  text-align: left;
																		  }
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(1) ul li,
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(2) ul li,
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(3) ul li,
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(4) ul li,
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(5) ul li,
																		  .custom-menu-primary .hs-menu-wrapper ul li:nth-child(6) ul li {
																		  text-align:left; }
																		  body.mobile-open { }
																		  .mobile-trigger {
																		  background: transparent;
																		  border: 0 none;
																		  border-radius: 4px;
																		  cursor: pointer;
																		  display: inline-block !important;
																		  font-size: 16px;
																		  font-weight: normal;
																		  left: 0;
																		  padding: 6px 0;
																		  position: absolute;
																		  text-align: left;
																		  text-transform: uppercase;
																		  top: 11.5px;
																		  width: 70px;
																		  height: 70px;
																		  padding:0px;
																		  }
																		  .mobile-trigger i { 
																		  display: inline-block; 
																		  position:relative;
																		  top:30px; 
																		  left: 20px; 
																		  margin-bottom:20px; 
																		  transition: height 0s;
																		  }
																		  .mobile-trigger i, 
																		  .mobile-trigger i::before, 
																		  .mobile-trigger i::after {
																		  height: 1.5px;
																		  background-color: #4d4d4d; 
																		  display:inline-block;
																		  width:32px; }
																		  .mobile-trigger i::before, 
																		  .mobile-trigger i::after { 
																		  transition-duration: 0.2s, 0.2s;
																		  transition-delay: 0.2s, 0s; }
																		  .mobile-trigger i::before { 
																		  transition-property: top, transform; }
																		  .mobile-trigger i::after { 
																		  transition-property: bottom, transform; }
																		  .mobile-trigger:hover i, 
																		  .mobile-trigger:hover i:before, 
																		  .mobile-trigger:hover i:after, 
																		  .mobile-open .mobile-trigger i, 
																		  .mobile-open .mobile-trigger i:before, 
																		  .mobile-open .mobile-trigger i:after {
																		  background-color: #4d4d4d; }
																		  body.mobile-open .mobile-trigger { 
																		  background:transparent; 
																		  top:0px; }
																		  body.mobile-open .mobile-trigger i {
																		  height:0px;
																		  transition: height 0s; }
																		  body.mobile-open .mobile-trigger i:before {
																		  top: 3px;
																		  transform: rotate(45deg); }
																		  body.mobile-open .mobile-trigger i:after {
																		  top: 3px;
																		  bottom:unset;
																		  transform: rotate(-45deg); }
																		  body.mobile-open .mobile-trigger i:before, 
																		  body.mobile-open .mobile-trigger i:after {
																		  transition-delay: 0s, 0.2s;
																		  }
																		  body.mobile-open  { 
																		  overflow:hidden; }
																		  .cta-container { 
																		  position: absolute; 
																		  top: 10px; 
																		  right:0; 
																		  padding-top: 0px; 
																		  padding-right:20px; 
																		  width:87% !important; }
																		  .cta-item {
																		  vertical-align:middle; }
																		  .cta-container .fr { 
																		  }
																		  body.mobile-open  .cta-container {
																		  }
																		  body.mobile-open .cta_button.login { display:none; }
																		  .cta-item .phone {
																		  display:none; }
																		  body.mobile-open .cta-item .phone { display:block; }
																		  .custom-menu-primary .hs-menu-wrapper { 
																		  background-color: #eff1f0 !important; }
																		  .custom-menu-primary .hs-menu-wrapper > ul { 
																		  margin:0 20px; padding-top:10px; }
																		  body.mobile-open .custom-menu-primary .hs-menu-wrapper > ul { 
																		  border-top:solid 1px #d6d6d6; 
																		  height: 60vh; 
																		  overflow-y: scroll;}
																		  .custom-menu-primary ul { 
																		  width: auto !important;}
																		  .custom-menu-primary .hs-menu-wrapper > ul li { 
																		  border:none; padding:0px; }
																		  .custom-menu-primary .hs-menu-wrapper.hs-menu-flow-horizontal > ul li a { 
																		  vertical-align:bottom; width:auto !important; }
																		  .child-trigger {
																		  display: inline-block !important;
																		  position: relative;
																		  top: inherit;
																		  right: inherit;
																		  color:#d4d4d4; }
																		  .child-trigger i, .child-trigger i::after{ 
																		  font-size:20px; }
																		  .custom-menu-primary.js-enabled {
																		  max-width: 100% !important;
																		  width: 100% !important;
																		  padding-top: 47px !important;
																		  position:relative !important;
																		  background:#fff !important; 
																		  border-radius: 16px;
																		  margin-top:10px; }
																		  body.mobile-open .custom-menu-primary.js-enabled { 
																		  background:#eff1f0 !important; margin-top:10px !important;}
																		  .custom-menu-primary.js-enabled .hs-menu-wrapper { 
																		  left:0; 
																		  margin-top:20px; 
																		  width:100%; 
																		  position:relative; 
																		  top:2px; 
																		  padding:0;
																		  height: 80vh; }
																		  .custom-menu-primary .hs-menu-wrapper >  ul li a { 
																		  color: #494949; }
																		  .custom-menu-primary ul li:hover { 
																		  border-bottom:none; }
																		  .custom-menu-primary .hs-menu-wrapper > ul li a:hover { 
																		  color:#0052a8; } 
																		  .custom-menu-primary ul li {
																		  padding:0 !important; }
																		  .hide-mobile { 
																		  display:none !important; }
																		  }
																		  @media (max-width: 587px) { 
																		  .cta-container { width:84% !important;}
																		  }
																		  @media (max-width: 495px) {
																		  .cta-container .login { display:none; }
																		  .cta-container { 
																		  padding-right: 0; margin-right:3%; width:80% !important; }
																		  }
																		  @media (max-width: 400px) {
																		  .logo {  
																		  width:auto !important; }
																		  .cta-container { 
																		  width:78% !important;} 
																		  }
																		  .custom-menu-primary ul.hs-menu-children-wrapper li a { transition:none !important; }
																	   </style>
																	</span>
																 </div>
																 
															  </div>
															  
														   </div>
														   <!--end row-->
														</div>
														
													 </div>
													 
												  </div>
												  <!--end row-->
											   </div>
											   
											</div>
											
										 </div>
										 <!--end row-->
									  </div>
									  
								   </div>
								</div>
								<!--end row-->
							 </div>
							 
						  </div>
						  
					   </div>
					   <!--end row-->
					</div>
					
				 </div>
				 
			  </div>
			  
		   </div>
		   <!--end row-->
		</div>
		
	 </div>
	 
  </div>
  
<div class="span12 widget-span widget-type-cell container"  data-widget-type="cell" data-x="0" data-w="12">
<div class="row-fluid-wrapper row-depth-1 row-number-3 ">
   <div class="row-fluid ">
	  <div class="span12 widget-span widget-type-cell header"  data-widget-type="cell" data-x="0" data-w="12">
		 <div class="row-fluid-wrapper row-depth-1 row-number-4 ">
			<div class="row-fluid ">
			   <div class="span12 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="0" data-w="12">
				  <div class="cell-wrapper layout-widget-wrapper">
					 <span id="hs_cos_wrapper_module_145458391138931596" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/our-cities.svg");?>" class="hs-image-widget " style="width:56px;border-width:0px;border:0px;" width="56" alt="Our Cities" title="Our Cities"></span>
				  </div>
				  <!--end layout-widget-wrapper -->
			   </div>
			   <!--end widget-span -->
			</div>
			<!--end row-->
		 </div>
		 <!--end row-wrapper -->
		 <div class="row-fluid-wrapper row-depth-1 row-number-5 ">
			<div class="row-fluid ">
			   <div class="span12 widget-span widget-type-header "  data-widget-type="header" data-x="0" data-w="12">
				  <div class="cell-wrapper layout-widget-wrapper">
					 <span id="hs_cos_wrapper_module_145458395604831981" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_header"  data-hs-cos-general-type="widget" data-hs-cos-type="header">
						<h2>
						   Our Cities<br>
						   <p>More cities coming soon.</p>
						</h2>
					 </span>
				  </div>
				  <!--end layout-widget-wrapper -->
			   </div>
			   <!--end widget-span -->
			</div>
			<!--end row-->
		 </div>
		 <!--end row-wrapper -->
	  </div>
	  <!--end widget-span -->
   </div>
   <!--end row-->
</div>
<!--end row-wrapper -->
<div class="row-fluid-wrapper row-depth-1 row-number-6 ">
   <div class="row-fluid ">
	  <div class="span12 widget-span widget-type-widget_container "  data-widget-type="widget_container" data-x="0" data-w="12">
		 <span id="hs_cos_wrapper_module_145458410807132742" class="hs_cos_wrapper hs_cos_wrapper_widget_container hs_cos_wrapper_type_widget_container"  data-hs-cos-general-type="widget_container" data-hs-cos-type="widget_container">
			<div id="hs_cos_wrapper_widget_1505823424939" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget"  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
			   <a class="ct" href="">
				  <div class="city">
					 <img src="<?php echo base_url("assets/images/abuja.jpg");?>" width="400" height="200" alt="Abuja"  sizes="(max-width: 400px) 100vw, 400px">
					 <span>
						<h4>Abuja</h4>
					 </span>
				  </div>
			   </a>
			   <style>
				  a.ct .city:hover span {
				  background-color: rgba(60, 153, 241, 0.6);
				  }
				  a.ct .city:hover img {
				  filter: grayscale(0%);
				  -webkit-filter: grayscale(0%);
				  -moz-filter: grayscale(0%);
				  }
				  .city img{
					max-height: 230px;
				  }
			   </style>
			</div>
			<div id="hs_cos_wrapper_widget_1505823424939" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget"  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
			   <a class="ct" href="">
				  <div class="city">
					 <img src="<?php echo base_url("assets/images/lapaz-accra.jpg");?>" width="400" height="200" alt="Lapaz-Accra"  sizes="(max-width: 400px) 100vw, 400px">
					 <span>
						<h4>Lapaz-Accra</h4>
					 </span>
				  </div>
			   </a>
			   <style>
				  a.ct .city:hover span {
				  background-color: rgba(60, 153, 241, 0.6);
				  }
				  a.ct .city:hover img {
				  filter: grayscale(0%);
				  -webkit-filter: grayscale(0%);
				  -moz-filter: grayscale(0%);
				  }
			   </style>
			</div>
			<div id="hs_cos_wrapper_widget_1454669054197" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget"  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
			   <a class="ct" href="">
				  <div class="city">
					 
				  </div>
			   </a>
			   <style>
				  a.ct .city:hover span {
				  background-color: rgba(60, 153, 241, 0.6);
				  }
				  a.ct .city:hover img {
				  filter: grayscale(0%);
				  -webkit-filter: grayscale(0%);
				  -moz-filter: grayscale(0%);
				  }
			   </style>
			</div>
			</span>
	  </div>
	  <!--end widget-span -->
   </div>
   <!--end row-->
</div>
<div class="row-fluid-wrapper row-depth-1 row-number-7 ">
   <div class="row-fluid ">
	  <div class="span12 widget-span widget-type-rich_text mr-cities-sn" style="display: none;" data-widget-type="rich_text" data-x="0" data-w="12">
		 <div class="cell-wrapper layout-widget-wrapper">
			<span id="hs_cos_wrapper_module_145458418179535074" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text"  data-hs-cos-general-type="widget" data-hs-cos-type="rich_text">
			   <h4>MORE CITIES COMING SOON</h4>
			</span>
		 </div>
	  </div>
   </div>
</div>
</div>
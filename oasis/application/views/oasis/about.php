<link rel="stylesheet" href="<?php echo base_url("css/about.css");?>">
<div class="body-container-wrapper">
<div class="body-container container-fluid">
<div class="row-fluid-wrapper row-depth-1 row-number-1 ">
   <div class="row-fluid ">
      <div class="span12 widget-span widget-type-cell banner-sec about"  data-widget-type="cell" data-x="0" data-w="12">
         <img src="../assets/images/b-logo3x.png" class="b-logo" />
         <div class="row-fluid-wrapper row-depth-1 row-number-2 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell "  data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-3" style="height:561px;">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell container"  data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-4 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-cell banner-caption"  data-widget-type="cell" data-x="0" data-w="12">
                                    <div class="row-fluid-wrapper row-depth-2 row-number-1 ">
                                       <div class="row-fluid ">
                                          <div class="span12 widget-span widget-type-rich_text "  data-widget-type="rich_text" data-x="0" data-w="12">
                                             <div class="cell-wrapper layout-widget-wrapper">
                                                <span id="hs_cos_wrapper_module_145458317557213871" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text"  data-hs-cos-general-type="widget" data-hs-cos-type="rich_text">
                                                   <h1>ABOUT OASIS</h1>
                                                </span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid-wrapper row-depth-2 row-number-3 ">
                                       <div class="row-fluid ">
                                          <div class="span12 widget-span widget-type-custom_widget "  data-widget-type="custom_widget" data-x="0" data-w="12">
                                             <div class="cell-wrapper layout-widget-wrapper">
                                                <span id="hs_cos_wrapper_module_145655185865019191" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget"  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                                </span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row-fluid-wrapper row-depth-1 row-number-5 ">
   <div class="row-fluid ">
      <div class="span12 widget-span widget-type-cell cmpny-sect" style="" data-widget-type="cell" data-x="0" data-w="12">
         <div class="row-fluid-wrapper row-depth-1 row-number-6 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell container" style="" data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-7 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell the-cmpny-cntnt" style="" data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-8 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-header " style="" data-widget-type="header" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_1492176729911542" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_header" style="" data-hs-cos-general-type="widget" data-hs-cos-type="header">
                                          <h2>The Company</h2>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row-fluid-wrapper row-depth-1 row-number-9 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-rich_text " style="" data-widget-type="rich_text" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_1492176733151544" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text" style="" data-hs-cos-general-type="widget" data-hs-cos-type="rich_text">
                                          <p>Oasis Express is a limited liability company incorporated in January 2018 and licensed by the Postal and Courier Service Regulatory Commission of Ghana. </p>
                                          <p>Oasis Express is dynamic company whose primary focus is to provide express pickup and delivery services, and solutions to everyday household needs. We are team of highly motivated and customer friendly professionals with a goal of relieving our customers of stress by meeting their unique needs and requirements.
                                             &nbsp;
                                          </p>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row-fluid-wrapper row-depth-1 row-number-10 ">
   <div class="row-fluid ">
      <div class="span12 widget-span widget-type-cell " style="background-color:OASIS;" data-widget-type="cell" data-x="0" data-w="12">
         <div class="row-fluid-wrapper row-depth-1 row-number-11 ">
            <div class="row-fluid ">
               <div class="span6 widget-span widget-type-cell our-story-img" style="" data-widget-type="cell" data-x="0" data-w="6">
                  <div class="row-fluid-wrapper row-depth-1 row-number-12 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-custom_widget " style="" data-widget-type="custom_widget" data-x="0" data-w="12">
                           <div class="cell-wrapper layout-widget-wrapper">
                              <img src="../assets/images/about.png"alt="OASIS Work" height="515" class="abt-img">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="span6 widget-span widget-type-cell about-hlfcntnt-sec" style="height:515px" data-widget-type="cell" data-x="6" data-w="6">
                  <div class="row-fluid-wrapper row-depth-1 row-number-13 ">
                     <div class="row-fluid " style="background:#0055a5;">
                        <div class="span12 widget-span widget-type-rich_text our-story-cntnt" style="" data-widget-type="rich_text" data-x="0" data-w="12">
                           <div class="cell-wrapper layout-widget-wrapper">
                              <span id="hs_cos_wrapper_module_145569416429711412" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text" style="" data-hs-cos-general-type="widget" data-hs-cos-type="rich_text">
                                 <h2>Our Vision</h2>
                                 <p><em>To be a leading and sustainable brand of choice in the domestic and international economic sphere.</em></p>
                                 <h2>Our Mission</h2>
                                 <p><em>To operate as a dynamic group with a wide range of services and logistics to relieve people of everyday stress.</em></p>
                              </span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row-fluid-wrapper row-depth-1 row-number-14 ">
   <div class="row-fluid ">
      <div class="span12 widget-span widget-type-cell the-team-main" style="" data-widget-type="cell" data-x="0" data-w="12">
         <div class="row-fluid-wrapper row-depth-1 row-number-15 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell container" style="" data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-16 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell header" style="" data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-17 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-linked_image " style="" data-widget-type="linked_image" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_145569449651928569" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image" style="" data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/icons/icon-the-team.svg");?>" class="hs-image-widget " style="width:56px;border-width:0px;border:0px;" width="56" alt="Executive Team " title="Executive Team "></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row-fluid-wrapper row-depth-1 row-number-18 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-header " style="" data-widget-type="header" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_145569478318431428" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_header" style="" data-hs-cos-general-type="widget" data-hs-cos-type="header">
                                          <h2>The Executive Team</h2>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row-fluid-wrapper row-depth-1 row-number-19 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-widget_container " style="" data-widget-type="widget_container" data-x="0" data-w="12">
                           <span id="hs_cos_wrapper_module_145569481859232392" class="hs_cos_wrapper hs_cos_wrapper_widget_container hs_cos_wrapper_type_widget_container" style="" data-hs-cos-general-type="widget_container" data-hs-cos-type="widget_container">
                              <div id="hs_cos_wrapper_widget_1485452901356" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget" style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-thumb">
                                       <img src="<?php echo base_url("assets/images/david.jpeg");?>" width="301" height="224" alt="Lisa Headshot.jpg"  sizes="(max-width: 301px) 100vw, 301px">
                                    </div>
                                    <div class="team-member-content">
                                       <h5>Lisa Capps</h5>
									 <h6>CEO</h6>
									 <p>
										<b>David Obasa</b> is the Ceo and Managing Director of Oasis Express, He has a passion to giving every customer a unique experience with this worldclass dispatch solutions
									 </p>
                                    </div>
                                 </div>
                              </div>
                              <div id="hs_cos_wrapper_widget_1485451911165" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget" style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-thumb">
                                       <img src="<?php echo base_url("assets/images/dinah.jpeg");?>" width="301" height="224" alt="Zaz Floreani"  sizes="(max-width: 301px) 100vw, 301px">
                                    </div>
                                    <div class="team-member-content">
                                       <h5>Dinah Pricilla </h5>
									 <h6>BUSINESS DEVELOPMENT</h6>
									 <p>
										Oasis Express is a specialized courier service as well as helping you with your daily errands. We have trained rider all over the city that are always ready to serve your daily needs as well as delivering your errands order as your request may be.
									 </p>
                                    </div>
                                 </div>
                              </div>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row-fluid-wrapper row-depth-1 row-number-14 ">
   <div class="row-fluid ">
      <div class="span12 widget-span widget-type-cell the-team-main" style="" data-widget-type="cell" data-x="0" data-w="12">
         <div class="row-fluid-wrapper row-depth-1 row-number-15 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell container" style="" data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-16 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell header" style="" data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-17 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-linked_image " style="" data-widget-type="linked_image" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_145569449651928569" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image" style="" data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/icon-how-it-works_1.svg");?>" class="hs-image-widget " style="width:56px;border-width:0px;border:0px;" width="56" alt="Our Core Values" title="Executive Team "></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row-fluid-wrapper row-depth-1 row-number-18 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-header " style="" data-widget-type="header" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_145569478318431428" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_header" style="" data-hs-cos-general-type="widget" data-hs-cos-type="header">
                                          <h2>Our Core Values</h2>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row-fluid-wrapper row-depth-1 row-number-19 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-widget_container " style="" data-widget-type="widget_container" data-x="0" data-w="12">
                           <span id="hs_cos_wrapper_module_145569481859232392" class="hs_cos_wrapper hs_cos_wrapper_widget_container hs_cos_wrapper_type_widget_container" style="" data-hs-cos-general-type="widget_container" data-hs-cos-type="widget_container">
                              <div id="hs_cos_wrapper_widget_1485452901356" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget" style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<h5>Professionalism</h5>
										<p>– With our highly experienced team of managers and service professionals, we provide high standard and competitive services.</p>
                                    </div>
                                 </div>
                              </div>
                              <div id="hs_cos_wrapper_widget_1485451911165" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget" style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<h5>Ethics</h5>
										<p>– To remain in competition, we abide by business principles and conduct.</p>
                                    </div>
                                 </div>
                              </div>
							  <div id="hs_cos_wrapper_widget_1485452901356" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget" style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<h5>Customer Friendly Services</h5>
										<p>– We focus on creating a positive experience for our customers to ensure value for money.</p>
                                    </div>
                                 </div>
                              </div>
                              <div id="hs_cos_wrapper_widget_1485451911165" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget" style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<h5>Speed and Reliability</h5>
										<p>– We offer prompt and convenient services that you can always trust.</p>
                                    </div>
                                 </div>
                              </div>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="section_wrapper">
<div class="the_content_wrapper">
<div class="vc_row wpb_row vc_row-fluid">
   <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner ">
         <div class="wpb_wrapper">
            <div class="wpb_text_column wpb_content_element ">
               <div class="wpb_wrapper">
                  '
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
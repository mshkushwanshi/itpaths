<style>
	.form-group {
		margin-top: 20px;
	}
</style>
<div class="col-md-6 col-md-offset-3">
    <div id="lgnPnl">
		<h2>Login</h2>
		<div class="form-group">
			<input type="text" class="form-control" name="unm" placeholder="Enter Username" />
			<div class="help-block"></div>
		</div>
		<div class="form-group">
			<input type="password" class="form-control" name="upd" placeholder="Enter Password" />
			<div class="help-block"></div>
		</div>
		<div class="form-group">
			<button class="btn btn-primary login">Login</button>
			<button class="btn btn-primary rstr">Register</button>
			<!--<a class="btn btn-link">Register</a>-->
			<div class="help-block"></div>
		</div>
	</div>
</div>
<script>
	var lgnSbmt = function(){
		if($('input[name="unm"').val() == "")
			$($('.help-block')[0]).html('Username is required');
		else if($('input[name="upd"').val() == ""){
			$($('.help-block')[0]).html('');
			$($('.help-block')[1]).html('Password is required');
		}
		else{
			$($('.help-block')[0]).html('');
			$($('.help-block')[1]).html('');
			$($('.help-block')[2]).html('Please wait..');
			$.ajax({
			  type: 'post',
			  url: "<?php echo base_url('/user/login');?>",
			  data: {"unm": $("input[name='unm']").val(), "upd": $("input[name='upd']").val()},
			  statusCode: {
				  401:function() { $($('.help-block')[2]).html('Invalid Credentials'); }
				},
				complete: function(res, s){
							if(s == "success"){
								var resJson = JSON.parse(res.responseText);
								if(resJson.role == 2){
									localStorage.setItem("data", res.responseText);
									$.get("<?php echo base_url("/oasis/cpanel");?>",
										function(res){
											$( "#dialog" ).dialog( "close" );
											$( "#apnl" ).dialog( "open" );
											$( "#apnl" ).dialog("option", "height", $(window).height() - 20);
											$( "#apnl" ).dialog("option", "position", { my: "center", at: "center", of: window });
											$( "#apnl" ).dialog("option", "title", "Admin Panel");
											setTimeout(function(){$("#apnl-dat").parent().parent().attr("style", $("#apnl-dat").parent().parent().attr("style") + "; z-index:100000000 !important");console.log('dd');}, 1000);
											$("#apnl-dat").html(res);
											$($('.help-block')[2]).html('');
											$('div#usr-info').attr('style', 'display: block');
											$('div.lgnPnl').attr('style', 'display: none');
											$('div.admnPnl').attr('style', 'display: block');
											var logout = $('div#usr-info a');
											var name = "Hi&nbsp;" + resJson.fname + "&nbsp;" + resJson.lname + "&nbsp;&nbsp;&nbsp;";
											$($('div#usr-info div')[1]).html(name);
											$($('div#usr-info div')[1]).append(logout);
										}
									);
								}
								else{
									$($('.help-block')[2]).html('Unauthorized access');
								}
							}
							else{
								$($('.help-block')[2]).html('Invalid Credentials');
							}
						},
			  error: function(){},
			  async: false
			});
		}
	};
	$(document).ready(function(){
		if($(window).width() <= 767)
			$(".col-md-6.col-md-offset-3").css('padding-top', '100px');
		else
			$(".col-md-6.col-md-offset-3").css('padding-top', '0');
		$(window).resize(function(){
		if($(window).width() <= 767)
			$(".col-md-6.col-md-offset-3").css('padding-top', '100px');
			else
				$(".col-md-6.col-md-offset-3").css('padding-top', '0');
		});
	});
	$(window).keypress(function(e){
		if(e.keyCode == 13){
			lgnSbmt();
		}
	});
	$('.btn.btn-primary.login').click(function(){
		lgnSbmt();
	});
	$($('.lgnpnl button.btn.btn-primary')[1]).click(function(){
		window.open("<?php echo base_url('/user/register');?>", "_parent");
	});
	var rstr = function(){
		$.get("<?php echo base_url("/user/register");?>", function(res, s){
			if(s == "success"){
				$("#dialog").dialog("close");
				$("#rstr").dialog("open");
				$("#rstr-content").html(res);
				setTimeout(function(){$( "#rstr" ).dialog("option", "height", $(window).height() - 250)}, 1000);
				$("#rstr").dialog("option", "title", "Register");
			}
		});
	}
	$('.btn.btn-primary.rstr').click(function(){
		rstr();
	});
</script>
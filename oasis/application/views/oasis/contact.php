<link rel="stylesheet" href="<?php echo base_url("css/about.css");?>">
<div class="body-container-wrapper">
<div class="body-container container-fluid">
<div class="row-fluid-wrapper row-depth-1 row-number-1 ">
   <div class="row-fluid ">
      <div class="span12 widget-span widget-type-cell banner-sec contact"  data-widget-type="cell" data-x="0" data-w="12">
         <div class="row-fluid-wrapper row-depth-1 row-number-2 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell "  data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-3" style="height:561px;">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell container"  data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-4 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-cell banner-caption contact"  data-widget-type="cell" data-x="0" data-w="12">
                                    <div class="row-fluid-wrapper row-depth-2 row-number-1 ">
                                       <div class="row-fluid ">
                                          <div class="span12 widget-span widget-type-rich_text "  data-widget-type="rich_text" data-x="0" data-w="12">
                                             <div class="cell-wrapper layout-widget-wrapper">
                                                <span id="hs_cos_wrapper_module_145458317557213871" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text"  data-hs-cos-general-type="widget" data-hs-cos-type="rich_text">
                                                   <h1>Contact US</h1>
												   <h3>Fast, Prompt and Convenient</h3>
                                                </span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid-wrapper row-depth-2 row-number-3 ">
                                       <div class="row-fluid ">
                                          <div class="span12 widget-span widget-type-custom_widget "  data-widget-type="custom_widget" data-x="0" data-w="12">
                                             <div class="cell-wrapper layout-widget-wrapper">
                                                <span id="hs_cos_wrapper_module_145655185865019191" class="hs_cos_wrapper hs_cos_wrapper_widget "  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                                </span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row-fluid-wrapper row-depth-1 row-number-5 ">
   <div class="row-fluid ">
      <div class="span12 widget-span widget-type-cell cmpny-sect" style="" data-widget-type="cell" data-x="0" data-w="12">
         <div class="row-fluid-wrapper row-depth-1 row-number-6 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell container" style="" data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-7 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell the-cmpny-cntnt" style="" data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-17 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-linked_image " style="" data-widget-type="linked_image" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_145569449651928569" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image" style="" data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/icon-how-it-works_1.svg");?>" class="hs-image-widget " style="width:56px;border-width:0px;border:0px;" width="56" alt="Our Core Values" title="Executive Team "></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
						   <div class="row-fluid-wrapper row-depth-1 row-number-8 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-header " style="" data-widget-type="header" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_1492176729911542" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_header" style="" data-hs-cos-general-type="widget" data-hs-cos-type="header">
                                          <h2>Contact US</h2>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row-fluid-wrapper row-depth-1 row-number-14 ">
   <div class="row-fluid ">
      <div class="span12 widget-span widget-type-cell the-team-main" style="" data-widget-type="cell" data-x="0" data-w="12">
         <div class="row-fluid-wrapper row-depth-1 row-number-15 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell container" style="" data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-19 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-widget_container " style="" data-widget-type="widget_container" data-x="0" data-w="12">
                           <span id="hs_cos_wrapper_module_145569481859232392" class="hs_cos_wrapper hs_cos_wrapper_widget_container hs_cos_wrapper_type_widget_container" style="" data-hs-cos-general-type="widget_container" data-hs-cos-type="widget_container">
                              <div id="hs_cos_wrapper_widget_1485451911165" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget" style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<div class="wrap mcb-wrap one-third  valign-top clearfix" style="margin-top:-360px"><div class="mcb-wrap-inner"><div class="column mcb-column one column_column "><div class="column_attr clearfix" style=" background-color:#ffffff; background-image:url('<?php echo base_url("home_internet_contact2.png");?>'); background-repeat:no-repeat; background-position:center top; padding:80px 50px 65px; background-position: right 30px; box-shadow: 0px 20px 35px rgba(27,48,77,.07);"><h4><b>Oasis Express</b></h4>
										<p>25 Nii <br>
										Torgbor Avenue UPSA Road,<br>
										Accra Ghana.</p>
										<hr class="no_line" style="margin: 0 auto 20px;">
										<p style="background: url(<?php echo base_url("assets/images/home_internet_contact3.png");?>) no-repeat left center; padding-left: 40px;">phone:<br>+030 (254) 7848</p>
										<p style="background: url(<?php echo base_url("assets/images/home_internet_contact4.png");?>) no-repeat left center; padding-left: 40px;">e-mail:<br> <a href="mailto:info@oasisexpress.com.gh">info@oasisexpress.com.gh</a></p>
										<p style="background: url(<?php echo base_url("assets/images/home_internet_contact5.png");?>) no-repeat left center; padding-left: 40px;">web:<br><a href="<?php echo base_url("");?>">https://oasisexpressgh.com/</a></p></div></div></div></div>
                                    </div>
                                 </div>
                              </div>
                              <div id="hs_cos_wrapper_widget_1485451911165" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget" style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<form action="/contact/#wpcf7-f147-p13-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="147">
<input type="hidden" name="_wpcf7_version" value="5.0">
<input type="hidden" name="_wpcf7_locale" value="en_US">
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f147-p13-o1">
<input type="hidden" name="_wpcf7_container_post" value="13">
</div>
<p><label> Your Name (required)<br>
    <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </label></p>
<p><label> Your Email (required)<br>
    <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </label></p>
<p><label> Subject<br>
    <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span> </label></p>
<p><label> Your Message<br>
    <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </label></p>
<p><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit">&nbsp;&nbsp;<input type="reset" value="Clear" class="wpcf7-form-control wpcf7-submit">&nbsp;&nbsp;<span class="status"></span>&nbsp;&nbsp;<span class="ajax-loader"></span></p>
<div class="wpcf7-response-output wpcf7-display-none"></div></form>
<script>
	$(".wpcf7-form-control.wpcf7-submit[type='submit']").click(function(e){
		if($("input[name='your-name']").val() == ""){
			$(".status").html("Please enter your name");
			$(".status").attr("style", "color:red;");
			return false;
		}
		else if($("input[name='your-email']").val() == ""){
			$(".status").html("Please enter your email");
			$(".status").attr("style", "color:red;");
			return false;
		}
		else if($("input[name='your-subject']").val() == ""){
			$(".status").html("Please enter your query title");
			$(".status").attr("style", "color:red;");
			return false;
		}
		else if($("textarea[name='your-message']").val() == ""){
			$(".status").html("Please enter detail of your enquiry");
			$(".status").attr("style", "color:red;");
			return false;
		}
		else{
			$(".status").html("");
			$.ajax({url: '<?php echo base_url("oasis/send");?>', type: 'post', data: {name: $("input[name='your-name']").val(), email: $("input[name='your-email']").val(), title: $("input[name='your-subject']").val(), message: $("textarea[name='your-message']").val()}}).done(function(e){
				$("input[name='your-name']").val("");
				$("input[name='your-email']").val("");
				$("input[name='your-subject']").val("");
				$("input[name='your-message']").val("");
				$(".status").html("Thank your for enquirying us, we'll reach you soon");
				$(".status").attr("style", "color:green;");
			}).error(function(e){
				$(".status").html("Communication error, please try again after sometime");
				$(".status").attr("style", "color:red;");
			});
			return false;
		}
		return false;
	});
</script>
                                    </div>
                                 </div>
                              </div>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="section_wrapper">
<div class="the_content_wrapper">
<div class="vc_row wpb_row vc_row-fluid">
   <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner ">
         <div class="wpb_wrapper">
            <div class="wpb_text_column wpb_content_element ">
               <div class="wpb_wrapper">
                  '
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
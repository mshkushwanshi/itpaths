<div class="row-fluid ">
	 <div class="span12 widget-span widget-type-cell container"  data-widget-type="cell" data-x="0" data-w="12">
		<div class="row-fluid-wrapper row-depth-1 row-number-3 ">
		   <div class="row-fluid ">
			  <div class="span12 widget-span widget-type-cell header updated"  data-widget-type="cell" data-x="0" data-w="12">
				 <div class="row-fluid-wrapper row-depth-1 row-number-4 ">
					<div class="row-fluid ">
					   <div class="span12 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="0" data-w="12">
						  <div class="cell-wrapper layout-widget-wrapper">
							 <span id="hs_cos_wrapper_module_14553576256404" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/our-customers-icon.svg");?>" class="hs-image-widget " style="width:56px;border-width:0px;border:0px;" width="56" alt="Our Customers" title="Our Customers"></span>
						  </div>
						  <!--end layout-widget-wrapper -->
					   </div>
					   <!--end widget-span -->
					</div>
					<!--end row-->
				 </div>
				 <!--end row-wrapper -->
				 <div class="row-fluid-wrapper row-depth-1 row-number-5 ">
					<div class="row-fluid ">
					   <div class="span12 widget-span widget-type-header "  data-widget-type="header" data-x="0" data-w="12">
						  <div class="cell-wrapper layout-widget-wrapper">
							 <span id="hs_cos_wrapper_module_14553576256405" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_header"  data-hs-cos-general-type="widget" data-hs-cos-type="header">
								<h2>A Few of Our Clients</h2>
							 </span>
						  </div>
						  <!--end layout-widget-wrapper -->
					   </div>
					   <!--end widget-span -->
					</div>
					<!--end row-->
				 </div>
				 <!--end row-wrapper -->
			  </div>
			  <!--end widget-span -->
		   </div>
		   <!--end row-->
		</div>
		<div class="row-fluid-wrapper row-depth-1 row-number-6 ">
		   <div class="row-fluid ">
			  <div class="span12 widget-span widget-type-cell our-cstmr-logo"  data-widget-type="cell" data-x="0" data-w="12">
				 <div class="row-fluid-wrapper row-depth-1 row-number-7 ">
					<div class="row-fluid ">
					   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="0" data-w="2">
						  <div class="cell-wrapper layout-widget-wrapper">
							 <span id="hs_cos_wrapper_module_14553576256407" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/d-cafe.jpg");?>" class="hs-image-widget " style="width:234px;border-width:0px;border:0px;" height="100" alt="Boundless Network/Zazzle" title="D-Cafe"  sizes="(max-width: 234px) 100vw, 234px"></span>
						  </div>
					   </div>
					   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="2" data-w="2">
						  <div class="cell-wrapper layout-widget-wrapper">
							 <span id="hs_cos_wrapper_module_14553576256408" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/boutique.jpg");?>" class="hs-image-widget " style="width:197px;border-width:0px;border:0px;" height="100" alt="Whole Food Market" title="Bread Boutique"></span>
						  </div>
					   </div>
					   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="2" data-w="2">
						  <div class="cell-wrapper layout-widget-wrapper">
							 <span id="hs_cos_wrapper_module_14553576256408" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/foodrockers.jpg");?>" class="hs-image-widget " style="width:197px;border-width:0px;border:0px;" height="100" alt="Foodrockers Restraunt" title="Foodrockers Restraunt"></span>
						  </div>
					   </div>
					   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="2" data-w="2">
						  <div class="cell-wrapper layout-widget-wrapper">
							 <span id="hs_cos_wrapper_module_14553576256408" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/fish-n-fries.jpg");?>" class="hs-image-widget " style="width:197px;border-width:0px;border:0px;" height="100" alt="Fish & Fries" title="Fish & Fries"  ></span>
						  </div>
					   </div><div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="2" data-w="2">
						  <div class="cell-wrapper layout-widget-wrapper">
							 <span id="hs_cos_wrapper_module_14553576256408" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/bernie.jpeg");?>" class="hs-image-widget " style="width:197px;border-width:0px;border:0px;" height="100" alt="Bernie" title="Bernie"  ></span>
						  </div>
					   </div>
					   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="2" data-w="2">
						  <div class="cell-wrapper layout-widget-wrapper">
							 <span id="hs_cos_wrapper_module_14553576256408" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/premium-bedding.jpeg");?>" class="hs-image-widget " style="width:197px;border-width:0px;border:0px;" height="100" alt="Premium Bedding" title="Premium Bedding"  ></span>
						  </div>
					   </div>
					   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="2" data-w="2">
						  <div class="cell-wrapper layout-widget-wrapper">
							 <span id="hs_cos_wrapper_module_14553576256408" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/mover.jpeg");?>" class="hs-image-widget " style="width:197px;border-width:0px;border:0px;" height="100" alt="Okodee Mmowere" title="Okodee Mmowere"  ></span>
						  </div>
					   </div>
					   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="2" data-w="2">
						  <div class="cell-wrapper layout-widget-wrapper">
							 <span id="hs_cos_wrapper_module_14553576256408" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/the-tissue.jpeg");?>" class="hs-image-widget " style="width:197px;border-width:0px;border:0px;" height="100" alt="The Tissue Place" title="The Tissue Place"  ></span>
						  </div>
					   </div>
					   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="2" data-w="2">
						  <div class="cell-wrapper layout-widget-wrapper">
							 <span id="hs_cos_wrapper_module_14553576256408" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/yaaba.jpeg");?>" class="hs-image-widget " style="width:197px;border-width:0px;border:0px;" height="100" alt="Yaaba Naturals" title="Yaaba Naturals"  ></span>
						  </div>
					   </div>
					</div>
				 </div>
				 <div class="row-fluid-wrapper row-depth-1 row-number-8 ">
				 </div>
			  </div>
		   </div>
		</div>
	 </div>
</div>
  
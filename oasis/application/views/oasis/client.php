<div class="row-fluid ">
   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="0" data-w="2">
	  <div class="cell-wrapper layout-widget-wrapper">
		 <span id="hs_cos_wrapper_module_145535762564013" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/Andrews.png");?>" class="hs-image-widget " style="width:195px;border-width:0px;border:0px;" width="195" alt="Andrews Kurth" title="Andrews Kurth"  sizes="(max-width: 195px) 100vw, 195px"></span>
	  </div>
	  <!--end layout-widget-wrapper -->
   </div>
   <!--end widget-span -->
   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="2" data-w="2">
	  <div class="cell-wrapper layout-widget-wrapper">
		 <span id="hs_cos_wrapper_module_145535762564014" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/airbnb.png");?>" class="hs-image-widget " style="width:228px;border-width:0px;border:0px;" width="228" alt="airbnb" title="airbnb"  sizes="(max-width: 228px) 100vw, 228px"></span>
	  </div>
	  <!--end layout-widget-wrapper -->
   </div>
   <!--end widget-span -->
   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="4" data-w="2">
	  <div class="cell-wrapper layout-widget-wrapper">
		 <span id="hs_cos_wrapper_module_145535762564015" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/carepoint-logo.png");?>" class="hs-image-widget " style="width:157px;border-width:0px;border:0px;" width="157" alt="Care Point" title="Care Point"  sizes="(max-width: 157px) 100vw, 157px"></span>
	  </div>
	  <!--end layout-widget-wrapper -->
   </div>
   <!--end widget-span -->
   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="6" data-w="2">
	  <div class="cell-wrapper layout-widget-wrapper">
		 <span id="hs_cos_wrapper_module_145535762564016" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/onetouchpoint.png");?>" class="hs-image-widget " style="width:241px;border-width:0px;border:0px;" width="241" alt="One Touch Point" title="One Touch Point"  sizes="(max-width: 241px) 100vw, 241px"></span>
	  </div>
	  <!--end layout-widget-wrapper -->
   </div>
   <!--end widget-span -->
   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="8" data-w="2">
	  <div class="cell-wrapper layout-widget-wrapper">
		 <span id="hs_cos_wrapper_module_145535762564017" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/HEB logo 600px.png");?>" class="hs-image-widget " style="width:120px;border-width:0px;border:0px;" width="120" alt="HEB logo 600px.png" title="HEB logo 600px.png"  sizes="(max-width: 120px) 100vw, 120px"></span>
	  </div>
	  <!--end layout-widget-wrapper -->
   </div>
   <!--end widget-span -->
   <div class="span2 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="10" data-w="2">
	  <div class="cell-wrapper layout-widget-wrapper">
		 <span id="hs_cos_wrapper_module_145535762564018" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/cushman.png");?>" class="hs-image-widget " style="width:215px;border-width:0px;border:0px;" width="215" alt="Cushman &amp; Wakefield | Oxford Commercial" title="Cushman &amp; Wakefield | Oxford Commercial"  sizes="(max-width: 215px) 100vw, 215px"></span>
	  </div>
	  <!--end layout-widget-wrapper -->
   </div>
   <!--end widget-span -->
</div>
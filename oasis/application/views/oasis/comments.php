                              <div class="span8 widget-span widget-type-cell tstimnl-sldr"  data-widget-type="cell" data-x="4" data-w="8">
                                 <div class="row-fluid-wrapper row-depth-1 row-number-25 ">
                                    <div class="row-fluid ">
                                       <div id="slider-container" class="span12 widget-span widget-type-cell slider-container"  data-widget-type="cell" data-x="0" data-w="12">
                                          <div class="row-fluid-wrapper row-depth-2 row-number-1 ">
                                             <div class="row-fluid ">
                                                <div class="span12 widget-span widget-type-widget_container slider"  data-widget-type="widget_container" data-x="0" data-w="12">
                                                   <span id="hs_cos_wrapper_module_14552785438035504" class="hs_cos_wrapper hs_cos_wrapper_widget_container hs_cos_wrapper_type_widget_container"  data-hs-cos-general-type="widget_container" data-hs-cos-type="widget_container">
                                                      <div id="hs_cos_wrapper_widget_1472508365848" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget"  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                                         <div class="slide1">
                                                            <div class="slide-thumbnail">
                                                               <img src="<?php echo base_url("assets/images/quote-icon.svg");?>" width="115" height="115" alt="Quote ">
                                                            </div>
                                                            <div class="slide_txt">
                                                               <p><span>"I used to do 100% of all our stuff deliveries myself, and with 8-10 deliveries every day, it was becoming a full-time job. Now OASIS handles my deliveries for me, with zero customer complaints. Today I watched my son play a high school football scrimmage in the middle of the work day, which I never would have been able to do before OASIS took over my delivery orders. For the first time in almost four years I have been able to close my monthly accounting procedures on time as I actually have time to get my books up to date. OASIS has been great to work with and has given me time to focus on what matters."</span></p>
                                                               <div class="slide-name">D'Cafe</div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div id="hs_cos_wrapper_widget_1455280603733" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget"  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                                         <div class="slide1">
                                                            <div class="slide-thumbnail">
                                                               <img src="<?php echo base_url("assets/images/quote-icon.svg");?>" width="115" height="115" alt="Quote ">
                                                            </div>
                                                            <div class="slide_txt">
                                                               <p>"OASIS has continued to provide Central Market with reliable service, while exceeding our expectations in their timeliness. Their drivers are always courteous and go above and beyond the needs of our business."</p>
                                                               <div class="slide-name">Bread Boutique<span class="tslide_designation"></span></div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row-fluid-wrapper row-depth-1 row-number-1 ">
                                    <div class="row-fluid ">
                                       <div class="span12 widget-span widget-type-raw_html "  data-widget-type="raw_html" data-x="0" data-w="12">
                                          <div class="cell-wrapper layout-widget-wrapper">
                                             <span id="hs_cos_wrapper_module_14552786778357898" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_raw_html"  data-hs-cos-general-type="widget" data-hs-cos-type="raw_html">
												<script>
                                                   var target = $("#hp-slideshow").offset().top;
                                                   var interval = setInterval(function() {
                                                   if ($(window).scrollTop() >= target) {
                                                   doSomeComplicatedStuff();
                                                   clearInterval(interval);
                                                   }
                                                   }, 250);
                                                   function doSomeComplicatedStuff() {
                                                   $(".slider-container").sliderUi({
                                                   autoPlay: true,
                                                   speed: 1000,
                                                   animate: "slow",
                                                   cssEasing: "cubic-bezier(0.285, 1.015, 0.165, 1.000)"
                                                   });
                                                   $("#caption-slide").sliderUi({
                                                   caption: true
                                                   });
                                                   }
                                                </script>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
<style type="text/css">
	.ewd-otp-message.mb-6{
		margin-top:5px;
	}
	div#ewd-otp-tracking-div input {
		margin-top: 15px;
	}
	#o-stat{
		display:none;
	}
	td.hdr {
		width: 139px;
	}
	table.odata {
		margin-top: -75px;
		margin-bottom: 15px;
	}
	table.odata td {
		border-bottom: 1px solid wheat;
	}
</style>
<div class="the_content_wrapper">
   <pre><code>
<div id="ewd-otp-tracking-div-div" class="mt-12"><h3>Track an Order</h3><div class="ewd-otp-message mb-6 wordwrap">Enter the order number you would like to track in the div below.</div><div action="#" method="post" id="ewd-otp-tracking-div" class="pure-div pure-div-aligned ewd-otp-non-ajax-div"><input type="hidden" name="ewd-otp-action" value="track"><input type="hidden" id="ewd-otp-field-labels" name="field-labels" value=""><div class="pure-control-group"><label for="Order_Number" id="ewd-otp-order-number-div" class="ewd-otp-field-label ewd-otp-bold active">Order Number: </label><input type="text" class="ewd-otp-text-input" id="ewd-otp-tracking-number" name="oid" placeholder="Enter Order Number"></div><div class="pure-control-group"><label for="Submit"></label>
<input type="submit" class="ewd-otp-submit pure-button pure-button-primary" name="Login_Submit" value="Track" id="trackBtn">&nbsp;<span class="error"></span></div></div></div>
</code></pre>
<div id="o-stat">
	<div id="o-data"></div>
	<button class="bckBtn">Back</button>
</div>
</div>
<script>
	var isDraw = true;
	$(".bckBtn").click(function(){
		$("input[name='oid']").val("");
		$("#ewd-otp-tracking-div-div").show();
		$("#o-stat").hide();
		isDraw = true;
	});
	var trackOdr = function(){
		if($('#ewd-otp-tracking-number').val() == "")
			$($('.error')[0]).html('Order number is required');
		else{
			$('.error').html('Please wait..');
			$.post("<?php echo base_url('/order/track');?>",
				{"oid": $("input[name='oid']").val()},
				function(res, s){
					if(s == "success"){
						if(res != null){
							$("#ewd-otp-tracking-div-div").hide();
							$('.error').html('');
							var resJson = res;
							var data = "<table class='odata'><tr>";
								data += "<td class='hdr'>Order Number:</td><td>" + $("input[name='oid']").val() + "</td>";
								data += "</tr><tr>";
								data += "<td class='hdr'>From:</td><td>" + resJson.from + "</td>";
								data += "</tr><tr>";
								data += "<td class='hdr'>To:</td><td>" + resJson.to + "</td>";
								data += "</tr><tr>";
								data += "<td class='hdr'>Order Date:</td><td>" + resJson.order_date + "</td>";
								data += "</tr><tr>";
								data += "<td class='hdr'>Distance:</td><td>" + resJson.distance + "</td>";
								data += "</tr><tr>";
								data += "<td class='hdr'>Status:</td><td>" + resJson.status + "</td>";
							if(resJson.edata){
								data += "</tr><tr>";
								data += "<td class='hdr'>Executive Name:</td><td>" + resJson.edata.fname + " " + resJson.edata.lname + "</td>";
								data += "</tr><tr>";
								data += "<td class='hdr'>Contact Number:</td><td>" + resJson.edata.phone + "</td>";
							}
							else{
								data += "</tr><tr>";
								data += "<td colspan='2'><b><i>Executive details will appear soon.</i></b></td>";
							}
							data +="</tr></table>";
							if(isDraw){
								$("#o-data table").remove()
								$("#o-data").append(data);
								isDraw = false;
							}
							$("#o-stat").show();
						}
						else{
							$('.error').html('Invalid Order Number');
						}
					}
					else{
						$('.error').html('Communication Error');
					}
				}
			);
		}
	};
	$(document).ready(function(){
		if($(window).width() <= 767)
			$(".col-md-6.col-md-offset-3").css('padding-top', '100px');
		else
			$(".col-md-6.col-md-offset-3").css('padding-top', '0');
		$(window).resize(function(){
		if($(window).width() <= 767)
			$(".col-md-6.col-md-offset-3").css('padding-top', '100px');
			else
				$(".col-md-6.col-md-offset-3").css('padding-top', '0');
		});
	});
	$(window).keypress(function(e){
		if(e.keyCode == 13){
			trackOdr();
		}
	});
	$('#trackBtn').click(function(){
		trackOdr();
	});
</script>
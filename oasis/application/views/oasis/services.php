<div class="span12 widget-span widget-type-custom_widget "  data-widget-type="custom_widget" data-x="0" data-w="12">
 <div class="cell-wrapper layout-widget-wrapper">
	<span id="hs_cos_wrapper_module_1487257378128641" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget"  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
	   <style type="text/css">
		  .item-block {
		  transition: all .2s ease-in-out;
		  float:none;
		  }
		  .item-block {
		  background: #fff;
		  }
		  .item-block:hover {
		  background-color: rgba(128, 176, 218, 0.7);
		  }
		  .item-block:hover .item-icon { border-right: solid 1px #0055a5; }
		  a .item-block  { color:#000; }
		  @media (max-width: 767px) {
		  .anything-goes.updated .item-block {
		  margin: 0 auto 2%;
		  max-width: 400px;
		  width: 90%;
		  }
		  }
		  }
	   </style>
	   <div class="anything-goes-content">
		  <a href="">
			 <div class="item-block">
				<div class="item-icon">
				   <div class="icon">
					  <img src="<?php echo base_url("assets/images/health-icon.svg");?>" width="67" height="62" alt="health-icon.svg">
				   </div>
				   <h6>Health Care</h6>
				</div>
				<div class="item-desc">
				   <ul>
					  <li>Medications</li>
					  <li>Medical Devices</li>
					  <li>Medical Supplies</li>
				   </ul>
				</div>
			 </div>
		  </a>
		  <a href="">
			 <div class="item-block">
				<div class="item-icon">
				   <div class="icon">
					  <img src="<?php echo base_url("assets/images/food-icon.svg");?>" width="51" height="59" alt="food-icon.svg">
				   </div>
				   <h6>Food &amp; Grocery</h6>
				</div>
				<div class="item-desc">
				   <ul>
					  <li>Restaurant &amp; Catering</li>
					  <li>Bakery</li>
					  <li>Groceries</li>
				   </ul>
				</div>
			 </div>
		  </a>
		  <a href="">
			 <div class="item-block">
				<div class="item-icon">
				   <div class="icon">
					  <img src="<?php echo base_url("assets/images/icon-business.svg");?>" width="51" height="66" alt="icon-business.svg">
				   </div>
				   <h6>Business Services</h6>
				</div>
				<div class="item-desc">
				   <ul>
					  <li>Sensitive Documents</li>
					  <li>Court Filings</li>
					  <li>Bank Runs</li>
				   </ul>
				</div>
			 </div>
		  </a>
		  <a href="">
			 <div class="item-block">
				<div class="item-icon">
				   <div class="icon">
					  <img src="<?php echo base_url("assets/images/icon-commercial.svg");?>" width="65" height="65" alt="icon-commercial.svg">
				   </div>
				   <h6>Commercial &amp; Industrial</h6>
				</div>
				<div class="item-desc">
				   <ul>
					  <li>Printed Materials</li>
					  <li>Parts &amp; Equipment</li>
					  <li>Promotional Items</li>
				   </ul>
				</div>
			 </div>
		  </a>
		  <a href="">
			 <div class="item-block">
				<div class="item-icon">
				   <div class="icon">
					  <img src="<?php echo base_url("assets/images/icon-retail.svg");?>" width="67" height="59" alt="icon-retail.svg">
				   </div>
				   <h6>Retail</h6>
				</div>
				<div class="item-desc">
				   <ul>
					  <li>Floral&nbsp;</li>
					  <li>Gift Baskets&nbsp;</li>
					  <li>In-Store Pickup</li>
				   </ul>
				</div>
			 </div>
		  </a>
	   </div>
	</span>
 </div>
</div>
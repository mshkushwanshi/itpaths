      <div class="body-container-wrapper">
         <div class="body-container container-fluid">
            <div class="row-fluid-wrapper row-depth-1 row-number-1 ">
               <div class="row-fluid ">
                  <div class="span12 widget-span widget-type-cell banner-sec"  data-widget-type="cell" data-x="0" data-w="12">
					 <img src="../assets/images/b-logo3x.png" class="b-logo" />
                     <div class="row-fluid-wrapper row-depth-1 row-number-2 ">
                        <div class="row-fluid ">
                           <div class="span12 widget-span widget-type-cell "  data-widget-type="cell" data-x="0" data-w="12">
                              <div class="row-fluid-wrapper row-depth-1 row-number-3 ">
                                 <div class="row-fluid ">
                                    <div class="span12 widget-span widget-type-cell container"  data-widget-type="cell" data-x="0" data-w="12">
                                       <div class="row-fluid-wrapper row-depth-1 row-number-4 ">
                                          <div class="row-fluid ">
                                             <div class="span12 widget-span widget-type-cell banner-caption"  data-widget-type="cell" data-x="0" data-w="12">
                                                <div class="row-fluid-wrapper row-depth-2 row-number-1 ">
                                                   <div class="row-fluid ">
                                                      <div class="span12 widget-span widget-type-rich_text "  data-widget-type="rich_text" data-x="0" data-w="12">
                                                         <div class="cell-wrapper layout-widget-wrapper">
                                                            <span id="hs_cos_wrapper_module_145458317557213871" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text"  data-hs-cos-general-type="widget" data-hs-cos-type="rich_text">
                                                               <h1>Fast, Prompt and Convenient</h1>
                                                               <h5>OASIS Express is here to redefine courier services. Speed is the essence of all our services, so whenever you request a rider be rest assured it will be delivered on time every time.</h5>
                                                            </span>
                                                         </div>
                                                         <!--end layout-widget-wrapper -->
                                                      </div>
                                                      <!--end widget-span -->
                                                   </div>
                                                   <!--end row-->
                                                </div>
                                                <!--end row-wrapper -->
                                                <div class="row-fluid-wrapper row-depth-2 row-number-2 ">
                                                   <div class="row-fluid ">
                                                      <div class="span12 widget-span widget-type-cta "  data-widget-type="cta" data-x="0" data-w="12">
                                                         <div class="cell-wrapper layout-widget-wrapper">
                                                            <span id="hs_cos_wrapper_module_145466512753326166" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_cta"  data-hs-cos-general-type="widget" data-hs-cos-type="cta">
                                                               <!--HubSpot Call-to-Action Code -->
                                                               <span class="hs-cta-wrapper" id="hs-cta-wrapper-8bf44b45-946a-499e-baae-d487292c55dc">
                                                                  <span class="hs-cta-node hs-cta-8bf44b45-946a-499e-baae-d487292c55dc" id="hs-cta-8bf44b45-946a-499e-baae-d487292c55dc" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_1818776_0571f27a-af78-4320-9503-7a4f15d3adc3" class="cta_button " href="<?php echo base_url("oasis/about");?>"title="What is OASIS?">Get STARTED</a></span>
                                                               </span>
                                                               <!-- end HubSpot Call-to-Action Code -->
                                                            </span>
                                                         </div>
                                                         <!--end layout-widget-wrapper -->
                                                      </div>
                                                      <!--end widget-span -->
                                                   </div>
                                                   <!--end row-->
                                                </div>
                                                <!--end row-wrapper -->
                                                <div class="row-fluid-wrapper row-depth-2 row-number-3 ">
                                                   <div class="row-fluid ">
                                                      <div class="span12 widget-span widget-type-custom_widget "  data-widget-type="custom_widget" data-x="0" data-w="12">
                                                         <div class="cell-wrapper layout-widget-wrapper">
                                                            <span id="hs_cos_wrapper_module_145655185865019191" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget"  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                                               <style>
                                                                  .banner-sec {
                                                                  background-image: url('../assets/images/banner-main-3.jpg');
                                                                  background-size:cover;
                                                                  background-color: orange;
                                                                  background-color: orange;
                                                                  background-color: orange;
                                                                  }
                                                                  @media screen and (max-width: 568px){
                                                                  .banner-sec {
                                                                  background-image: linear-gradient(rgba(0,52,101,0.7),rgba(0,85,165,0.7)),url('./images/oasis-graphics.png');
                                                                  }
                                                                  }
                                                               </style>
                                                            </span>
                                                         </div>
                                                         <!--end layout-widget-wrapper -->
                                                      </div>
                                                      <!--end widget-span -->
                                                   </div>
                                                   <!--end row-->
                                                </div>
                                                <!--end row-wrapper -->
                                             </div>
                                             <!--end widget-span -->
                                          </div>
                                          <!--end row-->
                                       </div>
                                       <!--end row-wrapper -->
                                    </div>
                                    <!--end widget-span -->
                                 </div>
                                 <!--end row-->
                              </div>
                              <!--end row-wrapper -->
                           </div>
                           <!--end widget-span -->
                        </div>
                        <!--end row-->
                     </div>
                     <!--end row-wrapper -->
                  </div>
                  <!--end widget-span -->
               </div>
               <!--end row-->
            </div>
            <!--end row-wrapper -->
            <div class="row-fluid-wrapper row-depth-1 row-number-1 ">
               <div class="row-fluid ">
                  <div class="span12 widget-span widget-type-cell hiw"  data-widget-type="cell" data-x="0" data-w="12">
                     <div class="row-fluid-wrapper row-depth-1 row-number-2 ">
                        <div class="row-fluid ">
                           <div class="span12 widget-span widget-type-cell container"  data-widget-type="cell" data-x="0" data-w="12">
                              <div class="row-fluid-wrapper row-depth-1 row-number-3 ">
                                 <div class="row-fluid ">
                                    <div class="span12 widget-span widget-type-cell hiw-content-sec"  data-widget-type="cell" data-x="0" data-w="12">
                                       <div class="row-fluid-wrapper row-depth-1 row-number-4 ">
                                          <div class="row-fluid ">
                                             <div class="span12 widget-span widget-type-cell "  data-widget-type="cell" data-x="0" data-w="12">
                                                <div class="row-fluid-wrapper row-depth-2 row-number-1 ">
                                                   <div class="row-fluid ">
                                                      <div class="span12 widget-span widget-type-cell header hwi-header"  data-widget-type="cell" data-x="0" data-w="12">
                                                         <div class="row-fluid-wrapper row-depth-2 row-number-2 ">
                                                            <div class="row-fluid ">
                                                               <div class="span12 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="0" data-w="12">
                                                                  <div class="cell-wrapper layout-widget-wrapper">
                                                                     <span id="hs_cos_wrapper_module_145491851049915179" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/hiw.png");?>" class="hs-image-widget " style="width:56px;border-width:0px;border:0px;" width="56" alt="hiw.png" title="hiw.png"  sizes="(max-width: 56px) 100vw, 56px"></span>
                                                                  </div>
                                                                  <!--end layout-widget-wrapper -->
                                                               </div>
                                                               <!--end widget-span -->
                                                            </div>
                                                            <!--end row-->
                                                         </div>
                                                         <!--end row-wrapper -->
                                                         <div class="row-fluid-wrapper row-depth-2 row-number-3 ">
                                                            <div class="row-fluid ">
                                                               <div class="span12 widget-span widget-type-header "  data-widget-type="header" data-x="0" data-w="12">
                                                                  <div class="cell-wrapper layout-widget-wrapper">
                                                                     <span id="hs_cos_wrapper_module_145491850288914676" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_header"  data-hs-cos-general-type="widget" data-hs-cos-type="header">
                                                                        <h2>How It Works</h2>
                                                                     </span>
                                                                  </div>
                                                                  <!--end layout-widget-wrapper -->
                                                               </div>
                                                               <!--end widget-span -->
                                                            </div>
                                                            <!--end row-->
                                                         </div>
                                                         <!--end row-wrapper -->
                                                      </div>
                                                      <!--end widget-span -->
                                                   </div>
                                                   <!--end row-->
                                                </div>
                                                <!--end row-wrapper -->
                                                <div class="row-fluid-wrapper row-depth-2 row-number-4 ">
                                                   <div class="row-fluid ">
                                                      <div class="span12 widget-span widget-type-widget_container "  data-widget-type="widget_container" data-x="0" data-w="12">
                                                         <span id="hs_cos_wrapper_module_14549119720464315" class="hs_cos_wrapper hs_cos_wrapper_widget_container hs_cos_wrapper_type_widget_container"  data-hs-cos-general-type="widget_container" data-hs-cos-type="widget_container">
                                                            <div id="hs_cos_wrapper_widget_1454912110138" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget"  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                                               <div class="main-hiw">
                                                                  <div class="desc">
                                                                     <h5>Start&nbsp;a&nbsp;OASIS&nbsp;from&nbsp;anywhere</h5>
                                                                     <p>24/7, at the office, home or on the go. Get upfront pricing and arrival times.</p>
                                                                  </div>
                                                                  <div class="section-img">
                                                                     <img src="<?php echo base_url("assets/images/iso.png");?>" width="510" height="249" alt="How OASIS Works Laptop"  sizes="(max-width: 510px) 100vw, 510px">
                                                                  </div>
                                                                  <div class="section-img mobile-img"></div>
                                                               </div>
                                                            </div>
                                                            <div id="hs_cos_wrapper_widget_1454912123578" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget"  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                                               <div class="main-hiw">
                                                                  <div class="desc">
                                                                     <h5>Easy Order Estimation</h5>
                                                                     <p>You can easily estimate your order while placing. You can track order distance as well as estimated fare</p>
                                                                  </div>
                                                                  <div class="section-img">
                                                                     <img src="<?php echo base_url("assets/images/estimation.png");?>" width="318" height="379" alt="How it Works iPhone"  sizes="(max-width: 318px) 100vw, 318px">
                                                                  </div>
                                                               </div>
                                                            </div>
															<div id="hs_cos_wrapper_widget_1454912131914" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget"  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                                               <div class="main-hiw">
                                                                  <div class="desc">
                                                                     <h5>Track Order</h5>
                                                                     <p>
                                                                        Track your OASIS order anywhere and anytime. You can track your
                                                                        <g class="gr_ gr_4 gr-alert gr_spell gr_inline_cards gr_run_anim ContextualSpelling" id="4" data-gr-id="4">order</g>
                                                                        on web application or through your mobile application
                                                                     </p>
                                                                  </div>
                                                                  <div class="section-img">
                                                                     <img src="<?php echo base_url("assets/images/track-order.png");?>" width="500" height="411" alt="How OASIS Works Agent Pickup"  sizes="(max-width: 500px) 100vw, 500px">
                                                                  </div>
                                                                  <div class="section-img mobile-img">
                                                                     <img src="<?php echo base_url("assets/images/image-agent-picks-up-1.png");?>" width="306" height="321" alt="How OASIS Works Driver Arrival"  sizes="(max-width: 306px) 100vw, 306px">
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </span>
                                                      </div>
                                                      <!--end widget-span -->
                                                   </div>
                                                   <!--end row-->
                                                </div>
                                                <!--end row-wrapper -->
                                                <div class="row-fluid-wrapper row-depth-2 row-number-5 ">
                                                   <div class="row-fluid ">
                                                      <div class="span12 widget-span widget-type-cta work-cta"  data-widget-type="cta" data-x="0" data-w="12">
                                                         <div class="cell-wrapper layout-widget-wrapper">
                                                            <span id="hs_cos_wrapper_module_145491650588010628" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_cta"  data-hs-cos-general-type="widget" data-hs-cos-type="cta">
                                                               <!--HubSpot Call-to-Action Code -->
                                                               <span class="hs-cta-wrapper" id="hs-cta-wrapper-fdcd66e9-a1cf-409f-8d01-28b803aca202">
                                                                  <span class="hs-cta-node hs-cta-fdcd66e9-a1cf-409f-8d01-28b803aca202" id="hs-cta-fdcd66e9-a1cf-409f-8d01-28b803aca202" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_1818776_59339062-9d86-44d4-ad0e-dc8d38390e24" class="cta_button " href="#"title="GET STARTED">GET STARTED</a></span>
                                                               </span>
                                                               <!-- end HubSpot Call-to-Action Code -->
                                                            </span>
                                                         </div>
                                                         <!--end layout-widget-wrapper -->
                                                      </div>
                                                      <!--end widget-span -->
                                                   </div>
                                                   <!--end row-->
                                                </div>
                                                <!--end row-wrapper -->
                                             </div>
                                             <!--end widget-span -->
                                          </div>
                                          <!--end row-->
                                       </div>
                                       <!--end row-wrapper -->
                                       <div class="row-fluid-wrapper row-depth-1 row-number-1 ">
                                          <div class="row-fluid ">
                                             <div class="span12 widget-span widget-type-cell OASIS-delivery"  data-widget-type="cell" data-x="0" data-w="12">
                                                <div class="row-fluid-wrapper row-depth-2 row-number-1 ">
                                                   <div class="row-fluid ">
                                                      <div class="span12 widget-span widget-type-cell header"  data-widget-type="cell" data-x="0" data-w="12">
                                                         <div class="row-fluid-wrapper row-depth-2 row-number-2 ">
                                                            <div class="row-fluid ">
                                                               <div class="span12 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="0" data-w="12">
                                                                  <div class="cell-wrapper layout-widget-wrapper">
                                                                     <span id="hs_cos_wrapper_module_145458362254423688" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/icon-delivers.svg");?>" class="hs-image-widget " style="width:56px;border-width:0px;border:0px;" width="56" alt="OASIS Delivers More" title="OASIS Delivers More"></span>
                                                                  </div>
                                                                  <!--end layout-widget-wrapper -->
                                                               </div>
                                                               <!--end widget-span -->
                                                            </div>
                                                            <!--end row-->
                                                         </div>
                                                         <!--end row-wrapper -->
                                                         <div class="row-fluid-wrapper row-depth-2 row-number-3 ">
                                                            <div class="row-fluid ">
                                                               <div class="span12 widget-span widget-type-header "  data-widget-type="header" data-x="0" data-w="12">
                                                                  <div class="cell-wrapper layout-widget-wrapper">
                                                                     <span id="hs_cos_wrapper_module_145458367487224082" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_header"  data-hs-cos-general-type="widget" data-hs-cos-type="header">
                                                                        <h2>OASIS Delivers More</h2>
                                                                     </span>
                                                                  </div>
                                                                  <!--end layout-widget-wrapper -->
                                                               </div>
                                                               <!--end widget-span -->
                                                            </div>
                                                            <!--end row-->
                                                         </div>
                                                         <!--end row-wrapper -->
                                                      </div>
                                                      <!--end widget-span -->
                                                   </div>
                                                   <!--end row-->
                                                </div>
                                                <!--end row-wrapper -->
                                                <div class="row-fluid-wrapper row-depth-2 row-number-4 ">
                                                   <div class="row-fluid ">
                                                      <div class="span12 widget-span widget-type-custom_widget "  data-widget-type="custom_widget" data-x="0" data-w="12">
                                                         <div class="cell-wrapper layout-widget-wrapper">
                                                            <span id="hs_cos_wrapper_module_146477370990210694" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget"  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                                               <div class="drop-del-sec">
                                                                  <div class="del-block">
                                                                     <div class="del-icon">
                                                                        <img src="<?php echo base_url("assets/images/passion-icon.svg");?>" width="64" height="56" alt="passion-icon.svg");?>">
                                                                     </div>
                                                                     <div class="del-desc">
                                                                        <h6>SERVICE WITH A PASSION</h6>
                                                                        <p>24 hours a day, 7 days a week, we go the extra mile to deliver the highest level of service.</p>
                                                                     </div>
                                                                  </div>
                                                                  <div class="del-block">
                                                                     <div class="del-icon">
                                                                        <img src="<?php echo base_url("assets/images/upfront-icon.svg");?>" width="40" height="56" alt="upfront-icon.svg");?>">
                                                                     </div>
                                                                     <div class="del-desc">
                                                                        <h6>UPFRONT PRICING</h6>
                                                                        <p>Review pricing before every OASIS, with no fuel surcharges or surge pricing.</p>
                                                                     </div>
                                                                  </div>
                                                                  <div class="del-block">
                                                                     <div class="del-icon">
                                                                        <img src="<?php echo base_url("assets/images/icon-schedule.svg");?>" width="50" height="50" alt="icon-schedule.svg");?>">
                                                                     </div>
                                                                     <div class="del-desc">
                                                                        <h6>FLEXIBLE SCHEDULING</h6>
                                                                        <p>On-demand or pre-scheduled, choose from ASAP, 4-Hour, 2-Hour or All Day.</p>
                                                                     </div>
                                                                  </div>
                                                                  <div class="del-block">
                                                                     <div class="del-icon">
                                                                        <img src="<?php echo base_url("assets/images/up-to-icon.svg");?>" width="39" height="52" alt="up-to-icon.svg");?>">
                                                                     </div>
                                                                     <div class="del-desc">
                                                                        <h6>UP-TO-THE-MINUTE ETAS</h6>
                                                                        <p>We remove the guesswork with clear arrival and delivery times.</p>
                                                                     </div>
                                                                  </div>
                                                                  <div class="del-block">
                                                                     <div class="del-icon">
                                                                        <img src="<?php echo base_url("assets/images/icon-time-track.svg");?>" width="61" height="48" alt="icon-time-track.svg");?>">
                                                                     </div>
                                                                     <div class="del-desc">
                                                                        <h6>REAL-TIME TRACKING &amp; CONFIRMATIONS</h6>
                                                                        <p>
                                                                           Get 
                                                                           <g class="gr_ gr_5 gr-alert gr_gramm gr_run_anim Grammar only-ins replaceWithoutSep" id="5" data-gr-id="5">peace</g>
                                                                           of mind with live map tracking, 
                                                                           <g class="gr_ gr_6 gr-alert gr_gramm gr_run_anim Punctuation only-ins replaceWithoutSep" id="6" data-gr-id="6">confirmations</g>
                                                                           and signature verifications.
                                                                        </p>
                                                                     </div>
                                                                  </div>
                                                                  <div class="del-block">
                                                                     <div class="del-icon">
                                                                        <img src="<?php echo base_url("assets/images/security-icon.svg");?>" width="42" height="50" alt="security-icon.svg");?>">
                                                                     </div>
                                                                     <div class="del-desc">
                                                                        <h6>SECURITY &amp; RELIABILITY</h6>
                                                                        <p>Fully insured, background-checked and HIPAA-compliant drivers are at your service.</p>
                                                                     </div>
                                                                  </div>
                                                                  <div class="del-block">
                                                                     <div class="del-icon">
                                                                        <img src="<?php echo base_url("assets/images/icon-api.svg");?>" width="50" height="50" alt="icon-api.svg");?>">
                                                                     </div>
                                                                     <div class="del-desc">
                                                                        <h6>FEATURE-RICH API</h6>
                                                                        <p>Offer instant gratification with a same-day delivery option for your customers.</p>
                                                                     </div>
                                                                  </div>
                                                                  <div class="del-block">
                                                                     <div class="del-icon">
                                                                        <img src="<?php echo base_url("assets/images/fleet-icon.svg");?>" width="61" height="53" alt="fleet-icon.svg");?>">
                                                                     </div>
                                                                     <div class="del-desc">
                                                                        <h6>FLEET REPLACEMENT</h6>
                                                                        <p>Maintaining your own fleet is costly and a liability. Reduce overhead with OASIS.</p>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <!-- WDT top bar -->
                                                               <div id="wrapper">
                                                                  <div class="wrapper">
                                                                     <!-- DEMO -->
                                                                     <div class="bx-wrapper" style="max-width: 800px;">
                                                                        <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 0px;">
                                                                           <div class="testimonials-slider" style="width: 1720%; position: relative; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                                                                              <div class="slide bx-clone" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                 <div class="main-head">
                                                                                    <div class="testimonials-carousel-thumbnail">
                                                                                       <img src="<?php echo base_url("assets/images/fleet-icon.svg");?>" width="61" height="53" alt="fleet-icon.svg");?>">
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="testimonials-carousel-context">
                                                                                    <div class="testimonials-carousel-content">
                                                                                       <h6>FLEET REPLACEMENT</h6>
                                                                                       <p>Maintaining your own fleet is costly and a liability. Reduce overhead with OASIS.</p>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="slide" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                 <div class="main-head">
                                                                                    <div class="testimonials-carousel-thumbnail">
                                                                                       <img src="<?php echo base_url("assets/images/passion-icon.svg");?>" width="64" height="56" alt="passion-icon.svg");?>">
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="testimonials-carousel-context">
                                                                                    <div class="testimonials-carousel-content">
                                                                                       <h6>SERVICE WITH A PASSION</h6>
                                                                                       <p>24 hours a day, 7 days a week, we go the extra mile to deliver the highest level of service.</p>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="slide" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                 <div class="main-head">
                                                                                    <div class="testimonials-carousel-thumbnail">
                                                                                       <img src="<?php echo base_url("assets/images/upfront-icon.svg");?>" width="40" height="56" alt="upfront-icon.svg");?>">
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="testimonials-carousel-context">
                                                                                    <div class="testimonials-carousel-content">
                                                                                       <h6>UPFRONT PRICING</h6>
                                                                                       <p>Review pricing before every OASIS, with no fuel surcharges or surge pricing.</p>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="slide" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                 <div class="main-head">
                                                                                    <div class="testimonials-carousel-thumbnail">
                                                                                       <img src="<?php echo base_url("assets/images/icon-schedule.svg");?>" width="50" height="50" alt="icon-schedule.svg");?>">
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="testimonials-carousel-context">
                                                                                    <div class="testimonials-carousel-content">
                                                                                       <h6>FLEXIBLE SCHEDULING</h6>
                                                                                       <p>On-demand or pre-scheduled, choose from ASAP, 4-Hour, 2-Hour or All Day.</p>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="slide" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                 <div class="main-head">
                                                                                    <div class="testimonials-carousel-thumbnail">
                                                                                       <img src="<?php echo base_url("assets/images/up-to-icon.svg");?>" width="39" height="52" alt="up-to-icon.svg");?>">
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="testimonials-carousel-context">
                                                                                    <div class="testimonials-carousel-content">
                                                                                       <h6>UP-TO-THE-MINUTE ETAS</h6>
                                                                                       <p>We remove the guesswork with clear arrival and delivery times.</p>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="slide" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                 <div class="main-head">
                                                                                    <div class="testimonials-carousel-thumbnail">
                                                                                       <img src="<?php echo base_url("assets/images/icon-time-track.svg");?>" width="61" height="48" alt="icon-time-track.svg");?>">
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="testimonials-carousel-context">
                                                                                    <div class="testimonials-carousel-content">
                                                                                       <h6>REAL-TIME TRACKING &amp; CONFIRMATIONS</h6>
                                                                                       <p>
                                                                                          Get 
                                                                                          <g class="gr_ gr_5 gr-alert gr_gramm gr_run_anim Grammar only-ins replaceWithoutSep" id="5" data-gr-id="5">peace</g>
                                                                                          of mind with live map tracking, 
                                                                                          <g class="gr_ gr_6 gr-alert gr_gramm gr_run_anim Punctuation only-ins replaceWithoutSep" id="6" data-gr-id="6">confirmations</g>
                                                                                          and signature verifications.
                                                                                       </p>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="slide" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                 <div class="main-head">
                                                                                    <div class="testimonials-carousel-thumbnail">
                                                                                       <img src="<?php echo base_url("assets/images/security-icon.svg");?>" width="42" height="50" alt="security-icon.svg");?>">
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="testimonials-carousel-context">
                                                                                    <div class="testimonials-carousel-content">
                                                                                       <h6>SECURITY &amp; RELIABILITY</h6>
                                                                                       <p>Fully insured, background-checked and HIPAA-compliant drivers are at your service.</p>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="slide" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                 <div class="main-head">
                                                                                    <div class="testimonials-carousel-thumbnail">
                                                                                       <img src="<?php echo base_url("assets/images/icon-api.svg");?>" width="50" height="50" alt="icon-api.svg");?>">
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="testimonials-carousel-context">
                                                                                    <div class="testimonials-carousel-content">
                                                                                       <h6>FEATURE-RICH API</h6>
                                                                                       <p>Offer instant gratification with a same-day delivery option for your customers.</p>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="slide" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                 <div class="main-head">
                                                                                    <div class="testimonials-carousel-thumbnail">
                                                                                       <img src="<?php echo base_url("assets/images/fleet-icon.svg");?>" width="61" height="53" alt="fleet-icon.svg");?>">
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="testimonials-carousel-context">
                                                                                    <div class="testimonials-carousel-content">
                                                                                       <h6>FLEET REPLACEMENT</h6>
                                                                                       <p>Maintaining your own fleet is costly and a liability. Reduce overhead with OASIS.</p>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                              <div class="slide bx-clone" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                 <div class="main-head">
                                                                                    <div class="testimonials-carousel-thumbnail">
                                                                                       <img src="<?php echo base_url("assets/images/passion-icon.svg");?>" width="64" height="56" alt="passion-icon.svg");?>">
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="testimonials-carousel-context">
                                                                                    <div class="testimonials-carousel-content">
                                                                                       <h6>SERVICE WITH A PASSION</h6>
                                                                                       <p>24 hours a day, 7 days a week, we go the extra mile to deliver the highest level of service.</p>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                        <div class="bx-controls bx-has-pager bx-has-controls-direction">
                                                                           <div class="bx-pager bx-default-pager">
                                                                              <div class="bx-pager-item"><a href="#" data-slide-index="0" class="bx-pager-link active">1</a></div>
                                                                              <div class="bx-pager-item"><a href="#" data-slide-index="1" class="bx-pager-link">2</a></div>
                                                                              <div class="bx-pager-item"><a href="#" data-slide-index="2" class="bx-pager-link">3</a></div>
                                                                              <div class="bx-pager-item"><a href="#" data-slide-index="3" class="bx-pager-link">4</a></div>
                                                                              <div class="bx-pager-item"><a href="#" data-slide-index="4" class="bx-pager-link">5</a></div>
                                                                              <div class="bx-pager-item"><a href="#" data-slide-index="5" class="bx-pager-link">6</a></div>
                                                                              <div class="bx-pager-item"><a href="#" data-slide-index="6" class="bx-pager-link">7</a></div>
                                                                              <div class="bx-pager-item"><a href="#" data-slide-index="7" class="bx-pager-link">8</a></div>
                                                                           </div>
                                                                           <div class="bx-controls-direction"><a class="bx-prev" href="#">Prev</a><a class="bx-next" href="#">Next</a></div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                               <!-- END DEMO -->
                                                               <div class="clearfix"></div>
                                                            </span>
                                                         </div>
                                                      </div>
                                                      <script type="text/javascript">
                                                         $('.testimonials-slider').bxSlider({
                                                         slideWidth: 800,
                                                         minSlides: 1,
                                                         maxSlides: 1,
                                                         slideMargin: 32,
                                                         auto: false,
                                                         autoControls: true
                                                         });
                                                      </script>
                                                   </div>
                                                   <!--end layout-widget-wrapper -->
                                                </div>
                                                <!--end widget-span -->
                                             </div>
                                             <!--end row-->
                                          </div>
                                          <!--end row-wrapper -->
                                          <div class="row-fluid-wrapper row-depth-2 row-number-5 ">
                                             <div class="row-fluid ">
                                                <div class="span12 widget-span widget-type-cell feature-cntnt-section"  data-widget-type="cell" data-x="0" data-w="12">
                                                   <div class="row-fluid-wrapper row-depth-2 row-number-6 ">
                                                      <div class="row-fluid ">
                                                         <div class="span12 widget-span widget-type-cell header"  data-widget-type="cell" data-x="0" data-w="12">
                                                            <div class="row-fluid-wrapper row-depth-2 row-number-7 ">
                                                               <div class="row-fluid ">
                                                                  <div class="span12 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="0" data-w="12">
                                                                     <div class="cell-wrapper layout-widget-wrapper">
                                                                        <span id="hs_cos_wrapper_module_14647734983276429" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/icons/featured-content.svg");?>" class="hs-image-widget " style="width:56px;border-width:0px;border:0px;" width="56" alt="featured-content.svg" title="featured-content.svg"></span>
                                                                     </div>
                                                                     <!--end layout-widget-wrapper -->
                                                                  </div>
                                                                  <!--end widget-span -->
                                                               </div>
                                                               <!--end row-->
                                                            </div>
                                                            <!--end row-wrapper -->
                                                            <div class="row-fluid-wrapper row-depth-2 row-number-8 ">
                                                               <div class="row-fluid ">
                                                                  <div class="span12 widget-span widget-type-header "  data-widget-type="header" data-x="0" data-w="12">
                                                                     <div class="cell-wrapper layout-widget-wrapper">
                                                                        <span id="hs_cos_wrapper_module_14647735113827118" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_header"  data-hs-cos-general-type="widget" data-hs-cos-type="header">
                                                                           <h2>Featured Content</h2>
                                                                        </span>
                                                                     </div>
                                                                     <!--end layout-widget-wrapper -->
                                                                  </div>
                                                                  <!--end widget-span -->
                                                               </div>
                                                               <!--end row-->
                                                            </div>
                                                            <!--end row-wrapper -->
                                                         </div>
                                                         <!--end widget-span -->
                                                      </div>
                                                      <!--end row-->
                                                   </div>
                                                   <!--end row-wrapper -->
                                                   <div class="row-fluid-wrapper row-depth-2 row-number-9 ">
                                                      <div class="row-fluid ">
                                                         <div class="span12 widget-span widget-type-custom_widget "  data-widget-type="custom_widget" data-x="0" data-w="12">
                                                            <div class="cell-wrapper layout-widget-wrapper">
                                                               <span id="hs_cos_wrapper_module_14647736760339937" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget"  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                                                  <div class="drop-del-sec1">
                                                                     <div class="fetur-cntnt">
                                                                        <div class="del-icon1">
                                                                           <img src="<?php echo base_url("assets/images/OASIS-delivery-1024x1024.jpg");?>" width="320" height="437" alt="OASIS Consumer Survey Findings 2018"  sizes="(max-width: 320px) 100vw, 320px">
                                                                        </div>
                                                                        <div class="del-desc1">
                                                                           <h6 style="text-align: center;"><span>Consumers Want Faster Delivery &amp; Are Willing to Pay for It</span></h6>
                                                                           <p style="text-align: center;">
                                                                              <span>
                                                                                 <g class="gr_ gr_5 gr-alert gr_gramm gr_inline_cards gr_disable_anim_appear Punctuation only-ins replaceWithoutSep" id="5" data-gr-id="5">
                                                                                    <g class="gr_ gr_5 gr-alert gr_gramm gr_inline_cards gr_run_anim Punctuation only-ins replaceWithoutSep" id="5" data-gr-id="5">For the third year in a row</g>
                                                                                 </g>
                                                                                 we’re announcing results of our annual consumer survey, which aims to uncover shoppers’ motivations and expectations surrounding delivery. Discover the stats here.
                                                                              </span>
                                                                           </p>
                                                                           <p style="text-align: center;">
                                                                              <!--HubSpot Call-to-Action Code -->
                                                                              <span class="hs-cta-wrapper" id="hs-cta-wrapper-efcb0395-5eb0-49ad-b585-6c169bd1ee7f">
                                                                                 <span class="hs-cta-node hs-cta-efcb0395-5eb0-49ad-b585-6c169bd1ee7f" id="hs-cta-efcb0395-5eb0-49ad-b585-6c169bd1ee7f" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23" class="cta_button " href="#"title="READ MORE">READ MORE</a></span>
                                                                              </span>
                                                                              <!-- end HubSpot Call-to-Action Code -->
                                                                           </p>
                                                                        </div>
                                                                     </div>
                                                                     <div class="fetur-cntnt">
                                                                        <div class="del-icon1">
                                                                           <img src="<?php echo base_url("assets/images/Amazon - Healthcare 320x191 (feat cont home page).png");?>" width="320" height="191" alt="Pharmacy Delivery"  sizes="(max-width: 320px) 100vw, 320px">
                                                                        </div>
                                                                        <div class="del-desc1">
                                                                           <h6>How Local Pharmacies Can Compete with Amazon&nbsp;</h6>
                                                                           <p><span>Local and regional pharmacies may be wondering how they can possibly compete with Amazon’s massive resources and infrastructure. And how can they? Here are a few ways to do that.</span></p>
                                                                           <div>
                                                                              <!--HubSpot Call-to-Action Code -->
                                                                              <span class="hs-cta-wrapper" id="hs-cta-wrapper-85f02b0f-c659-44f8-a045-8d7a43752832">
                                                                                 <span class="hs-cta-node hs-cta-85f02b0f-c659-44f8-a045-8d7a43752832" id="hs-cta-85f02b0f-c659-44f8-a045-8d7a43752832" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35" class="cta_button " href="#"title="READ MORE">READ MORE</a></span>
                                                                              </span>
                                                                              <!-- end HubSpot Call-to-Action Code -->
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                     <div class="fetur-cntnt">
                                                                        <div class="del-icon1">
                                                                           <img src="<?php echo base_url("assets/images/international.jpg");?>" width="320" height="191" alt="OASIS Agents"  sizes="(max-width: 320px) 100vw, 320px">
                                                                        </div>
                                                                        <div class="del-desc1">
                                                                           <h6>Business Insider On OASIS As the Next "FedEx Killer"</h6>
                                                                           <p>Read what Business Insider has to say about how we're taking on Uber, how we're different from peer-to-peer services, and how we're quickly on our way to becoming the national brand for same-day delivery.</p>
                                                                           <p>
                                                                              <!--HubSpot Call-to-Action Code -->
                                                                              <span class="hs-cta-wrapper" id="hs-cta-wrapper-164805c8-362a-43f1-872a-a89af1ab301f">
                                                                                 <span class="hs-cta-node hs-cta-164805c8-362a-43f1-872a-a89af1ab301f" id="hs-cta-164805c8-362a-43f1-872a-a89af1ab301f" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05" class="cta_button " href="#"title="READ MORE">READ MORE</a></span>
                                                                              </span>
                                                                              <!-- end HubSpot Call-to-Action Code -->
                                                                           </p>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <!-- WDT top bar -->
                                                                  <div id="wrapper1">
                                                                     <div class="wrapper1">
                                                                        <!-- DEMO -->
                                                                        <div class="bx-wrapper" style="max-width: 800px;">
                                                                           <div class="bx-viewport" style="width: 100%; overflow: hidden; position: relative; height: 0px;">
                                                                              <div class="testimonials-slider1" style="width: 645%; position: relative; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                                                                                 <div class="slide1 bx-clone" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                    <div class="main-head1">
                                                                                       <div class="testimonials-carousel-thumbnail1">
                                                                                          <img src="<?php echo base_url("assets/images/278x166BusinessInsiderContentModule.png");?>" width="320" height="191" alt="OASIS Agents"  sizes="(max-width: 320px) 100vw, 320px">
                                                                                       </div>
                                                                                    </div>
                                                                                    <div class="testimonials-carousel-context1">
                                                                                       <div class="testimonials-carousel-content1">
                                                                                          <h6>Business Insider On OASIS As the Next "FedEx Killer"</h6>
                                                                                          <p>Read what Business Insider has to say about how we're taking on Uber, how we're different from peer-to-peer services, and how we're quickly on our way to becoming the national brand for same-day delivery.</p>
                                                                                          <p>
                                                                                             <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-164805c8-362a-43f1-872a-a89af1ab301f"><span class="hs-cta-node hs-cta-164805c8-362a-43f1-872a-a89af1ab301f" id="hs-cta-164805c8-362a-43f1-872a-a89af1ab301f" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05" class="cta_button " href="#"title="READ MORE">READ MORE</a></span></span><!-- end HubSpot Call-to-Action Code -->
                                                                                          </p>
                                                                                       </div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="slide1" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                    <div class="main-head1">
                                                                                       <div class="testimonials-carousel-thumbnail1">
                                                                                          <img src="<?php echo base_url("assets/images/OASIS-delivery-1024x1024.jpg");?>" width="320" height="437" alt="OASIS Consumer Survey Findings 2018"  sizes="(max-width: 320px) 100vw, 320px">
                                                                                       </div>
                                                                                    </div>
                                                                                    <div class="testimonials-carousel-context1">
                                                                                       <div class="testimonials-carousel-content1">
                                                                                          <h6 style="text-align: center;"><span>Consumers Want Faster Delivery &amp; Are Willing to Pay for It</span></h6>
                                                                                          <p style="text-align: center;">
                                                                                             <span>
                                                                                                <g class="gr_ gr_5 gr-alert gr_gramm gr_inline_cards gr_disable_anim_appear Punctuation only-ins replaceWithoutSep" id="5" data-gr-id="5">
                                                                                                   <g class="gr_ gr_5 gr-alert gr_gramm gr_inline_cards gr_run_anim Punctuation only-ins replaceWithoutSep" id="5" data-gr-id="5">For the third year in a row</g>
                                                                                                </g>
                                                                                                we’re announcing results of our annual consumer survey, which aims to uncover shoppers’ motivations and expectations surrounding delivery. Discover the stats here.
                                                                                             </span>
                                                                                          </p>
                                                                                          <p style="text-align: center;">
                                                                                             <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-efcb0395-5eb0-49ad-b585-6c169bd1ee7f"><span class="hs-cta-node hs-cta-efcb0395-5eb0-49ad-b585-6c169bd1ee7f" id="hs-cta-efcb0395-5eb0-49ad-b585-6c169bd1ee7f" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23" class="cta_button " href="#"title="READ MORE">READ MORE</a></span></span><!-- end HubSpot Call-to-Action Code -->
                                                                                          </p>
                                                                                       </div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="slide1" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                    <div class="main-head1">
                                                                                       <div class="testimonials-carousel-thumbnail1">
                                                                                          <img src="<?php echo base_url("assets/images/Amazon - Healthcare 320x191 (feat cont home page).png");?>" width="320" height="191" alt="Pharmacy Delivery"  sizes="(max-width: 320px) 100vw, 320px">
                                                                                       </div>
                                                                                    </div>
                                                                                    <div class="testimonials-carousel-context1">
                                                                                       <div class="testimonials-carousel-content1">
                                                                                          <h6>How Local Pharmacies Can Compete with Amazon&nbsp;</h6>
                                                                                          <p><span>Local and regional pharmacies may be wondering how they can possibly compete with Amazon’s massive resources and infrastructure. And how can they? Here are a few ways to do that.</span></p>
                                                                                          <div>
                                                                                             <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-85f02b0f-c659-44f8-a045-8d7a43752832"><span class="hs-cta-node hs-cta-85f02b0f-c659-44f8-a045-8d7a43752832" id="hs-cta-85f02b0f-c659-44f8-a045-8d7a43752832" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_1818776_4623335b-5beb-4661-b497-b3b3b981bc35" class="cta_button " href="#"title="READ MORE">READ MORE</a></span></span><!-- end HubSpot Call-to-Action Code -->
                                                                                          </div>
                                                                                       </div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="slide1" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                    <div class="main-head1">
                                                                                       <div class="testimonials-carousel-thumbnail1">
                                                                                          <img src="<?php echo base_url("assets/images/278x166BusinessInsiderContentModule.png");?>" width="320" height="191" alt="OASIS Agents"  sizes="(max-width: 320px) 100vw, 320px">
                                                                                       </div>
                                                                                    </div>
                                                                                    <div class="testimonials-carousel-context1">
                                                                                       <div class="testimonials-carousel-content1">
                                                                                          <h6>Business Insider On OASIS As the Next "FedEx Killer"</h6>
                                                                                          <p>Read what Business Insider has to say about how we're taking on Uber, how we're different from peer-to-peer services, and how we're quickly on our way to becoming the national brand for same-day delivery.</p>
                                                                                          <p>
                                                                                             <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-164805c8-362a-43f1-872a-a89af1ab301f"><span class="hs-cta-node hs-cta-164805c8-362a-43f1-872a-a89af1ab301f" id="hs-cta-164805c8-362a-43f1-872a-a89af1ab301f" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_1818776_bf8784ee-68f5-4f6b-a1a4-cdcbc6c91d05" class="cta_button " href="#"title="READ MORE">READ MORE</a></span></span><!-- end HubSpot Call-to-Action Code -->
                                                                                          </p>
                                                                                       </div>
                                                                                    </div>
                                                                                 </div>
                                                                                 <div class="slide1 bx-clone" style="float: left; list-style: none; position: relative; width: 100px; margin-right: 32px;">
                                                                                    <div class="main-head1">
                                                                                       <div class="testimonials-carousel-thumbnail1">
                                                                                          <img src="<?php echo base_url("assets/images/OASIS-delivery-1024x1024.jpg");?>" width="320" height="437" alt="OASIS Consumer Survey Findings 2018"  sizes="(max-width: 320px) 100vw, 320px">
                                                                                       </div>
                                                                                    </div>
                                                                                    <div class="testimonials-carousel-context1">
                                                                                       <div class="testimonials-carousel-content1">
                                                                                          <h6 style="text-align: center;"><span>Consumers Want Faster Delivery &amp; Are Willing to Pay for It</span></h6>
                                                                                          <p style="text-align: center;">
                                                                                             <span>
                                                                                                <g class="gr_ gr_5 gr-alert gr_gramm gr_inline_cards gr_disable_anim_appear Punctuation only-ins replaceWithoutSep" id="5" data-gr-id="5">
                                                                                                   <g class="gr_ gr_5 gr-alert gr_gramm gr_inline_cards gr_run_anim Punctuation only-ins replaceWithoutSep" id="5" data-gr-id="5">For the third year in a row</g>
                                                                                                </g>
                                                                                                we’re announcing results of our annual consumer survey, which aims to uncover shoppers’ motivations and expectations surrounding delivery. Discover the stats here.
                                                                                             </span>
                                                                                          </p>
                                                                                          <p style="text-align: center;">
                                                                                             <!--HubSpot Call-to-Action Code --><span class="hs-cta-wrapper" id="hs-cta-wrapper-efcb0395-5eb0-49ad-b585-6c169bd1ee7f"><span class="hs-cta-node hs-cta-efcb0395-5eb0-49ad-b585-6c169bd1ee7f" id="hs-cta-efcb0395-5eb0-49ad-b585-6c169bd1ee7f" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_1818776_a81727ba-c01a-414a-a2a7-c971c1654b23" class="cta_button " href="#"title="READ MORE">READ MORE</a></span></span><!-- end HubSpot Call-to-Action Code -->
                                                                                          </p>
                                                                                       </div>
                                                                                    </div>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="bx-controls bx-has-pager bx-has-controls-direction">
                                                                              <div class="bx-pager bx-default-pager">
                                                                                 <div class="bx-pager-item"><a href="#" data-slide-index="0" class="bx-pager-link active">1</a></div>
                                                                                 <div class="bx-pager-item"><a href="#" data-slide-index="1" class="bx-pager-link">2</a></div>
                                                                                 <div class="bx-pager-item"><a href="#" data-slide-index="2" class="bx-pager-link">3</a></div>
                                                                              </div>
                                                                              <div class="bx-controls-direction"><a class="bx-prev" href="#">Prev</a><a class="bx-next" href="#">Next</a></div>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </div>
                                                                  <!-- END DEMO -->
                                                                  <div class="clearfix"></div>
                                                               </span>
                                                            </div>
                                                         </div>
														 <style>
															div.del-icon1 img {
																height: 137px !important;
															}
														 </style>
                                                         <script type="text/javascript">
                                                            $('.testimonials-slider1').bxSlider({
                                                            slideWidth: 800,
                                                            minSlides: 1,
                                                            maxSlides: 1,
                                                            slideMargin: 32,
                                                            auto: false,
                                                            autoControls: true
                                                            });
                                                         </script>
                                                      </div>
                                                   </div>
                                                   <!--end widget-span -->
                                                </div>
                                                <!--end row-->
                                             </div>
                                             <!--end row-wrapper -->
                                          </div>
                                          <!--end widget-span -->
                                       </div>
                                       <!--end row-->
                                    </div>
                                    <!--end row-wrapper -->
                                 </div>
                                 <!--end widget-span -->
                              </div>
                              <!--end row-->
                           </div>
                           <!--end row-wrapper -->
                        </div>
                        <!--end widget-span -->
                     </div>
                     <!--end row-->
                  </div>
                  <!--end row-wrapper -->
               </div>
               <!--end widget-span -->
            </div>
            <!--end row-->
         </div>
         <!--end row-wrapper -->
      </div>
      <!--end widget-span -->
      <!--end row-->
      <!--end row-wrapper -->
      <div class="row-fluid-wrapper row-depth-1 row-number-1 ">
         <div class="row-fluid ">
            <div class="span12 widget-span widget-type-cell our-cities"  data-widget-type="cell" data-x="0" data-w="12">
               <div class="row-fluid-wrapper row-depth-1 row-number-2 ">
                  <div class="row-fluid ">
                     <!-- Our Cities -->
					 <?php include "our-cities.php"; ?>
                     <!--end widget-span -->
                  </div>
                  <!--end row-->
               </div>
               <!--end row-wrapper -->
            </div>
            <!--end widget-span -->
         </div>
         <!--end row-->
      </div>
      <!--end row-wrapper -->
      <div class="row-fluid-wrapper row-depth-1 row-number-8 ">
         <div class="row-fluid ">
            <div class="span12 widget-span widget-type-cell OASIS-exp"  data-widget-type="cell" data-x="0" data-w="12">
               <div class="row-fluid-wrapper row-depth-1 row-number-9 ">
                  <div class="row-fluid ">
                     <div class="span12 widget-span widget-type-cell video-caption"  data-widget-type="cell" data-x="0" data-w="12">
                        <div class="row-fluid-wrapper row-depth-1 row-number-10 ">
                           <div class="row-fluid ">
                              <div class="span12 widget-span widget-type-cell container"  data-widget-type="cell" data-x="0" data-w="12">
                                 <div class="row-fluid-wrapper row-depth-1 row-number-11 ">
                                    <div class="row-fluid ">
                                       <div class="span12 widget-span widget-type-rich_text "  data-widget-type="rich_text" data-x="0" data-w="12">
                                          <div class="cell-wrapper layout-widget-wrapper">
                                             <span id="hs_cos_wrapper_module_145458453930139786" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text"  data-hs-cos-general-type="widget" data-hs-cos-type="rich_text">
                                                <h2>The OASIS Experience</h2>
                                                <p>Find out what makes us the best same-day delivery solution for your business.&nbsp;</p>
                                             </span>
                                          </div>
                                          <!--end layout-widget-wrapper -->
                                       </div>
                                       <!--end widget-span -->
                                    </div>
                                    <!--end row-->
                                 </div>
                                 <!--end row-wrapper -->
                                 <?php include 'player.php'; ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row-fluid-wrapper row-depth-1 row-number-13 ">
         <div class="row-fluid ">
            <div class="span12 widget-span widget-type-cell anything-goes updated"  data-widget-type="cell" data-x="0" data-w="12">
               <div class="row-fluid-wrapper row-depth-1 row-number-14 ">
                  <div class="row-fluid ">
                     <div class="span12 widget-span widget-type-cell container"  data-widget-type="cell" data-x="0" data-w="12">
                        <div class="row-fluid-wrapper row-depth-1 row-number-15 ">
                           <div class="row-fluid ">
                              <div class="span12 widget-span widget-type-cell header"  data-widget-type="cell" data-x="0" data-w="12">
                                 <div class="row-fluid-wrapper row-depth-1 row-number-16 ">
                                    <div class="row-fluid ">
                                       <div class="span12 widget-span widget-type-linked_image "  data-widget-type="linked_image" data-x="0" data-w="12">
                                          <div class="cell-wrapper layout-widget-wrapper">
                                             <span id="hs_cos_wrapper_module_145458521447054701" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/anyhting-goes-icon.svg");?>" class="hs-image-widget " style="width:56px;border-width:0px;border:0px;" width="56" alt="Anything Goes" title="Anything Goes"></span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row-fluid-wrapper row-depth-1 row-number-17 ">
                                    <div class="row-fluid ">
                                       <div class="span12 widget-span widget-type-rich_text "  data-widget-type="rich_text" data-x="0" data-w="12">
                                          <div class="cell-wrapper layout-widget-wrapper">
                                             <span id="hs_cos_wrapper_module_145467276720734373" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text"  data-hs-cos-general-type="widget" data-hs-cos-type="rich_text">
                                                <h2>Anything Goes</h2>
                                                <p>OASIS serves&nbsp;businesses in a wide variety of industries. &nbsp;Here are just a few examples of items we can deliver for you.</p>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="row-fluid-wrapper row-depth-1 row-number-18 ">
                           <div class="row-fluid ">
                              <?php include 'services.php'; ?>
                           </div>
                        </div>
                        <div class="row-fluid-wrapper row-depth-1 row-number-19 ">
                           <div class="row-fluid ">
                              <div id="hp-slideshow" class="span12 widget-span widget-type-cell " style="height:0px !important;" data-widget-type="cell" data-x="0" data-w="12"></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row-fluid-wrapper row-depth-1 row-number-20 ">
         <div class="row-fluid ">
            <div class="span12 widget-span widget-type-cell what-they-re-saying home-what-they-saying"  data-widget-type="cell" data-x="0" data-w="12">
               <div class="row-fluid-wrapper row-depth-1 row-number-21 ">
                  <div class="row-fluid ">
                     <div class="span12 widget-span widget-type-cell container"  data-widget-type="cell" data-x="0" data-w="12">
                        <div class="row-fluid-wrapper row-depth-1 row-number-22 ">
                           <div class="row-fluid ">
                              <div class="span4 widget-span widget-type-cell text-and-cta"  data-widget-type="cell" data-x="0" data-w="4">
                                 <div class="row-fluid-wrapper row-depth-1 row-number-23 ">
                                    <div class="row-fluid ">
                                       <div class="span12 widget-span widget-type-header "  data-widget-type="header" data-x="0" data-w="12">
                                          <div class="cell-wrapper layout-widget-wrapper">
                                             <span id="hs_cos_wrapper_module_145458539626461870" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_header"  data-hs-cos-general-type="widget" data-hs-cos-type="header">
                                                <h2>What Our Clients Are Saying</h2>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row-fluid-wrapper row-depth-1 row-number-24 ">
                                    <div class="row-fluid ">
                                       <div class="span12 widget-span widget-type-cta "  data-widget-type="cta" data-x="0" data-w="12">
                                          <div class="cell-wrapper layout-widget-wrapper">
                                             <span id="hs_cos_wrapper_module_145458542510662798" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_cta"  data-hs-cos-general-type="widget" data-hs-cos-type="cta">
                                                <span class="hs-cta-wrapper" id="hs-cta-wrapper-dcf6b59c-0dc4-4ca5-a88d-d4951754697a">
                                                   <span class="hs-cta-node hs-cta-dcf6b59c-0dc4-4ca5-a88d-d4951754697a" id="hs-cta-dcf6b59c-0dc4-4ca5-a88d-d4951754697a" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_1818776_049eb5a3-9d0f-439e-a3d1-7ba1f3ff1d02" class="cta_button " href="#"title="VIEW TESTIMONIALS">VIEW TESTIMONIALS</a></span>
                                                </span>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
							<?php include 'comments.php'; ?>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row-fluid-wrapper row-depth-1 row-number-2 ">
         <div class="row-fluid ">
            <div class="span12 widget-span widget-type-global_group "  data-widget-type="global_group" data-x="0" data-w="12">
               <div class="" data-global-widget-path="generated_global_groups/3878892628.html">
                  <div class="row-fluid-wrapper row-depth-1 row-number-1 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell our-customer-sec"  data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-2 ">
                              <?php include "our-clients.php" ?>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!--end row-->
      </div>

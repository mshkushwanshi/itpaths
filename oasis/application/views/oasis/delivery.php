<link rel="stylesheet" href="<?php echo base_url("css/about.css");?>">
<div class="body-container-wrapper">
<div class="body-container container-fluid">
<div class="row-fluid-wrapper row-depth-1 row-number-1 ">
   <div class="row-fluid ">
      <div class="span12 widget-span widget-type-cell banner-sec delivery"  data-widget-type="cell" data-x="0" data-w="12">
         <div class="row-fluid-wrapper row-depth-1 row-number-2 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell "  data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-3" style="height:561px;">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell container"  data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-4 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-cell banner-caption delivery"  data-widget-type="cell" data-x="0" data-w="12">
                                    <div class="row-fluid-wrapper row-depth-2 row-number-1 ">
                                       <div class="row-fluid ">
                                          <div class="span12 widget-span widget-type-rich_text "  data-widget-type="rich_text" data-x="0" data-w="12">
                                             <div class="cell-wrapper layout-widget-wrapper">
                                                <span id="hs_cos_wrapper_module_145458317557213871" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text"  data-hs-cos-general-type="widget" data-hs-cos-type="rich_text">
                                                   <h1>Flexible Delivery</h1>
												   <h3>Fast, Prompt and Convenient</h3>
                                                </span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid-wrapper row-depth-2 row-number-3 ">
                                       <div class="row-fluid ">
                                          <div class="span12 widget-span widget-type-custom_widget "  data-widget-type="custom_widget" data-x="0" data-w="12">
                                             <div class="cell-wrapper layout-widget-wrapper">
                                                <span id="hs_cos_wrapper_module_145655185865019191" class="hs_cos_wrapper hs_cos_wrapper_widget "  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                                </span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row-fluid-wrapper row-depth-1 row-number-5 ">
   <div class="row-fluid ">
      <div class="span12 widget-span widget-type-cell cmpny-sect" style="" data-widget-type="cell" data-x="0" data-w="12">
         <div class="row-fluid-wrapper row-depth-1 row-number-6 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell container" style="" data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-7 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell the-cmpny-cntnt" style="" data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-17 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-linked_image " style="" data-widget-type="linked_image" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_145569449651928569" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image" style="" data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/icon-how-it-works_1.svg");?>" class="hs-image-widget " style="width:56px;border-width:0px;border:0px;" width="56" alt="Our Core Values" title="Executive Team "></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
						   <div class="row-fluid-wrapper row-depth-1 row-number-8 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-header " style="" data-widget-type="header" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_1492176729911542" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_header" style="" data-hs-cos-general-type="widget" data-hs-cos-type="header">
                                          <h2>Our Flexible Delivery</h2>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row-fluid-wrapper row-depth-1 row-number-9 ">
							  <div class="row-fluid ">
								 <div class="span12 widget-span widget-type-rich_text " style="" data-widget-type="rich_text" data-x="0" data-w="12">
									<div class="cell-wrapper layout-widget-wrapper">
									   <span id="hs_cos_wrapper_module_1492176733151544" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text" style="" data-hs-cos-general-type="widget" data-hs-cos-type="rich_text">
										  <p>Our service packages are specifically designed to meet each of our customer’s unique service and daily operational requirements. Oasis Express coordinates same day and other time critical deliveries on a daily basis. We provide a full range of services that allow you to meet your time-critical delivery needs on either a pre-scheduled, routine basis or in extreme priority situations.
											 &nbsp;
										  </p>
									   </span>
									</div>
								 </div>
							  </div>
						   </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row-fluid-wrapper row-depth-1 row-number-14 ">
   <div class="row-fluid ">
      <div class="span12 widget-span widget-type-cell the-team-main" style="" data-widget-type="cell" data-x="0" data-w="12">
         <div class="row-fluid-wrapper row-depth-1 row-number-15 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell container" style="" data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-16 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell header" style="" data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-17 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-linked_image " style="" data-widget-type="linked_image" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_145569449651928569" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image" style="" data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/icon-how-it-works_1.svg");?>" class="hs-image-widget " style="width:56px;border-width:0px;border:0px;" width="56" alt="Our Core Values" title="Executive Team "></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row-fluid-wrapper row-depth-1 row-number-18 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-header " style="" data-widget-type="header" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_145569478318431428" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_header" style="" data-hs-cos-general-type="widget" data-hs-cos-type="header">
                                          <h2>Our Services</h2>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
				  <div class="row-fluid-wrapper row-depth-1 row-number-19 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-widget_container " style="" data-widget-type="widget_container" data-x="0" data-w="12">
                           <span id="hs_cos_wrapper_module_145569481859232392" class="hs_cos_wrapper hs_cos_wrapper_widget_container hs_cos_wrapper_type_widget_container" style="" data-hs-cos-general-type="widget_container" data-hs-cos-type="widget_container">
                              <div id="hs_cos_wrapper_widget_1485451911165" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget" style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<h5>Same Day Service</h5>
										<p>– If your package is ready and called in to us prior to noon, Monday through Saturday, we'll deliver it by 5:00PM to your desired locations.</p>
                                    </div>
                                 </div>
                              </div>
                              <div id="hs_cos_wrapper_widget_1485451911165" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget" style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<h5>Next Day Service</h5>
										<p>– If your package is ready and called into our representative after 1pm, we'll deliver it within 24hrs to your desired locations.</p>
                                    </div>
                                 </div>
                              </div>
							  <div id="hs_cos_wrapper_widget_1485451911165" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget" style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<h5>Express Service</h5>
										<p>– We will complete the delivery of your packages throughout the Greater Accra within 1 hour of your request.</p>
                                    </div>
                                 </div>
                              </div>
                              <div id="hs_cos_wrapper_widget_1485451911165" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget" style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<h5>Account Service</h5>
										<p>– We provide routine services for corporates, institutions and SMEs requiring an organized delivery network. Services are charged per month.</p>
                                    </div>
                                 </div>
                              </div>
							  <div id="hs_cos_wrapper_widget_1485451911165" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_custom_widget" style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<h5>Non-Account Service</h5>
										<p>– For individual customers who do not have pre-scheduled account but have the need for pick-up and delivery services. Services are charged per each delivery.</p>
                                    </div>
                                 </div>
                              </div>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="section_wrapper">
<div class="the_content_wrapper">
<div class="vc_row wpb_row vc_row-fluid">
   <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner ">
         <div class="wpb_wrapper">
            <div class="wpb_text_column wpb_content_element ">
               <div class="wpb_wrapper">
                  '
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
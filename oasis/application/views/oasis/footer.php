		<div class="footer-container-wrapper">
			 <div class="footer-container container-fluid">
				<div class="row-fluid-wrapper row-depth-1 row-number-1 ">
				   <div class="row-fluid ">
					  <div class="span12 widget-span widget-type-global_group "  data-widget-type="global_group" data-x="0" data-w="12">
						 <div class="" data-global-widget-path="generated_global_groups/3997129845.html">
							<div class="row-fluid-wrapper row-depth-1 row-number-1 ">
							   <div class="row-fluid ">
								  <div class="span12 widget-span widget-type-cell footer-sec"  data-widget-type="cell" data-x="0" data-w="12">
									 <div class="row-fluid-wrapper row-depth-1 row-number-2 ">
										<div class="row-fluid ">
										   <div class="span12 widget-span widget-type-cell ftr-upr-section"  data-widget-type="cell" data-x="0" data-w="12">
											  <div class="row-fluid-wrapper row-depth-1 row-number-3 ">
												 <div class="row-fluid ">
													<div class="span12 widget-span widget-type-cell global-container"  data-widget-type="cell" data-x="0" data-w="12">
													   <div class="row-fluid-wrapper row-depth-1 row-number-4 ">
														  <div class="row-fluid ">
															 <div class="span2 widget-span widget-type-linked_image footer-logo"  data-widget-type="linked_image" data-x="0" data-w="2">
																<div class="cell-wrapper layout-widget-wrapper">
																   <span id="hs_cos_wrapper_module_145466026431913" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image"  data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/o-logo3x.png");?>" class="hs-image-widget " style="border-width:0px;border:0px;" width="55" alt="OASIS Express" title="OASIS Express"  sizes="(max-width: 55px) 100vw, 55px"></span>
																</div>
															 </div>
															 <div class="span10 widget-span widget-type-simple_menu ftr-nav"  data-widget-type="simple_menu" data-x="2" data-w="10">
																<div class="cell-wrapper layout-widget-wrapper">
																   <span id="hs_cos_wrapper_module_145466026431914" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_simple_menu"  data-hs-cos-general-type="widget" data-hs-cos-type="simple_menu">
																	  <div id="hs_menu_wrapper_module_145466026431914" class="hs-menu-wrapper active-branch flyouts hs-menu-flow-horizontal" role="navigation" data-sitemap-name="">
																		 <ul>
																			<li class="hs-menu-item hs-menu-depth-1"><a href="" target="_self">Help</a></li>
																			<li class="hs-menu-item hs-menu-depth-1"><a href="" target="_self">About Us</a></li>
																			<li class="hs-menu-item hs-menu-depth-1"><a href="" target="_self">Become a Driver</a></li>
																		 </ul>
																	  </div>
																   </span>
																</div>
															 </div>
														  </div>
													   </div>
													</div>
												 </div>
											  </div>
										   </div>
										</div>
									 </div>
									 <div class="row-fluid-wrapper row-depth-1 row-number-5 ">
										<div class="row-fluid ">
										   <div class="span12 widget-span widget-type-cell global-container"  data-widget-type="cell" data-x="0" data-w="12">
											  <div class="row-fluid-wrapper row-depth-1 row-number-6 ">
												 <div class="row-fluid ">
													<div class="span12 widget-span widget-type-cell "  data-widget-type="cell" data-x="0" data-w="12">
													   <div class="row-fluid-wrapper row-depth-1 row-number-7 ">
														  <div class="row-fluid ">
															 <div class="span12 widget-span widget-type-cell ftr-lwr-section"  data-widget-type="cell" data-x="0" data-w="12">
																<div class="row-fluid-wrapper row-depth-2 row-number-1 ">
																   <div class="row-fluid ">
																	  <div class="span4 widget-span widget-type-cell ftr-content-sec"  data-widget-type="cell" data-x="0" data-w="4">
																		 <div class="row-fluid-wrapper row-depth-2 row-number-2 ">
																			<div class="row-fluid ">
																			   <div class="span12 widget-span widget-type-rich_text copyright-section"  data-widget-type="rich_text" data-x="0" data-w="12">
																				  <div class="cell-wrapper layout-widget-wrapper">
																					 <span id="hs_cos_wrapper_module_145466026432016" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text"  data-hs-cos-general-type="widget" data-hs-cos-type="rich_text">©2018 OASIS, Inc. <a href="">Terms</a>&nbsp;| <a href="">Privacy</a></span>
																				  </div>
																			   </div>
																			</div>
																		 </div>
																		 <div class="row-fluid-wrapper row-depth-2 row-number-3 ">
																			<div class="row-fluid ">
																			   <div class="span12 widget-span widget-type-rich_text mobile-content"  data-widget-type="rich_text" data-x="0" data-w="12">
																				  <div class="cell-wrapper layout-widget-wrapper">
																					 <span id="hs_cos_wrapper_module_145503121347136631" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text"  data-hs-cos-general-type="widget" data-hs-cos-type="rich_text">
																						<p>©2018 OASIS, Inc. <a href="">Terms</a>&nbsp;| <a href="">Privacy</a></p>
																					 </span>
																				  </div>
																			   </div>
																			</div>
																		 </div>
																	  </div>
																	  <div class="span3 widget-span widget-type-cell ftr-bbb-logo"  data-widget-type="cell" data-x="4" data-w="3">
																		 <div class="row-fluid-wrapper row-depth-2 row-number-4 ">
																			<div class="row-fluid ">
																			</div>
																		 </div>
																	  </div>
																	  <div class="span5 widget-span widget-type-cell social-and-call-section"  data-widget-type="cell" data-x="7" data-w="5">
																		 <div class="row-fluid-wrapper row-depth-2 row-number-5 ">
																			<div class="row-fluid ">
																			   <div class="span6 widget-span widget-type-raw_html social-sharing"  data-widget-type="raw_html" data-x="0" data-w="6">
																				  <div class="cell-wrapper layout-widget-wrapper">
																					 <span id="hs_cos_wrapper_module_145466026432017" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_raw_html"  data-hs-cos-general-type="widget" data-hs-cos-type="raw_html">
																						<ul>
																						   <li><a href="" target="_blank"><img src="<?php echo base_url("assets/images/yelp.svg");?>" alt="yelp"></a></li>
																						   <li><a href="" target="_blank"><img src="<?php echo base_url("assets/images/facebook.svg");?>" alt="Facebook"></a></li>
																						   <li><a href="" target="_blank"><img src="<?php echo base_url("assets/images/twitter.svg");?>" alt="Twitter"></a></li>
																						   <li><a href="" target="_blank"><img src="<?php echo base_url("assets/images/linked-in.svg");?>" alt="Linkedin"></a></li>
																						   <li><a href="" target="_blank"><img src="<?php echo base_url("assets/images/google.svg");?>" alt="Google"></a></li>
																						</ul>
																					 </span>
																				  </div>
																			   </div>
																			   <div class="span6 widget-span widget-type-cta footer-cta"  data-widget-type="cta" data-x="6" data-w="6">
																				  <div class="cell-wrapper layout-widget-wrapper">
																					 <span id="hs_cos_wrapper_module_14546680548972980" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_cta"  data-hs-cos-general-type="widget" data-hs-cos-type="cta">
																						<span class="hs-cta-wrapper" id="hs-cta-wrapper-cf38c5ba-fd04-47ba-98dc-cdea269db0dc">
																						   <span class="hs-cta-node hs-cta-cf38c5ba-fd04-47ba-98dc-cdea269db0dc" id="hs-cta-cf38c5ba-fd04-47ba-98dc-cdea269db0dc" style="visibility: visible;" data-hs-drop="true"><a id="cta_button_1818776_f814ad78-316a-4fe5-a84b-8be22084443c" class="cta_button " href=""title="+233 (0) 545 413 846-OASIS"><img src="<?php echo base_url("assets/images/telephone-ico.svg");?>"><span>+233 (0) 545 413 846</span></a></span>
																						</span>
																					</span>
																				  </div>
																			   </div>
																			</div>
																		 </div>
																	  </div>
																   </div>
																</div>
																<div class="row-fluid-wrapper row-depth-2 row-number-6 ">
																   <div class="row-fluid ">
																	  <div class="span12 widget-span widget-type-rich_text mobile-content"  data-widget-type="rich_text" data-x="0" data-w="12">
																		 <div class="cell-wrapper layout-widget-wrapper">
																			<span id="hs_cos_wrapper_module_145503135252937723" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text"  data-hs-cos-general-type="widget" data-hs-cos-type="rich_text"></span>
																		 </div>
																	  </div>
																   </div>
																</div>
															 </div>
														  </div>
													   </div>
													</div>
												 </div>
											  </div>
										   </div>
										</div>
									 </div>
								  </div>
							   </div>
							</div>
							<div class="row-fluid-wrapper row-depth-1 row-number-1 ">
							   <div class="row-fluid ">
								  <div class="span12 widget-span widget-type-raw_html "  data-widget-type="raw_html" data-x="0" data-w="12">
									 <div class="cell-wrapper layout-widget-wrapper">
										<span id="hs_cos_wrapper_module_1508903132082103" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_raw_html"  data-hs-cos-general-type="widget" data-hs-cos-type="raw_html">
										</span>
									 </div>
								  </div>
							   </div>
							</div>
						 </div>
					  </div>
				   </div>
				</div>
			 </div>
			</div>
		<!--<script type="text/javascript" src="<?php echo base_url("js/public_common.js");?>"></script>-->
		<link rel="stylesheet" id="wistia_popover_css" href="<?php echo base_url("css/jquery.fancybox.css");?>">
	</div>
  </body>
</html>
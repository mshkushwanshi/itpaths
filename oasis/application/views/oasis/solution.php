<link rel="stylesheet" href="<?php echo base_url("css/about.css");?>">
<div class="body-container-wrapper">
<div class="body-container container-fluid">
<div class="row-fluid-wrapper row-depth-1 row-number-1 ">
   <div class="row-fluid ">
      <div class="span12 widget-span widget-type-cell banner-sec solution"  data-widget-type="cell" data-x="0" data-w="12">
         <img src="../assets/images/b-logo3x.png" class="b-logo solutiom" />
         <div class="row-fluid-wrapper row-depth-1 row-number-2 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell "  data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-3" style="height:561px;">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell container"  data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-4 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-cell banner-caption solution"  data-widget-type="cell" data-x="0" data-w="12">
                                    <div class="row-fluid-wrapper row-depth-2 row-number-1 ">
                                       <div class="row-fluid ">
                                          <div class="span12 widget-span widget-type-rich_text "  data-widget-type="rich_text" data-x="0" data-w="12">
                                             <div class="cell-wrapper layout-widget-wrapper">
                                                <span id="hs_cos_wrapper_module_145458317557213871" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_rich_text"  data-hs-cos-general-type="widget" data-hs-cos-type="rich_text">
                                                   <h1>Complete Courier Solution</h1>
												   <h3>Fast, Prompt and Convenient</h3>
                                                </span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row-fluid-wrapper row-depth-2 row-number-3 ">
                                       <div class="row-fluid ">
                                          <div class="span12 widget-span widget-type-custom_widget "  data-widget-type="custom_widget" data-x="0" data-w="12">
                                             <div class="cell-wrapper layout-widget-wrapper">
                                                <span id="hs_cos_wrapper_module_145655185865019191" class="hs_cos_wrapper hs_cos_wrapper_widget "  data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                                </span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="row-fluid-wrapper row-depth-1 row-number-14 ">
   <div class="row-fluid ">
      <div class="span12 widget-span widget-type-cell the-team-main" style="" data-widget-type="cell" data-x="0" data-w="12">
         <div class="row-fluid-wrapper row-depth-1 row-number-15 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell container" style="" data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-16 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell header" style="" data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-17 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-linked_image " style="" data-widget-type="linked_image" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_145569449651928569" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image" style="" data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/icon-how-it-works_1.svg");?>" class="hs-image-widget " style="width:56px;border-width:0px;border:0px;" width="56" alt="Our Core Values" title="Executive Team "></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row-fluid-wrapper row-depth-1 row-number-18 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-header " style="" data-widget-type="header" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_145569478318431428" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_header" style="" data-hs-cos-general-type="widget" data-hs-cos-type="header">
                                          <h2>Our Express Solutions</h2>
                                       </span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row-fluid-wrapper row-depth-1 row-number-19 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-widget_container " style="" data-widget-type="widget_container" data-x="0" data-w="12">
                           <span id="hs_cos_wrapper_module_145569481859232392" class="hs_cos_wrapper hs_cos_wrapper_widget_container hs_cos_wrapper_type_widget_container" style="" data-hs-cos-general-type="widget_container" data-hs-cos-type="widget_container">
                              <div id="hs_cos_wrapper_widget_1485452901356" class="hs_cos_wrapper hs_cos_wrapper_widget " style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<h5>Advanced, real time information and management support systems.</h5>
										<p>– We offer our customers a secure, yet user-friendly portal for order, tracking and direct access to their time-critical delivery information and on a 24-hour basis.
We create both standard and customized delivery or shipment status reports that can be delivered to you on a real time basis.</p>
                                    </div>
                                 </div>
                              </div>
                              <div id="hs_cos_wrapper_widget_1485451911165" class="hs_cos_wrapper hs_cos_wrapper_widget " style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<h5>Speed and Reliability</h5>
										<p>– With our highly experienced team of managers and service professionals, we provide high standard and prompt services, which you can always trust. Expertly managed and documented work processes ensure that your time-critical delivery operation will consistently provide reliable, cost effective and accurate results.</p>
                                    </div>
                                 </div>
                              </div>
							  <div id="hs_cos_wrapper_widget_1485452901356" class="hs_cos_wrapper hs_cos_wrapper_widget " style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<h5>Network Planning</h5>
										<p>– You can startup, upgrade, expand or integrate your time critical delivery network without incurring significant internal cost and resource investment. Oasis has made significant investment in developing and providing the physical resources necessary for the management and operation of integrated critical delivery networks. Oasis Express provides route design, scheduling, dispatch, monitoring and contingency operating services to support reliable operation of time-critical transportation networks.</p>
                                    </div>
                                 </div>
                              </div>
                              <div id="hs_cos_wrapper_widget_1485451911165" class="hs_cos_wrapper hs_cos_wrapper_widget " style="" data-hs-cos-general-type="widget" data-hs-cos-type="custom_widget">
                                 <div class="the-team">
                                    <div class="team-member-content">
										<h5>Online Shopping</h5>
										<p>– Shop for your essential groceries, household and office supplies, and get it delivered to your preferred location. Your shopping just got better and easier. Just give us your shopping list or visit our online shop <a href="<?php echo base_url("");?>"><u>Write US</u></a></p>
                                    </div>
                                 </div>
                              </div>
                           </span>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="section_wrapper">
<div class="the_content_wrapper">
<div class="vc_row wpb_row vc_row-fluid">
   <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner ">
         <div class="wpb_wrapper">
            <div class="wpb_text_column wpb_content_element ">
               <div class="wpb_wrapper">
                  '
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
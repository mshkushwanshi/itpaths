<div class="body-container-wrapper">
   <div class="body-container co<div class="body-container-wrapper">
      <div class="body-container container-fluid">
         <div class="row-fluid-wrapper row-depth-1 row-number-1 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell inner-banner" style="" data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-2 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell container" style="" data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-3 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-raw_jinja hs-blog-header inner-page-header" style="" data-widget-type="raw_jinja" data-x="0" data-w="12">
                                    <img class="alignnone wp-image-128" src="http://localhost/oasis1/assets/images/faq-banner-1024x154.jpg" alt="" width="792" height="119" sizes="(max-width: 792px) 100vw, 792px" style="">
                                 </div>
                              </div>
                           </div>
                           <div class="row-fluid-wrapper row-depth-1 row-number-4 ">
                              <div class="row-fluid ">
                                 <div class="span12 widget-span widget-type-linked_image banner-logo-img" style="" data-widget-type="linked_image" data-x="0" data-w="12">
                                    <div class="cell-wrapper layout-widget-wrapper">
                                       <span id="hs_cos_wrapper_module_14565502174488122" class="hs_cos_wrapper hs_cos_wrapper_widget hs_cos_wrapper_type_linked_image" style="" data-hs-cos-general-type="widget" data-hs-cos-type="linked_image"><img src="<?php echo base_url("assets/images/icons/icon-blog.svg");?>" class="hs-image-widget " style="width:115px;border-width:0px;border:0px;" width="115" alt="OASIS FAQ" title="OASIS FAQ"></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row-fluid-wrapper row-depth-1 row-number-5 ">
            <div class="row-fluid ">
               <div class="span12 widget-span widget-type-cell blog-container blog-listing-temp" style="" data-widget-type="cell" data-x="0" data-w="12">
                  <div class="row-fluid-wrapper row-depth-1 row-number-6 ">
                     <div class="row-fluid ">
                        <div class="span12 widget-span widget-type-cell container" style="" data-widget-type="cell" data-x="0" data-w="12">
                           <div class="row-fluid-wrapper row-depth-1 row-number-7 ">
                              <div class="row-fluid ">
                                 <div class="span8 widget-span widget-type-cell content-section" style="" data-widget-type="cell" data-x="0" data-w="8">
                                    <div class="row-fluid-wrapper row-depth-1 row-number-8 ">
                                       <div class="row-fluid ">
                                          <div id="Content">
                                             <div class="content_wrapper clearfix">
                                                <!-- .sections_group -->
                                                <div class="sections_group">
                                                   <div class="entry-content" itemprop="mainContentOfPage">
                                                      <div class="section the_content has_content">
                                                         <div class="section_wrapper">
                                                            <div class="the_content_wrapper">
                                                               <p id="p1"><b>What is Oasis Express?</b><br>
                                                                  Oasis Express is the fastest, most convenient and coolest local delivery service. We can run errands for you, drop off and pick up items such as prescriptions, groceries, gifts, or we can place a timely shipment for you.
                                                               </p>
                                                               <p id="p2"><b>How does it work?</b><br>
                                                                  Our optimization matches your pickup and delivery details to the delivery location in real-time. We then weigh up the Express errands Driver performance history and determine if he is the best fit for you.
                                                               </p>
                                                               <p id="p3"><b>When would I use Oasis Express?</b><br>
                                                                  Left your phone at the office? Need some documents picked up? Or you need to deliver a Hamper, Cake&nbsp; we are here to lighten the load. We provide a delivery service that’s fast, convenient and simple, accommodating the demands of your hectic lifestyle.
                                                               </p>
                                                               <p id="p4"><b>Where is Oasis Express available?</b><br>
                                                                  Express errands is only available in Accra, Ghana. Other major cities coming soon.
                                                               </p>
                                                               <p><b>How much does it cost?</b><br>
                                                                  When you get a Oasis Express quote we’ll confirm the exact price of your delivery. The price may vary according to the size of delivery item required, the fragility, distance travelled and the time of day or day of week. No surprises.
                                                               </p>
                                                               <p id="p6"><b>Where can I send errands stuff to?</b><br>
                                                                  Basically, anywhere within Accra for now. Oasis Express will collect from and deliver to any address – residential and commercial. We just ask that you clearly specify any helpful instructions when making the booking. This will significantly reduce potential complications at the collection/delivery address which could lead to additional charges.
                                                               </p>
                                                               <p id="p7"><b>What hours is Oasis Express available?</b><br>
                                                                  Oasis Express is available from 8am to 6pm, monday to saturday.
                                                               </p>
                                                               <p><b>You may be required to pay more if…</b></p>
                                                               <ul>
                                                                  <li>delivery item is required to wait more than 10 minutes at either the point of collection or point of delivery without prior instruction.</li>
                                                                  <li>delivery item is required to collect from or deliver to a different address</li>
                                                                  <li>delivery item is required to do additional duties above and beyond delivery to the door of the delivery address.</li>
                                                               </ul>
                                                               <p id="p8">Oasis Express customer Service will always telephone you to confirm any additional charges and authorise payment. Please do not pay the dispatch rider any extra charges unless authorized by the Oasis Express customer Service.</p>
                                                               <p id="p9"><b>Do I need to package my items?</b><br>
                                                                  Customers must hand their items to the dispatch rider packaged and ready for transport. Please take particular care of small, loose items.
                                                               </p>
                                                               <p id="p10"><b>How long will my delivery take?</b><br>
                                                                  When you get a Oasis Express quote, we will always confirm a precise time promise for your delivery. Delivery times will vary depending on the size of vehicle required, the distance travelled and available capacity within our network. We are however growing every day, and will always thrive to deliver faster.
                                                               </p>
                                                               <p id="p11"><b>Can I amend or cancel my delivery?</b><br>
                                                                  If you wish to amend or cancel your order, please call express errands Customer Service on&nbsp;<b>(+233) 545 41 3846, </b>as early as possible. Depending on the timing of the cancellation or the nature of the amendment some additional charges may be incurred.
                                                               </p>
                                                               <p id="p12"><b>How do I get in touch with Oasis Express customer Service?</b><br>
                                                                  You can call Oasis Express Customer Service on <b>(+233) 545 41 3846</b>
                                                               </p>
                                                               <p id="p13"><b>Will the delivery item come to my door?</b><br>
                                                                  Yes, the delivery item is contracted to deliver from door to door.
                                                               </p>
                                                               <p id="p14"><b>What if I’m not in when your delivery item arrives?</b><br>
                                                                  When the delivery item arrives at the point of collection/delivery, we expect someone to be available to handover/sign for the package. In the event than nobody is available, Oasis Express Customer Services will call you and take further instructions. Please note that any changes to the scheduled delivery may incur an additional charge.<br>
                                                                  The dispatch rider will wait at the address for up to 10 minutes free of charge.
                                                               </p>
                                                               <p id="p15"><b>What do I do if my delivery item doesn’t show up?</b><br>
                                                                  If you’re concerned about the status of your collection/delivery, or need to make alternative arrangements, please ring Oasis Express Customer Service on <b>(+233) 545 41 3846</b>
                                                               </p>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="section section-page-footer">
                                                         <div class="section_wrapper clearfix">
                                                            <div class="column one page-pager">
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <!-- .four-columns - sidebar -->
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  </div>
	  </div>
	  <style>
		img.alignnone.wp-image-128{
			opacity: 0.8;
			border-top-left-radius: 15px;
			border-top-right-radius: 5px;
		}
	  </style>
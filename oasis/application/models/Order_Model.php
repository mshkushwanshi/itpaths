<?php
class Order_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function addOrder($oData, $pData, $type){
		$this->db->insert('wp3h_order', $oData);
		$this->db->select('*');
		$this->db->from('wp3h_paymode');
		$this->db->where('type',$type);
		
		$oId = $this->db->insert_id();
		$pData['oId'] = $oId;
		if($query=$this->db->get()){
			$pType = $query->row_array()['id'];
			$pData['type'] = $pType;
			$this->db->insert('wp3h_payment', $pData);
		}
		else{
			return false;
		}
	}
	public function getAllOrders(){
		$this->db->select("concat(fname, ' ', lname) as name, `oFrom` as `from`, `oTo` as `to`, `oStatus` as `status`, `oToken` as `token`, `oDistance` as `distance`, `oDate` as `order_date`");
		$this->db->from('wp3h_order');
		$this->db->join('wp3h_user', 'wp3h_user.id = wp3h_order.oCid');
		return $this->db->get()->result_array();
	}
	public function getAllOrdersByUser($id){
		$this->db->select("`oFrom` as `mFrom`, `oTo` as `mTo`, `oStatus` as `mType`, `oToken` as `mUserToken`, `oDistance` as `mDistance`, `oDate` as `mDate`");
		$this->db->from('wp3h_order');
		$this->db->join('wp3h_user', 'wp3h_user.id = wp3h_order.oCid');
		$this->db->where('wp3h_user.id', $id);
		return $this->db->get()->result_array();
	}
	public function updateOrderStatus($token, $status){
		$this->db->where('oToken', $token);
		$this->db->update('wp3h_order', array('oStatus' => $status));
	}
	public function getOrdersById($id){
		$this->db->select("`oFrom` as `from`, `oTo` as `to`, `oStatus` as `status`, `oDistance` as `distance`, `oDate` as `order_date`, `o.oEid` as `executive_id`");
		$this->db->from('wp3h_order o');
		$this->db->where('o.oId', $id);
		$order = $this->db->get()->row();
		if(isset($order))
		if($order->executive_id != 0){
			$this->db->select("u.fname as `fname`, u.lname as `lname`, `u`.`phone` as `phone`");
			$this->db->from('wp3h_user u');
			$this->db->where('u.id', $order->executive_id);
			$edata = $this->db->get()->row();
			$order->edata = $edata;
		}
		unset($order->executive_id);
		return $order;
	}
}?>
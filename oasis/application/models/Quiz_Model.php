<?php
class Quiz_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function register_Quiz($Quiz){
		$this->db->insert('wp3h_Quiz', $Quiz);
	}
	public function login_Quiz($data){
		$this->db->select('*');
		$this->db->from('wp3h_Quiz');
		$this->db->where('Quizname',$data['Quizname']);
		$this->db->where('password',$data['password']);		if($query=$this->db->get()){			$data = $query->row_array();			$now = new DateTime();			$this->db->where('id', $data['id']);			$this->db->update('wp3h_Quiz', array('logindate'=>$now->format('Y-m-d H:i:s')));
			return $query->row_array();
		}
		else{
			return false;
		}
	}
	public function email_check($email){
		$this->db->select('*');
		$this->db->from('wp3h_Quiz');
		$this->db->where('email',$email);
		$query=$this->db->get();
		if($query->num_rows()>0){
			return false;
		}else{
			return true;
		}
	}	public function update($data, $id){		$this->db->select('*');		$this->db->from('wp3h_Quiz');		$this->db->where('email',$data['email']);		$query=$this->db->get();		if($query->num_rows()>0 && $query->row_array()['id'] != $id){			return false;		}else{			$this->db->where('id', $id);			$this->db->update('wp3h_Quiz', $data);			return true;		}	}
}?>
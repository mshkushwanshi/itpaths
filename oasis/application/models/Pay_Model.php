<?php
class Pay_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function makePayment($data){
		$this->db->insert('wp3h_payment', $data);
	}
	public function getPayments($uid){
		$this->db->select('*');
		$this->db->from('wp3h_payment');
		$this->db->where('uid',$uid);
		if($query=$this->db->get()){
			return $query->row_array();
		}
		else{
			return false;
		}
	}
	public function addWallet($data){
		$this->db->select('*');
		$this->db->from('wp3h_user');
		$this->db->where('id',$data['uid']);
		$query=$this->db->get();
		if($query->num_rows() > 0){
			$this->db->where('uid', $data['uid']);
			$this->db->update('wp3h_wallet', $data);
		}
		else
			$this->db->insert('wp3h_wallet', $data);
	}
	public function getWalletPayments($uid){
		$this->db->select('*');
		$this->db->from('wp3h_wallet');
		$this->db->where('uid',$uid);
		if($query=$this->db->get()){
			return $query->row_array();
		}
		else{
			return false;
		}
	}	
	public function getPaytype(){
		$this->db->select('*');
		$this->db->from('wp3h_paymode');
		return $aid = $this->db->get()->row_array();
	}
	public function addPaytype($data){
		$this->db->insert('wp3h_paymode', $data);
	}
	public function addOrder($oData, $pData){
		$this->db->insert('wp3h_order', $oData);
		$this->db->select('*');
		$this->db->from('wp3h_paymode');
		$this->db->where('type',$pData['type']);
		
		$oId = $this->db->insert_id();
		$pData['oId'] = $oId;
		if($query=$this->db->get()){
			$pType = $query->row_array()['id'];
			$pData['type'] = $pType;
			$this->db->insert('wp3h_payment', $pData);
		}
		else{
			return false;
		}
	}
	public function getAllOrders(){
		$this->db->select("o.oId as id, concat(u.fname, ' ', u.lname) as name, `oFrom` as `from`, `oTo` as `to`, `oStatus` as `status`, `oDistance` as `distance`, `oDate` as `order_date`, m.type as payMode, p.status as payStatus, p.amount as amount, o.oEid as oEid");
		$this->db->from('wp3h_order o');
		$this->db->join('wp3h_user u', 'u.id = o.oCid');
		$this->db->join('wp3h_payment p', 'p.oId = o.oId');
		$this->db->join('wp3h_paymode m', 'm.id = p.type');
		$order = $this->db->get()->result_array();
		
		for($i = 0; $i<sizeOf($order); $i++){
			$order[$i]['id'] =  base64_encode($order[$i]['id']);
			if(isset($order))
				if(isset($order[$i]['oEid'])){
				
					$this->db->select("u.fname as `fname`, u.lname as `lname`, `u`.`phone` as `phone`");
					$this->db->from('wp3h_user u');
					$this->db->where('u.id', $order[$i]['oEid']);
					$edata = $this->db->get()->row();
					if(isset($edata))
					$order[$i]['edata'] = $edata;
				}
			unset($order[$i]['oEid']);
		}
		return $order;
	}
	public function getAllOrdersByUser($id){
	    $this->db->select("o.oId as id, `oFrom` as `mFrom`, `oTo` as `mTo`, `oStatus` as `mType`, `oDistance` as `mDistance`, `oDate` as `mDate`, m.type as payMode, p.status as payStatus, p.amount as amount, o.oEid as oEid");
		$this->db->from('wp3h_order o');
		$this->db->join('wp3h_user u', 'u.id = o.oCid');
		$this->db->join('wp3h_payment p', 'p.oId = o.oId');
		$this->db->join('wp3h_paymode m', 'm.id = p.type');
		$this->db->where('o.oCid', $id);
		$order = $this->db->get()->result_array();
		for($i = 0; $i<sizeOf($order); $i++){
			$order[$i]['id'] =  base64_encode($order[$i]['id']);
			if(isset($order))
				if(isset($order[$i]['oEid'])){
					$this->db->select("u.fname as `fname`, u.lname as `lname`, `u`.`phone` as `phone`");
					$this->db->from('wp3h_user u');
					$this->db->where('u.id', $order[$i]['oEid']);
					$edata = $this->db->get()->row();
					if(isset($edata)){
						$order[$i]['mWhoDelivers'] = $edata['fname'] ." ". $edata['lname'];
						$order[$i]['mPhoneNumber'] = $edata['phone'];
					}
				}
			unset($order[$i]['oEid']);
		}
		return $order;
	}
	public function updateOrderStatus($token, $status, $eid){
	    print_r($eid);
		$this->db->where('oId', $token);
		$this->db->update('wp3h_order', array('oStatus' => $status, 'oEid' => $eid));
		return true;
	}
}?>
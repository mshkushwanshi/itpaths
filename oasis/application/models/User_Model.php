<?php
class User_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	public function register_user($user){
		$this->db->insert('wp3h_user', $user);
	}
	public function login_user($data){
		$this->db->select('*');
		$this->db->from('wp3h_user');
		$this->db->where('username',$data['username']);
		$this->db->or_where('email',$data['username']);
		$this->db->where('password',$data['password']);
		if($query=$this->db->get()){
			$data = $query->row_array();
			$now = new DateTime();
			$this->db->where('id', $data['id']);
			$this->db->update('wp3h_user', array('logindate'=>$now->format('Y-m-d H:i:s')));
			return $query->row_array();
		}
		else{
			return false;
		}
	}
	public function email_check($email){
		$this->db->select('*');
		$this->db->from('wp3h_user');
		$this->db->where('email',$email);
		$query=$this->db->get();
		if($query->num_rows()>0){
			return false;
		}else{
			return true;
		}
	}	public function update($data, $id){		$this->db->select('*');		$this->db->from('wp3h_user');		$this->db->where('email',$data['email']);		$query=$this->db->get();		if($query->num_rows()>0 && $query->row_array()['id'] != $id){			return false;		}else{			$this->db->where('id', $id);			$this->db->update('wp3h_user', $data);			return true;		}	}
	public function getRole($name){
		$this->db->select('*');
		$this->db->from('wp3h_role');
		$this->db->where('name', $name);
		return $this->db->get()->row_array();
	}
	public function createRole($data){
		$cntr = 0;
		foreach($data as $sub){
			$dat = $this->getRole($sub->name);
			if($dat != null){
				unset($data[$cntr]);
			}
			$cntr++;
		}
		if(count($data) > 0){
			$this->db->insert_batch('wp3h_role', $data);
			return $data;
		}
		return false;
	}
	public function getAllRole(){
		$this->db->select('*');
		$this->db->from('wp3h_role');
		return $aid = $this->db->get()->row_array();
	}
	public function updateImg($id, $imgname){
		$this->db->where('id', $id);
		$this->db->update('wp3h_user', array('photo'=>$imgname));
		return true;
	}
	public function getUserById($id){
		return $this->getData('id', $id, 'wp3h_user')->row_array();
	}
	public function getUserData(){
		$this->db->select('id, fname, lname, email, username, role');
		$this->db->from('wp3h_user');
		return $aid = $this->db->get()->result_array();
	}
	public function getData($colName, $colVal, $tblName){
		$this->db->select('*');
		$this->db->from($tblName);
		$this->db->where($colName, $colVal);
		return $quiz=$this->db->get();
	}
	public function edit($data, $id){
		$this->db->where('id', $id);
		$this->db->update('wp3h_user', $data);
	}
	public function uedit($data, $id){
		$this->db->where('id', $id);
		$this->db->update('wp3h_user', $data);
	}
	public function getEList(){
		$this->db->select("wp3h_user.id as id, fname, lname");
		$this->db->from('wp3h_user');
		$this->db->join('wp3h_role', 'wp3h_user.role = wp3h_role.id');
		$this->db->where('wp3h_role.id', 3);
		$users = $this->db->get()->result_array();
		return $users;
	}
	public function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('wp3h_user');
		return true;
	}
}?>
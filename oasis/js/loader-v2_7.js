(function() {

    var __hs_cta_json = {"css":"a#cta_button_1818776_0571f27a-af78-4320-9503-7a4f15d3adc3 {\n  -webkit-font-smoothing:antialiased; \n  cursor:pointer; \n  -moz-user-select:none; \n  -webkit-user-select:none; \n  -o-user-select:none; \n  user-select:none; \n  display:inline-block; \n  font-weight:normal; \n  text-align:center; \n  text-decoration:none; \n  font-family:sans-serif; \n  background:#b5472d; \n  color:rgb(255, 255, 255); \n  border-radius:6px; \n  border-width:0px; \n  transition:all .4s ease; \n  -moz-transition:all .4s ease; \n  -webkit-transition:all .4s ease; \n  -o-transition:all .4s ease; \n  text-shadow:none; \n  line-height:1.5em; \n  padding:17px 68px; \n}\na#cta_button_1818776_0571f27a-af78-4320-9503-7a4f15d3adc3:hover {\nbackground:#b5472d; \ncolor:rgb(255,255,255); \n}\na#cta_button_1818776_0571f27a-af78-4320-9503-7a4f15d3adc3:active, #cta_button_1818776_0571f27a-af78-4320-9503-7a4f15d3adc3:active:hover {\nbackground:rgb(88,148,30); \ncolor:rgb(244,244,244); \n}\n\na#cta_button_1818776_0571f27a-af78-4320-9503-7a4f15d3adc3{\nfont-size: 14px;\nletter-spacing: 3px;\nfont-weight: 700;\nborder-radius: 50px;\n-webkit-appearance: none;\n}","image_html":"<a id=\"cta_button_1818776_0571f27a-af78-4320-9503-7a4f15d3adc3\" class=\"cta_button\" href=\"https://cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=0571f27a-af78-4320-9503-7a4f15d3adc3&placement_guid=8bf44b45-946a-499e-baae-d487292c55dc&portal_id=1818776&redirect_url=APefjpHFw73AnKVcqNPAMGu0M8mBZg_hhCRWe1Y76x88zPfJAfuYmzRngOKcXwUM0tEBK2qzcI_IQZNRHTObIBb3FKf5LuBbyGWzxMnXHDt4SVIfyeB0QeQD9qMWC2mMh25a0xB66y6NQvxCUMONtaaVXV-na_q786_Ya1eVgOaN_73YMJcHtw2Cox-LLTfINrJKtTJ9eWrUzz0YucKzi-ac3kmqu7AUT1qfzOXCMfUAo2ABCglIRqk&hsutk=161d54c39749f8d5bfdd1576dce14b42&canon=https%3A%2F%2Fwww.OASIS.com%2F&click=1153e672-1278-4f0c-86cd-4ff21a5357be&pageId=5371643716\"  cta_dest_link=\"https://www.OASIS.com/get-started/\"><img id=\"hs-cta-img-8bf44b45-946a-499e-baae-d487292c55dc\" class=\"hs-cta-img \" style=\"border-width: 0px; /*hs-extra-styles*/\" mce_noresize=\"1\" alt=\"START A OASIS\" src=\"https://cdn2.hubspot.net/hubshot/17/10/17/d858d0fd-f078-4177-9b70-6201d4535010.png\" /></a>","is_image":false,"placement_element_class":"hs-cta-8bf44b45-946a-499e-baae-d487292c55dc","raw_html":"<a id=\"cta_button_1818776_0571f27a-af78-4320-9503-7a4f15d3adc3\" class=\"cta_button \" href=\"https://cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=0571f27a-af78-4320-9503-7a4f15d3adc3&placement_guid=8bf44b45-946a-499e-baae-d487292c55dc&portal_id=1818776&redirect_url=APefjpHFw73AnKVcqNPAMGu0M8mBZg_hhCRWe1Y76x88zPfJAfuYmzRngOKcXwUM0tEBK2qzcI_IQZNRHTObIBb3FKf5LuBbyGWzxMnXHDt4SVIfyeB0QeQD9qMWC2mMh25a0xB66y6NQvxCUMONtaaVXV-na_q786_Ya1eVgOaN_73YMJcHtw2Cox-LLTfINrJKtTJ9eWrUzz0YucKzi-ac3kmqu7AUT1qfzOXCMfUAo2ABCglIRqk&hsutk=161d54c39749f8d5bfdd1576dce14b42&canon=https%3A%2F%2Fwww.OASIS.com%2F&click=1153e672-1278-4f0c-86cd-4ff21a5357be&pageId=5371643716\"  style=\"/*hs-extra-styles*/\" cta_dest_link=\"https://www.OASIS.com/get-started/\" title=\"START A OASIS\">START A OASIS</a>"};
    var __hs_cta = {};

    __hs_cta.drop = function() {
        var is_legacy = document.getElementById('hs-cta-ie-element') && true || false,
            html = __hs_cta_json.image_html,
            tags = __hs_cta.getTags(),
            is_image = __hs_cta_json.is_image,
            parent, _style;

        if (!is_legacy && !is_image) {
            parent = (document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]);

            _style = document.createElement('style');
            parent.insertBefore(_style, parent.childNodes[0]);
            try {
                default_css = ".hs-cta-wrapper p, .hs-cta-wrapper div { margin: 0; padding: 0; }";
                cta_css = default_css + " " + __hs_cta_json.css;
                // http://blog.coderlab.us/2005/09/22/using-the-innertext-property-with-firefox/
                _style[document.all ? 'innerText' : 'textContent'] = cta_css;
            } catch (e) {
                // addressing an ie9 issue
                _style.styleSheet.cssText = cta_css;
            }

            html = __hs_cta_json.raw_html;
        }

        for (var i = 0; i < tags.length; ++i) {

            var tag = tags[i];
            var images = tag.getElementsByTagName('img');
            var cssText = "";
            var align = "";
            for (var j = 0; j < images.length; j++) {
                align = images[j].align;
                images[j].style.border = '';
                images[j].style.display = '';
                cssText = images[j].style.cssText;
            }

            if (align == "right") {
                tag.style.display = "block";
                tag.style.textAlign = "right";
            } else if (align == "middle") {
                tag.style.display = "block";
                tag.style.textAlign = "center";
            }

            tag.innerHTML = html.replace('/*hs-extra-styles*/', cssText);
            tag.style.visibility = 'visible';
            tag.setAttribute('data-hs-drop', 'true');
            window.hbspt && hbspt.cta && hbspt.cta.afterLoad && hbspt.cta.afterLoad('8bf44b45-946a-499e-baae-d487292c55dc');
        }

        return tags;
    };

    __hs_cta.getTags = function() {
        var allTags, check, i, divTags, spanTags,
            tags = [];
            if (document.getElementsByClassName) {
                allTags = document.getElementsByClassName(__hs_cta_json.placement_element_class);
                check = function(ele) {
                    return (ele.nodeName == 'DIV' || ele.nodeName == 'SPAN') && (!ele.getAttribute('data-hs-drop'));
                };
            } else {
                allTags = [];
                divTags = document.getElementsByTagName("div");
                spanTags = document.getElementsByTagName("span");
                for (i = 0; i < spanTags.length; i++) {
                    allTags.push(spanTags[i]);
                }
                for (i = 0; i < divTags.length; i++) {
                    allTags.push(divTags[i]);
                }

                check = function(ele) {
                    return (ele.className.indexOf(__hs_cta_json.placement_element_class) > -1) && (!ele.getAttribute('data-hs-drop'));
                };
            }
            for (i = 0; i < allTags.length; ++i) {
                if (check(allTags[i])) {
                    tags.push(allTags[i]);
                }
            }
        return tags;
    };

    // need to do a slight timeout so IE has time to react
    setTimeout(__hs_cta.drop, 10);
    window._hsq = window._hsq || [];
    window._hsq.push(['trackCtaView', '8bf44b45-946a-499e-baae-d487292c55dc', '0571f27a-af78-4320-9503-7a4f15d3adc3']);
}());

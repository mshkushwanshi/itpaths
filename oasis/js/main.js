$(function(){
	$( "#dialog" ).dialog({
	  autoOpen: false,
	  show: {
		effect: "blind",
		duration: 1000
	  },
	  hide: {
		effect: "explode",
		duration: 1000
	  }
	});
	$( "#rstr" ).dialog({
	  autoOpen: false,
	  width: 400,
	  height: 500,
	  show: {
		effect: "blind",
		duration: 1000
	  },
	  hide: {
		effect: "explode",
		duration: 1000
	  }
	});
	$( "#track" ).dialog({
	  autoOpen: false,
	  show: {
		effect: "blind",
		duration: 1000
	  },
	  hide: {
		effect: "explode",
		duration: 1000
	  }
	});
	$( "#apnl" ).dialog({
	  height: $(window).height() - 20,
	  width: $(window).width() - 20,
	  resizable: true,
	  closeOnEscape: false,
	  autoOpen: false,
	  show: {
		effect: "blind",
		duration: 1000
	  },
	  hide: {
		effect: "explode",
		duration: 1000
	  }
	});
	$( "#cta_button_1818776_156e426f-5c99-4c33-a8f0-6438378633bf" ).on( "click", function() {
	  $( "#dialog" ).dialog( "open" );
	});
	$( ".track" ).on( "click", function() {
	  $( "#track" ).dialog( "open" );
	});
});
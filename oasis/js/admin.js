var home = function(){}
var profile  = function(){
	$("#udGrid").hide();
	$("#odGrid").hide();
	$("#udRole").hide();
	$("#odGrid-bar img").hide();
	$(".container.data").show();
	$(".text-themecolor.m-b-0.m-t-0").html("Profile");
	$($(".breadcrumb-item")[1]).html("Profile");
	var data = JSON.parse(localStorage.getItem("data"));
	$("input[name='fname']").val(JSON.parse(localStorage.getItem("data")).fname);
	$("input[name='lname']").val(JSON.parse(localStorage.getItem("data")).lname);
	$("input[name='dob']").val(JSON.parse(localStorage.getItem("data")).dob);
	$("input[name='username']").val(JSON.parse(localStorage.getItem("data")).username);
	$("input[name='email']").val(JSON.parse(localStorage.getItem("data")).email);
	$(".data").attr('style', 'display:block;');
	$(".col-md-10 select [value='" + JSON.parse(localStorage.getItem("data")).role + "']").attr("selected", "selected");
	return false;
}
var updtOrdr = function(oid){
	stat = $('#nstat').val();
	uid = $('#eid').val();
	$("#uodr").dialog("close");
	var json = {'oid': oid, 'stat': stat, 'eid': uid};
	$.get(domain + "pay/updateOrder",
			json,function(res){
		}
	);
}
var ordrInfo = function(node){
	var data = $(node).parent().parent().data().JSGridItem;
	var html = 
	"<table class='odrinfo'>" +
		"<tr><td class='hdr'>Order Id</td><td>" + data.id + "</td></tr>" +
		"<tr><td class='hdr'>Customer Name</td><td>" + data.name + "</td></tr>" +
		"<tr><td class='hdr'>From</td><td>" + data.from + "</td></tr>" +
		"<tr><td class='hdr'>To</td><td>" + data.to + "</td></tr>" +
		"<tr><td class='hdr'>Amount</td><td>" + data.amount + "</td></tr>" +
		"<tr><td class='hdr'>Payment Status</td><td>" + data.payStatus + "</td></tr>" +
		"<tr><td class='hdr'>Delivery Status</td><td>" + data.status + "</td></tr>" +
		"<tr><td class='hdr'>Distance</td><td>" + data.distance + "</td></tr>" +
		"<tr><td class='hdr' colspan='2'>Executive Detail</td></tr>" +
		"<tr><td class='hdr'>Executive Name</td><td>" +  data.edata.fname + " " + data.edata.lname + "</td></tr>" +
		"<tr><td class='hdr'>Executive Contact NO</td><td>" + data.edata.phone + "</td></tr>" +
	"</table>";
	$("#odr-info").html(html);
	$("#odr-data").dialog("open");yy
}
var edtOrdr = function(node){
	var data = $(node).parent().parent().data().JSGridItem;
	var status = data.status;
	var executive = data.edata;
	var astat = ["New", "Preparing", "In Progress", "Dispatched", "Delivered", "Cancelled"];
	var html = "<select id='nstat'>";
	for(i=0; i<astat.length; i++){
		if(astat[i] == status){
			html += "<option selected value='" + status + "'>" + astat[i] + "</option>";
		}
		else{
			html += "<option value='" + astat[i] + "'>" + astat[i] + "</option>";
		}
	}
	html +="</select>";
	$.ajax({url: domain + "user/getEList"})
	.done(function(res){
		var usrHtml = "<select id='eid'>";
		$.each(res, function(i, item) {
			var name = item.fname + " " + item.lname;
			if(executive != undefined){
			    usrHtml += "<option value='" + item.id + "' selected>" + name + "</option>";
			}
			else{
				usrHtml += "<option value='" + item.id + "'>" + name + "</option>";
			}
			
		});
		usrHtml += "</select>";
		var tbl = "<table><tr><td>Order Status:&nbsp;</td><td>" + html + "</td></tr><tr><td>Executive:&nbsp;</td><td>" + usrHtml +
		"</td></tr><tr><td colspan='2'><button onClick='updtOrdr(\"" + data.id + "\")'>Update</button></td></tr></table>";	
		$("#uodr-dat").html(tbl);
		$("#uodr").dialog("open");
	})
	.fail(function(res) {
		alert("Communication Error");
	});
};
var listOrderView = function(){
	$("#udGrid").hide();
	$("#udRole").hide();
	$("#odGrid").show();
	$("#odGrid-bar img").show();
	$(".container.data").hide();
	$(".text-themecolor.m-b-0.m-t-0").html("Orders");
	$($(".breadcrumb-item")[1]).html("Orders");
}
var listOrders = function(){
	$.ajax({url: window.api + "pay/getAllOrders"})
	.done(function(res){
		let editIconField = function(config) {
			jsGrid.Field.call(this, config);
		};
		let infoIconField = function(config) {
			jsGrid.Field.call(this, config);
		};
		editIconField.prototype = new jsGrid.Field({
		  css: "jsgrid-control-field action-button-cell dropdown",
		  width: 10,
		  align: "center",
		  //myCustomProperty: "foo",      // custom property

		  itemTemplate: function(value, item) {
			if (!item || !item.id || item.id < 0) {
			  return "";
			}
			return $("<img src='" + window.api + "/assets/images/edit.png' width='25' />&nbsp;")
			  .addClass("action-button btn btn-sm dropdown-toggle")
			  .attr("data-toggle", "dropdown")
			  .attr("onClick", "edtOrdr(this)")
			  .text("Edit");	
		  },
		});
		infoIconField.prototype = new jsGrid.Field({
		  css: "jsgrid-control-field action-button-cell dropdown",
		  width: 10,
		  align: "center",
		  //myCustomProperty: "foo",      // custom property

		  itemTemplate: function(value, item) {
			if (!item || !item.id || item.id < 0) {
			  return "";
			}
			return $("&nbsp;<img src='" + window.api + "/assets/images/info.png' width='25' />")
			  .addClass("action-button btn btn-sm dropdown-toggle")
			  .attr("data-toggle", "dropdown")
			  .attr("onClick", "ordrInfo(this)")
			  .text("Edit");	
		  },
		});
		jsGrid.fields.editIcon = editIconField;
		var status = [
			{ Name: "New", Id: "New" },
			{ Name: "Preparing", Id: "Preparing" },
			{ Name: "In Progress", Id: "In Progress" },
			{ Name: "Out for delivery", Id: "Out for delivery" },
			{ Name: "Delivered", Id: "Delivered" },
			{ Name: "Cancelled", Id: "Cancelled" }
		];
		jsGrid.fields.editIcon = editIconField;
		jsGrid.fields.infoIcon = infoIconField;
		$("#odGrid").jsGrid({
			width: "100%",
			height: "400px",
			inserting: false,
			editing: false,
			sorting: true,
			paging: true,
			data: res,
			fields: [
				{ name: "id", type: "text", title: "Order ID", width: 50 },
				{ name: "name", type: "text", title: "Customer Name", width: 50 },
				{ name: "from", type: "text", title: "From", css: ""},
				{ name: "to", type: "text", title: "To", sorting: true, css:""},
				{ name: "status", type: "select", title: "Status",items: status, valueField: "Id", textField: "Name"},
				{ name: "distance", type: "text", title: "Distance", css: "distance"},
				{ name: "order_date", type: "text", title: "Order Date", css: "odate"},
				{ name: "payMode", type: "text", title: "Payment Mode", css: "pmode"},
				{ name: "payStatus", type: "text", title: "Payment Status",css: "pstatus"},
				{ name: "amount", type: "text", title: "Amount", css: "amount"},
				{ type: "editIcon"},
				{ type: "infoIcon"}
			]
		});
	})
	.fail(function(res) {
		if(res.status == 401)
			$($('.help-block')[2]).html('Invalid login credentials');
		else
			$($('.help-block')[2]).html('Something went wrong');
	})
}

$('.dataBtn').click(function(){
	$("#user-data").hide();
	if($('.dataBtn').html() == "Edit"){
		$('.dataBtn').html('Update');
		$('input[type="text"]').prop('disabled', false);
		$('input[type="text"] [name="email"]').prop('disabled', true);
		$('.col-md-10 select').prop('disabled', false);
	}
	else {
		var data = JSON.parse(localStorage.getItem("data"));
		data.fname = $("input[name='fname']").val();
		data.lname = $("input[name='lname']").val();
		data.username = $("input[name='username']").val();
		data.dob = $("input[name='dob']").val();
		
		var json = {fname: $("input[name='fname']").val(), lname: $("input[name='lname']").val(), username: $("input[name='username']").val(), dob: $("input[name='dob']").val(), id: data.id };
		
		$.ajax({url: domain + "user/edit", method: 'post', data: json})
			.done(function(res){
				$('input[type="text"]').prop('disabl	ed', true);
				localStorage.setItem("data", JSON.stringify(data));
				$('.dataBtn').html("Edit");
			})
			.fail(function(res) {
				if(res.status == 401)
					$($('.help-block')[2]).html('Invalid login credentials');
				else
					$($('.help-block')[2]).html('Something went wrong');
			})
			.always(function() {

			});
	}
});
$("#odGrid-bar img").click(function(){
	listOrders();
});
var astat = function(stat){
	$("#astat").html(stat);
	setTimeout(function(){ $("#astat").html("");
		if($(".jsgrid-load-shader")){
			$(".jsgrid-load-shader").hide();
		}
		if($(".jsgrid-load-panel")){
			$(".jsgrid-load-panel").hide();
		}
	}, 3000);
};
var node;
$(document).ready(function(){
	listOrders();
	$("#odGrid-bar img").hide();
});
-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 24, 2018 at 01:44 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oasiwxrk_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp3h_address`
--

CREATE TABLE `wp3h_address` (
  `id` int(7) NOT NULL,
  `sid` int(7) DEFAULT NULL,
  `address` varchar(50) NOT NULL,
  `city` varchar(40) DEFAULT NULL,
  `country` varchar(30) DEFAULT NULL,
  `pin` int(8) DEFAULT NULL,
  `phoneno` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wp3h_app_users`
--

CREATE TABLE `wp3h_app_users` (
  `id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date DEFAULT NULL,
  `phone` int(11) NOT NULL,
  `cdate` date DEFAULT NULL,
  `mdate` date DEFAULT NULL,
  `ldate` date DEFAULT NULL,
  `country` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp3h_app_users`
--

INSERT INTO `wp3h_app_users` (`id`, `username`, `password`, `name`, `dob`, `phone`, `cdate`, `mdate`, `ldate`, `country`, `city`, `type`) VALUES
('TR1HLqsTnDR4thh5m0SAyzvcI1I3', 'aa@aa.aa', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', 'aa aa', '0000-00-00', 1234567890, '0000-00-00', '0000-00-00', '0000-00-00', '', '', 2),
('c0OMeMKxlXcGsczq608dXGrOzZP2', 'pooja.jadon989@gmail.com', '5b32df450d318a611b8abcbdfa934388', 'Pooja Jadon', '0000-00-00', 1234567890, NULL, NULL, NULL, NULL, NULL, 2),
('f9mdvpMv6VVZDAKh5SQoOZguMds2', 'ss@ss.ss', 'af15d5fdacd5fdfea300e88a8e253e82', 'ss ss', '0000-00-00', 1234567890, NULL, NULL, NULL, NULL, NULL, 2),
('eVKzLdINAdaWKLHeGZZwEwBZcNO2', 'qq@qq.qq', '343b1c4a3ea721b2d640fc8700db0f36', 'qq qq', '0000-00-00', 1234567890, NULL, NULL, NULL, NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `wp3h_courier`
--

CREATE TABLE `wp3h_courier` (
  `id` int(11) NOT NULL,
  `did` varchar(32) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `source` varchar(50) NOT NULL,
  `destination` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wp3h_order`
--

CREATE TABLE `wp3h_order` (
  `oId` bigint(20) NOT NULL,
  `oCid` varchar(100) NOT NULL,
  `oDate` datetime NOT NULL,
  `oFrom` varchar(200) NOT NULL,
  `oTo` varchar(200) NOT NULL,
  `ofLat` double NOT NULL,
  `ofLng` double NOT NULL,
  `oExp` varchar(3) NOT NULL,
  `oCExp` varchar(3) NOT NULL,
  `oSExp` varchar(3) NOT NULL,
  `oStatus` varchar(20) NOT NULL,
  `oToken` varchar(200) NOT NULL,
  `otLat` double NOT NULL,
  `otLng` double NOT NULL,
  `oDistance` varchar(10) NOT NULL,
  `oEid` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp3h_order`
--

INSERT INTO `wp3h_order` (`oId`, `oCid`, `oDate`, `oFrom`, `oTo`, `ofLat`, `ofLng`, `oExp`, `oCExp`, `oSExp`, `oStatus`, `oToken`, `otLat`, `otLng`, `oDistance`, `oEid`) VALUES
(1, 'i6I2JyQibmNl6UrnbZ9tY3zVVYg2', '2018-05-14 03:10:00', 'Stationsplein 16, 1012 AB Amsterdam, Netherlands', 'Aleje Jerozolimskie 23, Warszawa, Poland', 52.378041800000005, 4.898966400000001, 'no', 'no', 'no', 'preparing', 'fB5qnW4svxw:APA91bHXfDhONcI4NS0oDsxCOTLBKTVlPw31gC5siKBNDb6ub6MBjcDGDCgbTYtAdhXeHpqk5HHswta1Vh1tniruDOwBmzLTAsnx0rSF0ofMthKUI8MnIUzExvqplD0-RIERayhwKgz', 52.230384, 21.0157299, '0.99999', '34442a981870ebebe519275134aa863a'),
(2, 'i6I2JyQibmNl6UrnbZ9tY3zVVYg2', '2018-05-14 03:10:00', 'Stationsplein 16, 1012 AB Amsterdam, Netherlands', 'Aleje Jerozolimskie 23, Warszawa, Poland', 52.378041800000005, 4.898966400000001, 'no', 'no', 'no', 'preparing', 'fB5qnW4svxw:APA91bHXfDhONcI4NS0oDsxCOTLBKTVlPw31gC5siKBNDb6ub6MBjcDGDCgbTYtAdhXeHpqk5HHswta1Vh1tniruDOwBmzLTAsnx0rSF0ofMthKUI8MnIUzExvqplD0-RIERayhwKg', 52.230384, 21.0157299, '0.99999', '34442a981870ebebe519275134aa863a'),
(3, 'i6I2JyQibmNl6UrnbZ9tY3zVVYg2', '2018-05-14 03:10:00', 'Stationsplein 16, 1012 AB Amsterdam, Netherlands', 'Aleje Jerozolimskie 23, Warszawa, Poland', 52.378041800000005, 4.898966400000001, 'no', 'no', 'no', 'Delivered', 'fB5qnW4svxw:APA91bHXfDhONcI4NS0oDsxCOTLBKTVlPw31gC5siKBNDb6ub6MBjcDGDCgbTYtAdhXeHpqk5HHswta1Vh1tniruDOwBmzLTAsnx0rSF0ofMthKUI8MnIUzExvqplD0-RIERayhw', 52.230384, 21.0157299, '0.99999', '0'),
(4, 'i6I2JyQibmNl6UrnbZ9tY3zVVYg2', '2018-05-14 03:10:00', 'Stationsplein 16, 1012 AB Amsterdam, Netherlands', 'Aleje Jerozolimskie 23, Warszawa, Poland', 52.378041800000005, 4.898966400000001, 'no', 'no', 'no', 'New', 'fB5qnW4svxw:APA91bHXfDhONcI4NS0oDsxCOTLBKTVlPw31gC5siKBNDb6ub6MBjcDGDCgbTYtAdhXeHpqk5HHswta1Vh1tniruDOwBmzLTAsnx0rSF0ofMthKUI8MnIUzExvqplD0-RIERay', 52.230384, 21.0157299, '0.99999', '0'),
(5, 'i6I2JyQibmNl6UrnbZ9tY3zVVYg2', '2018-05-14 03:10:00', 'Stationsplein 16, 1012 AB Amsterdam, Netherlands', 'Aleje Jerozolimskie 23, Warszawa, Poland', 52.378041800000005, 4.898966400000001, 'no', 'no', 'no', 'New', 'fB5qnW4svxw:APA91bHXfDhONcI4NS0oDsxCOTLBKTVlPw31gC5siKBNDb6ub6MBjcDGDCgbTYtAdhXeHpqk5HHswta1Vh1tniruDOwBmzLTAsnx0rSF0ofMthKUI8MnIUzExvqplD0-RIERayhwKgzi', 52.230384, 21.0157299, '0.99999', '0'),
(6, 'i6I2JyQibmNl6UrnbZ9tY3zVVYg2', '2018-05-14 03:10:00', 'Stationsplein 16, 1012 AB Amsterdam, Netherlands', 'Aleje Jerozolimskie 23, Warszawa, Poland', 52.378041800000005, 4.898966400000001, 'no', 'no', 'no', 'New', 'fB5qnW4svxw:APA91bHXfDhONcI4NS0oDsxCOTLBKTVlPw31gC5siKBNDb6ub6MBjcDGDCgbTYtAdhXeHpqk5HHswta1Vh1tniruDOwBmzLTAsnx0rSF0ofMthKUI8MnIUzExvqplD0-RIERay', 52.230384, 21.0157299, '0.99999', '0'),
(7, 'i6I2JyQibmNl6UrnbZ9tY3zVVYg2', '2018-05-14 03:10:00', 'Stationsplein 16, 1012 AB Amsterdam, Netherlands', 'Aleje Jerozolimskie 23, Warszawa, Poland', 52.378041800000005, 4.898966400000001, 'no', 'no', 'no', 'New', 'fB5qnW4svxw:APA91bHXfDhON;cI4NS0oDsxCOTLBKTVlPw31gC5siKBNDb6ub6MBjcDGDCgbTYtAdhXeHpqk5HHswta1Vh1tniruDOwBmzLTAsnx0rSF0ofMthKUI8MnIUzExvqplD0-RIERayhwKgzi', 52.230384, 21.0157299, '0.99999', '0'),
(8, 'i6I2JyQibmNl6UrnbZ9tY3zVVYg2', '2018-05-14 03:10:00', 'Stationsplein 16, 1012 AB Amsterdam, Netherlands', 'Aleje Jerozolimskie 23, Warszawa, Poland', 52.378041800000005, 4.898966400000001, 'no', 'no', 'no', 'New', 'fB5qnW4svxw:APA9l1bHXfDhONcI4NS0oDsxCOTLBKTVlPw31gC5siKBNDb6ub6MBjcDGDCgbTYtAdhXeHpqk5HHswta1Vh1tniruDOwBmzLTAsnx0rSF0ofMthKUI8MnIUzExvqplD0-RIERayhwKgzi', 52.230384, 21.0157299, '0.99999', '0'),
(9, 'i6I2JyQibmNl6UrnbZ9tY3zVVYg2', '2018-05-14 03:10:00', 'Stationsplein 16, 1012 AB Amsterdam, Netherlands', 'Aleje Jerozolimskie 23, Warszawa, Poland', 52.378041800000005, 4.898966400000001, 'no', 'no', 'no', 'New', 'fB5qnW4svxw:APA91bHXfDhONcI4NS0oDsxCOTLBKTVlPw31gC5siKBNDb6ub6MBjcDGDCgbTY;tAdhXeHpqk5HHswta1Vh1tniruDOwBmzLTAsnx0rSF0ofMthKUI8MnIUzExvqplD0-RIERayhwKgzi', 52.230384, 21.0157299, '0.99999', '0'),
(10, 'i6I2JyQibmNl6UrnbZ9tY3zVVYg2', '2018-05-14 03:10:00', 'Stationsplein 16, 1012 AB Amsterdam, Netherlands', 'Aleje Jerozolimskie 23, Warszawa, Poland', 52.378041800000005, 4.898966400000001, 'no', 'no', 'no', 'New', 'fB5qnW4svxw:APA91bHXfDhONcI4NS0oDsxCOTLBKTVlPw31gC5siKBNDb6ub6MBjcDGDC;lgbTYtAdhXeHpqk5HHswta1Vh1tniruDOwB;mzLTAsnx0rSF0ofMthKUI8MnIUzExvqplD0-RIERayhwKgzi', 52.230384, 21.0157299, '0.99999', '0'),
(11, 'i6I2JyQibmNl6UrnbZ9tY3zVVYg2', '2018-05-14 03:10:00', 'Stationsplein 16, 1012 AB Amsterdam, Netherlands', 'Aleje Jerozolimskie 23, Warszawa, Poland', 52.378041800000005, 4.898966400000001, 'no', 'no', 'no', 'New', 'fB5qnW4svxw:APA91bHXfDhONcI4NS0oDsxCOTLBKTVlPw31gC5siKBNDb6ub6MBjcDGDCgbTYtAdhXeHpqk5HHswta1Vh1tniruDOwBmzLTAsnx0rSF0ofMthKUI8;MnIUzExvqplD0-RIERayhwKgzi', 52.230384, 21.0157299, '0.99999', '0'),
(12, 'i6I2JyQibmNl6UrnbZ9tY3zVVYg2', '2018-05-14 03:10:00', 'Stationsplein 16, 1012 AB Amsterdam, Netherlands', 'Aleje Jerozolimskie 23, Warszawa, Poland', 52.378041800000005, 4.898966400000001, 'no', 'no', 'no', 'New', 'fB5qnW4svxw:AP;A91bHXfDhONcI4NS0oDsxCOTLBKTVlPw31gC5siKBNDb6ub6MBjcDGDCgbTYtAdhXeHpqk5HHswta1Vh1tniruDOwBmzLTAsnx0rSF0ofMthKUI8MnIUzExvqplD0-RIERayhwKgzi', 52.230384, 21.0157299, '0.99999', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wp3h_payment`
--

CREATE TABLE `wp3h_payment` (
  `id` int(11) NOT NULL COMMENT 'payment id',
  `uid` varchar(32) NOT NULL COMMENT 'user id',
  `payTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'payment time',
  `amount` double NOT NULL COMMENT 'ride amount',
  `type` varchar(5) NOT NULL COMMENT 'payment type [wallet, cash, card]',
  `oId` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp3h_payment`
--

INSERT INTO `wp3h_payment` (`id`, `uid`, `payTime`, `amount`, `type`, `oId`) VALUES
(1, 'umesh29k', '2018-05-06 06:25:30', 11, '1', 0),
(2, 'i6I2JyQibmNl6UrnbZ9tY3zVVYg2', '2018-05-13 21:40:00', 1, '2', 12);

-- --------------------------------------------------------

--
-- Table structure for table `wp3h_paymode`
--

CREATE TABLE `wp3h_paymode` (
  `id` int(11) NOT NULL,
  `type` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp3h_paymode`
--

INSERT INTO `wp3h_paymode` (`id`, `type`) VALUES
(1, 'Cash'),
(2, 'Card'),
(3, 'Net Bankin'),
(4, 'Wallet');

-- --------------------------------------------------------

--
-- Table structure for table `wp3h_permission`
--

CREATE TABLE `wp3h_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `detail` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wp3h_role`
--

CREATE TABLE `wp3h_role` (
  `id` int(2) NOT NULL,
  `name` varchar(30) NOT NULL,
  `detail` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp3h_role`
--

INSERT INTO `wp3h_role` (`id`, `name`, `detail`) VALUES
(1, 'No Access', 'No access user'),
(2, 'Admin', 'Admin user'),
(3, 'Driver', 'Driver user'),
(4, 'Default', 'Default User'),
(5, 'Enterprise', 'Enterprise User');

-- --------------------------------------------------------

--
-- Table structure for table `wp3h_user`
--

CREATE TABLE `wp3h_user` (
  `id` varchar(50) NOT NULL,
  `fname` varchar(30) NOT NULL,
  `lname` varchar(30) NOT NULL,
  `dob` date DEFAULT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(50) NOT NULL,
  `logindate` timestamp NULL DEFAULT NULL,
  `role` int(2) NOT NULL,
  `cdate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `mdate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `photo` varchar(40) NOT NULL,
  `phone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp3h_user`
--

INSERT INTO `wp3h_user` (`id`, `fname`, `lname`, `dob`, `username`, `email`, `password`, `logindate`, `role`, `cdate`, `mdate`, `photo`, `phone`) VALUES
('3', 'Suresh', 'Singh', '1999-05-28', 'umesh', 'suresh@gmail.com', '0487cc982f7db39c51695026e4bdc692', '2018-03-12 11:22:18', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 0),
('34442a981870ebebe519275134aa863a', 'Umesh', 'Singh', '0000-00-00', 'umesh29k3', 'umesh29k3@gmail.com', '3ad9bc02864bf456b684d2351cd1b34e', NULL, 3, '2018-05-20 13:06:48', NULL, '', 0),
('4', 'Deepak', 'Kushwanshi', '1987-04-29', 'deepak', 'deepak@gmail.com', '498b5924adc469aa7b660f457e0fc7e5', '2018-06-23 17:16:52', 2, '0000-00-00 00:00:00', '2018-05-19 06:24:33', '', 0),
('4e694f1ddfd5b5e6959f5606f399a66c', 'Umesh', 'Singh', '0000-00-00', 'umesh29k2', 'umesh29k2@gmail.com', '31d5ec65081150537350dfd31a2cc523', NULL, 3, '2018-05-20 13:03:43', NULL, '', 0),
('5', 'Student', 'Detail', '2018-01-03', 'student', 'student@as.com', 'cd73502828457d15655bbd7a63fb0bc8', '2018-02-22 16:29:51', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1517872822_webcam.jpg', 0),
('77ea9ae1ed5b9b7836aa274f5202e56e', 'Umesh', 'Singh', '0000-00-00', 'umesh29k1', 'umesh29k1@gmail.com', '5f4a67f4a9e6d0675ebb94d3afe18140', NULL, 3, '2018-05-20 13:05:16', NULL, '', 0),
('c0OMeMKxlXcGsczq608dXGrOzZP2', 'Pooja', 'Jadon', NULL, 'pooja.jadon989@gmail.com', 'pooja.jadon989@gmail.com', '5b32df450d318a611b8abcbdfa934388', NULL, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1234567890),
('eVKzLdINAdaWKLHeGZZwEwBZcNO2', 'ss', 'ss', NULL, 'qq@qq.qq', 'qq@qq.qq', '343b1c4a3ea721b2d640fc8700db0f36', NULL, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1234567890),
('f9mdvpMv6VVZDAKh5SQoOZguMds2', 'ss', 'ss', NULL, 'ss@ss.ss', 'pooja.jadon989@gmail.com', 'af15d5fdacd5fdfea300e88a8e253e82', '2018-04-26 01:43:38', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1234567890),
('i6I2JyQibmNl6UrnbZ9tY3zVVYg2', 'aa', 'aa', '0000-00-00', 'aa@aa.aa', 'aa@aa.aa', '0b4e7a0e5fe84ad35fb5f95b9ceeac79', '2018-05-06 14:10:42', 2, '0000-00-00 00:00:00', '2018-05-06 14:10:58', '', 1234567890),
('umesh29k', 'Umesh', 'Kushwanshi', '0000-00-00', 'deepak1', 'umesh29k@gmail.com', '0326605db3dcf8c82c6dc20d4d344e07', '2018-05-21 15:50:36', 2, '2018-05-06 03:13:42', '2018-05-14 16:11:09', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp3h_wallet`
--

CREATE TABLE `wp3h_wallet` (
  `id` int(11) NOT NULL,
  `uid` varchar(32) NOT NULL,
  `amount` double NOT NULL,
  `paytime` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp3h_wallet`
--

INSERT INTO `wp3h_wallet` (`id`, `uid`, `amount`, `paytime`) VALUES
(0, 'umesh29k', 11, '2018-05-06 06:13:50'),
(2, 'umesh29', 12, '2018-05-06 05:32:11'),
(3, 'umeshk29', 12, '2018-05-06 05:33:04'),
(4, 'umeshk29', 10, '2018-05-06 05:33:33'),
(5, 'umeshk29', 11, '2018-05-06 05:33:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp3h_address`
--
ALTER TABLE `wp3h_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp3h_app_users`
--
ALTER TABLE `wp3h_app_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp3h_courier`
--
ALTER TABLE `wp3h_courier`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp3h_order`
--
ALTER TABLE `wp3h_order`
  ADD PRIMARY KEY (`oId`),
  ADD UNIQUE KEY `oId` (`oId`);

--
-- Indexes for table `wp3h_payment`
--
ALTER TABLE `wp3h_payment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp3h_paymode`
--
ALTER TABLE `wp3h_paymode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp3h_permission`
--
ALTER TABLE `wp3h_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp3h_role`
--
ALTER TABLE `wp3h_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp3h_user`
--
ALTER TABLE `wp3h_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp3h_wallet`
--
ALTER TABLE `wp3h_wallet`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp3h_address`
--
ALTER TABLE `wp3h_address`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp3h_courier`
--
ALTER TABLE `wp3h_courier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp3h_order`
--
ALTER TABLE `wp3h_order`
  MODIFY `oId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `wp3h_payment`
--
ALTER TABLE `wp3h_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'payment id', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp3h_paymode`
--
ALTER TABLE `wp3h_paymode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wp3h_permission`
--
ALTER TABLE `wp3h_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp3h_role`
--
ALTER TABLE `wp3h_role`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wp3h_wallet`
--
ALTER TABLE `wp3h_wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

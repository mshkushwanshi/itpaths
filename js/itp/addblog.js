let cat;
$.ajax({
  url: "https://itpaths.com/blog/getc"
}).done(function(d) {
    cat = $.parseJSON(d);
	$($("select[name='type']")[0]).empty();
	var o = new Option("Select Category", "");
    $($("select[name='type']")[0]).append(o);
    $.each($.parseJSON(d).filter(function(d){return d.pid == 0;}), function(i, c) {
        var o = new Option(c.title, c.id);
        if(c.title != "")
        $($("select[name='type']")[0]).append(o);
    });
}).fail(function(e){alert('Some error occurred!');});
$($("input[name='isCat']")[0]).change(function() {
    $.ajax({
      url: "https://itpaths.com/blog/getc"
    }).done(function(d) {
    	if($($("input[name='isCat']")[0]).prop('checked') == true){
        $($("select[name='getCat']")[0]).empty();
    	var o = new Option("Select Main Category", "");
        $($("select[name='getCat']")[0]).append(o);    
        $.each($.parseJSON(d).filter(function(d){return d.pid == 0;}), function(i, c) {
                
                var o = new Option(c.title, c.id);
    			$($("select[name='getCat']")[0]).append(o);
                $('.getCat').show();
            });
    	}
    }).fail(function(e){alert('Some error occurred!');});
});
$("#csubmit").click(function(){
   $.post(
        "https://itpaths.com/blog/addc", {getCat: $($("select[name='getCat']")[0]).val(), cname: $($("input[name='cname']")[0]).val(), descr: $($("textarea[name='descr']")[0]).val()}
    ).done(function(d) {
    	if($($("input[name='isCat']")[0]).prop('checked') == true){
            $.each($.parseJSON(d).filter(function(d){return d.pid == 0;}), function(i, c) {
                var o = new Option(c.title, c.id);
                $($("select[name='getCat']")[0]).empty();
    			$($("select[name='getCat']")[0]).append(o);
			if(c.title != "")
                $($("select[name='getCat']")[0]).show();
            });	
    	}
    $.ajax({
          url: "https://itpaths.com/blog/getc"
        }).done(function(d) {
            cat = $.parseJSON(d);
        	$($("select[name='type']")[0]).empty();
        	var o = new Option("Select Category", "");
            $($("select[name='type']")[0]).append(o);
            $.each($.parseJSON(d).filter(function(d){return d.pid == 0;}), function(i, c) {
                var o = new Option(c.title, c.id);
            if(c.title != "")
                $($("select[name='type']")[0]).append(o);
            });
        }).fail(function(e){alert('Some error occurred!');});
    }).fail(function(e){alert('Some error occurred!');}); 
});
$($("select[name='type']")[0]).change(function(){
	$($("select[name='ctype']")[0]).hide();
	$($("select[name='ctype']")[0]).empty();
	var o = new Option("Select Category", "");
    $($("select[name='ctype']")[0]).append(o);
    $.each(cat.filter(function(d){return d.pid == $($("select[name='type']")[0]).val();}), function(i, c) {
        var o = new Option(c.title, c.id);
        $($("select[name='ctype']")[0]).append(o);
		$($("select[name='ctype']")[0]).show();
    });
});
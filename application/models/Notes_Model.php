<?php
class Notes_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	
	public function addNotes($data){
		$this->db->insert('dnotes', $data);
		return true;
	}
	
	public function getAllNotes($id){
		$this->db->select('*');
		$this->db->from('dnotes');
		$this->db->where('uid',$id);
		$this->db->order_by("cdate", "desc");
		if($query=$this->db->get()){
			return $query->result();
		}
		else{
			return false;
		}
	}
	
	public function isNoteExist($data){
		$this->db->select('*');
		$this->db->from('dnotes');
		$this->db->where('ntitle',$data['ntitle']);
		$this->db->where('ndetail',$data['ndetail']);
		$this->db->where('uid',$data['uid']);
		$query=$this->db->get();
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public function update($data){
		$this->db->set('ntitle', "'".$data['ntitle']."'", FALSE);
		$this->db->set('ndetail', "'".$data['ndetail']."'", FALSE);
		$this->db->set('mdate', "'".$data['mdate']."'", FALSE);
		$this->db->where('nid', $data['nid']);
		$this->db->update('dnotes');
		return true;
	}
	
	public function getNoteById($id){
		$this->db->select('*');
		$this->db->from('dnotes');
		$this->db->where('nid',$id);
		return $this->db->get()->result_array();
	}
	
	public function delete($id){
		$sql = $this->db->select(array('nid'))->where('nid',$id)->get_compiled_select('dnotes', FALSE);
		$data = $this->db->get()->result_array();
		if(!empty($data)){
			$this->db->delete('dnotes', array('nid' => $id));
			return true;
		}
		else
			return false;
	}
}
?>
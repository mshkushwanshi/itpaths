<?php
class Blog_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	
	public function addBlog($data){
		$this->db->insert('bs_blog', $data);
		return true;
	}
	
	public function getBlog($id){
		$this->db->select(array('s.title as ctitle', 'bid as id', 'btitle', 'cdate', 'mdate', 'bcontent', 's.title as mtitle', 't_name as username', 's.parentid as pid', 'keywords', 'uid as u_id', 'm.description'));
		$this->db->from('bs_blog');
		$this->db->join('bs_category s', 'btype = s.id'); 
		$this->db->join('bs_category m', 's.parentid = m.id'); 
		$this->db->join('duser', 'uid = t_id'); 
		$this->db->where('bid',$id);
		$query=$this->db->get();
		if($query->num_rows() == 0){
			$this->db->select(array('s.title as ctitle', 'bid as id', 'btitle', 'cdate', 'mdate', 'bcontent', 's.title as mtitle', 't_name as username', 's.parentid as pid', 'keywords', 'uid as u_id'));
    		$this->db->from('bs_blog');
    		$this->db->join('bs_category s', 'btype = s.id');
    		$this->db->join('duser', 'uid = t_id'); 
    		$this->db->where('bid',$id);
    		$query = $this->db->get();
		}
		if(true){
			return $query->result();
		}
		else{
			return false;
		}
	}
	
	public function getAllBlog($id){
	    $data = array();
	    $this->db->select(array('title as ctitle','parentid as pid', 'description as cdescr'));
		$this->db->from('bs_category'); 
		$this->db->where('id',$id);
		if($query=$this->db->get()){
			$data['ctitle'] = $query->result();
		}
		
		$this->db->select(array('bid as id', 'btitle', 'cdate', 'bcontent',  't_name as username'));
		$this->db->from('bs_blog');
		$this->db->join('bs_category', 'btype = id'); 
		$this->db->join('duser', 'uid = t_id'); 
		$this->db->where('btype', $id);
		if($query=$this->db->get()){
			$data['data'] = $query->result();
		}
		return $data;
	}
	
	public function isBlogExist($data){
		$this->db->select('*');
		$this->db->from('bs_blog');
		$this->db->where('btitle',$data['btitle']);
		$this->db->where('bcontent',$data['bcontent']);
		$this->db->where('uid',$data['uid']);
		$query=$this->db->get();
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public function update($data){
	    $this->db->set($data);
	    $this->db->where('bid', $data['bid']);
		$this->db->update('bs_blog');
		return true;
	}
	
	public function getBlogById($id){
		$this->db->select('*');
		$this->db->from('bs_blog');
		$this->db->where('bid',$id);
		return $this->db->get()->result_array();
	}
	
	public function delete($id){
		$sql = $this->db->select(array('bid'))->where('bid',$id)->get_compiled_select('bs_blog', FALSE);
		$data = $this->db->get()->result_array();
		if(!empty($data)){
			$this->db->delete('bs_blog', array('bid' => $id));
			return true;
		}
		else
			return false;
	}
	
	public function getCat($isBlog){
	    if($isBlog){
	    $this->db->select('id, title, parentid as pid, count(id) as tblog');
		$this->db->from('bs_blog');
		$this->db->group_by('id');
		$this->db->join('bs_category s', 'btype = s.id'); 
		return $this->db->get()->result_array();
	    }
	    else{
	        $this->db->select('id, title, parentid as pid');
    		$this->db->from('bs_category');
    		return $this->db->get()->result_array();
    	}
	}
	
	public function addCat($data){
	    $this->db->insert('bs_category', $data);
		return true;
	}
}
?>
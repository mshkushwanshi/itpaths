<?php
class Order_Model extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	
	public function addOrder($data){
		$this->db->insert('bs_order', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	
	public function getOrder($id){
	    $this->db->select('*');
		$this->db->from('duser');
		$this->db->where('t_id',$id);
		$udata=$this->db->get()->result();
		$this->db->select(array('s.fname as fn', 'id as id', 's.lname as ln', 'timestamp as dt', 's.email as eml', 's.phone as phn', 's.add1 as ad1', 's.add2 as ad2', 's.city as cty', 's.state as stat', 's.zip as pin', 's.country as cntry', 's.services as svcs', 's.price as amount', 's.uid as ud', 'u.t_name as uname', 's.detail as dtl', 's.aid as dia'));
		$this->db->from('bs_order s');
		$this->db->join('duser u', 'u.t_id = s.uid');
		if(array_key_exists(0, $udata))
		if($udata[0]->t_role != "1"){
		    $this->db->where('s.uid',$id);
		}
		if($query=$this->db->get()){
			return $query->result();
		}
		else{
			return false;
		}
	}
	
	public function getAllOrder($id){
	    $data = array();
	    $this->db->select(array('title as ctitle','parentid as pid', 'description as cdescr'));
		$this->db->from('bs_category'); 
		$this->db->where('id',$id);
		if($query=$this->db->get()){
			$data['ctitle'] = $query->result();
		}
		
		$this->db->select(array('bid as id', 'btitle', 'cdate', 'bcontent',  't_name as username'));
		$this->db->from('bs_order');
		$this->db->join('bs_category', 'btype = id'); 
		$this->db->join('duser', 'uid = t_id'); 
		$this->db->where('btype', $id);
		if($query=$this->db->get()){
			$data['data'] = $query->result();
		}
		return $data;
	}
	
	public function isOrderExist($data){
		$this->db->select('*');
		$this->db->from('bs_order');
		$this->db->where('btitle',$data['btitle']);
		$this->db->where('bcontent',$data['bcontent']);
		$this->db->where('uid',$data['uid']);
		$query=$this->db->get();
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
	}
	
	public function update($data){
	    $this->db->set('btitle', "'".$data['btitle']."'", FALSE);
		$this->db->set('bcontent', "'".$data['bcontent']."'", FALSE);
		$this->db->set('mdate', "'".$data['mdate']."'", FALSE);
		$this->db->where('bid', $data['bid']);
		$this->db->update('bs_order');
		return true;
	}
	
	public function getOrderById($id){
		$this->db->select('*');
		$this->db->from('bs_order');
		$this->db->where('bid',$id);
		return $this->db->get()->result_array();
	}
	
	public function delete($id){
		$sql = $this->db->select(array('bid'))->where('bid',$id)->get_compiled_select('bs_order', FALSE);
		$data = $this->db->get()->result_array();
		if(!empty($data)){
			$this->db->delete('bs_order', array('bid' => $id));
			return true;
		}
		else
			return false;
	}
	
	public function getCat($isOrder){
	    if($isOrder){
	    $this->db->select('id, title, parentid as pid, count(id) as tOrder');
		$this->db->from('bs_order');
		$this->db->group_by('id');
		$this->db->join('bs_category s', 'btype = s.id'); 
		return $this->db->get()->result_array();
	    }
	    else{
	        $this->db->select('id, title, parentid as pid');
    		$this->db->from('bs_category');
    		return $this->db->get()->result_array();
    	}
	}
	
	public function addCat($data){
	    $this->db->insert('bs_category', $data);
		return true;
	}
}

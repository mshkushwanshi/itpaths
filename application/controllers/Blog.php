<?php
defined('BASEPATH') or exit('bo direct script access allowed');

class Blog extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Blog_Model');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url', 'text'));
	}
	public function about()
	{
		$header['description'] = "IT Paths knowledge base, a sub division of tapasvi info solution, where you will get all type IT solution information";
		$header['keywords'] = "IT Paths, Tapasvi Info Solution, IT Paths Knowledge Base, ITP Knowledge Base";
		$header['title'] = "About Us | ITPaths";
		$header['cats'] = $this->Blog_Model->getCat(true);
		$this->load->view('blog/header', $header);
		$this->load->view('blog/about');
		$this->load->view('blog/footer');
	}
	public function index()
	{
	    $header['description'] = "IT Paths knowledge base, a sub division of tapasvi info solution, where you will get all type IT solution information";
		$header['keywords'] = "IT Paths, Tapasvi Info Solution, IT Paths Knowledge Base, ITP Knowledge Base";
		$header['title'] = "Contents | ITPaths";
		$data['cats'] = $this->Blog_Model->getCat(true);
		$data['ctitle'] = "Our Blogs";
		$data['descr'] = "Our blogs by their categories";
		$header['cats'] = $this->Blog_Model->getCat(true);
		$bdata = array();
		foreach($data['cats'] as $cat){
		    $bdata[$cat['id']] = array('blogs'=> $this->Blog_Model->getAllBlog($cat['id'])['data'], 'cat' => $cat);
		}
		$data['bdata'] = $bdata;
		$this->load->view('blog/header', $header);
		$this->load->view('blog/index', $data);
		$this->load->view('blog/footer');
	}
	public function kb()
	{
		$header['description'] = "IT Paths knowledge base, a sub division of tapasvi info solution, where you will get all type IT solution information";
		$header['keywords'] = "IT Paths, Tapasvi Info Solution, IT Paths Knowledge Base, ITP Knowledge Base";
		$header['title'] = "ITPaths KB";
		$header['cats'] = $this->Blog_Model->getCat(true);
		$this->load->view('blog/header', $header);
		$this->load->view('blog/kb');
		$this->load->view('blog/footer');
	}
	public function contact()
	{
		$header['description'] = $data['blogs'][0]->btitle;
		$header['keywords'] = $data['blogs'][0]->keywords;
		$header['title'] = "Contact Us | ITPaths";
		$header['cats'] = $this->Blog_Model->getCat(true);
		$this->load->view('blog/header', $header);
		$this->load->view('blog/contact');
		$this->load->view('blog/footer');
	}
	public function insert()
	{
	    $header = array();
	    $session = $this->session->get_userdata();
		$session = $this->session->get_userdata();
		if (!array_key_exists("id", $session)) {
			$this->session->set_flashdata('error_msg', 'Invalid access attempt!');
			redirect('user');
		}
		else
		    $header['id'] = $session['id'];
		$header['description'] = "";
		$header['descr'] = "";
		$header['ctitle'] = "Add Blog";
		$header['title'] = $header['description'];
		$header['cats'] = $this->Blog_Model->getCat(true);
		$this->load->view("blog/header", $header);
		$this->load->view('blog/addBlog');
		$this->load->view("blog/footer");
	}
	public function add()
	{
		$session = $this->session->get_userdata();
		if (!array_key_exists("id", $session)) {
			$this->session->set_flashdata('error_msg', 'Invalid access attempt!');
			redirect('user/index/blog/insert');
		}
		
		if (empty($this->input->post()))
			redirect('blog/insert');
		$error = true;
		$now = new DateTime();
		$id = $this->input->post('id');
		if ($this->input->post('ctype') == "")
			$type = $this->input->post('type');
		else
			$type = $this->input->post('ctype');
		$title = $this->input->post('title');
		$keywords = $this->input->post("keywords");
		$description = $this->input->post("bdescr");
		$content = $this->input->post('content');
		if ($this->input->post('content') != "") {
			if ($title == "") {
				$title = substr($this->input->post('content'), 0, 20);
			}
			$data = array(
				'btitle' => $title,
				'btype' => $type,
				'cdate' => $now->format('Y-m-d H:i:s'),
				'mdate' => $now->format('Y-m-d H:i:s'),
				'bcontent' => base64_encode($content),
				'uid' => $this->session->get_userdata()['id'],
				'b_descr'=>$description,
				'keywords'=>$keywords
			);
			if ($id == "") {
				$validateData = array('btitle' => $title, 'bcontent' => base64_encode($content), 'uid' => $this->session->get_userdata()['id']);
				if (!$this->Blog_Model->isBlogExist($validateData)) {
					$this->Blog_Model->addBlog($data);
					$this->session->set_flashdata('success_msg', 'Blog is added successfully!');
					$error = false;
					redirect(base_url("blog/index/") . $data['bid']);
				} else {
					$this->session->set_flashdata('error_msg', 'Blog already exist!');
				}
			} else {
				$data['bid'] = $id;
				$data['mdate'] = $now->format('Y-m-d H:i:s');
				$this->Blog_Model->update($data);
				$this->session->set_flashdata('success_msg', 'Blog updated successfully!');
				redirect(base_url("blog/index/") . $data['bid']);
			}
		} else {
			$this->session->set_flashdata('error_msg', 'Invalid operation attempt!');
			redirect(base_url("blog/index/") . $data['bid']);
		}
		if ($type == 'ws') {
		} else {
			if (!$error)
				redirect(base_url("blog/index/?stat=$error_msg") + $type);
			else
				redirect(base_url("blog/insert"));
		}
	}
	public function edit()
	{
		$header = array();
	    $session = $this->session->get_userdata();
		$session = $this->session->get_userdata();
		if (!array_key_exists("id", $session)) {
			$this->session->set_flashdata('error_msg', 'Invalid access attempt!');
			redirect('user');
		}
		else
		    $header['id'] = $session['id'];
		$header['description'] = "";
		$header['descr'] = "";
		$header['ctitle'] = "Update Blog";
		$header['title'] = $header['description'];
		$header['cats'] = $this->Blog_Model->getCat(true);
		$id = $this->uri->segment(3);
		$now = new DateTime();
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		$data['blog'] = $this->Blog_Model->getBlogById($id);
		$data['mode'] = 'edit';
		$data['url'] = "blog/get/$id";
		$this->load->view("blog/header", $header);
		$this->load->view('blog/addBlog', $data);
		$this->load->view("blog/footer");
	}
	function get()
	{
		$session = $this->session->get_userdata();
		if (isset($session['id']))
			$header = array("id" => $session['id']);
		$data['blogs'] = $this->Blog_Model->getBlog($this->uri->segment(3));
		$data['bid'] = $this->uri->segment(3);
		if (isset($data['blogs'][0])) {
			$header['description'] = $data['blogs'][0]->btitle;
			$header['keywords'] = $data['blogs'][0]->keywords;
		} else {
			$header['description'] = "";
			$header['keywords'] = "";
		}
		$header['title'] = $header['description'];
		$header['cats'] = $this->Blog_Model->getBlog($this->uri->segment(3));
		$this->load->view("blog/header", $header);
		$this->load->view('blog/listBlog', $data);
		$this->load->view("blog/footer");
	}
	public function delete()
	{
		$id = $this->uri->segment(3);
		if ($id == "") {
			$this->session->set_flashdata('error_msg', 'Invalid delete attempt!!');
			redirect("Blog/index/" . $id);
		} else {
			if ($this->Blog_Model->delete($id)) {
				$this->session->set_flashdata('success_msg', 'Blog has been deleted successfully!');
				redirect('blog/index');
			} else {
				$this->session->set_flashdata('error_msg', 'Blog was already deleted!');
				redirect('blog/index/$id');
			}
		}
		//$this->Blog_Model->delete($this->input->post('id'));
	}
	public function getc()
	{
		echo json_encode($this->Blog_Model->getCat(false));
	}
	public function addc()
	{
		if ($this->input->post('cname') == "") {
			echo "Something went wrong, check your inputs!";
			return;
		}
		$data = array('title' => $this->input->post('cname'), 'description' => $this->input->post('descr'));
		if ($this->input->post('getCat') == "")
			$data['parentid'] = 0;
		$this->Blog_Model->addCat($data);
		echo "Category added!";
	}
	public function blogs()
	{
		$data['data'] = $this->Blog_Model->getAllBlog($this->uri->segment(3));
		$header['description'] = "IT Paths knowledge base, a sub division of tapasvi info solution, where you will get all type IT solution information";
		$header['keywords'] = "IT Paths, Tapasvi Info Solution, IT Paths Knowledge Base, ITP Knowledge Base";
		$header['title'] = "Contents | ITPaths";
		$header['cats'] = $this->Blog_Model->getCat(true);
		$this->load->view('blog/header', $header);
		$this->load->view('blog/index', $data);
		$this->load->view('blog/footer');
	}

	public function upload()
	{
	    $config = array();
	    $arr = array();
	    if(isset($_FILES['userfile'])){
	        $arr = explode(".",strtolower($_FILES['userfile']['name']));
    	    if($arr[count($arr) - 1] == "zip" || $arr[count($arr) - 1] == "rar" || $arr[count($arr) - 1] == "tar" || $arr[count($arr) - 1] == "7z"){
    	        $config['upload_path']          = $_SERVER['DOCUMENT_ROOT'].'/bundle/';    
    	    }
    	    else{
    	        $config['upload_path']          = $_SERVER['DOCUMENT_ROOT'].'/blog/img/';
    	    }
    	    $config['allowed_types']        = 'gif|jpg|png|jpeg|bpm|tif|txt|doc|docx|xls|xlsx|ppt|pptx|zip|rar|tar|7z|ZIP|TAR|RAR|7Z';
    		$config['max_size']             = 0;
    		$config['max_width']            = 0;
    		$config['max_height']           = 0;
    		$this->load->library('upload', $config);
    		$this->upload->initialize($config);
	    }
	    $data = array('upload_data' => '');
		if (!$this->upload->do_upload('userfile')) {
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload', $error);
		} else {
			$data = array('upload_data' => $this->upload->data());
			$this->load->view('upload', $data);
		}
	}
}
?>
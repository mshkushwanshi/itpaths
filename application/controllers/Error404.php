<?php
class Error404 extends CI_Controller{
   public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url', 'text'));
   }

   public function index(){
        $header['description'] = "We couldn't find the place, what you're looking for. But still you can try below helping locations.";
	    $header['keywords'] = "404 IT Paths, 404 Tapasvi, 404 Tapasvi Info";
		$header['title'] = "404 | ITPaths";
        $this->output->set_status_header('404');
        $this->load->view('solution/header', $header);
        $this->load->view('e_404');
        $this->load->view('solution/footer');
   }
}
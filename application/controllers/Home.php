<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function eindex(){
		$this->load->view('welcome_message');
	}
	public function editor() {
		$this->load->view('editor');
	}
	public function typography(){
		$this->load->helper('url');
		$this->load->view('itplay/header');
		$this->load->view('itplay/leftside');
		$this->load->view('itplay/typography');
		$this->load->view('itplay/footer');
	}
	public function index(){
		$this->load->helper('url');
		$this->load->view('itplay/header');
		$this->load->view('itplay/app');
		$this->load->view('itplay/signup');
		
		$this->load->view('itplay/leftside');
		$this->load->view('itplay/index');
		$this->load->view('itplay/footer');
	}
}

<?php
class Order extends CI_Controller {
	public function __construct(){
        parent::__construct();
  		$this->load->helper('url');
		$this->load->model('Order_Model');
		$this->load->model('Blog_Model');
		$this->load->library('session');
	}
	public function index(){
		$session = $this->session->get_userdata();
	    $id = "1";
	    if(array_key_exists ( "id" , $session )){
		    $id = $session['id'];
		}
	    $this->load->view("order/header", array("title"=>"New Order | ITPaths", "id"=> $id, 'cats' => $this->Blog_Model->getCat(true)));
		$this->load->view("order/new");
		$this->load->view("order/footer");
	}
	public function submit(){
	    $idt = base64_decode($_POST['idt']);
	    $price = str_replace("idt","",str_replace("##","",$idt)) * 100;
	    $fname = $_POST['fname'];
	    $lname = $_POST['lname'];
	    $email = $_POST['email'];
	    $phone = $_POST['phone'];
	    $add1 = $_POST['ad1'];
	    $add2 = $_POST['ad2'];
	    $mobile = $_POST['mobile'];
	    $city = $_POST['city'];
	    $state = $_POST['state'];
	    $country = $_POST['country'];
	    $pin = $_POST['pin'];
	    $services = $_POST['svcss'];
	    $id = $_POST['id'];
	    $fsdtl = $_POST['fsdtl'];
	    $aid = $_POST['ida'];
	    $data = array(
	            "fname" => $fname, "lname" => $lname, "email" => $email, "phone" => $phone.",".$mobile, "add1" => $add1 , "add2" => $add2, "city" => $city, "state" => $state,  "country" => $country,  "zip" => $pin,  "services" => $services, "price" => $price, "uid" => $id,
	                "detail" => $fsdtl,
	                "aid" => $aid
	        );
		$oid = $this->Order_Model->addOrder($data);
		
		$from_email = "info@itpaths.com"; 
        $to_email = $email; 
        if($email != null)
         //Load email library 
         $this->load->library('email'); 
         $this->email->set_mailtype("html");
         $this->email->from($from_email, $this->input->post('name')); 
         $this->email->to($to_email);
         $this->email->subject('Thank You - ITPaths'); 
         $this->email->message('<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"><link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
		 <img src="https://www.itpaths.com/img/logo.png" class="blog-logo" width="100px"><br><h3>Thank you for placing your order, <br>Once your order will be ready, you will get a preview URL to check your quick website layout. As per your approval, we\'ll publish your website to live. <br><br> Once you\'ve approved it, you will be asked for payment. <br><br>Help us to complete your payment to get your order completed as soon as possible.<br><br>Thanks for coosing our services!<br>We\'ll be glad to serve you ever. In case of any query, <br>You may also reach us through mobile or skype at:</h3><br>
         <table>
            <tr>
                <td><b><i class="fa fa-mobile" aria-hidden="true"></i></b></td><td><b> +91 -  9560939040</b></td>
            </tr>
            <tr>
                <td><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></td><td><b> +91 - 9555035440</b></td>
            </tr>
            <tr>
            <td><b>Skype </b></td><td><b>  u_mesh1</b></td>
            </tr>
        </table>
        <br><br><br>   Regards,<br>ITPaths<br>Tapasvi Info Solutions');
   
		$this->email->send();
		
		$json = file_get_contents("http://ipinfo.io/".$_SERVER['REMOTE_ADDR']."/geo");
		$json = json_decode($json, true);
		$country = "No data"; $region = "No data"; $city = "No data"; $loc = "No data"; $timezone = "No data"; $postal = "No data";
		if(isset($json['country']))
		$country  = $json['country'];
		if(isset($json['region']))
		$region   = $json['region'];
		if(isset($json['city']))
		$city     = $json['city'];
		if(isset($json['loc']))
		$loc     = $json['loc'];
		if(isset($json['timezone']))
		$timezone     = $json['timezone'];
		if(isset($json['postal']))
		$postal     = $json['postal'];
		
		$contactQuery = "<link href='https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk' crossorigin='anonymous'><link rel='stylesheet' href='https://pro.fontawesome.com/releases/v5.10.0/css/all.css' integrity='sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p' crossorigin='anonymous'/>
		<img src='https://www.itpaths.com/img/logo.png' class='blog-logo' width='100px'><h3>Contact Query</h3><br>Person's device IP: ".$_SERVER['REMOTE_ADDR']."<br>".
						"Country: ".$country."<br>".
						"Region: ".$region."<br>".
						"Citry: ".$city."<br>".
						"Timezone: ".$timezone."<br>".
						"Postal: ".$postal."<br>".
						"Location: ".$loc."<br>".
						"Device Detail: ".json_encode($_SERVER['HTTP_USER_AGENT'])."<br><br>".
						$this->input->post('query');
        $this->email->from($to_email, $fname." ".$lname); 
        $this->email->to($from_email);
        $this->email->subject("Order Request - " + $oid); 
		$this->email->message($contactQuery);
		echo json_encode(array("status"=>"success", "oid"=>base64_encode($oid)));
	}
	public function pay(){
	    $this->load->view("order/header", array("title"=>"Submit Order | ITPaths"));
		$this->load->view("order/pay");
		$this->load->view("order/footer");
	}
	public function get(){
	    $session = $this->session->get_userdata();
	    $id = "";
	    if(!array_key_exists ( "id" , $session )){
			$this->session->set_flashdata('error_msg', 'Invalid access attempt!');
			redirect('user/index/order/get');
		}
		else
		    $id = $session['id'];
		$data['orders'] = $this->Order_Model->getOrder($this->session->get_userdata()['id']);
		$this->load->view("order/header", array("title"=>"My Orders", "id" => $id, 'cats' => $this->Blog_Model->getCat(true)));
		$this->load->view('order/get', $data);
		$this->load->view("order/footer");
	}
}
?>
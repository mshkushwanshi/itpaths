<?php
class User extends CI_Controller {
	public function __construct(){
        parent::__construct();
  		$this->load->helper('url');
  	 	$this->load->model('User_Model');
        $this->load->library('session');
	}
	public function index(){
	    if(empty($this->session->get_userdata()['id'])){
	        $data['uri3'] = $this->uri->segment(3);
			$this->load->view("blog/header", array("title"=>"Login | ITPaths"));
			$this->load->view("login/login", $data);
			$this->load->view("blog/footer");
		}
		else if($this->uri->segment(3) == "blog")
			redirect('blog/get');
		else if($this->uri->segment(3) == "notes")
		    redirect('notes/get');
	}
	public function registerJson(){
		$now = new DateTime();
		echo json_encode($this->input->post());
		$user=array(
			't_name'=>$this->input->post('fname')." ".$this->input->post('lname'),
			't_username'=>$this->input->post('username'),
			't_email'=>$this->input->post('email'),
			't_pass'=>md5($this->input->post('password')),
			't_os'=>$this->input->post('os'),
			't_appname'=>$this->input->post('appname'),
			't_loc'=>$this->input->post('loc'),
			't_datetime'=>$now->format('Y-m-d H:i:s'),
			't_role'=>3,
			't_contact'=>$this->input->post('phone')
		);
        $email_check=$this->User_Model->email_check($user['t_email']);
		if($email_check){
		  $this->User_Model->register_user($user);
		  echo json_encode( array('success_msg' => 'Registered successfully.Now login to your account.') );
		}
		else{
		  echo json_encode( array('error_msg', 'Email already registered with us!'));
		}
	}
	public function subscribe(){
		$this->load->view("blog/header", array("title"=>"Register | ITPaths"));
		$this->load->view('login/register');
		$this->load->view("blog/footer");
	}
	
	public function register(){
		$now = new DateTime();
		if(empty($this->input->post())){
			redirect('user/subscribe');
		}
		else if(empty($this->input->post('email')) || empty($this->input->post('username')) || empty($this->input->post('password'))){
			$this->session->set_flashdata('error_msg', 'Invalid registration information!');
			redirect('user/subscribe');
		}
		$user=array(
			't_name'=>$this->input->post('fname')." ".$this->input->post('lname'),
			't_username'=>$this->input->post('username'),
			't_email'=>$this->input->post('email'),
			't_pass'=>md5($this->input->post('password')),
			't_os'=>$this->input->post('os'),
			't_appname'=>$this->input->post('appname'),
			't_loc'=>"",
			't_datetime'=>$now->format('Y-m-d H:i:s'),
			't_role'=>3,
			't_contact'=>$this->input->post('phone')
		);
        $email_check=$this->User_Model->email_check($user['t_email']);
		if($email_check){
		  $this->User_Model->register_user($user);
		  $this->session->set_flashdata('success_msg', 'Registered successfully.Now login to your account.');
		  redirect('user/login');
		}
		else{
		  $this->session->set_flashdata('error_msg', 'Email already registered with us!');
		  redirect('user');
		}
	}
	public function login(){
	    $data = array("uri3" => $this->uri->segment(3));
	    $header['description'] = "IT Paths knowledge base, a sub division of tapasvi info solution, where you will get all type IT solution information";
		$header['keywords'] = "IT Paths, Tapasvi Info Solution, IT Paths Knowledge Base, ITP Knowledge Base";
		$header['title'] = "Login | ITPaths";
		$this->load->view("blog/header", $header);
		$this->load->view("login/login", $data);
		$this->load->view("blog/footer");
	}
	function validate(){
		$user_login=array(
			'email'=>$this->input->post('email'),
			'password'=>md5($this->input->post('password'))
		);
		$data=$this->User_Model->login_user($user_login['email'],$user_login['password']);
		if($data){
			$this->session->set_userdata('id',$data['t_id']);
			$this->session->set_userdata('email',$data['t_email']);
			$this->session->set_userdata('username',$data['t_username']);
			$this->session->set_userdata('os',$data['t_os']);
			$this->session->set_userdata('appname',$data['t_appname']);
			if($this->uri->segment(3) == "blog")
    			redirect('blog/get');
    		else if($this->uri->segment(3) == "notes")
    		    redirect('notes/get');
    		else if($this->uri->segment(3) == "order")
    		    redirect('order/get');
    		else
    		    redirect('notes/get');
		}
		else{
			$this->session->set_flashdata('error_msg', 'Invalid user details!');
			$this->load->view("blog/header", array("title"=>"Login | ITPaths"));
			$this->load->view("login/login");
			$this->load->view("blog/footer");
		}
	}
	function validateJson(){
		$user_login=array(
			'email'=>$this->input->post('email'),
			'password'=>md5($this->input->post('password'))
		);
		$data=$this->User_Model->login_user($user_login['email'],$user_login['password']);
		if($data){
			echo json_encode( $arr );
		}
		else{
			echo json_encoder(array("error" => "Invalid user details!"));
		}
	}
	function profile(){
		$this->load->view("blog/header", array("title"=>"My Profile | ITPaths"));
		$this->load->view('login/user_profiles');
		$this->load->view("blog/footer");
	}
	public function logout(){
		$this->session->sess_destroy();
		redirect('user/login', 'refresh');
	}
}
?>
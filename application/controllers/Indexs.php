<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
-header('Access-Control-Allow-Methods: GET, POST');

class Indexs extends CI_Controller {
	private $data;
	public function __construct(){
        parent::__construct();
		$this->load->helper('url');
		$this->load->helper('text');
  	 	$this->load->model('Blog_Model');
		$this->load->library('session');
	}
	public function editor() {
		$this->load->view('editor');
	}
	public function typography(){
		$data = array("title" => "Typography | IT Paths");
		$data['cats'] = $this->Blog_Model->getCat(true);
		$this->load->helper('url');
		$this->load->view('solution/header', $data);
		$this->load->view('solution/leftside');
		$this->load->view('solution/typography');
		$this->load->view('solution/footer');
	}
	public function index(){
		$data = array("title" => "Digitally Automated Solution to Your Business | IT Paths | Tapasvi Info Solutions");
		$data['cats'] = $this->Blog_Model->getCat(true);
		$this->load->helper('url');
		$this->load->view('solution/header', $data);
		$this->load->view('solution/index');
		$this->load->view('solution/footer');
	}
	public function solution(){
		$data = array("title" => "Solution | IT Paths");
		$data['cats'] = $this->Blog_Model->getCat(true);
		$this->load->helper('url');
		$this->load->view('solution/header', $data);
		$this->load->view('solution/solution');
		$this->load->view('solution/footer');
	}
	public function scholarship(){
	    $data = array("title" => "Scholarship | IT Paths");
		$data['cats'] = $this->Blog_Model->getCat(true);
		$this->load->helper('url');
		$this->load->view('solution/header', $data);
		$this->load->view('solution/scholarship');
		$this->load->view('solution/footer');
	}
	public function gallery(){
		$data = array("title" => "Gallery | IT Paths");
		$data['cats'] = $this->Blog_Model->getCat(true);
		$this->load->helper('url');
		$this->load->view('solution/header', $data);
		$this->load->view('solution/gallery');
		$this->load->view('solution/footer');
	}
	public function blog(){
		$data = array("title" => "Blog | IT Paths");
		$data['cats'] = $this->Blog_Model->getCat(true);
		$this->load->helper('url');
		$this->load->view('solution/header', $data);
		$this->load->view('solution/blog');
		$this->load->view('solution/footer');
	}
	public function events(){
		$data = array("title" => "Event | IT Paths");
		$data['cats'] = $this->Blog_Model->getCat(true);
		$this->load->helper('url');
		$this->load->view('solution/header', $data);
		$this->load->view('solution/events');
		$this->load->view('solution/footer');
	}
	public function contact(){
	    $data = array("title" => "Contact | IT Paths");
		$data['cats'] = $this->Blog_Model->getCat(true);
		$this->load->helper('url');
		$this->load->view('solution/header', $data);
		$this->load->view('solution/contact');
		$this->load->view('solution/footer');
	}
	public function projects(){
	    $data = array("title" => "Contact | IT Paths");
		$data['cats'] = $this->Blog_Model->getCat(true);
		$this->load->helper('url');
		$this->load->view('solution/header', $data);
		$this->load->view('solution/projects');
		$this->load->view('solution/footer');
	}
	public function submit(){
	    $from_email = "info@itpaths.com"; 
        $to_email = $this->input->post('email'); 
        if($this->input->post('email') != null)
            echo "<script>var success = true;</script>";
         //Load email library 
         $this->load->library('email'); 
         
		if(!empty($this->input->post('query'))){
			$this->email->set_mailtype("html");
			$this->email->from($from_email, $this->input->post('name')); 
			$this->email->to($to_email);
			$this->email->subject('Thank You - ITPaths'); 
			$this->email->message('<img src="https://www.itpaths.com/img/logo.png" class="blog-logo" width="100px"><br><h3>Thank you for writing to us, we will reach you shortly.<br>You may also reach us through mobile or skype at:</h3><br>
				<table>
				<tr>
					<td><b>Mobile </b></td><td><b> +91 -  9560939040</b></td>
				</tr>
				<tr>
					<td><b>Alternate Mobile </b></td><td><b> +91 - 9555035440</b></td>
				</tr>
				<tr>
				<td><b>Skype </b></td><td><b>  u_mesh1</b></td>
				</tr>
				</table>
				<br><br><br>   Regards,<br>ITPaths<br>Tapasvi Info Solutions');
			$this->email->send();
			$json = file_get_contents("http://ipinfo.io/".$_SERVER['REMOTE_ADDR']."/geo");
			$json = json_decode($json, true);
			$country = "No data"; $region = "No data"; $city = "No data"; $loc = "No data"; $timezone = "No data"; $postal = "No data";
			if(isset($json['country']))
			$country  = $json['country'];
			if(isset($json['region']))
			$region   = $json['region'];
			if(isset($json['city']))
			$city     = $json['city'];
			if(isset($json['loc']))
			$loc     = $json['loc'];
			if(isset($json['timezone']))
			$timezone     = $json['timezone'];
			if(isset($json['postal']))
			$postal     = $json['postal'];

			$contactQuery = $this->input->post('query') . "<br><br><h3>Additional Information: </h3><br>Person's device IP: ".$_SERVER['REMOTE_ADDR']."<br>".
							"Country: ".$country."<br>".
							"Region: ".$region."<br>".
							"Citry: ".$city."<br>".
							"Timezone: ".$timezone."<br>".
							"Postal: ".$postal."<br>".
							"Location: ".$loc."<br>".
							"Device Detail: ".json_encode($_SERVER['HTTP_USER_AGENT'])."<br><br>";
			$this->email->from($to_email, $this->input->post('name')); 
			$this->email->to($from_email);
			$this->email->subject($this->input->post('subject')); 
			$this->email->message($contactQuery); 
		
			//Send mail 
			if($this->email->send()) 
				$this->session->set_flashdata("email_sent","Email sent successfully.");
			else 
				$this->session->set_flashdata("email_sent","Error in sending Email."); 
					$this->load->helper('url');
		}
		$data = array("title" => "Contact | IT Paths");
		$data['cats'] = $this->Blog_Model->getCat(true);
		$this->load->helper('url');
		$this->load->view('solution/header', $data);
		$this->load->view('solution/contact');
		$this->load->view('solution/footer');
	}
}

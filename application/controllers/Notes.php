<?php
class Notes extends CI_Controller {
	public function __construct(){
        parent::__construct();
  		$this->load->helper('url');
		$this->load->helper('text');
  	 	$this->load->model('Notes_Model');
        $this->load->library('session');
		$session = $this->session->get_userdata();
		if(!array_key_exists ( "id" , $session )){
			$this->session->set_flashdata('error_msg', 'Invalid access attempt!');
			redirect('user/index/notes/get');
		}
	}
	public function index(){
		redirect('notes/get');
	}
	public function insert(){
	    $header = array("title"=>"New Note | ITP", "descr"=>"Add your personal notes, which are secured and only accessible by you. We understand sensitivity of our customer's and make sure that their personal data is not accessed to others", "keywords"=>"'itpaths notes', 'tapasvi info solutions', 'it paths', 'add notes', 'add new notes', 'tapasvi notes'", "ctitle"=>"New Note");
	    $this->load->view("notes/header", $header);
		$this->load->view('notes/addNotes');
		$this->load->view("blog/footer");
	}
	public function add(){
	    $session = $this->session->get_userdata();
	    if(!array_key_exists ( "id" , $session )){
			$this->session->set_flashdata('error_msg', 'Invalid access attempt!');
			redirect('user/index/notes/insert');
		}
		if(empty($this->input->post()))
			redirect('notes/insert');
		$error = true;
		$now = new DateTime();
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$title = $this->input->post('title');
		$content = $this->input->post('content');
		if($this->input->post('content') != ""){
			if($title == ""){
				$title = substr($this->input->post('content'), 0, 20);
			}
			$data=array(
				'ntitle'=>$title,
				'ntype'=>$this->input->post('type'),
				'cdate'=>$now->format('Y-m-d H:i:s'),
				'mdate'=>$now->format('Y-m-d H:i:s'),
				'remindtime'=>null,
				'ndetail'=>$content,
				'uid' => $this->session->get_userdata()['id']
			);
			if($id == ""){
				$validateData = array('ntitle'=>$title, 'ndetail'=>$this->input->post('content'), 'uid' => $this->session->get_userdata()['id']);
				if(!$this->Notes_Model->isNoteExist($validateData)){
					$this->Notes_Model->addNotes($data);
					$this->session->set_flashdata('success_msg', 'Note is added successfully!');
					$error = false;
				}
				else{
					$this->session->set_flashdata('error_msg', 'Note already exist!');
				}
			}
			else{
				$data['nid'] = $id;
				$data['mdate'] = $now->format('Y-m-d H:i:s');
				$this->Notes_Model->update($data);
				$this->session->set_flashdata('success_msg', 'Note updated successfully!');
				redirect("notes/get");
			}
		}
		else{
			$this->session->set_flashdata('error_msg', 'Invalid operation attempt!');
		}
		if($type == 'ws'){
			
		}
		else{
			if(!$error)
				redirect('notes/get');
			else
				redirect('notes/insert');
		}
		
	}
	public function edit(){
	    $id = $this->input->get('id', true);
		$now = new DateTime();
		$title = $this->input->post('title');
		$detail = $this->input->post('detail');
		$data['notes'] = $this->Notes_Model->getNoteById($id);
		$header = array("title"=>"Edit Note | ITP", "descr"=>"Add your personal notes, which are secured and only accessible by you. We understand sensitivity of our customer's and make sure that their personal data is not accessed to others", "keywords"=>"'itpaths notes', 'tapasvi info solutions', 'it paths', 'add notes', 'add new notes', 'tapasvi notes'", "ctitle"=>"Edit Note");
	    $this->load->view("notes/header", $header);
		$this->load->view('notes/addNotes',$data);
		$this->load->view("blog/footer");
	}
	
	function get(){
	    $session = $this->session->get_userdata();
	    if(!array_key_exists ( "id" , $session )){
			$this->session->set_flashdata('error_msg', 'Invalid access attempt!');
			redirect('user/index/notes/get');
		}
		$data['notes'] = $this->Notes_Model->getAllNotes($this->session->get_userdata()['id']);
		$this->load->view("notes/header", array("title"=>"My Notes | IT Paths"));
		$this->load->view('notes/listNotes', $data);
		$this->load->view("blog/footer");
	}
	
	public function delete(){
		$id = $this->input->get('id', true);
		if($id == ""){
			echo "Invalid delete attempt!";
		}
		else{
			if($this->Notes_Model->delete($id)){
				$this->session->set_flashdata('success_msg', 'Note has been deleted successfully!');
				redirect('notes/get');
			}
			else{
				$this->session->set_flashdata('error_msg', 'Note was already deleted!');
				redirect('notes/get');
			}
				
		}
		//$this->Notes_Model->delete($this->input->post('id'));
	}
}
?>
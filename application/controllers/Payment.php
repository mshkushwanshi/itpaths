<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Payment extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
    }
    
	public function index()
	{
	    $this->load->view('blog/header');
		$this->load->view('pay');
		$this->load->view('blog/footer');
	}
    
}
?>
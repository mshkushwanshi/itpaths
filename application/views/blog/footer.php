 <!-- Footer -->
 <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="https://twitter.com/clyde_vs_bonnie">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="https://www.facebook.com/itpaths/">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="https://github.com/umesh29k">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">&copy;<?php echo date("Y");?>&nbsp;|&nbsp;ITPaths&nbsp;|&nbsp;Tapasvi Info Solutions</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo site_url('blog/vendor/jquery/jquery.min.js');?>"></script>
  <script src="<?php echo site_url('blog/vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo site_url('blog/js/clean-blog.min.js');?>"></script>

</body>

</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="<?php echo base_url("ckeditor/ckeditor.js");?>"></script>
<header class="masthead" style="background-image: url('<?php echo site_url('blog/blog.jpeg');?>')">
<?php if(isset($blog)) echo "<script>var blogId = '".$blog[0]['btype']."';</script>";?>
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h2><?php 
            if(isset($data['ctitle']))
                echo $data['ctitle'][0]->ctitle; 
            else
                echo $ctitle; ?></h2>
            <span class="subheading">
                <?php if(isset($data['ctitle'])) echo $data['ctitle'][0]->cdescr; else echo $descr; ?>
            </span>
          </div>
        </div>
      </div>
    </div>
</header>
<div class="login-panel panel panel-success" style="margin:15px;">
  <div class="panel-heading">
     <h3 class="panel-title">blog</h3>
  </div>
  <div class="panel-body">
     <?php
     $error_msg = $this->session->flashdata('error_msg');
     if ($error_msg != null) {
     ?>
        <div class="alert alert-danger">
           <?php echo $error_msg; ?>
        </div>
     <?php } ?>
     <form role="form" method="post" action="<?php echo base_url('blog/add'); ?>">
        <?php
        if (!empty($blog)) {
           $title = $blog[0]['btitle'];
           $content = str_replace("$$", "\n", $blog[0]['bcontent']);
           $keywords = $blog[0]['keywords'];
           $descr = $blog[0]['b_descr'];
        ?>
           <input value="<?php echo $blog[0]['bid']; ?>" name="id" type="hidden" />
        <?php
        } else {
           $title = "";
           $content = "";
        }

        ?>
        <fieldset>
           <div class="form-group">
              <input class="form-control" placeholder="Title" name="title" type="text" value="<?php echo $title; ?>" autofocus>
              <input value="1" name="type" type="hidden" />
           </div>
           <div class="form-group">
              <input class="form-control" placeholder="Search Keywords" name="keywords" type="text" value="<?php if(isset($keywords)) echo $keywords; ?>" autofocus>
           </div>
           <div class="form-group">
              <input class="form-control" placeholder="Blog Description" name="bdescr" type="text" value="<?php if(isset($descr)) echo $descr; ?>" autofocus>
           </div>
           <div class="form-group">
              <select name="type"></select>
              <select name="ctype" hidden></select>
              <a href="#ex1" rel="modal:open" class="btn btn-sg btn-primary">Create New?</a>
              <div id="fade"></div>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
              <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
              <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
              <div id="ex1" class="modal">
                 <div class="form-group">
                    <input class="form-control" placeholder="Category Title" name="cname" type="text" value="" autofocus>
                    <input type="checkbox" name="isCat" />&nbsp;Is this sub - category?
                 </div>
                 <div class="form-group getCat" hidden>
                    <select name="getCat">
                       <option value="">Select Category</option>
                    </select>
                 </div>
                 <div class="form-group">
                    <textarea class="form-control" placeholder="Contents" name="descr"><?php echo $content; ?></textarea>
                 </div>
                 <div class="form-group">
                    <input type="submit" id="csubmit">
                 </div>
              </div>
           </div>
           <script async src="<?php echo base_url("js/itp/addblog.js"); ?>"></script>
           <div class="form-group">
              <textarea class="form-control" placeholder="Contents" name="content" id="blog"><?php echo base64_decode($content); ?></textarea>
              <script>
                 CKEDITOR.replace('blog');
              </script>
           </div>
           <input type="hidden" name="bdata" value="" />
           <input class="btn btn-sg btn-primary" type="submit" value="<?php if(empty($title)) echo "Create"; else echo "Update"; ?>" name="submit">
        </fieldset>
     </form>
     <center><b>Looking for existing blog?</b> <br></b><a href="<?php echo base_url('blog/index'); ?>">Show blog</a></center>
     <br>
     <iframe src="<?php echo base_url() . "/blog/upload"; ?>" title="Upload blog images" scrolling="no" height="400px"></iframe>
  </div>
</div>
<script>
    $(document).ready(function(){
        if(blogId != undefined)
            setInterval(function() {
                
                   if ($("select>option[value='" + blogId + "'").length) {
                      updateCat();
                   }
            }, 500);
        $("input[type='submit'").click(function(){
           $("input[name='bdata'") .val(CKEDITOR.instances.blog.getData());
           if($("input[name='title'").val() == ""){
               alert("Invalid blog title!"); 
               return false;
           }
           else if($("input[name='bdata'").val() == ""){
               alert("Invalid blog contents!");
               return false;
           }
           else if($("input[name='keywords'").val() == ""){
               alert("Invalid blog keywords!");
               return false;
           }
           else if($("input[name='bdescr'").val() == ""){
               alert("Invalid blog description!");
               return false;
           }
           else
                return true;
        });
        
    });
    function updateCat(){
        $("select>option[value='" + blogId + "'").attr("selected", true);
    }
</script>

<style>
    .mx-auto {
        margin-top: 100px;
    }
    input.btn.btn-lg.btn-success {
        position: absolute;
        padding: 10px;
        border-bottom: 10px;
        padding-left: 15px;
        padding-right: 15px;
    }
    .panel-body center {
        position: absolute;
        right: 0;
        text-align: right;
        margin-right: 30px;
    }
    .panel-body {
        padding-bottom: 75px;
    }
    div#ex1 {
        height: 400px;
    }
    a.close-modal {
        margin-right: 15px;
        margin-top: 10px;
        width: 20px !important;
    }
</style><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="<?php echo base_url("ckeditor/ckeditor.js");?>"></script>

  <!-- Page Header -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
  <script src="http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script>
  <header class="masthead" style="background-image: url('<?php echo site_url('blog/blog.jpeg');?>')">
    <div class="overlay">
        <?php if(isset($this->session->get_userdata()['username'])){?>
            <div class="hidden-md-down ldata" align="right">
            <?php echo "Welcome, ".$this->session->get_userdata()['username'];?>
            </b>
            <br><span class="btn-primary" style="padding:5px;">Logout</span></div>
            <?php } ?>
        <style>
        .hidden-md-down.ldata {
            position: absolute;
            right: 0;
            margin-right: 10px;
        }
        </style>
    </div>s
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h2><?php 
            if(isset($data['ctitle']))
                echo $data['ctitle'][0]->ctitle; 
            else
                echo $ctitle; ?></h2>
            <span class="subheading">
                <?php if(isset($data['ctitle'])) echo $data['ctitle'][0]->cdescr; else echo $descr; ?>
            </span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto" id="blog-data">
        <!-- Pager -->
        <div class="clearfix">
            <?php $success_msg = $this->session->flashdata('success_msg');
                $error_msg = $this->session->flashdata('error_msg');
                if(isset($success_msg)) if ($success_msg != null) {
                ?>
                  <div class="alert alert-success">
                    <?php echo $success_msg; ?>
                  </div>
                <?php } ?>
            <?php if(isset($error_msg)) if ($error_msg != null) { ?>
                  <div class="alert alert-danger">
                    <?php echo $error_msg; ?>
                  </div>
                <?php } ?>
          <center><b>Add New blog?</b></b><a href="<?php echo base_url('blog/add'); ?>">&nbsp;&nbsp;Click Here!!</a></center>
          <?php if(isset($bdata)) foreach($bdata as $bd){ $cat = $bd['cat']; ?>
            <div class="col-12">
				<div id="catlist"></div>
				<div><span class='post-title' cat-id="<?php echo $cat['id'];?>">
				<?php echo $cat['title'];?>
				</span> &nbsp;( <?php echo $cat['tblog'];?> blogs )</div>
				<div class="col-12">
					<ol>
						<?php if(isset($bd['blogs'])) foreach($bd['blogs'] as $blog){ ?>
							<li>
								<div id="catlist"></div>
								<span class="btitle" id="<?php echo $blog->id;?>">
								<a href="<?php echo base_url("blog/get/").$blog->id."/".str_replace(" ", "-", $blog->btitle); ?>" style="text-decoration: none;"><?php echo $blog->btitle;?></a>
								</span>
								<span class="author">
								<div class='post-meta'>Posted by 
								<?php echo $blog->username;?>
								on <?php echo $blog->cdate;?></div>
								</span>
							</li>
						<?php } ?>
					</ol>
				</div>
				<hr>
			</div>	
		    <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <style>
    .site-heading {
		margin-top: 75px;
    }
	.post-title{
		font-size: 21px;
		font-weight: bold;
		color: #dc3545;
	}
	.btitle{
		cursor:pointer;
		font-size: 18px;
		font-weight: bold;
		color: #004085;
	}
	.author{
		font-size:16px;
		color: gray;
	}
  </style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $(".post-title").click(function(d){
      if($(this).attr('id') != undefined)
      window.open("<?php echo base_url("blog/get");?>/" + $(this).attr('id'), "_parent");
      else
      window.open("<?php echo base_url("blog/blogs");?>/" + $(this).attr('cat-id'), "_parent");
  });
  
});
</script>
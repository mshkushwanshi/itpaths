    <!-- Page Header -->
    <header class="masthead" style="background-image: url('<?php echo base_url('blog/blog.jpeg'); ?>')">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="page-heading">
          <h3><?php if (isset($blos[0])) echo $blogs[0]->mtitle; ?></h3>
          <span class="subheading"><?php if (isset($blos[0])) echo $blogs[0]->ctitle; ?></span>
        </div>
      </div>
      <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    </header>
    <meta charset="utf-8">
    <span style="background-color:red;">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="login-panel panel panel-success" style="margin:15px;">
              <div class="panel-body">
                <?php
                $success_msg = $this->session->flashdata('success_msg');
                $error_msg = $this->session->flashdata('error_msg');
                if ($success_msg != null) {
                ?>
                  <div class="alert alert-success">
                    <?php echo $success_msg; ?>
                  </div>
                <?php } ?>
                <?php if ($error_msg != null) { ?>
                  <div class="alert alert-danger">
                    <?php if(isset($error_msg)) echo $error_msg; ?>
                  </div>
                <?php } ?>

                <?php
                foreach ($blogs as $blog) {
                ?>
                  <div class="row">
                    <table>
                      <tr>
                        <td>
                          <div id="catlist"></div>
                          <h6 class='post-title'>
                            <?php echo $blog->mtitle . " / " . $blog->ctitle; ?>
                          </h6>
                          <h3 class='post-subtitle'>
                            <?php echo $blog->btitle; ?><?php if (isset($id)) { ?>
                            &nbsp;<a href="<?php echo base_url('blog/edit') . "/" . $bid; ?>"><img src="<?php echo base_url('images/edit.png'); ?>" width="25" id="edit" /></a>
                            &nbsp;<img src="<?php echo base_url('images/delete.png'); ?>" width="25" id="delete" /><?php } ?>
                          </h3>
                          <?php echo base64_decode($blog->bcontent); ?>
                          <p class='post-meta'>Posted by
                            <a href='#'><?php echo $blog->username; ?></a>
                            on <?php echo $blog->cdate; ?></p>
                  </div>
                  <hr>
                  </td>
                  </tr>
                  </table>
              </div>
            <?php } ?>
            <center><b>Add New blog?</b></b><a href="<?php echo base_url('blog/add'); ?>">&nbsp;&nbsp;Click Here!!</a></center>
            </div>
          </div>
        </div>

      </div>
      </div>
    </span>
    <style>
      td {
        word-wrap: break-word;
        padding: 5px;
      }

      .row {
        margin-bottom: 5px;
      }

      .content {
        min-width: 500px;
        overflow-y: auto;
        max-height: 200px;
        border: 1px solid #bfb1b1;
        border-radius: 4px;
        margin-right: 15px;
        margin-top: 5px;
        margin-bottom: 10px;
        overflow-x: hidden;
      }

      .action {
        margin-bottom: 5px;
      }

      #delete {
        cursor: pointer;
      }
      header.masthead {
        margin-top: 105px;
      }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <script>
      $(document).ready(function() {
        $("#delete").click(function() {
          $('#myModal').modal('toggle');
        });

        $("#cdelete").click(function() {
          window.open("<?php echo base_url('blog/delete/') . $bid; ?>", "_parent");
        });

      });
    </script>
    <hr>
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Confirm</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure to delete?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" id="cdelete">Yes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <style>
        p{
            text-align: justify;
        }
        img{
            padding: 2px;
        }
        }
    </style>
<script src="../ckeditor/ckeditor.js"></script>
<script src="../ckeditor/samples/js/sample.js"></script>
	
<div class="adjoined-bottom">
	<div class="grid-container">
		<div class="grid-width-100">
			<div id="editor">
				<h1>Hello world!</h1>
				<p>I'm an instance of <a href="http://ckeditor.com">CKEditor</a>.</p>
			</div>
		</div>
	</div>
</div>

<script>
	initSample();
</script>

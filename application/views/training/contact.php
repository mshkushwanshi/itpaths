    <section id="imgBanner">
      <h2>Contact</h2>
    </section>
    <section id="contact">
      <div class="container">
       <div class="row">
          <div class="col-lg-12 col-md-12"> 
            <div class="title_area">
              <h2 class="title_two">Looking for IT Consultation?</h2>
              <span></span> 
              <p>We are here to help you for providing best IT application and enterprise training solution.</p>
            </div>
          </div>
       </div>
       <div class="row">
        <?php 
			if(isset($email_sent))
				echo $email_sent;
		?>
         <div class="col-lg-8 col-md-8 col-sm-8">
           <div class="contact_form wow fadeInLeft">
              <form class="submitphoto_form" method = "post" action = "<?php echo site_url('indexs/submit');?>">
                <input type="text" class="wp-form-control wpcf7-text" placeholder="Your name" name = "name">
                <input type="mail" class="wp-form-control wpcf7-email" placeholder="Email address" name = "email">
                <input type="text" class="wp-form-control wpcf7-text" placeholder="Subject" name = "subject">
                <textarea class="wp-form-control wpcf7-textarea" cols="30" rows="10" placeholder="What kind of solution, you're looking for?" name = "query"></textarea>
                <input type="submit" value="Submit" class="wpcf7-submit">
              </form>
           </div>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-4">
           <div class="contact_address wow fadeInRight">
             <h3>Address</h3>
             <div class="address_group">
               <p>Building No 179, <p>Ushodhaya Enclave,</p><p>HIG, Phase 2</p>Hyderabad - 500049</p>
               <p>+91 9 560 939 040</p>
			   <p>+61 8 8121 7993</p>
               <p>Email: info@itpaths.com</p>
             </div>
             <div class="address_group">
              <ul class="footer_social">
                <li><a href="https://www.facebook.com/itpaths/" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="https://www.linkedin.com/in/umesh-singh-bb975515/" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="https://www.youtube.com/watch?v=JcOdckBIuKY&t=26s" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Youtube"><i class="fa fa-youtube"></i></a></li>
                </ul>
             </div>
           </div>
         </div>
       </div>
      </div>
	  <style>
		form.submitphoto_form input, form.submitphoto_form textarea {
			border: 1px solid gray;
		}
	  </style>
    </section>
    <section id="googleMap">
      <iframe width="100%" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed/v1/place?q=17.4884140,78.3500750&key=AIzaSyCHOvNBRJ3_iK_Yh9eJDFlqREu_-HJJkJk"></iframe>
    </section>
    

<!DOCTYPE html>
<html lang="en">
  <head>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
      (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-1882767817709882",
        enable_page_level_ads: true
      });
    </script>
    <meta charset="utf-8">
    <meta name="description" content="ITPaths Corp & Dev Sol">
    <meta name="keywords" content="otmm, otmm training, ecm, ecm training, opentext training, android, development">
    <meta name="author" content="Umesh Kushwanshi">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <title>ITPaths : Events Single</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/icon" href="<?php echo site_url('img/wpf-favicon.png');?>"/>
    <link href="<?php echo site_url('css/bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo site_url('css/font-awesome.min.css');?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo site_url('css/superslides.css');?>">
    <link href="<?php echo site_url('css/slick.css');?>" rel="stylesheet"> 
    <link rel='stylesheet prefetch' href="<?php echo site_url('css/jquery.circliful.css');?>">  
    <link rel="stylesheet" href="<?php echo site_url('css/animate.css');?>"> 
    <link type="text/css" media="all" rel="stylesheet" href="<?php echo site_url('css/jquery.tosrus.all.css');?>" />    
    <link id="switcher" href="<?php echo site_url('css/themes/default-theme.css');?>" rel="stylesheet">
    <link href="<?php echo site_url('style.css');?>" rel="stylesheet">
   
    <link href='http://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>   
    <link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css'>    

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body> 
	<a class="scrollToTop" href="<?php echo site_url('indexs/');?>"></a>
	<header id="header">
      <div class="menu_area">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo site_url('indexs/index');?>">IT<span>Paths</span></a>            
              <!-- <a class="navbar-brand" href="<?php echo site_url('indexs/index');?>"><img src="<?php echo site_url('img/logo.png');?>" alt="logo"></a>  -->
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
                <li><a href="<?php echo site_url('indexs/index');?>">Home</a></li>
                <li><a href="<?php echo site_url('indexs/contact');?>">Contact</a></li>
				<li><a href="<?php echo site_url('home/');?>">Video Portal</a></li>
				<li><a href="<?php echo site_url('notes/');?>">Notes</a></li>
              </ul>          
            </div>
          </div>     
        </nav>  
      </div>
    </header>
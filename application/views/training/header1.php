<!DOCTYPE html>
<html lang="en">
  <head>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
      (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-1882767817709882",
        enable_page_level_ads: true
      });
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <title>ITPaths : Events Single</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/icon" href="<?php echo site_url('img/wpf-favicon.png');?>"/>
    <!-- Bootstrap css file-->
    <link href="<?php echo site_url('css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Font awesome css file-->
    <link href="<?php echo site_url('css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- Superslide css file-->
    <link rel="stylesheet" href="<?php echo site_url('css/superslides.css');?>">
    <!-- Slick slider css file -->
    <link href="<?php echo site_url('css/slick.css');?>" rel="stylesheet"> 
    <!-- Circle counter cdn css file -->
    <link rel='stylesheet prefetch' href="<?php echo site_url('css/jquery.circliful.css');?>">  
    <!-- smooth animate css file -->
    <link rel="stylesheet" href="<?php echo site_url('css/animate.css');?>"> 
    <!-- preloader -->
    
    <!-- gallery slider css -->
    <link type="text/css" media="all" rel="stylesheet" href="<?php echo site_url('css/jquery.tosrus.all.css');?>" />    
    <!-- Default Theme css file -->
    <link id="switcher" href="<?php echo site_url('css/themes/default-theme.css');?>" rel="stylesheet">
    <!-- Main structure css file -->
    <link href="<?php echo site_url('style.css');?>" rel="stylesheet">
   
    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css'>   
    <link href='http://fonts.googleapis.com/css?family=Varela' rel='stylesheet' type='text/css'>    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body> 

    <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="<?php echo site_url('indexs/');?>"></a>
    <!-- END SCROLL TOP BUTTON -->

    <header id="header">
      <!-- BEGIN MENU -->
      <div class="menu_area">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <!-- LOGO -->
              <!-- TEXT BASED LOGO -->
              <a class="navbar-brand" href="<?php echo site_url('indexs/index');?>">IT<span>Paths</span></a>            
              <!-- IMG BASED LOGO  -->
              <!-- <a class="navbar-brand" href="<?php echo site_url('indexs/index');?>"><img src="<?php echo site_url('img/logo.png');?>" alt="logo"></a>  -->
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
                <li><a href="<?php echo site_url('indexs/index');?>">Home</a></li>
                <li><a href="<?php echo site_url('indexs/course');?>">Course</a></li>
                <li><a href="<?php echo site_url('indexs/scholarship');?>">Scholarship</a></li>
                <li class="active"><a href="<?php echo site_url('indexs/events-archive');?>">Events</a></li>
                <li><a href="<?php echo site_url('indexs/gallery');?>">Gallery</a></li>                
                <li><a href="<?php echo site_url('indexs/blog');?>">Blog</a></li>
                <li class="dropdown">
                  <a href="<?php echo site_url('#');?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Page<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="<?php echo site_url('indexs/404');?>">404 Page</a></li>
                    <li><a href="<?php echo site_url('#');?>">Link Two</a></li>
                    <li><a href="<?php echo site_url('#');?>">Link Three</a></li>               
                  </ul>
                </li>               
                <li><a href="<?php echo site_url('contact');?>">Contact</a></li>
              </ul>          
            </div>
          </div>     
        </nav>  
      </div>
      <!-- END MENU -->    
    </header>

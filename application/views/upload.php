<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<?php
if (isset($error)) echo "<p class='err'>" . $error . "</p>";
if (isset($upload_data)){ echo "<p class='success'>File uploaded successfully!</p>";
if($upload_data['file_ext'] == ".zip" || $upload_data['file_ext'] == ".tar" || $upload_data['file_ext'] == ".rar" || $upload_data['file_ext'] == ".7z")
    echo "<br/>Path: https://www.itpaths.com/bundle/".$upload_data['file_name']."<br/><br/>";
else
    echo "<br/>Path: https://www.itpaths.com/blog/img/".$upload_data['file_name']."<br/><br/>";
}
?>
<?php echo form_open_multipart('blog/upload'); ?>
<input id="imgInp" type="file" name="userfile" />
<br /><br />
<input type="submit" value="upload" />
</form>
<img src = "" id="blah" width = "200px" />
<style>
    .err {
        background-color: pink;
    }

    .success {
        border: green 1px solid;
        background-color: greenyellow;
    }
</style>
<script>
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
          $("#blah").show();
        }
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }
    $("#blah").hide();
    $("#imgInp").change(function() {
      readURL(this);
    });
</script>
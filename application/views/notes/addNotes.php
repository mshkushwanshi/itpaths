  <!-- Page Header -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <script src="<?php echo base_url("ckeditor/ckeditor.js");?>"></script>
  <script src="http://ajax.microsoft.com/ajax/jquery.templates/beta1/jquery.tmpl.min.js"></script>
  <header class="masthead" style="background-image: url('<?php echo site_url('blog/blog.jpeg');?>')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h2><?php 
            if(isset($data['ctitle']))
                echo $data['ctitle'][0]->ctitle; 
            else
                echo $ctitle; ?></h2>
            <span class="subheading">
                <?php if(isset($data['ctitle'])) echo $data['ctitle'][0]->cdescr; else echo $descr; ?>
            </span>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="mx-auto" id="blog-data">
        <!-- Pager -->
            <div class="col-lg-12 col-md-4 col-sm-12 col-xs-12 visible-lg visible-md visible-xs visible-sm">
                 <div class="panel-heading">
                    <h3 class="panel-title">Notes</h3>
                 </div>
                 <div class="panel-body">
                    <?php 
                       $error_msg = $this->session->flashdata('error_msg');
					   if($error_msg != null){
					?>
					   <div class="alert alert-danger">
                       <?php echo $error_msg; ?>
					   </div>
                    <?php } ?>
					<form role="form" method="post" action="<?php echo base_url('notes/add'); ?>">
					<?php 
						if(!empty($notes)){
							$title = $notes[0]['ntitle'];
							$content = $notes[0]['ndetail'];
					?>
							<input value = "<?php echo $notes[0]['nid'];?>" name="id" type="hidden"/>
					<?php
						}
						else{
							$title = "";
							$content = "";
						}
						
					?>
                       <fieldset>
						  <div class="form-group">
                             <input class="form-control" placeholder="Title" name="title" type="text" value = "<?php echo $title;?>" autofocus>
							 <input value = "1" name="type" type="hidden"/>
                          </div>
                          <div class="form-group">
							 <textarea class="form-control" placeholder="Contents" name="content" id="notes"><?php echo str_replace("$$", "", $content);?></textarea>
							 <script>
                                CKEDITOR.replace( 'notes' );
                            </script>
                          </div>
                          <input class="btn btn-lg btn-success" type="submit" value="Save" name="submit" >
                       </fieldset>
                    </form>
                    <center><a href="<?php echo base_url('notes/get'); ?>">Show Notes</a></center>
                 </div>
           </div>
      </div>
    </div>
  </div>
</div>
</div>
<hr>
<style>
    .mx-auto {
        margin-top: 100px;
    }
    input.btn.btn-lg.btn-success {
        position: absolute;
        padding: 10px;
        border-bottom: 10px;
        padding-left: 15px;
        padding-right: 15px;
    }
    .panel-body center {
        position: absolute;
        right: 0;
        text-align: right;
        margin-right: 30px;
    }
    .panel-body {
        padding-bottom: 75px;
    }
</style>
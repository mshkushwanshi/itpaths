    <!-- Page Header -->
    <?php
    echo '<script>
            var notes = ' . json_encode($notes) . '
        </script>';
    ?>
    <header class="masthead" style="background-image: url('<?php echo site_url("img/note.jpg");?>')">
        <div class="col-lg-8 col-md-10 mx-auto">
            <div class="page-heading">
                <h3>My Notes</h3>
                <span class="subheading">Sticky Notes</span>
            </div>
        </div>
    </header>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<section class="blog_area single-post-area section-padding">
         <div class="container">
            <div class="row">
               <div class="col-lg-4">
                  <div class="blog_right_sidebar">
                     <aside class="single_sidebar_widget search_widget">
						<form action="#">
                           <div class="form-group">
                              <div class="input-group mb-3">
                                 <input type="text" class="form-control" id="tsearch" onkeyup="search()" placeholder='Search Keyword'
                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Keyword'">
                                 <div class="input-group-append"></div>
                              </div>
                           </div>
                        </form>
                     </aside>
                     <aside class="single_sidebar_widget post_category_widget">
                        <h4 class="widget_title">My Notes</h4>&nbsp;<a href="<?php echo base_url('notes/add'); ?>"><img src="<?php echo base_url("img/add.webp");?>" alt="Add New" class="add-new" width="35px" title="Add New" /><!--<i class="fa fa-plus-circle" aria-hidden="true"></i>--></a>
						<ul id="notes-list" class="list cat-list"></ul>
                     </aside>
                  </div>
               </div>
			   <div class="col-lg-8 posts-list">
                  <div class="single-post">
                     <div class="feature-img">
                        <img class="img-fluid" src="<?php echo site_url("assets/img/blog/single_blog_1.png");?>" alt="">
                     </div>
					 <?php
					$success_msg = $this->session->flashdata('success_msg');
					$error_msg = $this->session->flashdata('error_msg');
					if ($success_msg != null) {
					?>
						<div class="alert alert-success">
							<?php echo $success_msg; ?>
						</div>
					<?php } ?>
					<?php if ($error_msg != null) { ?>
						<div class="alert alert-danger">
							<?php echo $error_msg; ?>
						</div>
					<?php } ?>
                     <div class="blog_details">
						<h4 id="nheader"></h4>
						<div id="ncontent"></div>
                     </div>
                  </div
               </div>
            </div>
         </div>
      </section>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            if (notes != undefined) {
                notes.forEach(function(n, i) {
                    $("#notes-list").append('<li><div onclick="loadnote(' + i + ')" class="nt row">' + n.ntitle + '</div><div class="dt row">' + n.cdate + '</div></li>');
                })
				loadnote(0)
            }
            $("#cdelete").click(function() {
                window.open("<?php echo base_url('').'notes/delete?id='; ?>" + $(this).attr("action"), "_parent");
            });
        });
        function loadnote(i) {
                var n = notes[i];
                var tool = '<img src="<?php echo base_url('images/edit.png'); ?>" title="edit note" alt="Edit Note" width="25" class="edit" onclick="window.open(\'<?php echo base_url(''); ?>notes/edit?id=' +
                    n.nid + '\', \'_parent\')"' +
                    n.nid + '" />&nbsp;<img src="<?php echo base_url('images/delete.png'); ?>" title="delete note" alt="Delete Note" width="23" class="delete" onClick="$(\'#myModal\').modal(\'toggle\');$(\'#cdelete\').attr(\'action\',\'' +
                    n.nid + '\');" />';
                var ncontent = '<div class="row header"><b>' + n.ntitle + '</b></div><div class="row content"><div><p class="row">' + n.ndetail.replace("$$", "<br>") +'</p></div><div class="ndt">modified by:- '+n.cdate+'</div></div>';
                console.log(n);
                $("#nheader").html(tool);
                $("#ncontent").html(ncontent);
                //$('#notes').modal('toggle');
            };
		function search() {
			var input, filter, ul, li, a, i, txtValue;
			filter = $("#tsearch").val();
			li = $("#notes-list li")
			for(i = 0; i<li.length; i++){
				a = $(li[i]).find(".nt.row")[0];
				txtValue = $(a).html();
				if (txtValue.toUpperCase().includes(filter.toUpperCase())) {
					li[i].style.display = "";
				} else {
					li[i].style.display = "none";
				}
			}
		}
    </script>
    <hr>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to delete?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="cdelete" action="">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
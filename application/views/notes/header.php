<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?php echo $title;?></title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo site_url('blog/vendor/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="<?php echo site_url('blog/vendor/fontawesome-free/css/all.min.css');?>" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="<?php echo site_url('blog/css/clean-blog.min.css');?>" rel="stylesheet">
  <link href="<?php echo base_url("css/nt.css");?>" rel="stylesheet" type="text/css">
  <script data-ad-client="ca-pub-5233448333018429" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <script src="https://www.anrdoezrs.net/am/100156229/impressions/page/am.js"></script>
</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mNav" style="background-color: white;">
    <div class="container">
      <a class="navbar-brand" href="<?php echo site_url('');?>"><img src="<?php echo site_url('img/logo.png');?>" class="blog-logo"/></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('');?>">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('#aboutUs');?>">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('#contact');?>">Contact</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('blog/index');?>">Blogs</a>
          </li>
          <!--<li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('order/');?>">Orders</a>
          </li>-->
          <li class="nav-item">
            <div class="nav-link">
                <a>Notes</a>
                <div class="nlme">
                    <ul>
                        <li>
                            <a class="nav-link" href="<?php echo site_url('notes/add');?>">New</a>
                        </li>
                        <li>
                            <a class="nav-link" href="<?php echo site_url('notes/get');?>">All</a>
                        </li>
                    </ul>
                </div>
            </div>  
          </li>
          
          <?php if(isset($this->session->get_userdata()['username'])){?>
            
            <div class="showhim">
                <img src="<?php echo base_url("img/member.webp");?>" class="ldata" width="50px" />&nbsp;<?php echo "Hello, ".$this->session->get_userdata()['username'];?>&nbsp;
                <div class="showme">
                    <ul class="hidden-md-down ldata">
                        <li>
                            <span class="btn-primary" style="padding:5px;" onClick="window.open('<?php echo base_url("user/logout");?>', '_parent')">Logout</span>
                        </li>
                    </ul>
                </div>
            </div>
            <?php } ?>
        </ul>
      </div>
    </div>
  </nav>
  <style>
    .hidden-md-down.ldata {
        position: absolute;
        right: 0;
        margin-right: 10px;
    }
    img.blog-logo {
        border-top-left-radius: 20px;
        border-top-right-radius: 5px;
    }
  </style>
  
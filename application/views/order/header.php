<!DOCTYPE html>
<html lang="en">
<head>
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <script src="<?php echo site_url('blog/vendor/jquery/jquery.min.js');?>"></script>
  <title><?php echo $title;?></title>
  <script type="text/javascript" src="<?php echo site_url('odr/view.js');?>"></script>
  <link href="<?php echo site_url('odr/view.css');?>" rel="stylesheet">
  <meta name="description" content="<?php if(isset($description)) echo $description;?>">
  <meta name="keywords" content="<?php if(isset($keywords)) echo $keywords;?>">
  <?php echo "<script>var id = '".$id."';</script>";?>
  <script data-ad-client="ca-pub-5233448333018429" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <script src="https://www.anrdoezrs.net/am/100156229/impressions/page/am.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="<?php echo site_url('css/superslides.css'); ?>">
  <link href="<?php echo site_url('css/slick.css'); ?>" rel="stylesheet">
  <link rel='stylesheet prefetch' href="<?php echo site_url('css/jquery.circliful.css'); ?>">
  <link rel="stylesheet" href="<?php echo site_url('css/animate.css'); ?>">
  <link type="text/css" media="all" rel="stylesheet" href="<?php echo site_url('css/jquery.tosrus.all.css'); ?>" />
  <link id="switcher" href="<?php echo site_url('css/themes/default-theme.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('style.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url("css/nt.css");?>" rel="stylesheet" type="text/css">
  <link rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Merriweather">
    <link rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Varela">
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
  <a class="scrollToTop" href="<?php echo site_url('indexs/'); ?>"></a>
  <header id="header">
    <div class="menu_area">
      <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo site_url('indexs/index'); ?>">
              <!-- IT<span>Paths</span> --></a>
            <a class="navbar-brand" href="<?php echo site_url('indexs/index'); ?>"><img src="<?php echo site_url('images/clogo.jpeg'); ?>" alt="logo"></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
              <li><a href="<?php echo site_url('indexs/'); ?>">Home</a></li>
              <li><a href="#aboutUs">About Us</a></li>
              <li><a href="<?php echo site_url('#contact'); ?>">Contact</a></li>
              <!--<li><a href="<?php echo site_url('home/'); ?>">Video Portal</a></li>-->
              <li class="dropdown"><a href="#">Blogs</a>
              <!-- <?php echo site_url('blog/index'); ?> -->
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">

                  <?php if (isset($cats)) foreach ($cats as $cat) { ?>
                    <a href="<?php echo base_url("")."blog/blogs/".$cat['id']; ?>">
                    <li class="dropdown-item" type="button"><?php echo $cat['title']; ?></li>
                    <?php } ?>
                    </a>
                </ul>
              </li>
              <li class="dropdown"><a href="#">Orders</a>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <a href="<?php echo base_url("")."order"; ?>">
                    <li class="dropdown-item" type="button"><i class="fa fa-file" aria-hidden="true"></i>&nbsp;&nbsp;Add New</li>
                    </a>
                    <a href="<?php echo base_url("")."order/get"; ?>">
                    <li class="dropdown-item" type="button"><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;&nbsp;List All</li>
                    </a>
                </ul>
              </li>
              <li class="dropdown"><a href="#">Notes</a>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <a href="<?php echo base_url("notes/add"); ?>">
                    <li class="dropdown-item" type="button"><i class="fa fa-file" aria-hidden="true"></i>&nbsp;&nbsp;Add New</li>
                    </a>
                    <a href="<?php echo base_url("notes/get"); ?>">
                    <li class="dropdown-item" type="button"><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;&nbsp;List All</li>
                    </a>
                </ul>
              </li>
            </ul>
            <?php if(isset($this->session->get_userdata()['username'])){?>
            
            <div class="showhim">
                <img src="<?php echo base_url("img/member.webp");?>" class="ldata" width="50px" />&nbsp;<?php echo "Hello, ".$this->session->get_userdata()['username'];?>&nbsp;
                <div class="showme">
                    <ul class="hidden-md-down ldata">
                        <li>
                            <span class="btn-primary" style="padding:5px;" onClick="window.open('<?php echo base_url("user/logout");?>', '_parent')">Logout</span>
                        </li>
                    </ul>
                </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </nav>
    </div>
  </header>
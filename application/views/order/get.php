    <!-- Page Header -->
    <?php
    echo '<script>
            var orders = ' . json_encode($orders) . '
        </script>';
    ?>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    
	<section class="blog_area single-post-area section-padding">
         <div class="container">
            <div class="row">
               <div class="col-lg-4">
                  <div class="blog_right_sidebar">
                     <aside class="single_sidebar_widget search_widget">
						<form action="#">
                           <div class="form-group">
                              <div class="input-group mb-3">
                                 <input type="text" class="form-control" id="tsearch" onkeyup="search()" placeholder='Search Keyword'
                                    onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Keyword'">
                                 <div class="input-group-append"></div>
                              </div>
                           </div>
                        </form>
                     </aside>
                     <aside class="single_sidebar_widget post_category_widget">
                        <h4 class="widget_title">My Orders</h4>
						<ul id="orders-list" class="list cat-list"></ul>
                     </aside>
                  </div>
               </div>
			   <div class="col-lg-8 posts-list">
                  <div class="single-post">
                     <div class="feature-img">
                        <img class="img-fluid" src="<?php echo site_url("assets/img/blog/single_blog_1.png");?>" alt="">
                     </div>
					 <?php
					$success_msg = $this->session->flashdata('success_msg');
					$error_msg = $this->session->flashdata('error_msg');
					if ($success_msg != null) {
					?>
						<div class="alert alert-success">
							<?php echo $success_msg; ?>
						</div>
					<?php } ?>
					<?php if ($error_msg != null) { ?>
						<div class="alert alert-danger">
							<?php echo $error_msg; ?>
						</div>
					<?php } ?>
                     <div class="blog_details">
						<h4 id="nheader">Order Details</h4>
						<div id="ncontent">
						    <table id="data"></table>
						</div>
                     </div>
                  </div
               </div>
            </div>
         </div>
      </section>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            if (orders != undefined) {
                orders.forEach(function(n, i) {
                    dt = "";
                    if(n.dt != undefined)
                    dt = n.dt.split(" ")[0];
                    $("#orders-list").append('<li><div onclick="loadnote(' + i + ')" class="nt row">' + n.fn + " " + n.ln + '</div><div class="dt row">' + dt + '</div></li>');
                })
				loadnote(0)
            }
            $("#cdelete").click(function() {
                window.open("<?php echo base_url('').'orders/delete?id='; ?>" + $(this).attr("action"), "_parent");
            });
        });
        function loadnote(i) {
                var n = orders[i];
                var tool = '<img src="<?php echo base_url('images/edit.png'); ?>" title="click to edit note" alt="click to edit note" width="25" class="edit" onclick="window.open(\'<?php echo base_url(''); ?>orders/edit?id=' +
                    n.nid + '\', \'_parent\')"' +
                    n.nid + '" />&nbsp;<img src="<?php echo base_url('images/delete.png'); ?>" title="click to delete note" width="23" class="delete" onClick="$(\'#myModal\').modal(\'toggle\');$(\'#cdelete\').attr(\'action\',\'' +
                    n.nid + '\');" />';
                setdata(n);
            };
		function search() {
			var input, filter, ul, li, a, i, txtValue;
			filter = $("#tsearch").val();
			li = $("#orders-list li")
			for(i = 0; i<li.length; i++){
				a = $(li[i]).find(".nt.row")[0];
				txtValue = $(a).html();
				if (txtValue.toUpperCase().includes(filter.toUpperCase())) {
					li[i].style.display = "";
				} else {
					li[i].style.display = "none";
				}
			}
		}
		function setdata(data){
		    var c = eval(0);
            $("#data").empty();
			var fn, ln;
			if(data.fn != undefined)
				fn = data.fn;
			if(data.ln != undefined)
				ln = data.ln;
            $("#data").append("<tr><th>Customer's Name </th><td> " + fn + " " + ln + "</td></tr>");
			if(data.eml != undefined)
            $("#data").append("<tr><th>Email </th><td> " + data.eml + "</td></tr>");
			if(data.phn != undefined){
				var p = data.phn;
				if(data.phn != undefined)
				var pl = ["Phone", "Mobile"];
				p = data.phn.split(",");
				for(i=0; i<p.length; i++)
					$("#data").append("<tr><th>" + pl[i] + " </th><td> " + p[i] + "</td></tr>");
			}
			if(data.svcs != undefined)
			$("#data").append("<tr><th>Services </th><td> " + data.svcs + "</td></tr>");
            if(data.ad1 != undefined)
            $("#data").append("<tr><th>Address 1: </th><td> " + data.ad1 + "</td></tr>");
            if(data.ad2 != undefined)
			$("#data").append("<tr><th>Address 2 </th><td> " + data.ad2 + "</td></tr>");
			if(data.cty != undefined)
            $("#data").append("<tr><th>City: </th><td> " + data.cty + "</td></tr>");
			if(data.stat != undefined)
            $("#data").append("<tr><th>Dtate </th><td> " + data.stat  + "</td></tr>");
			if(data.pin != undefined)
            $("#data").append("<tr><th>Pincode: </th><td> " + data.pin + "</td></tr>");
			if(data.cntry != undefined)
            $("#data").append("<tr><th>Country: </th><td> " + data.cntry + "</td></tr>");
			if(data.dt != undefined)
            $("#data").append("<tr><th>Order Date: </th><td> " + data.dt + "</td></tr>");
            if(data.descr != undefined)
            $("#data").append("<tr><th>Business Details: </th><td> " + data.descr  + "</td></tr>");
            if(data.dia != undefined)
                if(data.dia != "")
                    $("#data").append("<tr><th>Agent Reference: </th><td> " + data.dia  + "</td></tr>");
            if(data.dtl != undefined)
                if(data.dtl != "")
                    $("#data").append("<tr><th>Business Service Detail: </th><td> " + data.dtl  + "</td></tr>");
		}
    </script>
    <hr>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Confirm</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to delete?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="cdelete" action="">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <style>
		.row.content p {
			margin: 5px;
		}
		.row.header {
			margin-left: 5px;
		}
		.row.content {
			max-width: 100%;
			max-height: 480px;
			overflow: auto;
			margin-left: 5px;
		}
		ul#orders-list {
			list-style-type: none;
		}
        .dt.row {
			font-size: 12px;
		}
        #sidelinks .nt {
            cursor: pointer;
        }
        #sidelinks .dt {
            font-size: 15px;
        }
        table.content td {
            vertical-align: top;
        }
		aside.single_sidebar_widget.post_category_widget ul{
			max-height: 500px;
			overflow-y: auto;
			overflow-x: hidden;
		}
        td {
            word-wrap: break-word;
            padding: 5px;
        }

        .row {
            margin-bottom: 5px;
        }
        .content {
            overflow-y: auto;
            max-height: 200px;
            border: 1px solid #bfb1b1;
            border-radius: 4px;
            margin-right: 15px;
            margin-top: 5px;
            margin-bottom: 10px;
            overflow-x: hidden;
        }

        .action {
            margin-bottom: 5px;
        }

        img {
            cursor: pointer;
        }
        th {
            vertical-align: top;
            min-width: 165px;
        }
    </style>
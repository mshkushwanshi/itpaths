<style>
#footer {
    width: 100% !important;
}
</style>
<link href="<?php echo site_url('css/font-awesome.min.css'); ?>" rel="stylesheet">
  
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo site_url('blog/vendor/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
  <script src="<?php echo site_url('blog/js/clean-blog.min.js');?>"></script>
	<footer id="footer">

	  <!-- Start footer top area -->

	  <div class="footer_top">

		<div class="container">

		  <div class="row">

			<div class="col-ld-3  col-md-3 col-sm-3">

			  <div class="single_footer_widget">

				<h3>About Us</h3>

				<p>We believe on customer satisfaction and are known to be most trusted service provider to our clients. We work on AI, Web applications, Mobile application and continuously working to make ourselves best AI solution provider.</p>

			  </div>

			</div>

			<div class="col-ld-3  col-md-3 col-sm-3">

			  <div class="single_footer_widget">

				<h3>Community</h3>

				<ul class="footer_widget_nav">

				  <li><a href="<?php echo site_url('home');?>">News &amp; Media</a></li>

				</ul>

			  </div>

			</div>

			<div class="col-ld-3  col-md-3 col-sm-3">

			  <div class="single_footer_widget">

				<h3>Others</h3>

				<ul class="footer_widget_nav">

				  <li><a href="<?php echo site_url('notes');?>">Notes</a></li>

				</ul>

			  </div>

			</div>

			<div class="col-ld-3  col-md-3 col-sm-3">

			  <div class="single_footer_widget">

				<h3>Social Links</h3>

				<ul class="footer_social">

				  <li><a data-toggle="tooltip" data-placement="top" title="Facebook" class="soc_tooltip" href="https://www.facebook.com/itpaths/"><i class="fa fa-facebook"></i></a></li>

				  <li><a data-toggle="tooltip" data-placement="top" title="Twitter" class="soc_tooltip"  href="https://twitter.com/clyde_vs_bonnie"><i class="fa fa-twitter"></i></a></li>

				  <!--<li><a data-toggle="tooltip" data-placement="top" title="Google+" class="soc_tooltip"  href="#"><i class="fa fa-google-plus"></i></a></li>-->

				  <li><a data-toggle="tooltip" data-placement="top" title="Linkedin" class="soc_tooltip"  href="https://www.linkedin.com/in/umesh-singh-bb975515/"><i class="fa fa-linkedin"></i></a></li>

				  <!--<li><a data-toggle="tooltip" data-placement="top" title="Youtube" class="soc_tooltip"  href="#"><i class="fa fa-youtube"></i></a></li>-->

				</ul>

			  </div>

			</div>

		  </div>

		</div>
	  </div>
	  <div class="footer_bottom">
		<div class="container">
		  <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
			  <div class="footer_bootomLeft">
				<p class="copyright text-muted">&copy;<?php echo date("Y");?>&nbsp;|&nbsp;All Rights Reserved</p>
			  </div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6">
			  <div class="footer_bootomRight">
				<p>Designed by <a href="<?php echo site_url('#');?>" rel="nofollow">ITPaths&nbsp;|&nbsp;Tapasvi Info Solutions</a></p>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</footer>
	<script src="<?php echo site_url('js/jquery-1.11.1.js');?>"></script>
	<script src="<?php echo site_url('js/queryloader2.min.js');?>" type="text/javascript"></script>
	<script src="<?php echo site_url('js/wow.min.js');?>"></script>  
	<script src="<?php echo site_url('js/bootstrap.min.js');?>"></script>
	<script src="<?php echo site_url('js/slick.min.js');?>"></script>
	<script src="<?php echo site_url('js/jquery.easing.1.3.js');?>"></script>
	<script src="<?php echo site_url('js/jquery.animate-enhanced.min.js');?>"></script>
	<script src="<?php echo site_url('js/jquery.superslides.min.js');?>" type="text/javascript" charset="utf-8"></script>   
	<script src="<?php echo site_url('js/jquery.circliful.min.js');?>"></script>
	<script type="text/javascript" language="javascript" src="<?php echo site_url('js/jquery.tosrus.min.all.js');?>"></script>   
	<script src="<?php echo site_url('js/custom.js');?>"></script>
  </body>
</html>
<!-- Page Header -->
<script src="<?php echo base_url("ckeditor/ckeditor.js");?>"></script>
<script type="text/javascript" src="<?php echo site_url('odr/view.js');?>"></script>
  <link href="<?php echo site_url('odr/view.css');?>" rel="stylesheet">
  
<div class="container order col-xs-12 col-sm-12 col-lg-12">
    <div class="row">
      <div class="col-md-12">
         <p align="justify"></p>
         <div id="form_container">
             <div align="center">
              <img src="https://www.itpaths.com/odr/ldr.gif" width="200px" id="ldr">
             </div>
             <h1><a>Order Now!!</a></h1>
             <form id="form_112820" class="appnitro" method="post">
                <div class="form_description">
                   <h2>Order Now!!</h2>
                   <p>We are targeting to spread our business all over the world at the cheapest rate. We're here to serve our customers best in services.</p>
                </div>
                <ul>
                   <li id="li_1">
                      <label class="description" for="element_1">Name </label>
                      <span>
                      <input id="element_1_1" name="fname" class="element text" maxlength="255" size="20" value="" placeholder="First Name" type="text">
                      <label>First Name</label>
    				  <label class="err"></label>
                      </span>
                      <span>
                      <input id="element_1_2" name="lname" class="element text" maxlength="255" size="20" value="" placeholder="Last Name"type="text">
                      <label>Last Name</label>
    				  <label class="err"></label>
                      </span>                  
                   </li>
                   <li id="li_3">
                      <label class="description" for="element_3">Email </label>
                      <div>
                         <input id="element_3" name="email" class="element text medium" type="text" maxlength="255" size="20" value="" placeholder="Email">
    					  <label class="err"></label>					 
                      </div>
                   </li>
                   <li id="li_5">
                      <label class="description" for="element_1">Phone </label>
                      <span>
                      <input id="element_5_1" name="phone" class="element text" type="text" maxlength="255" size="20" value="" placeholder="(###)(###)(###)">
                      <label>Phone</label>
    				  <label class="err"></label>
                      </span>
                      <span>
                      <input id="element_5_2" name="mobile" class="element text" type="text" maxlength="255" size="20" value="" placeholder="##########">
                      <label>Mobile</label>
    				  <label class="err"></label>
                      </span>
                   </li>
                   <li id="li_2">
                      <label class="description" for="element_2">Address </label>
                      <div>
                         <input id="element_2_1" name="ad1" class="element text large" value="" type="text" placeholder="Street Address">
                         <label for="element_2_1">Street Address</label>
    					 <label class="err"></label>
                      </div>
                      <div>
                         <input id="element_2_2" name="ad2" class="element text large" value="" type="text" placeholder="Address">
                         <label for="element_2_2">Address Line 2</label>
    					 <label class="err"></label>
                      </div>
                      <div class="left">
                         <input id="element_2_3" name="city" class="element text medium" value="" type="text" placeholder="City">
                         <label for="element_2_3">City</label>
    					 <label class="err"></label>
                      </div>
                      <div class="right">
                         <input id="element_2_4" name="state" class="element text medium" value="" type="text" placeholder="State">
                         <label for="element_2_4">State / Province / Region</label>
    					 <label class="err"></label>
                      </div>
                      <div class="left">
                         <input id="element_2_5" name="pin" class="element text medium" maxlength="15" value="" type="text" placeholder="Zip Code">
                         <label for="element_2_5">Postal / Zip Code</label>
    					 <label class="err"></label>
                      </div>
                      <div class="right">
                         <select class="element select medium" id="element_2_6" name="cntry">
                            <option value="" selected="selected"></option>
                         </select>
                         <label for="element_2_6">Country</label>
    					 <label class="err"></label>
                      </div>
                   </li>
                   <li id="li_6">
                      <br>
                      <label class="description" for="element_6">Our Services </label>
                      <span>
                      <input id="element_6_1" name="svcs" class="element checkbox" type="checkbox" value="Business Website (Static)" ip="2" checked="checked">
                      <label class="choice" for="element_6_1">Business Website (Static)</label>
    				  <input id="element_6_1" name="svcs" class="element checkbox" type="checkbox" value="Business Website (Dynamic)" ip="12">
                      <label class="choice" for="element_6_2">Business Website (Dynamic)</label>
                      <input id="element_6_2" name="svcs" class="element checkbox" type="checkbox" value="Basic Business Android Application" ip="5">
                      <label class="choice" for="element_6_3">Basic Business Android Application</label>
                      <input id="element_6_3" name="svcs" class="element checkbox" type="checkbox" value="Basic Business IOS Applications" ip="7">
                      <label class="choice" for="element_6_4">Basic Business IOS Applications</label>
    				  <input id="element_6_3" name="svcs" class="element checkbox" type="checkbox" value="Advanced IOS Applications" ip="17">
                      <label class="choice" for="element_6_5">Advanced IOS Applications</label>
    				  <input id="element_6_3" name="svcs" class="element checkbox" type="checkbox" value="Advanced Android Applications" ip="15">
                      <label class="choice" for="element_6_6">Advanced Android Applications</label>
    				  <input id="element_6_3" name="svcs" class="element checkbox" type="checkbox" value="Clone Android Applications" ip="0">
                      <label class="choice" for="element_6_7">Clone Android Applications</label>
    				  <input id="element_6_3" name="svcs" class="element checkbox" type="checkbox" value="Clone IOS Applications" ip="0">
                      <label class="choice" for="element_6_8">Clone IOS Applications</label>
    				  </span> 
                   </li>
                   <li id="">
                      <label class="description" for="element_4">Agent's reference no. </label>
                      <span>
                      <input id="element_4_1" name="ida" class="element text currency" size="30" value="" type="text" placeholder="username / mobile-no / email-id">
    				  <label>Agent's registered ID</label>
    				  <label class="err"></label>
                      </span>
                   </li>
                   <li id="">
                      <label class="description" for="element_4">Your services:</label>
                      <span>
                      <textarea class="form-control" placeholder="Contents" name="content" id="fabout"></textarea>
    								 <script>
                                        CKEDITOR.replace( 'fabout' );</script>
    				  <label style="color:blue;font-weight:bold;">Tell us something more about your services</label>
    				  <label class="err"></label>
                      </span>
                   </li>
                   <!--<li id="li_4">
                      <label class="description" for="element_4">Estimated Price </label>
                      <span class="symbol">₹</span>
                      <span>-->
                      <input id="element_4_1" name="price" class="element text currency" size="10" value="200" type="hidden" disabled>
    				  <!--<label>Cost estimation as per basic functionalities</label>
    				  <label class="err"></label>
                      </span>
                   </li>-->
                   <li>
                       <p class="note">You will only be asked to pay, when your web-preview will be ready to live!</p>
                   </li>
    			   <li class="buttons">
    				  <input id="sbmt" class="button_text" type="submit" name="submit" value="Submit" onclick="fcheck();return false;">
    				  <script>
                        function fcheck(){
                            var isTrue = true;
                    		$("input").each(function(e, ele){
                    			if($(ele).val() == ""){
                    				if($(ele).attr("placeholder") != undefined)
                    					$(ele).parent().find(".err").html($(ele).attr("placeholder").toLowerCase() + " can't be empty!");
                    				else
                    					$(ele).parent().find(".err").html("price can't be empty!");
                    				isTrue = false;
                    			}
                    			else
                					$(ele).parent().find(".err").html("");
                    		});
                            if(isTrue){
                                sconfirm();
                                $('#myModal').modal('toggle');
                                return true;
                            }
                            else{
                              return false;
                            }
                        }
                        $(document).ready(function(){
                            /*$("button#sbmt").click(function(){var e=$("input").serializeArray(),t="",a=$($("input[type='text'")[10]).val()/100;$("input[type='checkbox']:checked").each(function(e,a){t+=$(a).val()+", "}),t=t.substring(0,t.length-2),e[11]={name:"country",value:$("select").val()},e[12]={name:"id",value:id},e[13]={name:"svcss",value:t},e[13]={name:"idt",value:btoa("##"+a+"idt")},$.ajax({url:"/order/submit",method:"POST",data:e,dataType:"json"}).always(function(){console.log("sdsd");var e=$("#form_container");$(e).empty(),$(e).append("<p class='note'><h2>Your request has been submitted successfully!</h2><h4>We will process your request and will connect you soon, once your requested product will be ready for preview!</h4></p>")}),$("p.note").css("border",0),$("#form_container").css("border",0)});*/
                             $("button#sbmt").click(function(){
                                var input = $("input").serializeArray();
                                var svcs="";
                                var idt = $($("input[type='text'")[10]).val()/100;
                                $("input[type='checkbox']:checked").each(function(e, ele){
                                    svcs += $(ele).val() + ", ";
                                });
                                svcs = svcs.substring(0, svcs.length - 2);
                                input[11]={name:"country", value:$("select").val()};
                                input[12]={name:"id", value:id};
                                input[13]={name:"svcss", value:svcs};
                                input[14]={name:"idt", value:btoa("##" + idt + "idt")};
                                input[15]={name:"fsdtl", value:CKEDITOR.instances['fabout'].getData()};
                                input[16]={name:"ida", value:$("input[name='ida'").val()};
                                
                                $.ajax({
                                  url: "/order/submit",
                                  method: "POST",
                                  data: input,
                                  dataType: "json"
                                }).always(function(i, d){
                                  var div = $("#form_container");
                                  $("#ldr").hide();
                                  $(div).append("<p><h4 style='color:blue;'>Request ID: "+d.oid+"</h4><h4>Your request has been registered successfully. We will process your request and will connect you soon, once your requested product will be ready for preview!</h4></p>");
                                });
                                $("#form_container form").hide();
                                $("#form_container h1").hide();
                                $("#ldr").show();
                            });
                        });
                        $("input[type='checkbox']").change(function(){
                            var th = 0;
                            $("input[type='checkbox']:checked").each(function(e, ele){
                                console.log($(ele).attr("ip"));
                                th += $(ele).attr("ip") * 100;
                                $($("input[type='text']")[11]).val(th);
                            });
                        });
    				  </script>
    				</li>
                </ul>
             </form>
         </div>
         <img id="bottom" src="bottom.png" alt="">
      </div>
    </div>
</div>
<hr>
<div class="modal" id="myModal" role="dialog">
<div class="modal-dialog">
   <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" onclick="sconfirm">&times;</button>
      </div>
      <div class="modal-body">
      <table id="data"></table>
      <p class="note">
            Do you need any correction? Click on cancel button!
      </p>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" id="sbmt">Submit</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
   </div>
</div>

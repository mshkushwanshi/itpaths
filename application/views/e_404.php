<div class="row h-100" align="center">
   <div class="col-sm-12 my-auto">
    <div class="card card-block w-25">
        <img src="<?php echo base_url("images/e404.png");?>" class="err" />
        <table>
            <tr>
                <td><a href="<?php echo base_url("");?>"> Home </a></td>
                <td><a href="<?php echo base_url("order/new");?>"> Order </a></td>
                <td><a href="<?php echo base_url("blog");?>"> Blogs </a></td>
            </tr>
            <tr>
                <td><a href="https://teams.itpaths.com"> HRML Portal </a></td>
                <td><a href="https://teams.itpaths.com/agile"> Agile Board </a></td>
                <td><a href="https://teams.itpaths.com/drive"> Drive </a></td>
            </tr>
            <tr>
                <td><a href="<?php echo base_url("notes");?>"> Notes </a></td>
                <td><a href="<?php echo base_url("#contact");?>"> Contact Us </a></td>
                <td><a href="<?php echo base_url("#aboutUs");?>"> About Us </a></td>
            </tr>
        </table>
    </div>
   </div>
</div>
<style>
.err{
    top: 24px;
    margin-top: 82px;
}
body {
    background-color: #f7f8f9;
}
td {
    padding-bottom: 15px;
}
td a {
    font-size: 18px;
    margin: 30px;
    padding-bottom: 20px;
    color: #655a52;
}
</style>
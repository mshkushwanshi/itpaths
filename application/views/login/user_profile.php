<?php
$user_id=$this->session->userdata('id');

if(!$user_id){
  redirect('user/login');
}

 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Welcome User</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
	  <span style="background-color:red;">
         <div class="container">
			<img src = "<?php echo site_url('/img/logo.png');?>" height = "40px" style="fload:left;margin:5px;"/>
            <div class="row">
               <div class="col-md-4 col-md-offset-4">
                  <div class="login-panel panel panel-success" style="margin:15px;">
                     <div class="panel-heading">
                        <h3 class="panel-title">User Profile</h3>
                     </div>
					 
					  <table class="table table-bordered table-striped">
						<tr>
						  <th colspan="2"><h4 class="text-center">User Info</h3></th>
						</tr>
						  <tr>
							<td>User Name</td>
							<td><?php echo $this->session->userdata('username'); ?></td>
						  </tr>
						  <tr>
							<td>User Email</td>
							<td><?php echo $this->session->userdata('email');  ?></td>
						  </tr>
						  <tr>
							<td>OS</td>
							<td><?php echo $this->session->userdata('os');  ?></td>
						  </tr>
						  <tr>
							<td>AppName</td>
							<td><?php echo $this->session->userdata('appname');  ?></td>
						  </tr>
					  </table>
					  <a href="<?php echo base_url('user/logout');?>" >  <button type="button" class="btn-primary">Logout</button></a>
					</div>
               </div>
			   <?php echo "Welcome, ".$this->session->get_userdata()['username'];?>
			   <a href="<?php echo base_url('user/logout');?>" >  <button type="button" class="btn-primary" style="margin:15px; border-radius:7px;">Logout</button></a>
            </div>
         </div>
      </span>
   </body>
</html>
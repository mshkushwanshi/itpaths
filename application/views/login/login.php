<!-- Page Header -->
<header class="masthead" style="background-image: url('<?php echo base_url('img/lbg2.jpg');?>')">
   <div class="overlay"></div>
   <div class="container">
      <div class="row">
         <div class="col-lg-8 col-md-10 mx-auto">
            <div class="page-heading">
               <br/><br/>
               <span style="background-color:red;">
                  <div class="login-panel panel panel-success" style="margin:15px;">
                     <?php
                        $success_msg= $this->session->flashdata('success_msg');
                        $error_msg= $this->session->flashdata('error_msg');
                        if($success_msg){
                        ?>
                     <div class="alert alert-success">
                        <?php echo $success_msg; ?>
                     </div>
                     <?php
                        }
                        if($error_msg){
                        ?>
                     <div class="alert alert-danger">
                        <?php echo $error_msg; ?>
                     </div>
                     <?php
                        }
                        ?>
                    <h3>Login</h3>
                     <form role="form" method="post" action="<?php echo base_url('user/validate/');if(isset($uri3)) echo $uri3; ?>">
                        <fieldset>
                           <div class="form-group">
                              <input class="form-control" placeholder="E-mail" name="email" type="text" autofocus>
                           </div>
                           <div class="form-group">
                              <input class="form-control" placeholder="Password" name="password" type="password" value="">
                           </div>
                           <input class="btn btn-success" type="submit" value="login" name="login">
                        </fieldset>
                     </form>
                     <center><b>Not registered ?</b>
                        <br>
                        </b><a href="<?php echo base_url('user/subscribe'); ?>">Register here</a>
                     </center>
                  </div>
               </span>
            </div>
         </div>
      </div>
   </div>
</header>
<hr>
<style>
    input.btn.btn-success {
        left: 0;
        position: absolute;
        margin-left: 32px;
        border-radius: 5px;
    }
    .login-panel.panel.panel-success center {
        position: absolute;
        right: 0;
        margin-right: 30px;
    }
    .col-lg-8.col-md-10.mx-auto {
        margin-top: 75px;
        padding-bottom: 75px;
    }
    .login-panel.panel.panel-success h3 {
        text-align: left;
    }
</style>
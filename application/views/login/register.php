    <!-- Page Header -->
    <header class="masthead" style="background-image: url('<?php echo base_url('img/register.jpg');?>')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page-heading">
                  <div class="login-panel panel panel-success" style="margin-top:75px;">
                     <div class="panel-heading">
                        <h3 class="panel-title">New User</h3>
                     </div>
                     <div class="panel-body">
                        <?php
                           $error_msg=$this->session->flashdata('error_msg');
                           if($error_msg){
                             echo $error_msg;
                           }
                            ?>
                        <form role="form" method="post" action="<?php echo base_url('user/register'); ?>">
                           <fieldset>
							         <div class="form-group">
                                 <input class="form-control" placeholder="First Name" name="fname" type="text" autofocus>
                              </div>
                              <div class="form-group">
                              <input class="form-control" placeholder="Last Name" name="lname" type="text" autofocus>
                              </div>
                              <div class="form-group">
                                 <input class="form-control" placeholder="Username" name="username" type="text" autofocus>
                              </div>
                              <div class="form-group">
                                 <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                              </div>
                              <div class="form-group">
                                 <input class="form-control" placeholder="Password" name="password" type="password" value="">
                              </div>
                              <div class="form-group">
                                 <input class="form-control" placeholder="Confirm Password" name="cpassword" type="password" value="">
                              </div>
                              <div class="form-group">
                                 <input class="form-control" placeholder="OS" name="os" type="hidden" value="">
                              </div>
                              <div class="form-group">
                                 <input class="form-control" placeholder="AppName" name="appname" type="hidden" value="">
                              </div>
							         <div class="form-group">
                                 <input class="form-control" placeholder="Phone Number" name="phone" type="text" value="">
                              </div>
                              <input class="btn btn-sm btn-primary" type="submit" value="Register" name="register" >
                           </fieldset>
                        </form>
                        <center><b>Already registered ?</b> <br></b><a href="<?php echo base_url('user/login'); ?>">Login here</a></center>
                        <!--for centered text-->
                     </div>
                  </div>
               </div>
        </div>
      </div>
    </div>
  </header>

  <hr>
  <style>
    l.login-panel.panel.panel-success center {
        position: absolute;
        right: 0;
        margin-right: 30px;
    }
    .col-lg-8.col-md-10.mx-auto {
        margin-top: 75px;
    }
    .login-panel.panel.panel-success h3 {
        text-align: left;
    }
    input.btn.btn-sm.btn-primary {
        position: absolute;
        left: 0;
        margin-left: 15px;
        border-radius: 5px;
    }
    .panel-body center {
        position: absolute;
        right: 0;
        text-align: right;
        margin-right: 15px;
    }
    .panel-body {
        padding-bottom: 15px;
    }
  </style>
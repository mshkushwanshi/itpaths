
    <!--=========== BEGIN solution BANNER SECTION ================-->
    <section id="imgBanner">
      <h2>solution Details</h2>
    </section>
    <!--=========== END solution BANNER SECTION ================-->

    
    <!--=========== BEGIN solution BANNER SECTION ================-->
    <section id="solutionArchive">
      <div class="container">
        <div class="row">
          <!-- start solution content -->
          <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="solutionArchive_content">              
             <div class="singlesolution_ferimg_area">
               <div class="singlesolution_ferimg">
                 <img src="img/solution-single.jpg" alt="img">
               </div>  
                <div class="singlesolution_bottom">
                  <h2>Introduction To Matrix</h2>
                  <span class="singlesolution_author">
                    <img alt="img" src="img/author.jpg">
                    Richard Remus, Teacher
                  </span>
                  <span class="singlesolution_price">$20</span>
                </div>
             </div>
             <div class="single_solution_content">
               <h2>About The Coures</h2>
               <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
               <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
               <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
               <table class="table table-striped solution_table">
                <thead>
                  <tr>          
                    <th>solution Title</th>
                    <th>Instructor</th>
                    <th>Timing</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>          
                    <td>Basic Of Matrix</td>
                    <td>Dr. Steve Palmer</td>
                    <td>08:00 to 13:00</td>
                  </tr>
                  <tr>
                    <td>Matrix Overview</td>          
                    <td>Jacob</td>                    
                    <td>08:00 to 13:00</td>
                  </tr>
                  <tr>
                    <td>Matrix Application</td>          
                    <td>Kimberly Jones</td>                    
                    <td>08:00 to 13:00</td>
                  </tr>
                  <tr>
                    <td>Advanced Matrix</td>          
                    <td>Dr. Klee</td>                    
                    <td>08:00 to 13:00</td>
                  </tr>
                </tbody>
              </table>
             </div>
             <!-- start related solution -->
             <div class="related_solution">
                <h2>More solutions</h2>
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="single_solution wow fadeInUp" >
                      <div class="singsolution_imgarea">
                        <img src="img/solution-1.jpg">
                        <div class="mask">                         
                          <a class="solution_more" href="#">View solution</a>
                        </div>
                      </div>
                      <div class="singsolution_content">
                        <h3 class="singsolution_title"><a href="#">Introduction To Matrix</a></h3>
                        <p class="singsolution_price"><span>$20</span> Per One Month</p>
                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                      </div>
                      <div class="singsolution_author">
                        <img alt="img" src="img/author.jpg">
                        <p>Richard Remus, Teacher</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="single_solution wow fadeInUp" >
                      <div class="singsolution_imgarea">
                        <img src="img/solution-1.jpg">
                        <div class="mask">                         
                          <a class="solution_more" href="#">View solution</a>
                        </div>
                      </div>
                      <div class="singsolution_content">
                        <h3 class="singsolution_title"><a href="#">Introduction To Matrix</a></h3>
                        <p class="singsolution_price"><span>$20</span> Per One Month</p>
                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                      </div>
                      <div class="singsolution_author">
                        <img alt="img" src="img/author.jpg">
                        <p>Richard Remus, Teacher</p>
                      </div>
                    </div>
                  </div>                                    
                </div>
              </div>
              <!-- End related solution -->
            </div>
          </div>
          <!-- End solution content -->

          <!-- start solution archive sidebar -->
          <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="solutionArchive_sidebar">
              <!-- start single sidebar -->
              <div class="single_sidebar">
                <h2>Events <span class="fa fa-angle-double-right"></span></h2>
                <ul class="news_tab">
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#" class="news_img">
                          <img alt="img" src="img/news.jpg" class="media-object">
                        </a>
                      </div>
                      <div class="media-body">
                       <a href="#">Dummy text of the printing and typesetting industry</a>
                       <span class="feed_date">27.02.15</span>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#" class="news_img">
                          <img alt="img" src="img/news.jpg" class="media-object">
                        </a>
                      </div>
                      <div class="media-body">
                       <a href="#">Dummy text of the printing and typesetting industry</a>
                       <span class="feed_date">28.02.15</span>                
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#" class="news_img">
                          <img alt="img" src="img/news.jpg" class="media-object">
                        </a>
                      </div>
                      <div class="media-body">
                       <a href="#">Dummy text of the printing and typesetting industry</a>
                       <span class="feed_date">28.02.15</span>                
                      </div>
                    </div>
                  </li>                  
                </ul>
              </div>
              <!-- End single sidebar -->
              <!-- start single sidebar -->
              <div class="single_sidebar">
                <h2>Quick Links <span class="fa fa-angle-double-right"></span></h2>
                <ul>
                  <li><a href="#">Link 1</a></li>
                  <li><a href="#">Link 2</a></li>
                  <li><a href="#">Link 3</a></li>
                  <li><a href="#">Link 4</a></li>
                  <li><a href="#">Link 5</a></li>
                </ul>
              </div>
              <!-- End single sidebar -->
              <!-- start single sidebar -->
              <div class="single_sidebar">
                <h2>Sponsor Add <span class="fa fa-angle-double-right"></span></h2>
                <a class="side_add" href="#"><img src="img/side-add.jpg" alt="img"></a>
              </div>
              <!-- End single sidebar -->
            </div>
          </div>
          <!-- start solution archive sidebar -->
        </div>
      </div>
    </section>
    <!--=========== END solution BANNER SECTION ================-->

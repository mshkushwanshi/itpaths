    <!--=========== BEGIN solution BANNER SECTION ================-->
    <section id="imgBanner">
      <h2>solution Archive</h2>
    </section>
    <!--=========== END solution BANNER SECTION ================-->
    
    <!--=========== BEGIN solution BANNER SECTION ================-->
    <section id="solutionArchive">
      <div class="container">
        <div class="row">
          <!-- start solution content -->
          <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="solutionArchive_content">
              <div class="row">
                <!-- start single solution -->
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="single_solution wow fadeInUp">
                    <div class="singsolution_imgarea">
                      <img src="img/solution-1.jpg" />
                      <div class="mask">                         
                        <a href="solution-single.html" class="solution_more">View solution</a>
                      </div>
                    </div>
                    <div class="singsolution_content">
                    <h3 class="singsolution_title"><a href="solution-single.html">Introduction To Matrix</a></h3>
                    <p class="singsolution_price"><span>$20</span> Per One Month</p>
                    <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                    </div>
                    <div class="singsolution_author">
                      <img src="img/author.jpg" alt="img">
                      <p>Richard Remus, Teacher</p>
                    </div>
                  </div>
                </div>
                <!-- End single solution -->
                <!-- start single solution -->
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="single_solution wow fadeInUp">
                    <div class="singsolution_imgarea">
                      <img src="img/solution-1.jpg" />
                      <div class="mask">                         
                        <a href="solution-single.html" class="solution_more">View solution</a>
                      </div>
                    </div>
                    <div class="singsolution_content">
                    <h3 class="singsolution_title"><a href="solution-single.html">Introduction To Matrix</a></h3>
                    <p class="singsolution_price"><span>$20</span> Per One Month</p>
                    <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                    </div>
                    <div class="singsolution_author">
                      <img src="img/author.jpg" alt="img">
                      <p>Richard Remus, Teacher</p>
                    </div>
                  </div>
                </div>
                <!-- End single solution -->
                <!-- start single solution -->
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="single_solution wow fadeInUp">
                    <div class="singsolution_imgarea">
                      <img src="img/solution-1.jpg" />
                      <div class="mask">                         
                        <a href="solution-single.html" class="solution_more">View solution</a>
                      </div>
                    </div>
                    <div class="singsolution_content">
                    <h3 class="singsolution_title"><a href="solution-single.html">Introduction To Matrix</a></h3>
                    <p class="singsolution_price"><span>$20</span> Per One Month</p>
                    <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                    </div>
                    <div class="singsolution_author">
                      <img src="img/author.jpg" alt="img">
                      <p>Richard Remus, Teacher</p>
                    </div>
                  </div>
                </div>
                <!-- End single solution -->
                <!-- start single solution -->
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="single_solution wow fadeInUp">
                    <div class="singsolution_imgarea">
                      <img src="img/solution-1.jpg" />
                      <div class="mask">                         
                        <a href="solution-single.html" class="solution_more">View solution</a>
                      </div>
                    </div>
                    <div class="singsolution_content">
                    <h3 class="singsolution_title"><a href="solution-single.html">Introduction To Matrix</a></h3>
                    <p class="singsolution_price"><span>$20</span> Per One Month</p>
                    <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                    </div>
                    <div class="singsolution_author">
                      <img src="img/author.jpg" alt="img">
                      <p>Richard Remus, Teacher</p>
                    </div>
                  </div>
                </div>
                <!-- End single solution -->
                <!-- start single solution -->
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="single_solution wow fadeInUp">
                    <div class="singsolution_imgarea">
                      <img src="img/solution-1.jpg" />
                      <div class="mask">                         
                        <a href="solution-single.html" class="solution_more">View solution</a>
                      </div>
                    </div>
                    <div class="singsolution_content">
                    <h3 class="singsolution_title"><a href="solution-single.html">Introduction To Matrix</a></h3>
                    <p class="singsolution_price"><span>$20</span> Per One Month</p>
                    <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                    </div>
                    <div class="singsolution_author">
                      <img src="img/author.jpg" alt="img">
                      <p>Richard Remus, Teacher</p>
                    </div>
                  </div>
                </div>
                <!-- End single solution -->
                <!-- start single solution -->
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="single_solution wow fadeInUp">
                    <div class="singsolution_imgarea">
                      <img src="img/solution-1.jpg" />
                      <div class="mask">                         
                        <a href="solution-single.html" class="solution_more">View solution</a>
                      </div>
                    </div>
                    <div class="singsolution_content">
                    <h3 class="singsolution_title"><a href="solution-single.html">Introduction To Matrix</a></h3>
                    <p class="singsolution_price"><span>$20</span> Per One Month</p>
                    <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                    </div>
                    <div class="singsolution_author">
                      <img src="img/author.jpg" alt="img">
                      <p>Richard Remus, Teacher</p>
                    </div>
                  </div>
                </div>
                <!-- End single solution -->
              </div>
              <!-- start previous & next button -->
              <div class="single_blog_prevnext">
                <a href="#" class="prev_post wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;"><i class="fa fa-angle-left"></i>Previous</a>
                <a href="#" class="next_post wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">Next<i class="fa fa-angle-right"></i></a>
              </div>
            </div>
          </div>
          <!-- End solution content -->

          <!-- start solution archive sidebar -->
          <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="solutionArchive_sidebar">
              <!-- start single sidebar -->
              <div class="single_sidebar">
                <h2>Events <span class="fa fa-angle-double-right"></span></h2>
                <ul class="news_tab">
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#" class="news_img">
                          <img alt="img" src="img/news.jpg" class="media-object">
                        </a>
                      </div>
                      <div class="media-body">
                       <a href="#">Dummy text of the printing and typesetting industry</a>
                       <span class="feed_date">27.02.15</span>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#" class="news_img">
                          <img alt="img" src="img/news.jpg" class="media-object">
                        </a>
                      </div>
                      <div class="media-body">
                       <a href="#">Dummy text of the printing and typesetting industry</a>
                       <span class="feed_date">28.02.15</span>                
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#" class="news_img">
                          <img alt="img" src="img/news.jpg" class="media-object">
                        </a>
                      </div>
                      <div class="media-body">
                       <a href="#">Dummy text of the printing and typesetting industry</a>
                       <span class="feed_date">28.02.15</span>                
                      </div>
                    </div>
                  </li>                  
                </ul>
              </div>
              <!-- End single sidebar -->
              <!-- start single sidebar -->
              <div class="single_sidebar">
                <h2>Quick Links <span class="fa fa-angle-double-right"></span></h2>
                <ul>
                  <li><a href="#">Link 1</a></li>
                  <li><a href="#">Link 2</a></li>
                  <li><a href="#">Link 3</a></li>
                  <li><a href="#">Link 4</a></li>
                  <li><a href="#">Link 5</a></li>
                </ul>
              </div>
              <!-- End single sidebar -->
              <!-- start single sidebar -->
              <div class="single_sidebar">
                <h2>Sponsor Add <span class="fa fa-angle-double-right"></span></h2>
                <a class="side_add" href="#"><img src="img/side-add.jpg" alt="img"></a>
              </div>
              <!-- End single sidebar -->
            </div>
          </div>
          <!-- start solution archive sidebar -->
        </div>
      </div>
    </section>
    <!--=========== END solution BANNER SECTION ================-->
    

<div class="container" style="margin-top:100px">
<ul class="news_tab">
         <li>
            <div class="media">
               <div class="media-left">
                  <a class="news_img" href="#">
                  <img class="media-object" src="<?php echo site_url('img/content/lipstick.jpeg'); ?>" alt="img">
                  </a>
               </div>
               <div class="media-body">
                  <a href="#">Boutique Brand Management</a>
                  <span class="feed_date">Solution provided for multiple countries to manage their products images, documents and videos, which was complete DAM and SAP integrated solution </span>
               </div>
            </div>
         </li>
         <li>
            <div class="media">
               <div class="media-left">
                  <a class="news_img" href="<?php echo base_url('blog/get/4'); ?>">
                  <img class="media-object" src="<?php echo site_url('img/content/animation.jpeg'); ?>" alt="img">
                  </a>
               </div>
               <div class="media-body">
                  <a href="4">Media Vault</a>
                  <span class="feed_date">Animation video content management solution to manage all videos, audios, images and their documents to get managed and shared easily</span>
               </div>
            </div>
         </li>
         <li>
            <div class="media">
               <div class="media-left">
                  <a class="news_img" href="#">
                  <img class="media-object" src="<?php echo site_url('img/content/insurance.jpeg'); ?>" alt="img">
                  </a>
               </div>
               <div class="media-body">
                  <a href="#">Claim Payment Automation</a>
                  <span class="feed_date">To provide an automated solution to process payment of submitted claimed with less or without human intervention</span>
               </div>
            </div>
         </li>
         <li>
            <div class="media">
               <div class="media-left">
                  <a class="news_img" href="#">
                  <img class="media-object" src="<?php echo site_url('img/content/aircraft.jpeg'); ?>" alt="img">
                  </a>
               </div>
               <div class="media-body">
                  <a href="#">Aircraft Product Management</a>
                  <span class="feed_date">A solution to enterprise contents and their product assets like documents, images, video and audio</span>
               </div>
            </div>
         </li>
      </ul>
</div>
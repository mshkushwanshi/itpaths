<!DOCTYPE html>
<html lang="en">

<head>
  <script src="<?php echo base_url("js/jquery-3.5.1.slim.min.js");?>"></script>
  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <script>
    (adsbygoogle = window.adsbygoogle || []).push({
      google_ad_client: "ca-pub-1882767817709882",
      enable_page_level_ads: true
    });
  </script>
  <meta charset="utf-8">
  <meta name="description" content="Tapasvi Info Solutions, in incorporate with IT Paths Solution, a leading Indian sowtware consultancy services provides all OpenText customizing solution for SAP and AI automation using Tensorflow integration for our business automation">
  <meta name="keywords" content="tapasvi, tapasvi info, tapasvi info solutions,it paths, itpaths.com, data access management, enterprise content management, web experience management, business rules automation, artificial intelligenece solution, dam solution, ecm solution, bpm solution, software company in hathras, it company in hathras, it company in agra, software company in agra">
  <meta name="author" content="Umesh Kushwanshi">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="google-site-verification" content="JlCHdcjhuFhyqUPD2PQAQV9-6Gwj1J7tiAoB48WCQAs" />
  <link rel="shortcut icon" type="image/icon" href="<?php echo site_url('img/wpf-favicon.png'); ?>" />
  <link href="<?php echo site_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
<!-- Latest compiled and minified JavaScript -->
  <script src="<?php echo base_url("js/bootstrap.min.js");?>"></script>
  <link href="<?php echo site_url('css/font-awesome.min.css'); ?>" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo site_url('css/superslides.css'); ?>">
  <link href="<?php echo site_url('css/slick.css'); ?>" rel="stylesheet">
  <link rel='stylesheet prefetch' href="<?php echo site_url('css/jquery.circliful.css'); ?>">
  <link rel="stylesheet" href="<?php echo site_url('css/animate.css'); ?>">
  <link type="text/css" media="all" rel="stylesheet" href="<?php echo site_url('css/jquery.tosrus.all.css'); ?>" />
  <link id="switcher" href="<?php echo site_url('css/themes/default-theme.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('style.css'); ?>" rel="stylesheet">
    <link rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Merriweather">
    <link rel="stylesheet"
  href="https://fonts.googleapis.com/css?family=Varela">
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script data-ad-client="ca-pub-5233448333018429" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body>
  <a class="scrollToTop" href="<?php echo site_url('indexs/'); ?>"></a>
  <header id="header">
    <div class="menu_area">
      <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo site_url('indexs/index'); ?>">
              <!-- IT<span>Paths</span> --></a>
            <a class="navbar-brand" href="<?php echo site_url('indexs/index'); ?>"><img src="<?php echo site_url('images/clogo.jpeg'); ?>" alt="logo"></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul id="top-menu" class="nav navbar-nav navbar-right main-nav">
              <li><a href="<?php echo site_url('indexs/'); ?>">Home</a></li>
              <li><a href="#aboutUs">About Us</a></li>
              <li><a href="<?php echo site_url('#contact'); ?>">Contact</a></li>
              <!--<li><a href="<?php echo site_url('home/'); ?>">Video Portal</a></li>-->
              <li class="dropdown"><a href="<?php echo base_url("blog");?>">Blogs</a>
              <!-- <?php echo site_url('blog/index'); ?> 
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                  <?php if (isset($cats)) foreach ($cats as $cat) { ?>
                    <a href="<?php echo base_url("")."blog/blogs/".$cat['id']; ?>">
                    <li class="dropdown-item" type="button"><?php echo $cat['title']; ?></li>
                    <?php } ?>
                    </a>
                </ul>-->
              </li>
              <!--<li class="dropdown"><a href="#">Orders</a>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <a href="<?php echo base_url("")."order"; ?>">
                    <li class="dropdown-item" type="button"><i class="fa fa-file" aria-hidden="true"></i>&nbsp;&nbsp;Add New</li>
                    </a>
                    <a href="<?php echo base_url("")."order/get"; ?>">
                    <li class="dropdown-item" type="button"><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;&nbsp;List All</li>
                    </a>
                </ul>
              </li>-->
              <li class="dropdown"><a href="#">Notes</a>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <a href="<?php echo base_url("notes/add"); ?>">
                    <li class="dropdown-item" type="button"><i class="fa fa-file" aria-hidden="true"></i>&nbsp;&nbsp;Add New</li>
                    </a>
                    <a href="<?php echo base_url("notes/get"); ?>">
                    <li class="dropdown-item" type="button"><i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;&nbsp;List All</li>
                    </a>
                </ul>
              </li>
              <li class="dropdown"><a href="#">Organization</a>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                    <a href="https://portal.itpaths.com" target="_blank">
                    <li class="dropdown-item" type="button"><i class="fa fa-product-hunt" aria-hidden="true"></i>&nbsp;&nbsp;Agile</li>
                    </a>
                    <a href="https://portal.itpaths.com/hrms" target="_blank">
                    <li class="dropdown-item" type="button"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;&nbsp;HRMS</li>
                    </a>
                    <a href="https://portal.itpaths.com/mail" target="_blank">
                    <li class="dropdown-item" type="button"><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;Mails</li>
                    </a>
                    <a href="http://tapasviinfosolutions.slack.com/" target="_blank">
                    <li class="dropdown-item" type="button"><i class="fa fa-commenting-o" aria-hidden="true"></i>&nbsp;&nbsp;Slack</li>
                    </a>
                    <a href="https://portal.itpaths.com/hrms/meet" target="_blank">
                    <li class="dropdown-item" type="button"><i class="fa fa-meetup" aria-hidden="true"></i>&nbsp;&nbsp;Meeting</li>
                    </a>
                    <a href="https://portal.itpaths.com/drive" target="_blank">
                    <li class="dropdown-item" type="button"><i class="fa fa-hdd-o" aria-hidden="true"></i>&nbsp;&nbsp;Drive</li>
                    </a>
                    <a href="https://github.com/Tapasvi-Info-Solutions" target="_blank">
                    <li class="dropdown-item" type="button"><i class="fa fa-code" aria-hidden="true"></i>&nbsp;&nbsp;Code Base</li>
                    </a>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </header>
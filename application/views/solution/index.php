<section id="slider">
   <div class="row">
      <div class="col-lg-12 col-md-12">
         <div class="slider_area">
            <div id="slides">
                <ul class="slides-container">
                    <li>
                     <img src="<?php echo site_url('img/slider/2.jpg');?>" alt="img">
                     <div class="slider_caption">
                    	<h2>Enterprise Solution</h2>
                    	<p>Our enterprise solutions is based on world's most popular IT products and technologies</p>
                    	<p>OpenText products solutions for Data access management [DAM], Web Experience Management [WEM], Extented Contnet Management [xECM] and Business Process Management [BPM]
                    	    </p>
                    	    <p>ERP, Hybris & SAP integration with OpenText</p>
                    		<p>Responsive and Native UI development for web and mobile applications</p>
                    		<p>Kubernetes, Docker containerization using AWS, Azure and GCP</p>
                    	<!--<a class="slider_btn" href="#">Know More</a>-->
                     </div>
                     </li>
                    <!-- Start single slider-->
                    <li>
                     <img src="<?php echo site_url('img/slider/3.jpg');?>" alt="img">
                     <div class="slider_caption slider_right_caption">
                    	<h2>Best Production Support</h2>
                    	<p>We're live for 24/7 support to our customer for real time deliverables with minium SLA. Our automated system to manage our production support, ensure not to miss any production issues un-attended by routing tickets to concerned team and our automated algorithms analyze root - cause of failure with the help of defined workflows</p>
                    	<!--<a class="slider_btn" href="#">Know More</a>-->
                     </div>
                    </li>
                    <!-- Start single slider-->
                    <li>
                     <img src="<?php echo site_url('img/slider/4.jpg');?>" alt="img">
                     <div class="slider_caption">
                    	<h2>Find out you in better way</h2>
                    	<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search</p>
                    	<!--<a class="slider_btn" href="#">Know More</a>-->
                     </div>
                    </li>
                </ul>
               <nav class="slides-navigation">
                  <a href="#" class="next"></a>
                  <a href="#" class="prev"></a>
               </nav>
            </div>
         </div>
      </div>
   </div>
</section>
<img class="col-lg-12 col-md-4 col-sm-12 col-xs-12 visible-lg visible-md visible-xs visible-sm" src="<?php echo site_url('img/slider/2.png'); ?>" alt="img">
<img class="col-lg-12 col-md-4 col-sm-12 col-xs-12 visible-lg visible-md visible-xs visible-sm" src="<?php echo site_url('img/slider/1.png'); ?>" alt="img">
<!-- <section id="process" class="wpb_row full-row">
   <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner ">
         <div class="wpb_wrapper">
            <div class="buy-process-wrap">
               <div class="buy-process-items">
                  <div class="buy-process-item">
                     <div class="text-wrap">
                        <h4><strong>Requirement Gathering</strong></h4>
                        <p>Understanding requirements and prepare wire - frames basic flow charts</p>
                     </div>
                     <span>1</span>
                     <div class="icon-wrapper">
                        <i class="fa fa-calendar"></i>
                     </div>
                  </div>
                  <div class="buy-process-item">
                     <div class="text-wrap">
                        <h4><strong>Design Discussion</strong></h4>
                        <p width="200px">Design a prototype as per the business logic and validation and come with the all the possible test scenarios</p>
                     </div>
                     <span>2</span>
                     <div class="icon-wrapper">
                        <i class="fa fa-laptop"></i>
                     </div>
                  </div>
                  <div class="buy-process-item featured">
                     <div class="text-wrap">
                        <h4><strong>Development and Testing</strong></h4>
                        <p>We do development and testing as agiled sprint planning and stories</p>
                     </div>
                     <span>3</span>
                     <div class="icon-wrapper">
                        <i class="fa fa-keyboard-o"></i>
                     </div>
                  </div>
                  <div class="buy-process-item">
                     <div class="text-wrap">
                        <h4><strong>Delivery</strong></h4>
                        <p>After product development and testing, We do agiled timely delivery</p>
                     </div>
                     <span>4</span>
                     <div class="icon-wrapper">
                        <i class="fa fa-paper-plane"></i>
                     </div>
                  </div>
                  <div class="buy-process-item">
                     <div class="text-wrap">
                        <h4><strong>Support</strong></h4>
                        <p>We belive in seamless product delivery and help our client for live product support all the time</p>
                     </div>
                     <span>5</span>
                     <div class="icon-wrapper">
                        <i class="fa fa-shield"></i>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section> -->
<style>
   a.anchor {
      display: block;
      position: relative;
      top: 250px;
      visibility: hidden;
   }
</style>
<a name="aboutUs"></a>
<section id="aboutUs">
   <div class="container">
      <div class="row">
         <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="aboutus_area wow fadeInLeft">
               <h2 class="titile">About Us</h2>
               <p><b>With IT Paths a branch of Tapasvi Info Solutions,
                     we believe a technology must be advance and automated while supporting your business.</b></p>
               <p>
                  Together, let’s make your products smarter. Your customer experiences more exceptional. Your people more productive.
                  Your processes more profitable. And your systems more powerful.</p>
               <p>We bring an advance solution from every aspect of your business with digital using our data access management solution from across the world securely.</p>
               <p>We provide enterprise and business solution as more robust and secured by defining a well formed and managed process hierarchy using our xECM and BPM solution.</P>
               <p>We provide business automation solution to our customers with less <b>Human</b> intervention using our Rule Engine solutions.</b>
                  <p>Simulaneously, we're also rasing a helping hand to our customers by providing corporate trainings</p>
            </div>
         </div>
         <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="newsfeed_area wow fadeInRight">
               <ul class="nav nav-tabs feed_tabs" id="myTab2">
                  <li class="active"><a href="#news" data-toggle="tab">Services</a></li>
                  <li><a href="#notice" data-toggle="tab">Projects</a></li>
                  <li><a href="#events" data-toggle="tab">Corporate Trainings</a></li>
               </ul>

               <div class="tab-content">
                  <div class="tab-pane fade in active" id="news">
                     <ul class="news_tab">
                        <li>
                           <div class="media">
                              <div class="media-left">
                                 <a class="news_img" href="#">
                                    <img class="media-object" src="<?php echo site_url('img/content/cloud.png'); ?>" alt="img">
                                 </a>
                              </div>
                              <div class="media-body">
                                 <a href="<?php echo base_url('blog/get/1'); ?>">Complete Cloud Solution</a>
                                 <span class="feed_date">We provide flexible and afforable cloud based solution on Kubernetes, Docker</span>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="media">
                              <div class="media-left">
                                 <a class="news_img" href="#">
                                    <img class="media-object" src="<?php echo site_url('img/content/ot-sap-flow.png'); ?>" alt="img">
                                 </a>
                              </div>
                              <div class="media-body">
                                 <a href="#">OpenText with SAP Solution</a>
                                 <span class="feed_date">We provide complete SAP solution for OpenText Products like xECM, DAM with our custom services integration</span>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="media">
                              <div class="media-left">
                                 <a class="news_img" href="#">
                                    <img class="media-object" src="<?php echo site_url('img/content/ot-dam-workfow.png'); ?>" alt="img">
                                 </a>
                              </div>
                              <div class="media-body">
                                 <a href="#">Business Rule Automation Solution</a>
                                 <span class="feed_date">We provide easy and robust rules automation solution to customers from insurance and banking domain using latest rules frameworks like Drools, Red Hat Decision Manager and etc.</span>
                              </div>
                           </div>
                        </li>
                        <li>
                           <div class="media">
                              <div class="media-left">
                                 <a class="news_img" href="#">
                                    <img class="media-object" src="<?php echo site_url('img/content/angular.png'); ?>" alt="img">
                                 </a>
                              </div>
                              <div class="media-body">
                                 <a href="#">Mobile & Web Solution</a>
                                 <span class="feed_date">We also provide complete mobile and web solution using our angular and react-navtive development solution across cross-platform</span>
                              </div>
                           </div>
                        </li>
                     </ul>
                     <!-- <a class="see_all" href="#">See All</a> -->
                  </div>
                  <!-- Start notice tab content -->
                  <div class="tab-pane fade " id="notice">
                  <div class="tab-pane fade in active" id="news">
                  <ul class="news_tab">
         <li>
            <div class="media">
               <div class="media-left">
                  <a class="news_img" href="#">
                  <img class="media-object" src="<?php echo site_url('img/content/lipstick.jpeg'); ?>" alt="img">
                  </a>
               </div>
               <div class="media-body">
                  <a href="#">Boutique Brand Management</a>
                  <span class="feed_date">Solution provided for multiple countries to manage their products images, documents and videos, which was complete DAM and SAP integrated solution </span>
               </div>
            </div>
         </li>
         <li>
            <div class="media">
               <div class="media-left">
                  <a class="news_img" href="<?php echo base_url('blog/get/4'); ?>">
                  <img class="media-object" src="<?php echo site_url('img/content/animation.jpeg'); ?>" alt="img">
                  </a>
               </div>
               <div class="media-body">
                  <a href="4">Media Vault</a>
                  <span class="feed_date">Animation video content management solution to manage all videos, audios, images and their documents to get managed and shared easily</span>
               </div>
            </div>
         </li>
         <li>
            <div class="media">
               <div class="media-left">
                  <a class="news_img" href="#">
                  <img class="media-object" src="<?php echo site_url('img/content/insurance.jpeg'); ?>" alt="img">
                  </a>
               </div>
               <div class="media-body">
                  <a href="#">Claim Payment Automation</a>
                  <span class="feed_date">To provide an automated solution to process payment of submitted claimed with less or without human intervention</span>
               </div>
            </div>
         </li>
         <li>
            <div class="media">
               <div class="media-left">
                  <a class="news_img" href="#">
                  <img class="media-object" src="<?php echo site_url('img/content/aircraft.jpeg'); ?>" alt="img">
                  </a>
               </div>
               <div class="media-body">
                  <a href="#">Aircraft Product Management</a>
                  <span class="feed_date">A solution to enterprise contents and their product assets like documents, images, video and audio</span>
               </div>
            </div>
         </li>
      </ul>
                     <a class="see_all" href="<?php echo base_url("/indexs/projects");?>">See All</a>
                  </div>
                  
                  </div>
                  <div class="tab-pane fade " id="events">
                     <ul class="news_tab">
                        <li>
                           <div class="media">
                              <div class="media-left">
                                 <a class="news_img" href="#">
                                    <img class="media-object" src="<?php echo site_url('img/news.jpg'); ?>" alt="img">
                                 </a>
                              </div>
                              <div class="media-body">
                                 <a href="#">Drools</a>
                                 <span class="feed_date">Corporate trainings for BRMS, JBPM and Drools components</span>
                              </div>
                           </div>
                        </li>
						<li>
                           <div class="media">
                              <div class="media-left">
                                 <a class="news_img" href="#">
                                    <img class="media-object" src="<?php echo site_url('img/news.jpg'); ?>" alt="img">
                                 </a>
                              </div>
                              <div class="media-body">
                                 <a href="#">OpenText [ DAM, xECM, & AppWork ]</a>
                                 <span class="feed_date">Corporate trainings for OpenText Products</span>
                              </div>
                           </div>
                        </li>
						<li>
                           <div class="media">
                              <div class="media-left">
                                 <a class="news_img" href="#">
                                    <img class="media-object" src="<?php echo site_url('img/news.jpg'); ?>" alt="img">
                                 </a>
                              </div>
                              <div class="media-body">
                                 <a href="#">Red Hat Decision Manager</a>
                                 <span class="feed_date">Corporate trainings for RHDM rules administration and development solution</span>
                              </div>
                           </div>
                        </li>
                     </ul>
                     <a class="see_all" href="#">See All</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<section id="whyUs">
   <!-- Start why us top -->
   <div class="row">
      <div class="col-lg-12 col-sm-12">
         <div class="whyus_top">
            <div class="container">
               <!-- Why us top titile -->
               <div class="row">
                  <div class="col-lg-12 col-md-12">
                     <div class="title_area">
                        <h2 class="title_two">Why Us</h2>
                        <span></span>
                     </div>
                  </div>
               </div>
               <!-- End Why us top titile -->
               <!-- Start Why us top content  -->
               <div class="row">
                  <div class="col-lg-3 col-md-3 col-sm-3">
                     <div class="single_whyus_top wow fadeInUp">
                        <div class="whyus_icon">
                           <span class="fa fa-desktop"></span>
                        </div>
                        <h3>Technology</h3>
                        <p>We deliver solution with the latest technologies and including their upgrades</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3">
                     <div class="single_whyus_top wow fadeInUp">
                        <div class="whyus_icon">
                           <span class="fa fa-users"></span>
                        </div>
                        <h3>Best Product Delivery</h3>
                        <p>We deliver timely solution through the well experienced corporate workers who are currently working in the live product environments</p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3">
                     <div class="single_whyus_top wow fadeInUp">
                        <div class="whyus_icon">
                           <span class="fa fa-flask"></span>
                        </div>
                        <h3>Corporate Training</h3>
                        <p>Have good internal / external corporate training experience, deliver online and offline class room traings </p>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3">
                     <div class="single_whyus_top wow fadeInUp">
                        <div class="whyus_icon">
                           <span class="fa fa-support"></span>
                        </div>
                        <h3>Support</h3>
                        <p>Always open for supports for our delivered products and live corporate trainings solutions</p>
                     </div>
                  </div>
               </div>
               <!-- End Why us top content  -->
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 col-sm-12">
         <div class="whyus_bottom">
            <div class="slider_overlay"></div>
            <div class="container">
               <div class="skills">
                  <div class="col-lg-3 col-md-3 col-sm-3">
                     <div class="single_skill wow fadeInUp">
                        <div id="myStat" data-dimension="150" data-info="" data-width="10" data-fontsize="25" data-percent="90" data-fgcolor="#999" data-bgcolor="#fff" data-icon="fa-task"></div>
                        <h4>24/7 Support</h4>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3">
                     <div class="single_skill wow fadeInUp">
                        <div id="myStathalf2" data-dimension="150" data-info="" data-width="10" data-fontsize="25" data-percent="99.9" data-fgcolor="#999" data-bgcolor="#fff" data-icon="fa-task"></div>
                        <h4>99.9% Success Rate</h4>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3">
                     <div class="single_skill wow fadeInUp">
                        <div id="myStat2" data-dimension="150" data-info="" data-width="10" data-fontsize="25" data-percent="85" data-fgcolor="#999" data-bgcolor="#fff" data-icon="fa-task"></div>
                        <h4>Contigeous Engagement</h4>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3">
                     <div class="single_skill wow fadeInUp">
                        <div id="myStat3" data-dimension="150" data-info="" data-width="10" data-fontsize="25" data-percent="99" data-fgcolor="#999" data-bgcolor="#fff" data-icon="fa-task"></div>
                        <h4>Certified Solutions</h4>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- End why us bottom -->
</section>
<section id="oursolutions">
   <div class="container">
      <!-- Our solutions titile -->
      <div class="row">
         <div class="col-lg-12 col-md-12">
            <div class="title_area">
               <h2 class="title_two">Our solutions</h2>
               <span></span>
            </div>
         </div>
      </div>
      <!-- End Our solutions titile -->
      <!-- Start Our solutions content -->
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="oursolution_content">
               <ul class="solution_nav">
                  <li>
                     <div class="single_solution">
                        <div class="simg">
                           <img src="<?php echo site_url('img/ot-dam.jpg'); ?>" />
                           <!--<div class="mask">
                              <a href="#" class="solution_more">Join Us</a>
                           </div>-->
                        </div>
                        <div class="singsolution_content">
                           <h3 class="singsolution_title"><a href="#">Digital Management</a></h3>
                           <p class="singsolution_price">Enterprise asset management</p>
                           <p>Providing an easy and secured access to their enterprise content across platform, anytime and anywhere</p>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="single_solution">
                        <div class="simg">
                           <img src="<?php echo site_url('img/cyber-security.jpg'); ?>" />
                           <!--<div class="mask">
                              <a href="#" class="solution_more">Join US</a>
                           </div>-->
                        </div>
                        <div class="singsolution_content">
                           <h3 class="singsolution_title"><a href="#">Enterprise Solution</a></h3>
                           <p class="singsolution_price">Secured business solution</p>
                           <p>Providing a secured content and business menagement solution using its defined business hierarchy</p>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="single_solution">
                        <div class="simg">
                           <img src="<?php echo site_url('img/tensorflow.png'); ?>" />
                           <!--<div class="mask">
                              <a href="#" class="solution_more">Join Us</a>
                           </div>-->
                        </div>
                        <div class="singsolution_content">
                           <h3 class="singsolution_title"><a href="#">AI Solution</a></h3>
                           <p class="singsolution_price">ML & Deep Learning Solution</p>
                              <p>A complete ML solution using latest AI frameworks like Tensorflow, Python and Deep Learning concepts with latest libraries</p>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="single_solution">
                        <div class="singsolution_imgarea">
                           <img src="<?php echo site_url('img/solution-1.png'); ?>" />
                           <div class="mask">
                              <a href="#" class="solution_more">View solution</a>
                           </div>
                        </div>
                        <div class="singsolution_content">
                           <h3 class="singsolution_title"><a href="#">Angular JS 4</a></h3>
                           <p class="singsolution_price">Development</p>
                           <p>Complete integration and development solution for latest version of Angular JS</p>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="single_solution">
                        <div class="singsolution_imgarea">
                           <img src="<?php echo site_url('img/solution-1.png'); ?>" />
                           <div class="mask">
                              <a href="#" class="solution_more">View solution</a>
                           </div>
                        </div>
                        <div class="singsolution_content">
                           <h3 class="singsolution_title"><a href="#">Android Development</a></h3>
                           <p class="singsolution_price">Beginner & Advanced</p>
                           <p>Complete android solution from beginner to advanced level of learning with real world examples</p>
                        </div>
                     </div>
                  </li>
                  <li>
                     <div class="single_solution">
                        <div class="singsolution_imgarea">
                           <img src="<?php echo site_url('img/solution-1.png'); ?>" />
                           <div class="mask">
                              <a href="#" class="solution_more">View solution</a>
                           </div>
                        </div>
                        <div class="singsolution_content">
                           <h3 class="singsolution_title"><a href="#">Python</a></h3>
                           <p class="singsolution_price">Configuration & Development</p>
                           <p>Complete development solution on Python with real world application examples</p>
                        </div>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
<!--
<section id="ourTutors">
   <div class="container">
         <div class="row">
         
           <div class="col-lg-12 col-md-12"> 
         
             <div class="title_area">
         
               <h2 class="title_two">Our CEO</h2>
         
               <span></span> 
         
             </div>
         
           </div>
         
         </div>
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="ourTutors_content">
               <ul class="tutors_nav">
                  <li>
                     <div class="single_tutors">
                        <div class="tutors_thumb">
                          <img src="<?php echo site_url('img/me-2.png'); ?>" />
                        </div>
                        <div class="singTutors_content">
                          <h3 class="tutors_name">Umesh Singh</h3>
                          <span>CEO</span>
                          <p>Hope is a waking dream, so never leave it :-)</p>
                        </div>
                        <div class="singTutors_social">
                          <ul class="tutors_socnav">
                            <li><a class="fa fa-facebook" href="https://www.facebook.com/kushwanshimsh"></a></li>
                            <li><a class="fa fa-twitter" href="https://twitter.com/clyde_vs_bonnie"></a></li>
                            <li><a class="fa fa-google-plus" href="https://plus.google.com/+UmeshKushwanshi"></a></li>
                          </ul>
                        </div>
                        </div>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
-->
<section id="contact">
  <div class="container">
   <div class="row">
      <div class="col-lg-12 col-md-12"> 
        <div class="title_area">
          <h2 class="title_two">Looking for OpenText Consultation?</h2>
            <span></span> 
            <p>We're here to help you for providing OpenText based ECM, BPM and DAM solution with SAP integration, as well as customized integration to any web Portal using REST API and Devops deployments to Kubernates containers. We also deliver solution for Angular portal integration to our OpenText containers.</p>
            <p>We've worked with several enterprise clients with the collaboration team-work. Our popular services are:</p>
            <ol>
              <li>Data Access Management [ DAM ] & SAP Solution</li>
              <li>Extended Content Management Services [ xECM ]</li>
              <li>Business Process and Rules Management [ BPM & DMN ]</li>
              <li>AI based Chatbot Solution</li>
              <li>Mobile video application development like tiktok</li>
              <li>Educational Exam Portal</li>
              <li>Screen Sharing and Video Confrencing Application Development</li>
              <li>Dating Application Solution</li>
            </ol>
        </div>
      </div>
   </div>
    <h5>
        For more details and quick consultation, kindly wirte us:
    </h5>
   <div class="row">
    <?php 
		if(isset($email_sent))
			echo $email_sent;
	?>
     <div class="col-lg-8 col-md-8 col-sm-8">
       <div class="contact_form wow fadeInLeft">
          <form class="submitphoto_form" method = "post" action = "<?php echo site_url('indexs/submit');?>">
            <input type="text" class="wp-form-control wpcf7-text" placeholder="Your name" name = "name">
            <input type="mail" class="wp-form-control wpcf7-email" placeholder="Email address" name = "email">
            <input type="text" class="wp-form-control wpcf7-text" placeholder="Subject" name = "subject">
            <textarea class="wp-form-control wpcf7-textarea" cols="30" rows="10" placeholder="What kind of solution, you're looking for?" name = "query"></textarea>
            <input type="submit" value="Submit" class="wpcf7-submit" type="button">
            <!--class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"-->
        </form>
       </div>
     </div>
     <div class="col-lg-4 col-md-4 col-sm-4">
       <div class="contact_address wow fadeInRight">
         <h3>Address</h3>
         <div class="address_group">
           <p>Tapasvi Info Solution</p>
           <p>Plot No. A - 401</p>
           <p>Xrbia, Marunji Road</p>
           <p>Pune - 411057</p>
           <p>Maharashtra, India</p>
           <p>+91 8 860 864 566</p>
           <p>+91 7 906 453 793</p>
           <p>Email: info@itpaths.com</p>
         </div>
         <div class="address_group">
          <ul class="footer_social">
            <li><a href="https://www.facebook.com/itpaths/" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="https://www.linkedin.com/in/umesh-singh-bb975515/" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="https://www.youtube.com/watch?v=JcOdckBIuKY&t=26s" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Youtube"><i class="fa fa-youtube"></i></a></li>
            </ul>
         </div>
       </div>
     </div>
   </div>
  </div>
  <style>
	form.submitphoto_form input, form.submitphoto_form textarea {
		border: 1px solid gray;
	}
  </style>
</section>
<section id="googleMap">
  <!-- <iframe width="100%" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed/v1/place?q=17.4884140,78.3500750&key=AIzaSyCHOvNBRJ3_iK_Yh9eJDFlqREu_-HJJkJk"></iframe> -->
  <iframe width="100%" height="500" frameborder="0" style="border:0" marginheight="0" marginwidth="0"  src="https://www.google.com/maps/d/embed?mid=1z_v1CGAWULW2PnT1YAM-GxTZP31_JtbN&key=AIzaSyDDDnYYIvv_CFH6AlF0IiPplbFntgvycLI" allowfullscreen></iframe>
</section>

<!-- Global site tag (gtag.js) - Google Ads: 856036216 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-856036216"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'AW-856036216');
    function defer() {
        if (window.jQuery) {
           if(typeof success != "undefined")
            $('#myModal').modal('toggle');
        } else {
            setTimeout(function() { defer() }, 50);
        }
    }
    defer();
</script>
</div>
</section>
<?php echo "<script>console.log('".$_SERVER['REMOTE_ADDR']."');</script>";?>
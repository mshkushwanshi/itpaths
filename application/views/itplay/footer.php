	  <!--body wrapper end-->
			   <div class="footer">
				<div class="footer-grid">
					<h3>Navigation</h3>
					<ul class="list1">
					  <li><a href="index.html">Home</a></li>
					  <li><a href="radio.html">All Songs</a></li>
					  <li><a href="browse.html">Albums</a></li>
					  <li><a href="radio.html">New Collections</a></li>
					  <li><a href="blog.html">Blog</a></li>
					  <li><a href="contact.html">Contact</a></li>
				    </ul>
				</div>
				<div class="footer-grid">
					<h3>Our Account</h3>
				    <ul class="list1">
					  <li><a href="#" data-toggle="modal" data-target="#myModal5">Your Account</a></li>
					  <li><a href="#">Personal information</a></li>
					  <li><a href="#">Addresses</a></li>
					  <li><a href="#">Discount</a></li>
					  <li><a href="#">Orders history</a></li>
					  <li><a href="#">Addresses</a></li>
					  <li><a href="#">Search Terms</a></li>
				    </ul>
				</div>
				<div class="footer-grid">
					<h3>Our Support</h3>
					<ul class="list1">
					  <li><a href="contact.html">Site Map</a></li>
					  <li><a href="#">Search Terms</a></li>
					  <li><a href="#">Advanced Search</a></li>
					  <li><a href="#">Mobile</a></li>
					  <li><a href="contact.html">Contact Us</a></li>
					  <li><a href="#">Mobile</a></li>
					  <li><a href="#">Addresses</a></li>
				    </ul>
				  </div>
					  <div class="footer-grid">
						<h3>Newsletter</h3>
						<p class="footer_desc">Our newsletter description</p>
						<div class="search_footer">
						 <form>
						   <input type="text" placeholder="Email...." required="">
						  <input type="submit" value="Submit">
						  </form>
						</div>
					 </div>
					 <div class="footer-grid footer-grid_last">
						<h3>About Us</h3>
						<p class="footer_desc">About Us description</p>
						<p class="f_text">Phone:  &nbsp;&nbsp;&nbsp;09560939040</p>
						<p class="email">Email : &nbsp;<span><a href="mailto:info@itpaths.com">info(at)itpaths.com</a></span></p>	
					 </div>
					 <div class="clearfix"> </div>
				</div>
			</div>
		<!-- /w3l-agile -->
		<!--footer section start-->
			<footer>
			   <p>&copy 2017 ITPaths. All Rights Reserved | Design by <a href="https://ITPaths.com/" target="_blank">ITPaths.</a></p>
			</footer>
        <!--footer section end-->

      <!-- main content end-->
   </section>
  
<script src="<?php echo site_url('js/jquery.nicescroll.js');?>"></script>
<script src="<?php echo site_url('js/scripts.js');?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo site_url('js/bootstrap.js');?>"></script>
</body>
</html>